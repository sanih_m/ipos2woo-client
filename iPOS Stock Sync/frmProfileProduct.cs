﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class frmProfileProduct : Form
    {
        public LastProductCache cache;
        private ListViewItem[] products; //array to cache items for the virtual list
        private int lastSearchIndex;

        int total, selected, limit;

        public frmProfileProduct(LastProductCache productCache)
        {
            cache = productCache;
            total = cache.data.Count;
            limit = Program.access_user[0]["max_product"].AsInt;

            
            selected = 0;
            products = new ListViewItem[total];
            var idx = 0;
            foreach (var item in cache.data)
            {
                if (item.Value.active)
                {
                    selected++;
                }

                var listItem = new ListViewItem(item.Value.sku + " - " + item.Value.name);
                listItem.Checked = item.Value.active;
                listItem.Tag = item.Value.id;
                products[idx] = (listItem);
                idx++;
            }
            InitializeComponent();
        }

        void RenderStatus()
        {
            lblSelected.Text = selected.ToString();
            lblTotal.Text = total.ToString();
            lblLimit.Text = limit.ToString();
        }

        private void frmProfileProduct_Load(object sender, EventArgs e)
        {
            lvProduct.VirtualListSize = cache.data.Count;
            lvProduct.VirtualMode = true;

            // This is what you need, for drawing unchecked checkboxes
            lvProduct.OwnerDraw = true;
            lvProduct.DrawItem +=
                new DrawListViewItemEventHandler(listView_DrawItem);

            // Redraw when checked or doubleclicked
            lvProduct.MouseClick += new MouseEventHandler(listView_MouseClick);
            lvProduct.MouseDoubleClick += new MouseEventHandler(listView_MouseDoubleClick);


        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            lastSearchIndex = 0;
        }

        void FindItem()
        {
            ListViewItem lvi = lvProduct.FindItemWithText(txtSearch.Text, true, lastSearchIndex, true);

            //Select the item found and scroll it into view.
            if (lvi != null)
            {
                lvProduct.Focus();
                lvi.Selected = true;
                lvProduct.SelectedIndices.Clear();
                lvProduct.SelectedIndices.Add(lvi.Index);
                lvProduct.EnsureVisible(lvi.Index);
            }
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {

        }

        void listView_DrawItem(object sender,
            DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
            if (!e.Item.Checked)
            {
                e.Item.Checked = true;
                e.Item.Checked = false;
            }
        }

        void listView_MouseClick(object sender, MouseEventArgs e)
        {
            ListView lv = (ListView)sender;
            ListViewItem lvi = lv.GetItemAt(e.X, e.Y);
            if (lvi != null)
            {
                if (e.X < (lvi.Bounds.Left + 16))
                {
                    lvi.Checked = !lvi.Checked;

                    lv.Invalidate(lvi.Bounds);
                    if (lvi.Checked)
                    {
                        selected++;
                        if(selected > limit && limit > 0)
                        {
                            lvi.Checked = false;
                            MessageBox.Show("Produk terpilih sudah mencapai batas paket langganan anda, Silahkan tingkatkan langganan anda untuk menambahkan produk lainnya untuk di sinkronisasi.");
                            selected--;
                            return;
                        }
                    }
                    else
                    {
                        selected--;
                    }
                    cache.data[lvi.Tag.ToString()].active = lvi.Checked;
                    cache.data[lvi.Tag.ToString()].interacted = true;
                    RenderStatus();
                }
            }
        }

        void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListView lv = (ListView)sender;
            ListViewItem lvi = lv.GetItemAt(e.X, e.Y);
            if (lvi != null)
            {
                lv.Invalidate(lvi.Bounds);

                if (lvi.Checked)
                {
                    selected++;
                    if (selected > limit && limit > 0)
                    {
                        lvi.Checked = false;
                        MessageBox.Show("Produk terpilih sudah mencapai batas paket langganan anda, Silahkan tingkatkan langganan anda untuk menambahkan produk lainnya untuk di sinkronisasi.");
                        selected--;
                        return;
                    }
                }
                else
                {
                    selected--;
                }
                cache.data[lvi.Tag.ToString()].active = lvi.Checked;
                cache.data[lvi.Tag.ToString()].interacted = true;
                RenderStatus();
            } 
        }


        private void frmProfileProduct_Shown(object sender, EventArgs e)
        {
           
            RenderStatus();
            if(selected > limit && limit > 0)
            {
                MessageBox.Show("Produk terpilih sudah mencapai batas paket langganan anda, Silahkan tingkatkan langganan anda untuk menambahkan produk lainnya untuk di sinkronisasi.");
                for (int i = lvProduct.Items.Count-1; i >= 0; i--)
                {
                    if (selected > limit && limit > 0)
                    {
                        lvProduct.Items[i].Checked = false;
                        selected--;
                        cache.data[lvProduct.Items[i].ToString()].active = false;
                    }
                }
                lvProduct.Refresh();
                RenderStatus();  
            }
            lvProduct.CheckBoxes = true;
        }

        private void lvProduct_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            e.Item = products[e.ItemIndex];
        }

        private void lvProduct_CacheVirtualItems(object sender, CacheVirtualItemsEventArgs e)
        {
            ////We've gotten a request to refresh the cache.
            ////First check if it's really neccesary.
            //if (lvCache != null && e.StartIndex >= firstItem && e.EndIndex <= firstItem + lvCache.Length)
            //{
            //    //If the newly requested cache is a subset of the old cache, 
            //    //no need to rebuild everything, so do nothing.
            //    return;
            //}

            ////Now we need to rebuild the cache.
            //firstItem = e.StartIndex;
            //int length = e.EndIndex - e.StartIndex + 1; //indexes are inclusive
            //lvCache = new ListViewItem[length];

            ////Fill the cache with the appropriate ListViewItems.
            //for (int i = 0; i < length; i++)
            //{
            //    var x = (i + firstItem) * (i + firstItem);
            //    var item = products[x];
            //    lvCache[i] = new ListViewItem(item.sku + " - " + item.name);
            //    lvCache[i].Checked = item.active;
            //    //lvCache[i].SubItems.Add(item.name);
            //    //lvCache[i].SubItems.Add(item.id.ToString());
            //}

        }

        private void lvProduct_SearchForVirtualItem(object sender, SearchForVirtualItemEventArgs e)
        {
            
        }

        private void lvProduct_SearchForVirtualItem_1(object sender, SearchForVirtualItemEventArgs e)
        {
            var idx = 0;
            foreach (var data in cache.data)
            {
                var item = data.Value;
                if (e.Text != "" && (item.sku.ToLower().Contains(e.Text.ToLower()) || item.name.ToLower().Contains(e.Text.ToLower())))
                {
                    e.Index = idx;
                    lastSearchIndex = e.Index+1;
                    if(lastSearchIndex >= cache.data.Count)
                    {
                        lastSearchIndex = 0;
                    }
                    return;
                }
                idx++;
            }
        }

        private void lvProduct_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lvProduct_ItemChecked(object sender, ItemCheckedEventArgs e)
        {

        }

        private void btnToggleCheck_Click(object sender, EventArgs e)
        {
            
        }

        private void cbToggleCheck_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tombol ini akan menggunakan produk sesuai dengan batasan produk anda", "Peringatan", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                selected = 0;
                for (int i = 0; i <= lvProduct.Items.Count - 1; i++)
                {
                    lvProduct.Items[i].Checked = true;
                    var id = lvProduct.Items[i].Tag.ToString();
                    if (cache.data.ContainsKey(id))
                    {
                        cache.data[id].active = true;
                    }
                    selected++;
                    if (selected >= Program.productLimit() && Program.productLimit() > 0)
                    {
                        break;
                    }
                }
                lvProduct.Refresh();
            }
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                FindItem();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            FindItem();
        }
    }
}
