﻿namespace iPOS_Stock_Sync
{
    partial class frmProfileSync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            PresentationControls.CheckBoxProperties checkBoxProperties1 = new PresentationControls.CheckBoxProperties();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfileSync));
            this.bWorker = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bMarketInit = new System.ComponentModel.BackgroundWorker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInterval = new System.Windows.Forms.NumericUpDown();
            this.btnToogleTimer = new System.Windows.Forms.Button();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.cbWarehouse = new PresentationControls.CheckBoxComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnProducts = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSyncMore = new System.Windows.Forms.Button();
            this.tcProcess = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCommand = new System.Windows.Forms.TextBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmsSync = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sinkronSpesifikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sinkronBerdasarkanProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSinkronisasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odf = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterval)).BeginInit();
            this.panel2.SuspendLayout();
            this.tcProcess.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.cmsSync.SuspendLayout();
            this.SuspendLayout();
            // 
            // bWorker
            // 
            this.bWorker.WorkerSupportsCancellation = true;
            this.bWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bWorker_DoWorkAsync);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bMarketInit
            // 
            this.bMarketInit.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bMarketInit_DoWork);
            this.bMarketInit.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bMarketInit_RunWorkerCompleted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(234, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jeda";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtInterval
            // 
            this.txtInterval.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInterval.Location = new System.Drawing.Point(271, 11);
            this.txtInterval.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.txtInterval.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(51, 22);
            this.txtInterval.TabIndex = 1;
            this.txtInterval.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtInterval.ValueChanged += new System.EventHandler(this.txtInterval_ValueChanged);
            // 
            // btnToogleTimer
            // 
            this.btnToogleTimer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToogleTimer.Location = new System.Drawing.Point(551, 11);
            this.btnToogleTimer.Name = "btnToogleTimer";
            this.btnToogleTimer.Size = new System.Drawing.Size(118, 23);
            this.btnToogleTimer.TabIndex = 5;
            this.btnToogleTimer.Tag = "0";
            this.btnToogleTimer.Text = "Mulai Sinkron";
            this.btnToogleTimer.UseVisualStyleBackColor = true;
            this.btnToogleTimer.Click += new System.EventHandler(this.btnToogleTimer_Click);
            // 
            // btnClearLog
            // 
            this.btnClearLog.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearLog.Location = new System.Drawing.Point(696, 11);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(94, 23);
            this.btnClearLog.TabIndex = 6;
            this.btnClearLog.Text = "Bersihkan Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnConfig
            // 
            this.btnConfig.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.Location = new System.Drawing.Point(459, 11);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(86, 23);
            this.btnConfig.TabIndex = 7;
            this.btnConfig.Text = "Konfigurasi";
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // cbWarehouse
            // 
            checkBoxProperties1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbWarehouse.CheckBoxProperties = checkBoxProperties1;
            this.cbWarehouse.DisplayMemberSingleItem = "";
            this.cbWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWarehouse.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbWarehouse.FormattingEnabled = true;
            this.cbWarehouse.Items.AddRange(new object[] {
            ""});
            this.cbWarehouse.Location = new System.Drawing.Point(7, 11);
            this.cbWarehouse.Name = "cbWarehouse";
            this.cbWarehouse.Size = new System.Drawing.Size(221, 21);
            this.cbWarehouse.TabIndex = 8;
            this.cbWarehouse.SelectedIndexChanged += new System.EventHandler(this.cbWarehouse_SelectedIndexChanged);
            this.cbWarehouse.SelectionChangeCommitted += new System.EventHandler(this.cbWarehouse_SelectionChangeCommitted);
            this.cbWarehouse.SelectedValueChanged += new System.EventHandler(this.cbWarehouse_SelectedValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(328, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Detik";
            // 
            // btnProducts
            // 
            this.btnProducts.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProducts.Location = new System.Drawing.Point(368, 11);
            this.btnProducts.Name = "btnProducts";
            this.btnProducts.Size = new System.Drawing.Size(85, 23);
            this.btnProducts.TabIndex = 10;
            this.btnProducts.Text = "☑ Produk";
            this.btnProducts.UseVisualStyleBackColor = true;
            this.btnProducts.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSyncMore);
            this.panel2.Controls.Add(this.btnProducts);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.cbWarehouse);
            this.panel2.Controls.Add(this.btnConfig);
            this.panel2.Controls.Add(this.btnClearLog);
            this.panel2.Controls.Add(this.btnToogleTimer);
            this.panel2.Controls.Add(this.txtInterval);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(802, 43);
            this.panel2.TabIndex = 2;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // btnSyncMore
            // 
            this.btnSyncMore.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSyncMore.Location = new System.Drawing.Point(668, 11);
            this.btnSyncMore.Name = "btnSyncMore";
            this.btnSyncMore.Size = new System.Drawing.Size(22, 23);
            this.btnSyncMore.TabIndex = 11;
            this.btnSyncMore.Text = "⋮";
            this.btnSyncMore.UseVisualStyleBackColor = true;
            this.btnSyncMore.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // tcProcess
            // 
            this.tcProcess.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tcProcess.Controls.Add(this.tabPage1);
            this.tcProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcProcess.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcProcess.ImageList = this.imageList1;
            this.tcProcess.Location = new System.Drawing.Point(0, 43);
            this.tcProcess.Margin = new System.Windows.Forms.Padding(0);
            this.tcProcess.Name = "tcProcess";
            this.tcProcess.SelectedIndex = 0;
            this.tcProcess.Size = new System.Drawing.Size(802, 428);
            this.tcProcess.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(794, 401);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Proses Utama";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.txtLog);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(788, 378);
            this.panel4.TabIndex = 1;
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.Color.Black;
            this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLog.ForeColor = System.Drawing.Color.White;
            this.txtLog.Location = new System.Drawing.Point(0, 0);
            this.txtLog.Margin = new System.Windows.Forms.Padding(0);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(788, 378);
            this.txtLog.TabIndex = 8;
            this.txtLog.Text = "";
            this.txtLog.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLog_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.txtCommand);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 381);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(788, 17);
            this.panel1.TabIndex = 0;
            // 
            // txtCommand
            // 
            this.txtCommand.BackColor = System.Drawing.Color.Black;
            this.txtCommand.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommand.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCommand.ForeColor = System.Drawing.Color.White;
            this.txtCommand.Location = new System.Drawing.Point(0, 0);
            this.txtCommand.Margin = new System.Windows.Forms.Padding(0);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(788, 16);
            this.txtCommand.TabIndex = 11;
            this.txtCommand.TextChanged += new System.EventHandler(this.txtCommand_TextChanged);
            this.txtCommand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCommand_KeyDown);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "tokopedia.png");
            this.imageList1.Images.SetKeyName(1, "shopee.png");
            this.imageList1.Images.SetKeyName(2, "bukalapak.png");
            this.imageList1.Images.SetKeyName(3, "lazada.png");
            this.imageList1.Images.SetKeyName(4, "blibli.png");
            // 
            // cmsSync
            // 
            this.cmsSync.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsSync.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sinkronSpesifikToolStripMenuItem,
            this.sinkronBerdasarkanProdukToolStripMenuItem,
            this.resetSinkronisasiToolStripMenuItem});
            this.cmsSync.Name = "cmsSync";
            this.cmsSync.Size = new System.Drawing.Size(223, 70);
            // 
            // sinkronSpesifikToolStripMenuItem
            // 
            this.sinkronSpesifikToolStripMenuItem.Name = "sinkronSpesifikToolStripMenuItem";
            this.sinkronSpesifikToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.sinkronSpesifikToolStripMenuItem.Text = "Sinkron Spesifik";
            this.sinkronSpesifikToolStripMenuItem.Click += new System.EventHandler(this.sinkronSpesifikToolStripMenuItem_Click);
            // 
            // sinkronBerdasarkanProdukToolStripMenuItem
            // 
            this.sinkronBerdasarkanProdukToolStripMenuItem.Name = "sinkronBerdasarkanProdukToolStripMenuItem";
            this.sinkronBerdasarkanProdukToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.sinkronBerdasarkanProdukToolStripMenuItem.Text = "Sinkron Berdasarkan Produk";
            this.sinkronBerdasarkanProdukToolStripMenuItem.Click += new System.EventHandler(this.sinkronBerdasarkanProdukToolStripMenuItem_Click);
            // 
            // resetSinkronisasiToolStripMenuItem
            // 
            this.resetSinkronisasiToolStripMenuItem.Name = "resetSinkronisasiToolStripMenuItem";
            this.resetSinkronisasiToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.resetSinkronisasiToolStripMenuItem.Text = "Reset Data Sinkronisasi";
            this.resetSinkronisasiToolStripMenuItem.Click += new System.EventHandler(this.resetSinkronisasiToolStripMenuItem_Click);
            // 
            // frmProfileSync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 471);
            this.Controls.Add(this.tcProcess);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmProfileSync";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProfileSync_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmProfileSync_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            this.VisibleChanged += new System.EventHandler(this.frmMain_VisibleChanged);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.txtInterval)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tcProcess.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.cmsSync.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker bWorker;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker bMarketInit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtInterval;
        private System.Windows.Forms.Button btnToogleTimer;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.Button btnConfig;
        private PresentationControls.CheckBoxComboBox cbWarehouse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnProducts;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtCommand;
        public System.Windows.Forms.TabControl tcProcess;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnSyncMore;
        private System.Windows.Forms.ContextMenuStrip cmsSync;
        private System.Windows.Forms.ToolStripMenuItem sinkronSpesifikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetSinkronisasiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sinkronBerdasarkanProdukToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog odf;
    }
}

