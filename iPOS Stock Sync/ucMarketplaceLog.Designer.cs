﻿namespace iPOS_Stock_Sync
{
    partial class ucMarketplaceLog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.samakanDataProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.chbSendMonitoring = new System.Windows.Forms.CheckBox();
            this.chbSyncMarketplace = new System.Windows.Forms.CheckBox();
            this.chbCheckID = new System.Windows.Forms.CheckBox();
            this.btnSyncMore = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.imporDataPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.samakanDataProdukToolStripMenuItem,
            this.kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem,
            this.imporDataPenjualanToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(257, 92);
            // 
            // samakanDataProdukToolStripMenuItem
            // 
            this.samakanDataProdukToolStripMenuItem.Name = "samakanDataProdukToolStripMenuItem";
            this.samakanDataProdukToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.samakanDataProdukToolStripMenuItem.Text = "Samakan Data Produk";
            this.samakanDataProdukToolStripMenuItem.Click += new System.EventHandler(this.samakanDataProdukToolStripMenuItem_Click);
            // 
            // kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem
            // 
            this.kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem.Name = "kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem";
            this.kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem.Text = "Samakan Penjualan Ke Monitoring";
            this.kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem.Click += new System.EventHandler(this.kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.chbSendMonitoring);
            this.panel1.Controls.Add(this.chbSyncMarketplace);
            this.panel1.Controls.Add(this.chbCheckID);
            this.panel1.Controls.Add(this.btnSyncMore);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(525, 49);
            this.panel1.TabIndex = 15;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(420, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chbSendMonitoring
            // 
            this.chbSendMonitoring.AutoSize = true;
            this.chbSendMonitoring.Checked = true;
            this.chbSendMonitoring.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbSendMonitoring.Location = new System.Drawing.Point(240, 3);
            this.chbSendMonitoring.Name = "chbSendMonitoring";
            this.chbSendMonitoring.Size = new System.Drawing.Size(116, 17);
            this.chbSendMonitoring.TabIndex = 18;
            this.chbSendMonitoring.Text = "Kirim Ke Monitoring";
            this.chbSendMonitoring.UseVisualStyleBackColor = true;
            // 
            // chbSyncMarketplace
            // 
            this.chbSyncMarketplace.AutoSize = true;
            this.chbSyncMarketplace.Checked = true;
            this.chbSyncMarketplace.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbSyncMarketplace.Location = new System.Drawing.Point(115, 3);
            this.chbSyncMarketplace.Name = "chbSyncMarketplace";
            this.chbSyncMarketplace.Size = new System.Drawing.Size(119, 17);
            this.chbSyncMarketplace.TabIndex = 17;
            this.chbSyncMarketplace.Text = "Sinkronisasi Produk";
            this.chbSyncMarketplace.UseVisualStyleBackColor = true;
            // 
            // chbCheckID
            // 
            this.chbCheckID.AutoSize = true;
            this.chbCheckID.Checked = true;
            this.chbCheckID.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbCheckID.Location = new System.Drawing.Point(3, 3);
            this.chbCheckID.Name = "chbCheckID";
            this.chbCheckID.Size = new System.Drawing.Size(106, 17);
            this.chbCheckID.TabIndex = 16;
            this.chbCheckID.Text = "Cocokan Produk";
            this.chbCheckID.UseVisualStyleBackColor = true;
            // 
            // btnSyncMore
            // 
            this.btnSyncMore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSyncMore.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSyncMore.Location = new System.Drawing.Point(501, 22);
            this.btnSyncMore.Name = "btnSyncMore";
            this.btnSyncMore.Size = new System.Drawing.Size(22, 23);
            this.btnSyncMore.TabIndex = 15;
            this.btnSyncMore.Text = "⋮";
            this.btnSyncMore.UseVisualStyleBackColor = true;
            this.btnSyncMore.Click += new System.EventHandler(this.btnSyncMore_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(3, 23);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(499, 22);
            this.txtSearch.TabIndex = 13;
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.Color.Black;
            this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLog.ForeColor = System.Drawing.Color.White;
            this.txtLog.Location = new System.Drawing.Point(0, 49);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(525, 437);
            this.txtLog.TabIndex = 16;
            this.txtLog.Text = "";
            this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged_1);
            // 
            // imporDataPenjualanToolStripMenuItem
            // 
            this.imporDataPenjualanToolStripMenuItem.Name = "imporDataPenjualanToolStripMenuItem";
            this.imporDataPenjualanToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.imporDataPenjualanToolStripMenuItem.Text = "Impor Data Penjualan";
            this.imporDataPenjualanToolStripMenuItem.Click += new System.EventHandler(this.imporDataPenjualanToolStripMenuItem_Click);
            // 
            // ucMarketplaceLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.panel1);
            this.Name = "ucMarketplaceLog";
            this.Size = new System.Drawing.Size(525, 486);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnSyncMore;
        public System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.ToolStripMenuItem samakanDataProdukToolStripMenuItem;
        private System.Windows.Forms.CheckBox chbSyncMarketplace;
        private System.Windows.Forms.CheckBox chbCheckID;
        private System.Windows.Forms.CheckBox chbSendMonitoring;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imporDataPenjualanToolStripMenuItem;
    }
}
