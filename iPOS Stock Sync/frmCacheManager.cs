﻿using iPOS_Stock_Sync.Modules;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class frmCacheManager : Form
    {
        CacheTokopediaProduct cache_tokopedia;
        CacheShopeeProduct cache_shopee;
        CacheBukalapakProduct cache_bukalapak;
        CacheLazadaProduct cache_lazada;
        CacheType currentType;
        int currentProfileID;
        string cache_file = "";

        public enum CacheType
        {
            Tokopedia,
            Shopee,
            Bukalapak,
            Lazada
        }

        public frmCacheManager(CacheType type, int profileID)
        {
            currentProfileID = profileID;
            currentType = type;
            InitializeComponent();
        }

        private void frmCacheManager_Load(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvData.SelectedItems.Count == 0) return;
            txtKey.Text = lvData.SelectedItems[0].SubItems[0].Text;
            txtValue.Text = lvData.SelectedItems[0].SubItems[1].Text;
        }


        void LoadData()
        {
            switch (currentType)
            {
                case CacheType.Tokopedia:
                    cache_file = Application.StartupPath + TokopediaCore.cachename.Replace("{IDX}", currentProfileID.ToString());
                    try
                    {
                        var data = File.ReadAllText(cache_file);
                        cache_tokopedia = JsonConvert.DeserializeObject<CacheTokopediaProduct>(data);
                    }
                    catch (Exception)
                    {
                        cache_tokopedia = new CacheTokopediaProduct();
                    }
                    this.Text = "Kelola Daftar ID Tokopedia";
                    break;
                case CacheType.Shopee:
                    cache_file = Application.StartupPath + ShopeeCore.cachename.Replace("{IDX}", currentProfileID.ToString());
                    try
                    {
                        var data = File.ReadAllText(cache_file);
                        cache_shopee = JsonConvert.DeserializeObject<CacheShopeeProduct>(data);
                    }
                    catch (Exception)
                    {
                        cache_shopee = new CacheShopeeProduct();
                    }
                    this.Text = "Kelola Daftar ID Shopee";
                    break;
                case CacheType.Bukalapak:
                    cache_file = Application.StartupPath + BukalapakCore.cachename.Replace("{IDX}", currentProfileID.ToString());
                    try
                    {
                        var data = File.ReadAllText(cache_file);
                        cache_bukalapak = JsonConvert.DeserializeObject<CacheBukalapakProduct>(data);
                    }
                    catch (Exception)
                    {
                        cache_bukalapak = new CacheBukalapakProduct();
                    }
                    this.Text = "Kelola Daftar ID Bukalapak";
                    break;
                case CacheType.Lazada:
                    cache_file = Application.StartupPath + LazadaCore.cachename.Replace("{IDX}", currentProfileID.ToString());
                    try
                    {
                        var data = File.ReadAllText(cache_file);
                        cache_lazada = JsonConvert.DeserializeObject<CacheLazadaProduct>(data);
                    }
                    catch (Exception)
                    {
                        cache_lazada = new CacheLazadaProduct();
                    }
                    this.Text = "Kelola Daftar ID Lazada";
                    break;
                default:
                    break;
            }
            RefreshList();
        }

        void RefreshList()
        {
            lvData.Items.Clear();
            switch (currentType)
            {
                case CacheType.Tokopedia:
                    foreach (var item in cache_tokopedia.marketID)
                    {
                        lvData.Items.Add(item.Key, item.Key, -1).SubItems.Add(item.Value);
                    }
                    break;
                case CacheType.Shopee:
                    foreach (var item in cache_shopee.marketID)
                    {
                        lvData.Items.Add(item.Key, item.Key, -1).SubItems.Add(item.Value);
                    }
                    break;
                case CacheType.Bukalapak:
                    foreach (var item in cache_bukalapak.marketID)
                    {
                        lvData.Items.Add(item.Key, item.Key, -1).SubItems.Add(item.Value);
                    }
                    break;
                case CacheType.Lazada:
                    foreach (var item in cache_lazada.marketID)
                    {
                        lvData.Items.Add(item.Key, item.Key, -1).SubItems.Add(item.Value);
                    }
                    break;
                default:
                    break;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text != "")
            {
                for (int i = lvData.Items.Count - 1; i >= 0; i--)
                {
                    var item = lvData.Items[i];
                    if (item.Text.ToLower().Contains(txtSearch.Text.ToLower()))
                    {
                        item.BackColor = SystemColors.Highlight;
                        item.ForeColor = SystemColors.HighlightText;
                    }
                    else
                    {
                        lvData.Items.Remove(item);
                    }
                }
                if (lvData.SelectedItems.Count == 1)
                {
                    lvData.Focus();
                }
            }
            else
            {
                RefreshList();
            }
        }

        private void lvData_Enter(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvData.Items)
            {
                item.BackColor = SystemColors.Window;
                item.ForeColor = SystemColors.WindowText;
            }
        }

        private void frmCacheManager_Shown(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtKey.Text.Equals("") || txtValue.Text.Equals(""))
            {
                MessageBox.Show("Silahkan isi / pilih data terlebih dahulu");
                return;
            }
            switch (currentType)
            {
                case CacheType.Tokopedia:
                    if (cache_tokopedia.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_tokopedia.marketID[txtKey.Text] = txtValue.Text;
                        lvData.Items[txtKey.Text].SubItems[1].Text = txtValue.Text;
                    }
                    else
                    {
                        cache_tokopedia.marketID.Add(txtKey.Text, txtValue.Text);
                        lvData.Items.Add(txtKey.Text, txtKey.Text, -1).SubItems.Add(txtValue.Text);
                    }
                    
                    break;
                case CacheType.Shopee:
                    if (cache_shopee.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_shopee.marketID[txtKey.Text] = txtValue.Text;
                        lvData.Items[txtKey.Text].SubItems[1].Text = txtValue.Text;
                    }
                    else
                    {
                        cache_shopee.marketID.Add(txtKey.Text, txtValue.Text);
                        lvData.Items.Add(txtKey.Text, txtKey.Text, -1).SubItems.Add(txtValue.Text);
                    }
                    break;
                case CacheType.Bukalapak:
                    if (cache_bukalapak.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_bukalapak.marketID[txtKey.Text] = txtValue.Text;
                        lvData.Items[txtKey.Text].SubItems[1].Text = txtValue.Text;
                    }
                    else
                    {
                        cache_bukalapak.marketID.Add(txtKey.Text, txtValue.Text);
                        lvData.Items.Add(txtKey.Text, txtKey.Text, -1).SubItems.Add(txtValue.Text);
                    }
                    break;
                case CacheType.Lazada:
                    if (cache_lazada.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_lazada.marketID[txtKey.Text] = txtValue.Text;
                        lvData.Items[txtKey.Text].SubItems[1].Text = txtValue.Text;
                    }
                    else
                    {
                        cache_lazada.marketID.Add(txtKey.Text, txtValue.Text);
                        lvData.Items.Add(txtKey.Text, txtKey.Text, -1).SubItems.Add(txtValue.Text);
                    }
                    break;
                default:
                    break;
            }
            lvData.Items[txtKey.Text].EnsureVisible();
            txtKey.Clear();
            txtValue.Clear();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtKey.Text.Equals("") || txtValue.Text.Equals(""))
            {
                MessageBox.Show("Silahkan isi / pilih data terlebih dahulu");
                return;
            }
            if (MessageBox.Show("Apa anda yakin?", "Peringatan", MessageBoxButtons.YesNo) == DialogResult.No) return;
            switch (currentType)
            {
                case CacheType.Tokopedia:
                    if (cache_tokopedia.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_tokopedia.marketID.Remove(txtKey.Text);
                        
                    }
                    

                    break;
                case CacheType.Shopee:
                    if (cache_shopee.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_shopee.marketID.Remove(txtKey.Text);
                       
                    }
                    
                    break;
                case CacheType.Bukalapak:
                    if (cache_bukalapak.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_bukalapak.marketID.Remove(txtKey.Text);

                    }

                    break;
                case CacheType.Lazada:
                    if (cache_lazada.marketID.ContainsKey(txtKey.Text))
                    {
                        cache_lazada.marketID.Remove(txtKey.Text);
                    }

                    break;
                default:
                    break;
            }
            lvData.Items[txtKey.Text].Remove();
            txtKey.Clear();
            txtValue.Clear();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            switch (currentType)
            {
                case CacheType.Tokopedia:
                    cache_tokopedia.lastupdate = DateTime.Now;
                    File.WriteAllText(cache_file, JsonConvert.SerializeObject(cache_tokopedia));
                    break;
                case CacheType.Shopee:
                    cache_shopee.lastupdate = DateTime.Now;
                    File.WriteAllText(cache_file, JsonConvert.SerializeObject(cache_shopee));
                    break;
                case CacheType.Bukalapak:
                    cache_bukalapak.lastupdate = DateTime.Now;
                    File.WriteAllText(cache_file, JsonConvert.SerializeObject(cache_bukalapak));
                    break;
                case CacheType.Lazada:
                    cache_lazada.lastupdate = DateTime.Now;
                    File.WriteAllText(cache_file, JsonConvert.SerializeObject(cache_lazada));
                    break;
                default:
                    break;
            }
            this.Close();
        }
    }
}
