﻿namespace iPOS_Stock_Sync
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbHideStartup = new System.Windows.Forms.CheckBox();
            this.cbRunStartup = new System.Windows.Forms.CheckBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMonitorToken = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMonitorURL = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbHideStartup
            // 
            this.cbHideStartup.AutoSize = true;
            this.cbHideStartup.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHideStartup.Location = new System.Drawing.Point(12, 35);
            this.cbHideStartup.Name = "cbHideStartup";
            this.cbHideStartup.Size = new System.Drawing.Size(150, 17);
            this.cbHideStartup.TabIndex = 16;
            this.cbHideStartup.Text = "Sembunyikan Di TaskBar";
            this.cbHideStartup.UseVisualStyleBackColor = true;
            // 
            // cbRunStartup
            // 
            this.cbRunStartup.AutoSize = true;
            this.cbRunStartup.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRunStartup.Location = new System.Drawing.Point(12, 12);
            this.cbRunStartup.Name = "cbRunStartup";
            this.cbRunStartup.Size = new System.Drawing.Size(197, 17);
            this.cbRunStartup.TabIndex = 15;
            this.cbRunStartup.Text = " Jalankan Ketika Masuk Windows";
            this.cbRunStartup.UseVisualStyleBackColor = true;
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.Location = new System.Drawing.Point(15, 129);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(260, 23);
            this.btnApply.TabIndex = 17;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Ipos2Woo Token";
            // 
            // txtMonitorToken
            // 
            this.txtMonitorToken.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonitorToken.Location = new System.Drawing.Point(108, 84);
            this.txtMonitorToken.Name = "txtMonitorToken";
            this.txtMonitorToken.Size = new System.Drawing.Size(164, 22);
            this.txtMonitorToken.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Ipos2Woo URL";
            // 
            // txtMonitorURL
            // 
            this.txtMonitorURL.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMonitorURL.Location = new System.Drawing.Point(108, 58);
            this.txtMonitorURL.Name = "txtMonitorURL";
            this.txtMonitorURL.Size = new System.Drawing.Size(164, 22);
            this.txtMonitorURL.TabIndex = 20;
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 164);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtMonitorURL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMonitorToken);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.cbHideStartup);
            this.Controls.Add(this.cbRunStartup);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmConfig";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Konfigurasi";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbHideStartup;
        private System.Windows.Forms.CheckBox cbRunStartup;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMonitorToken;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMonitorURL;
    }
}