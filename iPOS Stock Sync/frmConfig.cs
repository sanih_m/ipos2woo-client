﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class frmConfig : Form
    {
        public frmConfig()
        {
            InitializeComponent();
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            cbRunStartup.Checked = Program.config.startup;
            cbHideStartup.Checked = Program.config.autohide;
            txtMonitorURL.Text = Program.config.monitorURL;
            txtMonitorToken.Text = Program.config.monitorToken;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
            ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (cbRunStartup.Checked)
                rk.SetValue(Application.ProductName, "\"" + Application.ExecutablePath + "\"");
            else
                rk.DeleteValue(Application.ProductName, false);
            Program.config.startup = cbRunStartup.Checked;
            Program.config.autohide = cbHideStartup.Checked;
            if (!Program.config.monitorToken.Equals(txtMonitorToken.Text) || !Program.config.monitorURL.Equals(txtMonitorURL.Text))
            {
                Program.config.branchID = ""; 
            }
            Program.config.monitorToken = txtMonitorToken.Text;
            Program.config.monitorURL = txtMonitorURL.Text;
            Program.SaveConfig();
            Close();
        }
    }
}
