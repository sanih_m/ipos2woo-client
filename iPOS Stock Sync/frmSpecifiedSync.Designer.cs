﻿namespace iPOS_Stock_Sync
{
    partial class frmSpecifiedSync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chbWoo = new System.Windows.Forms.CheckBox();
            this.chbTokped = new System.Windows.Forms.CheckBox();
            this.chbShopee = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chbMonitoring = new System.Windows.Forms.CheckBox();
            this.chbLazada = new System.Windows.Forms.CheckBox();
            this.chbBukalapak = new System.Windows.Forms.CheckBox();
            this.chbBlibli = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chbWoo
            // 
            this.chbWoo.AutoSize = true;
            this.chbWoo.Checked = true;
            this.chbWoo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbWoo.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbWoo.Location = new System.Drawing.Point(12, 12);
            this.chbWoo.Name = "chbWoo";
            this.chbWoo.Size = new System.Drawing.Size(102, 17);
            this.chbWoo.TabIndex = 0;
            this.chbWoo.Text = "Woocommerce";
            this.chbWoo.UseVisualStyleBackColor = true;
            // 
            // chbTokped
            // 
            this.chbTokped.AutoSize = true;
            this.chbTokped.Checked = true;
            this.chbTokped.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbTokped.Location = new System.Drawing.Point(12, 35);
            this.chbTokped.Name = "chbTokped";
            this.chbTokped.Size = new System.Drawing.Size(80, 17);
            this.chbTokped.TabIndex = 1;
            this.chbTokped.Text = "Tokopedia";
            this.chbTokped.UseVisualStyleBackColor = true;
            // 
            // chbShopee
            // 
            this.chbShopee.AutoSize = true;
            this.chbShopee.Checked = true;
            this.chbShopee.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbShopee.Location = new System.Drawing.Point(12, 58);
            this.chbShopee.Name = "chbShopee";
            this.chbShopee.Size = new System.Drawing.Size(65, 17);
            this.chbShopee.TabIndex = 2;
            this.chbShopee.Text = "Shopee";
            this.chbShopee.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Mulai";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chbMonitoring
            // 
            this.chbMonitoring.AutoSize = true;
            this.chbMonitoring.Checked = true;
            this.chbMonitoring.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbMonitoring.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbMonitoring.Location = new System.Drawing.Point(12, 149);
            this.chbMonitoring.Name = "chbMonitoring";
            this.chbMonitoring.Size = new System.Drawing.Size(112, 17);
            this.chbMonitoring.TabIndex = 4;
            this.chbMonitoring.Text = "Monitoring Web";
            this.chbMonitoring.UseVisualStyleBackColor = true;
            // 
            // chbLazada
            // 
            this.chbLazada.AutoSize = true;
            this.chbLazada.Checked = true;
            this.chbLazada.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbLazada.Location = new System.Drawing.Point(12, 104);
            this.chbLazada.Name = "chbLazada";
            this.chbLazada.Size = new System.Drawing.Size(61, 17);
            this.chbLazada.TabIndex = 6;
            this.chbLazada.Text = "Lazada";
            this.chbLazada.UseVisualStyleBackColor = true;
            // 
            // chbBukalapak
            // 
            this.chbBukalapak.AutoSize = true;
            this.chbBukalapak.Checked = true;
            this.chbBukalapak.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbBukalapak.Location = new System.Drawing.Point(12, 81);
            this.chbBukalapak.Name = "chbBukalapak";
            this.chbBukalapak.Size = new System.Drawing.Size(79, 17);
            this.chbBukalapak.TabIndex = 5;
            this.chbBukalapak.Text = "Bukalapak";
            this.chbBukalapak.UseVisualStyleBackColor = true;
            // 
            // chbBlibli
            // 
            this.chbBlibli.AutoSize = true;
            this.chbBlibli.Checked = true;
            this.chbBlibli.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbBlibli.Location = new System.Drawing.Point(12, 126);
            this.chbBlibli.Name = "chbBlibli";
            this.chbBlibli.Size = new System.Drawing.Size(51, 17);
            this.chbBlibli.TabIndex = 7;
            this.chbBlibli.Text = "Blibli";
            this.chbBlibli.UseVisualStyleBackColor = true;
            // 
            // frmSpecifiedSync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(135, 207);
            this.Controls.Add(this.chbBlibli);
            this.Controls.Add(this.chbLazada);
            this.Controls.Add(this.chbBukalapak);
            this.Controls.Add(this.chbMonitoring);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chbShopee);
            this.Controls.Add(this.chbTokped);
            this.Controls.Add(this.chbWoo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmSpecifiedSync";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lakukan pada";
            this.Load += new System.EventHandler(this.frmForceSync_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.CheckBox chbWoo;
        public System.Windows.Forms.CheckBox chbTokped;
        public System.Windows.Forms.CheckBox chbShopee;
        public System.Windows.Forms.CheckBox chbMonitoring;
        public System.Windows.Forms.CheckBox chbLazada;
        public System.Windows.Forms.CheckBox chbBukalapak;
        public System.Windows.Forms.CheckBox chbBlibli;
    }
}