﻿namespace iPOS_Stock_Sync
{
    partial class frmProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfile));
            this.lvProfile = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmsLvProfile = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bukaUlangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aktifkanNonaktifkanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hapusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trayicon = new System.Windows.Forms.NotifyIcon(this.components);
            this.bWorker = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cloneMarketplaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tokopediaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pengaturanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cekUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keluarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tambahProfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblVersion = new System.Windows.Forms.ToolStripMenuItem();
            this.tokopediaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shopeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnPackage = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatusd = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmsLvProfile.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvProfile
            // 
            this.lvProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvProfile.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvProfile.ContextMenuStrip = this.cmsLvProfile;
            this.lvProfile.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvProfile.FullRowSelect = true;
            this.lvProfile.HideSelection = false;
            this.lvProfile.Location = new System.Drawing.Point(9, 29);
            this.lvProfile.Margin = new System.Windows.Forms.Padding(2);
            this.lvProfile.Name = "lvProfile";
            this.lvProfile.Size = new System.Drawing.Size(293, 186);
            this.lvProfile.TabIndex = 0;
            this.lvProfile.UseCompatibleStateImageBehavior = false;
            this.lvProfile.View = System.Windows.Forms.View.Details;
            this.lvProfile.SelectedIndexChanged += new System.EventHandler(this.lvProfile_SelectedIndexChanged);
            this.lvProfile.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvProfile_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Profile Name";
            this.columnHeader1.Width = 106;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Status";
            this.columnHeader2.Width = 133;
            // 
            // cmsLvProfile
            // 
            this.cmsLvProfile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bukaUlangToolStripMenuItem,
            this.aktifkanNonaktifkanToolStripMenuItem,
            this.hapusToolStripMenuItem});
            this.cmsLvProfile.Name = "cmsLvProfile";
            this.cmsLvProfile.Size = new System.Drawing.Size(195, 70);
            this.cmsLvProfile.Opening += new System.ComponentModel.CancelEventHandler(this.cmsLvProfile_Opening);
            // 
            // bukaUlangToolStripMenuItem
            // 
            this.bukaUlangToolStripMenuItem.Name = "bukaUlangToolStripMenuItem";
            this.bukaUlangToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.bukaUlangToolStripMenuItem.Text = "Buka Ulang";
            this.bukaUlangToolStripMenuItem.Click += new System.EventHandler(this.bukaUlangToolStripMenuItem_Click);
            // 
            // aktifkanNonaktifkanToolStripMenuItem
            // 
            this.aktifkanNonaktifkanToolStripMenuItem.Name = "aktifkanNonaktifkanToolStripMenuItem";
            this.aktifkanNonaktifkanToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.aktifkanNonaktifkanToolStripMenuItem.Text = "Aktifkan / Nonaktifkan";
            this.aktifkanNonaktifkanToolStripMenuItem.Click += new System.EventHandler(this.aktifkanNonaktifkanToolStripMenuItem_Click_1);
            // 
            // hapusToolStripMenuItem
            // 
            this.hapusToolStripMenuItem.Name = "hapusToolStripMenuItem";
            this.hapusToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.hapusToolStripMenuItem.Text = "Hapus";
            this.hapusToolStripMenuItem.Click += new System.EventHandler(this.hapusToolStripMenuItem_Click);
            // 
            // trayicon
            // 
            this.trayicon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayicon.Icon")));
            this.trayicon.Text = "Ipos Stock Sync";
            this.trayicon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trayicon_MouseDoubleClick);
            // 
            // bWorker
            // 
            this.bWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.bWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.tambahProfilToolStripMenuItem,
            this.lblVersion,
            this.mnPackage});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(313, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cloneMarketplaceToolStripMenuItem,
            this.pengaturanToolStripMenuItem,
            this.cekUpdateToolStripMenuItem,
            this.keluarToolStripMenuItem});
            this.menuToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(49, 22);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // cloneMarketplaceToolStripMenuItem
            // 
            this.cloneMarketplaceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tokopediaToolStripMenuItem1});
            this.cloneMarketplaceToolStripMenuItem.Name = "cloneMarketplaceToolStripMenuItem";
            this.cloneMarketplaceToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.cloneMarketplaceToolStripMenuItem.Text = "Clone Marketplace";
            this.cloneMarketplaceToolStripMenuItem.Visible = false;
            // 
            // tokopediaToolStripMenuItem1
            // 
            this.tokopediaToolStripMenuItem1.Name = "tokopediaToolStripMenuItem1";
            this.tokopediaToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.tokopediaToolStripMenuItem1.Text = "Tokopedia";
            // 
            // pengaturanToolStripMenuItem
            // 
            this.pengaturanToolStripMenuItem.Name = "pengaturanToolStripMenuItem";
            this.pengaturanToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.pengaturanToolStripMenuItem.Text = "Pengaturan";
            this.pengaturanToolStripMenuItem.Click += new System.EventHandler(this.pengaturanToolStripMenuItem_Click);
            // 
            // cekUpdateToolStripMenuItem
            // 
            this.cekUpdateToolStripMenuItem.Name = "cekUpdateToolStripMenuItem";
            this.cekUpdateToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.cekUpdateToolStripMenuItem.Text = "Cek Update";
            this.cekUpdateToolStripMenuItem.Click += new System.EventHandler(this.cekUpdateToolStripMenuItem_Click);
            // 
            // keluarToolStripMenuItem
            // 
            this.keluarToolStripMenuItem.Name = "keluarToolStripMenuItem";
            this.keluarToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.keluarToolStripMenuItem.Text = "Keluar";
            this.keluarToolStripMenuItem.Click += new System.EventHandler(this.keluarToolStripMenuItem_Click);
            // 
            // tambahProfilToolStripMenuItem
            // 
            this.tambahProfilToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tambahProfilToolStripMenuItem.Name = "tambahProfilToolStripMenuItem";
            this.tambahProfilToolStripMenuItem.Size = new System.Drawing.Size(89, 22);
            this.tambahProfilToolStripMenuItem.Text = "Tambah Profil";
            this.tambahProfilToolStripMenuItem.Click += new System.EventHandler(this.tambahProfilToolStripMenuItem_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tokopediaToolStripMenuItem,
            this.shopeeToolStripMenuItem});
            this.lblVersion.Enabled = false;
            this.lblVersion.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(56, 22);
            this.lblVersion.Text = "version";
            // 
            // tokopediaToolStripMenuItem
            // 
            this.tokopediaToolStripMenuItem.Name = "tokopediaToolStripMenuItem";
            this.tokopediaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tokopediaToolStripMenuItem.Text = "Tokopedia";
            this.tokopediaToolStripMenuItem.Click += new System.EventHandler(this.tokopediaToolStripMenuItem_Click_1);
            // 
            // shopeeToolStripMenuItem
            // 
            this.shopeeToolStripMenuItem.Name = "shopeeToolStripMenuItem";
            this.shopeeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.shopeeToolStripMenuItem.Text = "Shopee";
            this.shopeeToolStripMenuItem.Click += new System.EventHandler(this.shopeeToolStripMenuItem_Click);
            // 
            // mnPackage
            // 
            this.mnPackage.Enabled = false;
            this.mnPackage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnPackage.Name = "mnPackage";
            this.mnPackage.Size = new System.Drawing.Size(40, 22);
            this.mnPackage.Text = "Trial";
            this.mnPackage.Click += new System.EventHandler(this.mnPackage_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusd});
            this.statusStrip1.Location = new System.Drawing.Point(0, 221);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 8, 0);
            this.statusStrip1.Size = new System.Drawing.Size(313, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatusd
            // 
            this.lblStatusd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusd.ForeColor = System.Drawing.Color.Red;
            this.lblStatusd.Name = "lblStatusd";
            this.lblStatusd.Size = new System.Drawing.Size(160, 17);
            this.lblStatusd.Text = "Cabang ini belum terhubung.";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 243);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lvProfile);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmProfile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iPos Sync";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProfile_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmProfile_FormClosed);
            this.Load += new System.EventHandler(this.frmProfile_Load);
            this.Shown += new System.EventHandler(this.frmProfile_Shown);
            this.Resize += new System.EventHandler(this.frmProfile_Resize);
            this.cmsLvProfile.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvProfile;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.NotifyIcon trayicon;
        private System.ComponentModel.BackgroundWorker bWorker;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pengaturanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keluarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tambahProfilToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusd;
        private System.Windows.Forms.ToolStripMenuItem cekUpdateToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip cmsLvProfile;
        private System.Windows.Forms.ToolStripMenuItem aktifkanNonaktifkanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hapusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lblVersion;
        private System.Windows.Forms.ToolStripMenuItem tokopediaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shopeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnPackage;
        private System.Windows.Forms.ToolStripMenuItem bukaUlangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cloneMarketplaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tokopediaToolStripMenuItem1;
    }
}