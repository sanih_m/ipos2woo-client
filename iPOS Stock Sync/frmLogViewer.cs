﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class frmLogViewer : Form
    {
        public List<Tuple<string, Color>> logQueue = new List<Tuple<string, Color>>();
        public Boolean clearLog = false;

        string Title = "";

        public frmLogViewer(string title)
        {
            Title = title;
            InitializeComponent();
        }

        public void Log(string text, Color? color = null)
        {
            logQueue.Add(new Tuple<string, Color>(text, color ?? Color.White));
        }


        void HighlightText(string word, Color color)
        {

            if (word == string.Empty)
                return;

            int s_start = txtLog.SelectionStart, startIndex = 0, index;

            while ((index = txtLog.Text.IndexOf(word, startIndex)) != -1)
            {
                txtLog.Select(index, word.Length);
                txtLog.SelectionColor = color;

                startIndex = index + word.Length;
            }

            txtLog.SelectionStart = s_start;
            txtLog.SelectionLength = 0;
            txtLog.SelectionColor = Color.White;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            
        }

        void AppendLog(string text, Color color, bool addNewLine = false)
        {
            txtLog.SuspendLayout();
            txtLog.SelectionColor = color;
            txtLog.AppendText(addNewLine
                ? $"{text}{Environment.NewLine}"
                : text);
            txtLog.ScrollToCaret();
            txtLog.ResumeLayout();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (clearLog)
            {
                txtLog.Clear();
                clearLog = false;
            }
            if (logQueue.Count > 0)
            {
                if (txtLog.Lines.Length > 1000)
                {
                    txtLog.Clear();
                }
                while (logQueue.Count > 0)
                { 
                    AppendLog(logQueue[0].Item1, logQueue[0].Item2, true);
                    logQueue.RemoveAt(0);
                }
            }
        }

        private void frmLogViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                timer1.Enabled = false;
                Hide();
            }
        }

        private void frmLogViewer_Load(object sender, EventArgs e)
        {
            this.Text = Title;
        }

        private void frmLogViewer_Shown(object sender, EventArgs e)
        {
            
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                var pos = txtLog.Find(txtSearch.Text);
                if(pos > -1)
                {
                    txtLog.Focus();
                    txtLog.SelectionStart = txtLog.Find(txtSearch.Text);
                    txtLog.ScrollToCaret();
                }
            }
        }

        private void frmLogViewer_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if (clearLog)
                {
                    txtLog.Clear();
                    clearLog = false;
                }
                timer1.Enabled = true;
            }
        }
    }
}
