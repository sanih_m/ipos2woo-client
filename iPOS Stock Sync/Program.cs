﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;
using Npgsql;
using System.IO.Compression;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;

namespace iPOS_Stock_Sync
{
    public static class Program
    {
        public enum Appmodule
        {
            Ipos = 0,
            PlazaKamera = 1,
            Onglai = 2,
        }

        public static Appmodule module = Appmodule.Ipos;
        public static string config_file = Application.StartupPath + "/config.json";
        public static string cache_woo_file = Application.StartupPath + "/cache_woocommerce.json";
        public static Boolean disableAutoStart = false;
        public static Boolean verified = false;
        public static AppConfig config;
        public static System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
        public static Dictionary<string, frmProfileSync> Syncers = new Dictionary<string, frmProfileSync>();

        public static string devPassword = "loopback28";
        public static string secretKey = "0b00233467297e41931781a9f12dbb0b";
        public static int id_user = 0;
        public static List<SimpleJSON.JSONNode> access_user = new List<SimpleJSON.JSONNode>();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var assembly = typeof(Program).Assembly;
            var attribute = (GuidAttribute)assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0];
            using (Mutex mutex = new Mutex(false, "Global\\" + attribute.Value))
            {
                if (!mutex.WaitOne(0, false))
                {
                    MessageBox.Show("Sudah ada proses Ipos2Wo yang sedang berjalan.");
                    return;
                }

                LoadConfig();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmProfile());
            }
        }

        public static void LogToMain(int idx,string text)
        {
            Syncers[config.profiles[idx].name].Log(text);
        }

        public static void SaveConfig()
        {
            var json = JsonConvert.SerializeObject(config);
            File.WriteAllText(config_file, json);
            File.Copy(config_file, config_file + ".backup", true);
        }

        public static void LoadConfig()
        {
            try
            {
                config = JsonConvert.DeserializeObject<AppConfig>(File.ReadAllText(config_file));
            }
            catch (Exception)
            {
                config = new AppConfig();
            }
        }

        public static void CloneDirectory(string root, string dest)
        {
            foreach (var directory in Directory.GetDirectories(root))
            {
                string dirName = Path.GetFileName(directory);
                if (!Directory.Exists(Path.Combine(dest, dirName)))
                {
                    Directory.CreateDirectory(Path.Combine(dest, dirName));
                }
                CloneDirectory(directory, Path.Combine(dest, dirName));
            }

            foreach (var file in Directory.GetFiles(root))
            {
                File.Copy(file, Path.Combine(dest, Path.GetFileName(file)));
            }
        }

        public static string SurroundWithSingleQuotes(this string text)
        {
            return "'" + text + "'";
        }

        public static string SurroundWithDoubleQuotes(this string text)
        {
            return '"' + text + '"';
        }

        public static int productLimit()
        {
            if(access_user.Count > 0)
            {
                if(access_user[0] != null)
                {
                    return access_user[0]["max_product"].AsInt;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }

        public static string packageName()
        {
            if (access_user.Count > 0)
            {
                if (access_user[0] != null)
                {
                    return Program.StripQuotes(access_user[0]["name"].ToString());
                }
                else
                {
                    return "-";
                }
            }
            else
            {
                return "-";
            }
        }

        public static int profileLimit()
        {
            if (access_user.Count > 0)
            {
                if (access_user[0] != null)
                {
                    return access_user[0]["max_profile"].AsInt;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Compresses the string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string CompressString(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);

            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }
            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            return Convert.ToBase64String(compressedData);
        }

        /// <summary>
        /// Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string DecompressString(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static bool DeepCompare(this object obj, object another)
        {
            if (ReferenceEquals(obj, another)) return true;
            if ((obj == null) || (another == null)) return false;
            //Compare two object's class, return false if they are difference
            if (obj.GetType() != another.GetType()) return false;

            var result = true;
            //Get all properties of obj
            //And compare each other
            foreach (var property in obj.GetType().GetProperties())
            {
                var objValue = property.GetValue(obj);
                var anotherValue = property.GetValue(another);
                if (!objValue.Equals(anotherValue)) result = false;
            }

            return result;
        }

        public static int CompareProduct(SingleProduct p1, SingleProduct p2)
        {
            if (!p1.id.Equals(p2.id))
            {
                System.Diagnostics.Debug.WriteLine("diff : id | "+p1.name.ToString()+" vs "+p2.name.ToString());
                return 2;
            }
            if (!p1.name.Equals(p2.name))
            {
                System.Diagnostics.Debug.WriteLine("diff : name");
                return 3;
            }
            if (!p1.cost.Equals(p2.cost))
            {
                System.Diagnostics.Debug.WriteLine("diff : cost");
                return 4;
            }
            if (p1.price1 > -1 && !p1.price1.Equals(p2.price1))
            {
                System.Diagnostics.Debug.WriteLine("diff : price1");
                return 5;
            }
            if (p1.price2 > -1 && !p1.price2.Equals(p2.price2))
            {
                System.Diagnostics.Debug.WriteLine("diff : price2");
                return 6;
            }
            if (p1.price3 > -1 && !p1.price3.Equals(p2.price3))
            {
                System.Diagnostics.Debug.WriteLine("diff : price3");
                return 7;
            }
            if (p1.price4 > -1 && !p1.price4.Equals(p2.price4))
            {
                System.Diagnostics.Debug.WriteLine("diff : price3");
                return 8;
            }
            if (!p1.sku.Equals(p2.sku))
            {
                System.Diagnostics.Debug.WriteLine("diff : sku");
                return 9;
            }
            if (p1.stock > -1 && !p1.stock.Equals(p2.stock))
            {
                //Syncers["test"].Log(p1.stock + " vs " + p2.stock);
                System.Diagnostics.Debug.WriteLine("diff : stock");
                return 10;
            }
            if (!p1.weight.Equals(p2.weight))
            {
                System.Diagnostics.Debug.WriteLine("diff : weight");
                return 11;
            }
            if (!p1.description.Equals(p2.description))
            {
                System.Diagnostics.Debug.WriteLine("diff : description");
                return 12;
            }
            if (!p1.publish_date.Equals(p2.publish_date))
            {
                System.Diagnostics.Debug.WriteLine("diff : publish_date");
                return 13;
            }

            if (!p1.unit.Equals(p2.unit))
            {
                System.Diagnostics.Debug.WriteLine("diff : unit");
                return 14;
            }

            return 0;
        }

        public static bool CompareTokopediaProduct(SingleProduct p1, TokopediaProduct p2, ProfileConfig config)
        {
            if (config.tokped_price == 1)
            {
                if (!p1.price1.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price1");
                    Console.WriteLine(p1.price1+" vs "+p2.level_price);
                    return false;
                }
            }
            else if (config.tokped_price == 2)
            {
                if (!p1.price2.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price2");
                    return false;
                }
            }
            else if (config.tokped_price == 3)
            {
                if (!p1.price3.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price3");
                    return false;
                }
            }
            else if (config.tokped_price == 4)
            {
                if (!p1.price4.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price4");
                    return false;
                }
            }
            if (!p1.stock.Equals(p2.stock))
            {
                Console.WriteLine("diff : stock");
                return false;
            }
            return true;
        }

        public static bool CompareShopeeProduct(SingleProduct p1, ShopeeProduct p2, ProfileConfig config)
        {
            if (config.shopee_price == 1)
            {
                if (!p1.price1.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price1");
                    return false;
                }
            }
            else if (config.shopee_price == 2)
            {
                if (!p1.price2.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price2");
                    return false;
                }
            }
            else if (config.shopee_price == 3)
            {
                if (!p1.price3.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price3");
                    return false;
                }
            }
            else if (config.shopee_price == 4)
            {
                if (!p1.price4.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price4");
                    return false;
                }
            }
            if (!p1.stock.Equals(p2.stock))
            {
                Console.WriteLine("diff : stock");
                return false;
            }
            return true;
        }

        public static bool CompareBukalapakProduct(SingleProduct p1, BukalapakProduct p2, ProfileConfig config)
        {
            if (config.bukalapak_price == 1)
            {
                if (!p1.price1.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price1");
                    return false;
                }
            }
            else if (config.bukalapak_price == 2)
            {
                if (!p1.price2.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price2");
                    return false;
                }
            }
            else if (config.bukalapak_price == 3)
            {
                if (!p1.price3.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price3");
                    return false;
                }
            }
            else if (config.bukalapak_price == 4)
            {
                if (!p1.price4.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price4");
                    return false;
                }
            }
            if (!p1.stock.Equals(p2.stock))
            {
                Console.WriteLine("diff : stock");
                return false;
            }
            return true;
        }

        public static bool CompareLazadaProduct(SingleProduct p1, LazadaProduct p2, ProfileConfig config)
        {
            if (config.lazada_price == 1)
            {
                if (!p1.price1.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price1");
                    return false;
                }
            }
            else if (config.lazada_price == 2)
            {
                if (!p1.price2.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price2");
                    return false;
                }
            }
            else if (config.lazada_price == 3)
            {
                if (!p1.price3.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price3");
                    return false;
                }
            }
            else if (config.lazada_price == 4)
            {
                if (!p1.price4.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price4");
                    return false;
                }
            }
            if (!p1.stock.Equals(p2.stock))
            {
                Console.WriteLine("diff : stock");
                return false;
            }
            return true;
        }

        public static bool CompareBlibliProduct(SingleProduct p1, BlibliProduct p2, ProfileConfig config)
        {
            if (config.blibli_price == 1)
            {
                if (!p1.price1.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price1");
                    return false;
                }
            }
            else if (config.blibli_price == 2)
            {
                if (!p1.price2.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price2");
                    return false;
                }
            }
            else if (config.blibli_price == 3)
            {
                if (!p1.price3.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price3");
                    return false;
                }
            }
            else if (config.blibli_price == 4)
            {
                if (!p1.price4.Equals(p2.level_price))
                {
                    Console.WriteLine("diff : price4");
                    return false;
                }
            }
            if (!p1.stock.Equals(p2.stock))
            {
                Console.WriteLine("diff : stock");
                return false;
            }
            return true;
        }

        public static bool CompareCustomer(Customer c1, Customer c2)
        {
            if (!c1.id.Equals(c2.id))
            {
                System.Diagnostics.Debug.WriteLine("diff : id | " + c1.name.ToString() + " vs " + c2.name.ToString());
                return false;
            }
            if (!c1.code.Equals(c2.code))
            {
                System.Diagnostics.Debug.WriteLine("diff : code | " + c1.name.ToString() + " vs " + c2.name.ToString());
                return false;
            }
            if (!c1.name.Equals(c2.name))
            {
                System.Diagnostics.Debug.WriteLine("diff : name | " + c1.name.ToString() + " vs " + c2.name.ToString());
                return false;
            }
            if (!c1.address.Equals(c2.address))
            {
                System.Diagnostics.Debug.WriteLine("diff : address | " + c1.name.ToString() + " vs " + c2.name.ToString());
                return false;
            }
            return true;
        }

        public static string StripQuotes(string str)
        {
            return str.Replace("\"", "").Trim();
        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static void WriteDevLog(string module,string message,string stacktrace)
        {
            
        }

        public static DialogResult ShowInputDialog(ref string input, string title)
        {
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            inputBox.ClientSize = size;
            inputBox.Text = title;
            inputBox.StartPosition = FormStartPosition.CenterParent;

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        public static string CleanSQLString(string input)
        {
            return input.Replace("'", "''").Replace("\"", "\"\"").Replace("&amp;","");
        }
    }


}
