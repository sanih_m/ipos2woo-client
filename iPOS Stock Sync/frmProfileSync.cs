﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;
using SimpleJSON;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Security;
using System.Net;
using Flurl;
using Flurl.Http;
using MaterialSkin;
using iPOS_Stock_Sync.Modules;
using Microsoft.VisualBasic;
using System.Diagnostics;

namespace iPOS_Stock_Sync
{   
    public partial class frmProfileSync : Form
    {
        
        int elapsedSecs = 0;
        bool isTimer = false;
        bool isDelay = false;
        bool isSynchronizing = false;
        
        public int idxProfile = -1;

        public string cache_woo_file = "cache_woo_{IDX}.json";
        public WooCache cache_woo;

        public string cache_product_file = "cache_products_{IDX}.json";
        public LastProductCache cache_product;

        public string cache_customers_file = "cache_customers_{IDX}.json";
        public LastCustomerCache cache_customers;

        public string cache_sales_file = "cache_sales_{IDX}.json";
        public LastSalesCache cache_sales;

        public string monitoring_file = Application.StartupPath + "/" + "monitoring_product_{IDX}.log";
        public string monitoring_cust_file = Application.StartupPath + "/" + "monitoring_cust_{IDX}.log";
        public string monitoring_sales_file = Application.StartupPath + "/" + "monitoring_sales_{IDX}.log";

        public Modules.IposCore instance_ipos;
        public Modules.PlazaKameraCore instance_pk;
        public Modules.OnglaiCore instance_ong;

        public Modules.TokopediaCore instance_tokped;
        public Modules.ShopeeCore instance_shopee;
        public Modules.BukalapakCore instance_bukalapak;
        public Modules.LazadaCore instance_lazada;
        public Modules.BlibliCore instance_blibli;

        Task MarketInitialization,SyncWorker;

        public Boolean allow_woocommerce = true;
        public Boolean allow_tokopedia = true;
        public Boolean allow_shopee = true;
        public Boolean allow_bukalapak = true;
        public Boolean allow_lazada = true;
        public Boolean allow_blibli = true;
        public Boolean allow_monitoring = true;

        public Boolean firstTime = true;

        public Dictionary<string, Product> listProduct = new Dictionary<string, Product>();
        public Dictionary<string, SingleProduct> listSingleProduct = new Dictionary<string, SingleProduct>();
        public Dictionary<string, Customer> listCustomer = new Dictionary<string, Customer>();
        public Dictionary<string, Sales> listSales = new Dictionary<string, Sales>();
        public Dictionary<string, string> listWooPublish = new Dictionary<string, string>();
        public Dictionary<string, MonitoredCustomer> listMonitoredCustomers = new Dictionary<string, MonitoredCustomer>();
        public Dictionary<string, MonitoredSales> listMonitoredSales = new Dictionary<string, MonitoredSales>();
        public Dictionary<string, MonitoredProduct> listMonitored = new Dictionary<string, MonitoredProduct>();

        List<Tuple<string, Color>> logQueue = new List<Tuple<string, Color>>();

        ProfileConfig profile = null;
        int profileHash = 0;

        public string command = "";
        public List<String> syncProductFilter = new List<string>();
        

        public frmProfileSync(int idx)
        {
            idxProfile = idx;
            profile = Program.config.profiles[idxProfile];
            cache_woo_file = cache_woo_file.Replace("{IDX}", idxProfile.ToString());
            cache_product_file = cache_product_file.Replace("{IDX}", idxProfile.ToString());
            cache_customers_file = cache_customers_file.Replace("{IDX}", idxProfile.ToString());
            cache_sales_file = cache_sales_file.Replace("{IDX}", idxProfile.ToString());

            monitoring_file = monitoring_file.Replace("{IDX}", idxProfile.ToString());
            monitoring_cust_file = monitoring_cust_file.Replace("{IDX}", idxProfile.ToString());
            monitoring_sales_file = monitoring_sales_file.Replace("{IDX}", idxProfile.ToString());

            this.Text = profile.name;

            InitializeComponent();

        }

        public void Log(string text,Color? color = null)
        {
            logQueue.Add(new Tuple<string, Color>(text, color ?? Color.White));
        }

        
        public void SaveWooCache()
        {
            var json = JsonConvert.SerializeObject(cache_woo);
            File.WriteAllText(cache_woo_file, json);
        }

        private Dictionary<String,Product> getAllProduct()
        {
            if(Program.module == Program.Appmodule.Ipos)
            {
                return instance_ipos.allProducts;
            }else if(Program.module == Program.Appmodule.Onglai)
            {
                return instance_ong.allProducts;
            }
            else
            {
                return null;
            }
        }

        public void LoadWooCache()
        {
            try
            {
                var file = File.ReadAllText(cache_woo_file);
                if (file.Trim().Equals(""))
                {
                    cache_woo = new WooCache();
                }
                else
                {
                    cache_woo = JsonConvert.DeserializeObject<WooCache>(file);
                    if (cache_woo == null) cache_woo = new WooCache();
                }
            }
            catch (Exception)
            {
                cache_woo = new WooCache();
            }
        }

        public void SaveProductCache()
        {
            var json = JsonConvert.SerializeObject(cache_product);
            File.WriteAllText(cache_product_file, json);
        }

        public void LoadProductCache()
        {
            try
            {
                var data = File.ReadAllText(cache_product_file);
                if (data.Trim().Equals(""))
                {
                    cache_product = new LastProductCache();
                }
                else
                {
                    cache_product = JsonConvert.DeserializeObject<LastProductCache>(data);
                    if (cache_product == null) cache_product = new LastProductCache();
                }

            }
            catch (Exception)
            {
                cache_product = new LastProductCache();
            }
        }

        public void SaveCustomerCache()
        {
            var json = JsonConvert.SerializeObject(cache_customers);
            File.WriteAllText(cache_customers_file, json);
        }

        public void LoadCustomerCache()
        {
            try
            {
                var data = File.ReadAllText(cache_customers_file);
                if (data.Trim().Equals(""))
                {
                    cache_customers = new LastCustomerCache();
                }
                else
                {
                    cache_customers = JsonConvert.DeserializeObject<LastCustomerCache>(data);
                    if (cache_customers == null) cache_customers = new LastCustomerCache();
                }
            }
            catch (Exception)
            {
                cache_customers = new LastCustomerCache();
            }
        }

        public void SaveSalesCache()
        {
            var json = JsonConvert.SerializeObject(cache_sales);
            File.WriteAllText(cache_sales_file, json);
        }

        public void LoadSalesCache()
        {
            try
            {
                var data = File.ReadAllText(cache_sales_file);
                
                if (data.Trim().Equals(""))
                {
                    cache_sales = new LastSalesCache();
                }
                else
                {
                    cache_sales = JsonConvert.DeserializeObject<LastSalesCache>(data);
                    if(cache_sales == null) cache_sales = new LastSalesCache();
                }
            }
            catch (Exception)
            {
                cache_sales = new LastSalesCache();
            }
        }

        private void frmMain_Shown(object sender, EventArgs e)
        {
            profile = Program.config.profiles[idxProfile];
            if (profile == null)
            {
                profile = new ProfileConfig();
                var cfg = new frmProfileConfig(idxProfile);
                cfg.ShowDialog();
            }
            if (Program.module == Program.Appmodule.Ipos)
            {
                if (instance_ipos == null)
                {
                    instance_ipos = new IposCore(idxProfile);
                }
            }
            else if (Program.module == Program.Appmodule.PlazaKamera)
            {
                if (instance_pk == null)
                {
                    instance_pk = new PlazaKameraCore(idxProfile);
                }
            }
            else if (Program.module == Program.Appmodule.Onglai)
            {
                if (instance_ong == null)
                {
                    instance_ong = new OnglaiCore(idxProfile);
                }
            }
            if (Program.module != Program.Appmodule.Ipos || instance_ipos.ConnectDB())
            {
                initConfig();
                if (profile.autostart && !Program.disableAutoStart)
                {
                    btnToogleTimer_Click(this, null);
                }
            }
        }

        void initConfig()
        {
            profile = Program.config.profiles[idxProfile];
            try
            {
                txtInterval.Value = profile.interval;
                cbWarehouse.Items.Clear();
                List<String> warehouses = new List<string>();
                if(Program.module == Program.Appmodule.Ipos)
                {
                    warehouses = instance_ipos.GetWarehouses();
                }
                else if(Program.module == Program.Appmodule.PlazaKamera)
                {
                    warehouses = instance_pk.GetWarehouses();
                }
                else if (Program.module == Program.Appmodule.Onglai)
                {
                    warehouses = instance_ong.GetWarehouses();
                }
                foreach (var item in warehouses)
                {
                    cbWarehouse.Items.Add(item);
                }
                if (!profile.warehouse.Equals(""))
                {
                    var split = profile.warehouse.Split(',');
                    foreach (string item in split)
                    {
                        foreach (var item2 in cbWarehouse.CheckBoxItems)
                        {
                            if(item2.Text.Equals(item.Replace("'", ""))) item2.Checked = true;
                        }
                    }
                }

                var newHash = profile.tokped_email.GetHashCode() +
                    profile.tokped_password.GetHashCode() +
                    profile.tokped_on.GetHashCode() +
                    profile.shopee_email.GetHashCode() +
                    profile.shopee_password.GetHashCode() +
                    profile.shopee_on.GetHashCode() +
                    profile.bukalapak_email.GetHashCode() +
                    profile.bukalapak_password.GetHashCode() +
                    profile.bukalapak_on.GetHashCode() +
                    profile.lazada_email.GetHashCode() +
                    profile.lazada_password.GetHashCode() +
                    profile.lazada_on.GetHashCode() +
                    profile.blibli_email.GetHashCode() +
                    profile.blibli_password.GetHashCode() +
                    profile.blibli_on.GetHashCode();
                if (profileHash != 0 && profileHash != newHash)
                {
                    if (instance_tokped != null)
                    {
                        instance_tokped.stop();
                    }
                    if (instance_shopee != null)
                    {
                        instance_shopee.stop();
                    }
                    if (instance_bukalapak != null)
                    {
                        instance_bukalapak.stop();
                    }
                    if (instance_lazada != null)
                    {
                        instance_lazada.stop();
                    }
                    if (instance_blibli != null)
                    {
                        instance_blibli.stop();
                    }
                    bMarketInit.RunWorkerAsync();
                }

                if(instance_tokped != null)
                {
                    instance_tokped.calculateNextCheckID(profile);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message+e.StackTrace);
                var cfg = new frmProfileConfig(idxProfile);
                if (cfg.ShowDialog() == DialogResult.OK)
                {
                    if (Program.module != Program.Appmodule.Ipos || instance_ipos.ConnectDB())
                    {
                        initConfig();
                    }
                }
            }
        }
         
  

       
        private void txtInterval_ValueChanged(object sender, EventArgs e)
        {
  
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtLog.Clear();
        }

        void setServicePoint()
        {
            //ServicePointManager.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                     | SecurityProtocolType.Tls11
                                     | SecurityProtocolType.Tls12;
            //ServicePointManager.ServerCertificateValidationCallback +=
            //(sender, certificate, chain, sslPolicyErrors) => true;
            //ServicePointManager.SetTcpKeepAlive(true, 10000, 10000);
            //ServicePointManager.DefaultConnectionLimit = 100;
            //ServicePointManager.MaxServicePointIdleTime = 5000;
        }

        void BeginMainSync()
        {
            if (!bWorker.IsBusy && (SyncWorker == null || SyncWorker.IsCompleted))
            {
                var TroubledMarkets = "";
                if (instance_tokped != null)
                {
                    if (instance_tokped.isStuck && instance_tokped.logControl.isWorking)
                    {
                        DeleteMarketplaceTab(TokopediaCore.marketName.ToLower());
                        TroubledMarkets += TokopediaCore.marketName.ToLower();
                    }
                }
                if (instance_shopee != null)
                {
                    if (instance_shopee.isStuck && instance_shopee.logControl.isWorking)
                    {
                        DeleteMarketplaceTab(ShopeeCore.marketName.ToLower());
                        TroubledMarkets += ShopeeCore.marketName.ToLower();
                    }
                }
                if (instance_bukalapak != null)
                {
                    if (instance_bukalapak.isStuck && instance_bukalapak.logControl.isWorking)
                    {
                        DeleteMarketplaceTab(BukalapakCore.marketName.ToLower());
                        TroubledMarkets += BukalapakCore.marketName.ToLower();
                    }
                }
                if (instance_lazada != null)
                {
                    if (instance_lazada.isStuck && instance_lazada.logControl.isWorking)
                    {
                        DeleteMarketplaceTab(LazadaCore.marketName.ToLower());
                        TroubledMarkets += LazadaCore.marketName.ToLower();
                    }
                }
                if (instance_blibli != null)
                {
                    if (instance_blibli.isStuck && instance_blibli.logControl.isWorking)
                    {
                        DeleteMarketplaceTab(BlibliCore.marketName.ToLower());
                        TroubledMarkets += BlibliCore.marketName.ToLower();
                    }
                }

                if (!TroubledMarkets.Equals(""))
                {
                    Task.Run(() => MarketInitialization = ConfigureMarketplace(TroubledMarkets, () =>
                    {
                        bWorker.RunWorkerAsync();
                    }));
                }
                else
                {
                    bWorker.RunWorkerAsync();
                } 
            }
        }

        private void bWorker_DoWorkAsync(object sender, DoWorkEventArgs ea)
        {
            if (MarketInitialization == null || !MarketInitialization.IsCompleted)
            {
                bMarketInit.RunWorkerAsync();
            }

            Task.Run(() => SyncWorker = DoSyncAsync());
        }

        //cek sync distop? kalo distop, set semua status jadi beres
        public bool isSyncStopped()
        {
            if (!isSynchronizing)
            {
                if(instance_tokped != null)
                {
                    instance_tokped.isSyncDone = true;
                    if(instance_tokped.logControl != null) instance_tokped.logControl.isWorking = false;
                    instance_tokped.isStuck = false;
                }
                if (instance_shopee != null)
                {
                    instance_shopee.isSyncDone = true;
                    if (instance_shopee.logControl != null) instance_shopee.logControl.isWorking = false;
                    instance_shopee.isStuck = false;
                }
                if (instance_bukalapak != null)
                {
                    instance_bukalapak.isSyncDone = true;
                    if (instance_bukalapak.logControl != null) instance_bukalapak.logControl.isWorking = false;
                    instance_bukalapak.isStuck = false;
                }
                if (instance_lazada != null)
                {
                    instance_lazada.isSyncDone = true;
                    if (instance_lazada.logControl != null) instance_lazada.logControl.isWorking = false;
                    instance_lazada.isStuck = false;
                }
                if (instance_blibli != null)
                {
                    instance_blibli.isSyncDone = true;
                    if (instance_blibli.logControl != null) instance_blibli.logControl.isWorking = false;
                    instance_blibli.isStuck = false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        async Task DoSyncAsync()
        {
            while (MarketInitialization == null || !MarketInitialization.IsCompleted)
            {
                // Console.WriteLine("Waiting for Market init");
                await Task.Delay(1).ConfigureAwait(false);
            }
            isSynchronizing = true;
            try
            {
                Log("========== SINKRONISASI DIMULAI ==========");
                await Task.Delay(3000).ConfigureAwait(false);
                var startSecs = Program.watch.ElapsedMilliseconds;
                
                profile = Program.config.profiles[idxProfile];

                listSingleProduct.Clear();        
                listProduct.Clear();
                listCustomer.Clear();
                listSales.Clear();
                listWooPublish.Clear();
                listMonitored.Clear();
                listMonitoredCustomers.Clear();
                listMonitoredSales.Clear();


                var url = "";
                var resp = "";
                var content = "";
                var obj = new JSONNode();
                var deletedProducts = "";
                var deletedCustomers = "";
                var isNewItems = false;

                firstTime = cache_product == null || cache_product.data == null || cache_product.data.Count == 0;

                var isHttps = profile.woo_url.Equals("") ? false : profile.woo_url.Substring(0, 5).Equals("https");
                if (isHttps)
                {
                    setServicePoint();
                }

              
                var client = new RestClient();
                var request = new RestRequest();

                IRestResponse response = null;
                List<JsonArray> items = new List<JsonArray>();


                if (isSyncStopped()) return;
                try
                {
                    url = Program.config.monitorURL
                            .AppendPathSegment("/setting/keyword_produk_blacklist");
                    resp = await url.GetStringAsync().ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    resp = e.Message;
                }
                var product_blacklist = resp.Split(',');



                if (isSyncStopped()) return;
                if (profile.primary_key == ProfileConst.PK_CODE)
                {
                    Log("Membaca data produk dari database berdasarkan kode item...");
                }
                else if (profile.primary_key == ProfileConst.PK_BARCODE)
                {
                    Log("Membaca data produk dari database berdasarkan barcode...");
                }

                var oldProducts = cache_product.data.ToDictionary(entry => entry.Key,
                                               entry => (SingleProduct)entry.Value.Clone());
                var oldCustomers = cache_customers.data.ToDictionary(entry => entry.Key,
                                               entry => (Customer)entry.Value.Clone());
                var oldSales = cache_sales.data.ToDictionary(entry => entry.Key,
                                               entry => (Sales)entry.Value.Clone());

                
                if (syncProductFilter.Count > 0)
                {
                    List<SingleProduct> deletedCache = new List<SingleProduct>();
                    //hapus cache yg difilter
                    foreach (var item in oldProducts)
                    {
                        if (syncProductFilter.Contains(item.Value.sku))
                        {
                            deletedCache.Add(item.Value);
                            cache_product.data.Remove(item.Key);
                        }
                    }
                    SaveProductCache();
                    foreach (var item in deletedCache)
                    {
                        if (instance_tokped != null && instance_tokped.cache != null)
                        {
                            instance_tokped.RemoveProductFromCache(item.sku, true);
                        }
                        if (instance_shopee != null && instance_shopee.cache != null)
                        {
                            instance_shopee.RemoveProductFromCache(item.sku, true);
                        }
                        if (instance_bukalapak != null && instance_bukalapak.cache != null)
                        {
                            instance_bukalapak.RemoveProductFromCache(item.sku, true);
                        }
                        if (instance_lazada != null && instance_lazada.cache != null)
                        {
                            instance_lazada.RemoveProductFromCache(item.sku, true);
                        }
                        if (instance_blibli != null && instance_blibli.cache != null)
                        {
                            instance_blibli.RemoveProductFromCache(item.sku, true);
                        }
                        oldProducts.Remove(item.id); 
                    }
                }

                if (isSyncStopped()) return;
                if (profile.woo_send_publish)
                {
                    #region "Ambil Data Publish";
                    Log("Mengambil data publish dari Woocommerce...");
                    try
                    {
                        url = profile.woo_url
                                .AppendPathSegment("/wp-json/custom-routes/v1/fetchpoststatus");
                        content = await url
                        .PostUrlEncodedAsync(new
                        {
                            secret = "iposXwoocommerce",
                        }).ReceiveString().ConfigureAwait(false);
                        if (profile.main_dev.Equals(Program.devPassword))
                        {
                            Log("Respons : " + content);
                        }
                        obj = SimpleJSON.JSON.Parse(content);
                        if (obj.AsArray.Count > 0)
                        {
                            Log("Terdapat " + obj.AsArray.Count + " data publish");
                            foreach (JSONNode item in obj.AsArray)
                            {
                                var row = SimpleJSON.JSON.Parse(item.ToString());
                                if (listWooPublish.ContainsKey(row[1].ToString().Replace("\"", "")))
                                {
                                    if (row[0].ToString().Replace("\"", "").Equals("publish"))
                                    {
                                        listWooPublish[row[1].ToString().Replace("\"", "")] = row[2].ToString().Replace("\"", "") + "|" + row[3].ToString().Replace("\"", "") + "|" + row[4].ToString().Replace("\"", "");
                                    }
                                    else
                                    {
                                        listWooPublish.Remove(row[1].ToString().Replace("\"", ""));
                                    }
                                }
                                else
                                {
                                    if (row[0].ToString().Replace("\"", "").Equals("publish"))
                                    {
                                        listWooPublish.Add(row[1].ToString().Replace("\"", ""), row[2].ToString().Replace("\"", "") + "|" + row[3].ToString().Replace("\"", "") + "|" + row[4].ToString().Replace("\"", ""));
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        content = e.Message;
                        if (profile.main_dev.Equals(Program.devPassword))
                        {
                            Log("Respons error : " + content);
                        }
                    }
                    #endregion
                }


                if (isSyncStopped()) return;
                if (Program.module == Program.Appmodule.Ipos)
                {
                    instance_ipos.Fetch(product_blacklist);
                }
                else if(Program.module == Program.Appmodule.PlazaKamera)
                {
                    await instance_pk.Fetch(product_blacklist).ConfigureAwait(false);
                }
                else if (Program.module == Program.Appmodule.Onglai)
                {
                    await instance_ong.Fetch(product_blacklist).ConfigureAwait(false);
                }

                //sesuaikan produk dengan semua variable
                foreach (var item in listProduct)
                {
                    var p = item.Value;

                    
                    if (profile.primary_key == ProfileConst.PK_CODE)
                    {
                        var sp = new SingleProduct();
                        sp.active = p.active;
                        sp.interacted = p.interacted;
                        sp.sku = p.code;
                        sp.id = p.id;
                        sp.name = p.name;
                        sp.price1 = p.price1;
                        sp.price2 = p.price2;
                        sp.price3 = p.price3;
                        sp.price4 = p.price4;
                        sp.cost = p.price_main;
                        sp.publish_date = p.publish_date;
                        sp.stock = p.stock;
                        sp.unit = p.unit;
                        sp.weight = p.weight;
                        sp.description = p.description;
                        sp.stockdetail = p.stockdetail.ToDictionary(entry => entry.Key,
                                                   entry => entry.Value);
                        if (listSingleProduct.ContainsKey(sp.id))
                        {
                            listSingleProduct[sp.id] = sp;
                        }
                        else
                        {
                            listSingleProduct.Add(sp.id, sp);
                        }

                        if (cache_product.data.ContainsKey(sp.id))
                        {
                            cache_product.data[sp.id] = (SingleProduct)sp.Clone();
                        }
                        else
                        {
                            cache_product.data.Add(sp.id, (SingleProduct)sp.Clone());
                        }

                        if (cache_woo.list_sku.ContainsKey(sp.sku))
                        {
                            cache_woo.list_sku[sp.sku] = sp.id;
                        }
                        else
                        {
                            cache_woo.list_sku.Add(sp.sku, sp.id);
                        }
                    }
                    else if (profile.primary_key == ProfileConst.PK_BARCODE)
                    {
                        if (p.units != null)
                        {
                            foreach (var unit in p.units)
                            {
                                var b = unit.Value;
                                if (!b.barcode.Equals(""))
                                {
                                    var sp = new SingleProduct();
                                    sp.active = p.active;
                                    sp.interacted = p.interacted;
                                    sp.sku = b.barcode;
                                    sp.id = p.id+"_"+b.name;
                                    sp.name = p.name;
                                    sp.cost = b.cost;
                                    sp.publish_date = p.publish_date;
                                    sp.stock = (float)Math.Round(p.stock / float.Parse(b.conversion.ToString()), 2);
                                    sp.unit = b.name;
                                    sp.weight = p.weight;
                                    sp.description = p.description;
                                    foreach (var sd in p.stockdetail)
                                    {
                                        var ws = new WarehouseStock();
                                        ws.name = sd.Value.name;
                                        ws.stock = (float)Math.Round(sd.Value.stock / float.Parse(b.conversion.ToString()), 2);
                                        sp.stockdetail.Add(sd.Key, ws);
                                    }
                                    if (b.prices != null)
                                    {
                                        foreach (var data in b.prices)
                                        {
                                            var prices = data.Value;
                                            if (prices.level == 1)
                                            {
                                                sp.price1 = prices.price;
                                            }
                                            else if (prices.level == 2)
                                            {
                                                sp.price2 = prices.price;
                                            }
                                            else if (prices.level == 3)
                                            {
                                                sp.price3 = prices.price;
                                            }
                                            else if (prices.level == 4)
                                            {
                                                sp.price4 = prices.price;
                                            }
                                        }
                                    }
                                    if (listSingleProduct.ContainsKey(sp.id))
                                    {
                                        listSingleProduct[sp.id] = sp;
                                    }
                                    else
                                    {
                                        listSingleProduct.Add(sp.id, sp);
                                    }

                                    if (cache_product.data.ContainsKey(sp.id))
                                    {
                                        cache_product.data[sp.id] = (SingleProduct)sp.Clone();
                                    }
                                    else
                                    {
                                        cache_product.data.Add(sp.id, (SingleProduct)sp.Clone());
                                    }

                                    if (cache_woo.list_sku.ContainsKey(sp.sku))
                                    {
                                        cache_woo.list_sku[sp.sku] = sp.id;
                                    }
                                    else
                                    {
                                        cache_woo.list_sku.Add(sp.sku, sp.id);
                                    }
                                }
                            }
                        }
                    }
                }



                if (isSyncStopped()) return;
                if (profile.main_send_product)
                {
                    //kumpulkan produk yg telah dihapus dari ipos untuk dikirim ke monitoring
                    foreach (var item in oldProducts)
                    {
                        if (!listSingleProduct.ContainsKey(item.Key))
                        {
                            if((syncProductFilter.Count > 0 && !syncProductFilter.Contains(item.Value.sku)))
                            {
                                continue;
                            }
                            if (!deletedProducts.Equals(""))
                            {
                                deletedProducts += ",";
                            }
                            deletedProducts += item.Value.sku.ToString();
                        }
                    }
                }


                if (isSyncStopped()) return;
                Log("Memeriksa perubahan " + listSingleProduct.Count.ToString() + " produk...");

                isNewItems = listSingleProduct.Count > oldProducts.Count;

                //System.Diagnostics.Debug.WriteLine(profile.woo_price);
                foreach (var source in new Dictionary<string, SingleProduct>(listSingleProduct))
                {
                    if (!source.Value.active) continue;
                    //System.Diagnostics.Debug.WriteLine(item.name + " : " + item.stock_synced.is_woo + " | " + item.price1_synced.is_woo + " | " + item.price2_synced.is_woo + " | " + item.price3_synced.is_woo);
                    if (!listMonitored.ContainsKey(source.Value.id))
                    {
                        var different = 0;
                        
                        if (oldProducts.ContainsKey(source.Value.id))
                        {
                            SingleProduct oldItem = oldProducts[source.Value.id];
                            different = Program.CompareProduct(source.Value, oldItem);
                        }
                        else
                        {
                            different = 1;
                        }
                        if (different > 0)
                        {
                            if (profile.main_dev.Equals(Program.devPassword)) Log("Ada perbedaan " + source.Value.id + " " + source.Value.sku+ " : "+different.ToString());
                            MonitoredProduct monitored = new MonitoredProduct();
                            monitored.id = source.Value.id;
                            monitored.sku = source.Value.sku;
                            monitored.name = source.Value.name;
                            monitored.prices = source.Value.price1 + "|" + source.Value.price2 + "|" + source.Value.price3 + "|" + source.Value.price4 + "|" + source.Value.cost;
                            monitored.weight = source.Value.weight;
                            monitored.description = source.Value.description;
                            monitored.stock = source.Value.stock;
                            monitored.woocommerce_publish_at = source.Value.publish_date;
                            monitored.stockdetail = "";
                            if (source.Value.stockdetail != null)
                            {
                                foreach (var detail in source.Value.stockdetail)
                                {
                                    if (monitored.stockdetail != "")
                                    {
                                        monitored.stockdetail += "|";
                                    }
                                    monitored.stockdetail += detail.Value.name + "#" + detail.Value.stock;
                                    //monitored.stock += detail.Value.stock;
                                }
                            }
                            monitored.unit = source.Value.unit;

                            if (instance_tokped != null)
                            {
                                monitored.tokopedia_sync = 1;
                                monitored.tokopedia_id = "";
                                monitored.tokopedia_stock = source.Value.stock;
                                switch (profile.tokped_price)
                                {
                                    case 1:
                                        monitored.tokopedia_price = source.Value.price1;
                                        break;
                                    case 2:
                                        monitored.tokopedia_price = source.Value.price2;
                                        break;
                                    case 3:
                                        monitored.tokopedia_price = source.Value.price3;
                                        break;
                                    case 4:
                                        monitored.tokopedia_price = source.Value.price4;
                                        break;
                                    default:
                                        monitored.tokopedia_price = -1;
                                        break;
                                }
                                monitored.tokopedia_status = -1;
                            }

                            if (instance_shopee != null)
                            {
                                monitored.shopee_sync = 1;
                                monitored.shopee_id = "";
                                monitored.shopee_stock = source.Value.stock;
                                switch (profile.shopee_price)
                                {
                                    case 1:
                                        monitored.shopee_price = source.Value.price1;
                                        break;
                                    case 2:
                                        monitored.shopee_price = source.Value.price2;
                                        break;
                                    case 3:
                                        monitored.shopee_price = source.Value.price3;
                                        break;
                                    case 4:
                                        monitored.shopee_price = source.Value.price4;
                                        break;
                                    default:
                                        monitored.shopee_price = -1;
                                        break;
                                }
                                monitored.shopee_status = -1;
                            }

                            if (instance_bukalapak != null)
                            {
                                monitored.bukalapak_sync = 1;
                                monitored.bukalapak_id = "";
                                monitored.bukalapak_stock = source.Value.stock;
                                switch (profile.bukalapak_price)
                                {
                                    case 1:
                                        monitored.bukalapak_price = source.Value.price1;
                                        break;
                                    case 2:
                                        monitored.bukalapak_price = source.Value.price2;
                                        break;
                                    case 3:
                                        monitored.bukalapak_price = source.Value.price3;
                                        break;
                                    case 4:
                                        monitored.bukalapak_price = source.Value.price4;
                                        break;
                                    default:
                                        monitored.bukalapak_price = -1;
                                        break;
                                }
                                monitored.bukalapak_status = -1;
                            }

                            if (instance_lazada != null)
                            {
                                monitored.lazada_sync = 1;
                                monitored.lazada_id = "";
                                monitored.lazada_stock = source.Value.stock;
                                switch (profile.lazada_price)
                                {
                                    case 1:
                                        monitored.lazada_price = source.Value.price1;
                                        break;
                                    case 2:
                                        monitored.lazada_price = source.Value.price2;
                                        break;
                                    case 3:
                                        monitored.lazada_price = source.Value.price3;
                                        break;
                                    case 4:
                                        monitored.lazada_price = source.Value.price4;
                                        break;
                                    default:
                                        monitored.lazada_price = -1;
                                        break;
                                }
                                monitored.lazada_status = -1;
                            }

                            if (instance_blibli != null)
                            {
                                monitored.blibli_sync = 1;
                                monitored.blibli_id = "";
                                monitored.blibli_stock = source.Value.stock;
                                switch (profile.blibli_price)
                                {
                                    case 1:
                                        monitored.blibli_price = source.Value.price1;
                                        break;
                                    case 2:
                                        monitored.blibli_price = source.Value.price2;
                                        break;
                                    case 3:
                                        monitored.blibli_price = source.Value.price3;
                                        break;
                                    case 4:
                                        monitored.blibli_price = source.Value.price4;
                                        break;
                                    default:
                                        monitored.blibli_price = -1;
                                        break;
                                }
                                monitored.blibli_status = -1;
                            }


                            listMonitored.Add(monitored.id, monitored);

                            if (!cache_woo.list_id.ContainsKey(source.Value.id))
                            {
                                //queue pada nama berguna untuk sync to draft

                                if (!cache_woo.list_queue.ContainsKey(source.Value.sku.ToString()))
                                {
                                    cache_woo.list_queue.Add(source.Value.sku, source.Value.sku + "|" + source.Value.name);
                                }
                                else
                                {
                                    cache_woo.list_queue[source.Value.sku] = source.Value.sku + "|" + source.Value.name;
                                }
                            }
                        }
                        else
                        {
                            listSingleProduct.Remove(source.Key);
                            if (profile.main_dev.Equals(Program.devPassword)) Log("Tidak Ada perbedaan");
                        }

                    }
                    if (isSyncStopped()) return;
                }
                if (listMonitored.Count > 0)
                {
                    Log("Perubahan terdeteksi pada " + listMonitored.Count.ToString() + " produk.");
                }
                else
                {
                    Log("Belum ada perubahan yang terdeteksi pada produk.");
                }


                if (isSyncStopped()) return;
                if (profile.main_send_customer)
                {
                    
                    //kumpulkan pelanggan yg telah dihapus dari ipos untuk dikirim ke monitoring
                    foreach (var item in oldCustomers)
                    {
                        if (!listCustomer.ContainsKey(item.Key))
                        {
                            if (!deletedCustomers.Equals(""))
                            {
                                deletedCustomers += ",";
                            }
                            deletedCustomers += item.Value.id.ToString();
                        }
                    }

                    Log("Memeriksa " + listCustomer.Count.ToString() + " data pelanggan...");
                    foreach (var source in new Dictionary<string, Customer>(listCustomer))
                    {
                        if (!listMonitoredCustomers.ContainsKey(source.Value.id))
                        {
                            if (!oldCustomers.ContainsKey(source.Value.id) || (oldCustomers.ContainsKey(source.Value.id) && !Program.CompareCustomer(source.Value, oldCustomers[source.Value.id])))
                            {
                                MonitoredCustomer monitored = new MonitoredCustomer();
                                monitored.id = source.Value.id;
                                monitored.code = source.Value.code;
                                monitored.name = source.Value.name;
                                monitored.address = source.Value.address;
                                monitored.id_monitoring = source.Value.id_monitoring;
                                listMonitoredCustomers.Add(monitored.id, monitored);
                            }
                        }
                    }
                    if (listMonitoredCustomers.Count > 0)
                    {
                        Log("Perubahan terdeteksi pada " + listMonitoredCustomers.Count.ToString() + " data pelanggan.");
                    }
                    else
                    {
                        Log("Belum ada perubahan yang terdeteksi pada data pelanggan.");
                    }
                }

                //Log("Monitor count : " + listMonitored.Count.ToString());


                if (isSyncStopped()) return;
                if (profile.woo_on && !profile.woo_url.Equals("") && allow_woocommerce)
                {

                    var permalink = "";
                    if (profile.woo_permalink == 1)
                    {
                        permalink = "/?rest_route=";
                    }
                    else
                    {
                        permalink = "/wp-json";
                    }

                    if (cache_woo.list_id.Count == 0 || !cache_woo.lastUrl.Equals(profile.woo_url) || cache_woo.list_queue.Count > 0)
                    {
                        var idx2 = 0;
                        var count2 = 0;
                        var countFound = 0;
                        var countAll = cache_woo.list_queue.Count;
                        Log("Mengambil ID produk dari WooCommerce sesuai dengan SKU...");
                        //setServicePoint();
                        client = new RestClient(profile.woo_url);


                        var woourl = permalink + "/custom-routes/v1/getallproduct";
                        request = new RestRequest(woourl, Method.POST);
                        request.AddParameter("secret", "iposXwoocommerce");

                        response = client.Execute(request);
                        content = response.Content; // raw content as string
                        if (profile.main_dev.Equals(Program.devPassword))
                        {
                            Log(woourl);
                            Log("Respons getallproduct : " + content);
                        }
                        obj = SimpleJSON.JSON.Parse(content);
                        if (isSyncStopped()) return;
                        if (obj != null)
                        {
                            if (!obj["code"].Equals(null))
                            {
                                //Log(content);
                                return;
                            }
                            else
                            {
                                var arr = obj.AsArray;
                                List<string> deletedQueue = new List<string>();
                                if (arr.Count > 0)
                                {
                                    foreach (JSONNode item in arr)
                                    {
                                        var s = item["sku"].ToString().Replace("\"", "");
                                        deletedQueue.Add(s);
                                        //Log("checking : " + s);
                                        if (cache_woo.list_sku.ContainsKey(s))
                                        {
                                            var oid = cache_woo.list_sku[s];
                                            //Log("oid : " + oid);
                                            if (!cache_woo.list_id.ContainsKey(oid))
                                            {
                                                cache_woo.list_id.Add(oid, item["id"].ToString().Replace("\"", ""));
                                                countFound++;
                                            }
                                            else
                                            {
                                                cache_woo.list_id[oid] = item["id"].ToString().Replace("\"", "");
                                            }
                                        }
                                        if (isSyncStopped()) return;
                                    }

                                    var keys = cache_woo.list_queue.Keys.ToArray();
                                    foreach (string key in keys)
                                    {
                                        foreach (string item in deletedQueue)
                                        {
                                            if (item.ToUpper().Equals(key.ToUpper()))
                                            {
                                                cache_woo.list_queue.Remove(key);
                                                break;
                                            }
                                        }

                                        if (isSyncStopped()) return;
                                    }
                                }
                            }
                        }

                        if (isSyncStopped()) return;
                        //sync to draft
                        if (cache_woo.list_queue.Count > 0 && profile.woo_to_draft)
                        {
                            Log("Terdapat produk yang akan dimasukan ke draft.");
                            String[] skus = new string[(cache_woo.list_queue.Count / 500) + 1];
                            foreach (var data in cache_woo.list_queue)
                            {
                                if (skus[idx2] == null)
                                {
                                    skus[idx2] = data.Value;
                                }
                                else
                                {
                                    skus[idx2] = skus[idx2] + "," + data.Value;
                                }
                                count2++;
                                if (count2 % 500 == 0)
                                {
                                    idx2++;
                                }

                                if (isSyncStopped()) return;
                            }

                            var no = 1;
                            foreach (string sku in skus)
                            {
                                Log("Memasukan produk draft " + no + "/" + skus.LongLength);
                                request = new RestRequest(permalink + "/custom-routes/v1/inserttodraft", Method.POST);
                                request.AddParameter("secret", "iposXwoocommerce");
                                request.AddParameter("data", sku);

                                response = client.Execute(request);
                                content = response.Content; // raw content as string
                                if (profile.Equals(Program.devPassword))
                                {
                                    Log("Respons permintaan : " + content);
                                }
                                obj = SimpleJSON.JSON.Parse(content);


                                request = null;
                                response = null;

                                if (obj != null)
                                {
                                    if (!obj["code"].Equals(null))
                                    {
                                        //Log(content);
                                        return;
                                    }
                                    else
                                    {
                                        var arr = obj.AsArray;
                                        List<string> deletedQueue = new List<string>();
                                        if (arr.Count > 0)
                                        {
                                            foreach (JSONNode item in arr)
                                            {
                                                countFound++;
                                                var s = item["sku"].ToString().Replace("\"", "");
                                                deletedQueue.Add(s);
                                                if (cache_woo.list_sku.ContainsKey(s))
                                                {
                                                    var oid = cache_woo.list_sku[s];
                                                    if (!cache_woo.list_id.ContainsKey(oid))
                                                    {
                                                        cache_woo.list_id.Add(oid, item["id"].ToString().Replace("\"", ""));
                                                    }
                                                    else
                                                    {
                                                        cache_woo.list_id[oid] = item["id"].ToString().Replace("\"", "");
                                                    }
                                                }

                                                if (isSyncStopped()) return;
                                            }

                                            foreach (string item in deletedQueue)
                                            {
                                                if (cache_woo.list_queue.ContainsKey(item))
                                                {
                                                    cache_woo.list_queue.Remove(item);
                                                }

                                                if (isSyncStopped()) return;
                                            }
                                        }
                                    }
                                }
                                no++;

                                if (isSyncStopped()) return;
                            }
                        }
                        //Log("ID yang ditemukan : " + countFound);
                        //Log("ID yang belum ditemukan : " + (countAll - countFound));
                        if (countFound > 0)
                        {
                            cache_woo.lastUrl = profile.woo_url;
                            cache_woo.lastupdate = DateTime.Now;
                            SaveWooCache();
                        }
                    }


                    if (isSyncStopped()) return;
                    Log("Membandingkan produk di WooCommerce & iPos...");
                    var idx = 0;
                    var count = 0;
                    foreach (System.Collections.Generic.KeyValuePair<string, SingleProduct> item in listSingleProduct)
                    {
                        if (!item.Value.active) continue;
                        if (cache_woo.list_id.ContainsKey(item.Value.id))
                        {
                            if (listMonitored.ContainsKey(item.Value.id))
                            {
                                var monitored = listMonitored[item.Value.id];
                                monitored.woocommerce_at = DateTime.Now;
                                listMonitored[item.Value.id] = monitored;
                            }

                            if (profile.main_dev.Equals(Program.devPassword))
                            {
                                //Log("Found " + item.Value.sku);
                            }

                            if (items.Count == 0)
                            {
                                items.Add(new JsonArray());
                            }

                            var id = cache_woo.list_id[item.Value.id];
                            JsonObject o = new JsonObject();
                            o.Add("id", id);

                            if (item.Value.weight > -1)
                            {
                                o.Add("weight", item.Value.weight);
                            }
                            if (item.Value.stock > -1)
                            {
                                o.Add("stock_quantity", item.Value.stock);
                            }


                            
                            //pakai kode item
                            if (profile.woo_price == 1 && item.Value.price1 > -1)
                            {
                                //hanya gunakan harga level 1
                                o.Add("regular_price", item.Value.price1);
                            }
                            else if (profile.woo_price == 2 && item.Value.price2 > -1)
                            {
                                //hanya gunakan harga level 2
                                o.Add("regular_price", item.Value.price2);
                            }
                            else if (profile.woo_price == 3 && item.Value.price3 > -1)
                            {
                                //hanya gunakan harga level 3
                                o.Add("regular_price", item.Value.price3);
                            }
                            else if (profile.woo_price == 4 && item.Value.price4 > -1)
                            {
                                //hanya gunakan harga level 4
                                o.Add("regular_price", item.Value.price4);
                            }
                            else
                            {
                                //gunakan role based plugin / tokopres

                                if (item.Value.price1 > -1)
                                {
                                    o.Add("regular_price", item.Value.price1);
                                }

                                var arr = new JsonArray();

                                if (!profile.woo_role1.Equals("") && item.Value.price1 > -1)
                                {
                                    JsonObject update = new JsonObject();
                                    if (profile.woo_price == 0)
                                    {
                                        //jika pakai role based

                                        update.Add("role", profile.woo_role1);
                                        update.Add("regular_price", item.Value.price1);
                                    }
                                    else if (profile.woo_price == 5)
                                    {
                                        //jika pakai tokopres

                                        if (profile.woo_role1.Equals("reseller"))
                                        {
                                            update.Add("reseller_price", item.Value.price1);
                                        }
                                        else if (profile.woo_role1.Equals("agen"))
                                        {
                                            update.Add("agent_price", item.Value.price1);
                                        }
                                        else if (profile.woo_role1.Equals("regular"))
                                        {
                                            update.Add("regular_price", item.Value.price1);
                                        }
                                        else if (profile.woo_role1.Equals("sale"))
                                        {
                                            update.Add("sale_price", item.Value.price1);
                                        }
                                    }
                                    arr.Add(update);
                                    update = null;
                                }

                                if (!profile.woo_role2.Equals("") && item.Value.price2 > -1)
                                {
                                    JsonObject update = new JsonObject();
                                    if (profile.woo_price == 0)
                                    {
                                        //jika pakai role based

                                        update.Add("role", profile.woo_role2);
                                        update.Add("regular_price", item.Value.price2);
                                    }
                                    else if (profile.woo_price == 5)
                                    {
                                        //jika pakai tokopres

                                        if (profile.woo_role2.Equals("reseller"))
                                        {
                                            update.Add("reseller_price", item.Value.price2);
                                        }
                                        else if (profile.woo_role2.Equals("agen"))
                                        {
                                            update.Add("agent_price", item.Value.price2);
                                        }
                                        else if (profile.woo_role2.Equals("regular"))
                                        {
                                            update.Add("regular_price", item.Value.price2);
                                        }
                                        else if (profile.woo_role2.Equals("sale"))
                                        {
                                            update.Add("sale_price", item.Value.price2);
                                        }
                                    }
                                    arr.Add(update);
                                    update = null;
                                }

                                if (!profile.woo_role3.Equals("") && item.Value.price3 > -1)
                                {
                                    JsonObject update = new JsonObject();
                                    if (profile.woo_price == 0)
                                    {
                                        //jika pakai role based

                                        update.Add("role", profile.woo_role3);
                                        update.Add("regular_price", item.Value.price3);
                                    }
                                    else if (profile.woo_price == 5)
                                    {
                                        //jika pakai tokopres

                                        if (profile.woo_role3.Equals("reseller"))
                                        {
                                            update.Add("reseller_price", item.Value.price3);
                                        }
                                        else if (profile.woo_role3.Equals("agen"))
                                        {
                                            update.Add("agent_price", item.Value.price3);
                                        }
                                        else if (profile.woo_role3.Equals("regular"))
                                        {
                                            update.Add("regular_price", item.Value.price3);
                                        }
                                        else if (profile.woo_role3.Equals("sale"))
                                        {
                                            update.Add("sale_price", item.Value.price3);
                                        }
                                    }
                                    arr.Add(update);
                                    update = null;
                                }

                                if (!profile.woo_role4.Equals("") && item.Value.price4 > -1)
                                {
                                    JsonObject update = new JsonObject();
                                    if (profile.woo_price == 0)
                                    {
                                        //jika pakai role based

                                        update.Add("role", profile.woo_role4);
                                        update.Add("regular_price", item.Value.price4);
                                    }
                                    else if (profile.woo_price == 5)
                                    {
                                        //jika pakai tokopres

                                        if (profile.woo_role4.Equals("reseller"))
                                        {
                                            update.Add("reseller_price", item.Value.price4);
                                        }
                                        else if (profile.woo_role4.Equals("agen"))
                                        {
                                            update.Add("agent_price", item.Value.price4);
                                        }
                                        else if (profile.woo_role4.Equals("regular"))
                                        {
                                            update.Add("regular_price", item.Value.price4);
                                        }
                                        else if (profile.woo_role4.Equals("sale"))
                                        {
                                            update.Add("sale_price", item.Value.price4);
                                        }
                                    }
                                    arr.Add(update);
                                    update = null;
                                }

                                o.Add("roles", arr);
                            }

                            o.Add("sku", item.Value.sku);
                            items[idx].Add(o);
                            if (items[idx].Count == 500)
                            {
                                idx++;
                                items.Add(new JsonArray());
                            }
                            count++;
                        }

                        if (isSyncStopped()) return;
                    }

                    if (profile.main_dev.Equals(Program.devPassword))
                    {
                        Log(items.ToString());
                    }

                    if (items.Count > 0 && items[items.Count - 1].Count == 0)
                    {
                        items.RemoveAt(items.Count - 1);
                    }


                    if (isSyncStopped()) return;
                    if (items.Count > 0)
                    {
                        Log("Terdapat " + count + " produk iPos di WooCommerce.");

                        foreach (JsonArray item in items)
                        {
                            if (isSyncStopped()) return;
                            Log("Sinkronisasi " + item.Count.ToString() + " produk ke WooCommerce...");

                            try
                            {
                                var woourl = permalink + "/custom-routes/v1/syncfromipos";
                                if(profile.woo_rest_method == 0)
                                {
                                    url = profile.woo_url
                                       .AppendPathSegment(permalink + "/custom-routes/v1/syncfromipos");
                                    content = await url
                                    .PostUrlEncodedAsync(new
                                    {
                                        secret = "iposXwoocommerce",
                                        type = profile.woo_price,
                                        data = item.ToString(),
                                    }).ReceiveString().ConfigureAwait(false);
                                }
                                else if(profile.woo_rest_method == 1)
                                {
                                    request = new RestRequest(permalink + "/custom-routes/v1/syncfromipos", Method.POST);
                                    request.AddParameter("secret", "iposXwoocommerce");
                                    request.AddParameter("type", profile.woo_price);
                                    request.AddParameter("data", item.ToString());
                                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                                    client = new RestClient(profile.woo_url);
                                    response = client.Execute(request);
                                    content = response.Content; // raw content as string
                                    if (profile.main_dev.Equals(Program.devPassword))
                                    {
                                        Log(response.ErrorMessage);
                                    }
                                }
                                
                            }
                            catch (Exception e)
                            {
                                content = e.InnerException+" : "+e.Message;
                            }

                            if (profile.main_dev.Equals(Program.devPassword))
                            {
                                Log("Respons sinkronisasi wp : " + content);
                               
                            }


                            obj = SimpleJSON.JSON.Parse(content);


                            request = null;
                            response = null;

                            if (obj != null)
                            {
                                if (!obj["code"].Equals(null))
                                {
                                    //Log(content);
                                    return;
                                }
                                else
                                {
                                    var arr = obj["changed"].AsArray;
                                    if (arr.Count > 0)
                                    {
                                        foreach (JSONNode i in arr)
                                        {
                                            if (cache_woo.list_sku.ContainsKey(i.ToString().Replace("\"", "")))
                                            {
                                                var oid = cache_woo.list_sku[i.ToString().Replace("\"", "")];
                                                if (cache_woo.list_id.ContainsKey(oid))
                                                {
                                                    cache_woo.list_id.Remove(oid);
                                                }
                                               
                                               
                                            }
                                            if (isSyncStopped()) return;
                                        }
                                        Log(arr.Count + " produk sudah tidak ditemukan and akan diseleksi pada sinkronisasi selanjutnya.");
                                        SaveWooCache();
                                    }
                                }
                            }

                            if (isSyncStopped()) return;
                        }
                        Log(count + " produk telah disinkronisasi.");
                    }
                    else
                    {
                        Log("Tidak ada produk iPos yang sesuai dengan produk di WooCommerce.");
                    }



                    if (isSyncStopped()) return;
                    //ambil pesanan
                    Log("Mengambil data pesanan dari Woocommerce...");
                    try
                    {
                        url = profile.woo_url
                                .AppendPathSegment(permalink + "/custom-routes/v1/getshoporders");
                        content = await url
                        .PostUrlEncodedAsync(new
                        {
                            secret = "iposXwoocommerce",
                        }).ReceiveString().ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                        content = e.Message;
                    }

                    if (profile.Equals(Program.devPassword))
                    {
                        Log("Respons : " + content);
                    }
                    obj = SimpleJSON.JSON.Parse(content);
                }

                
                if (isSyncStopped()) return;
                if (profile.tokped_on && instance_tokped != null && instance_tokped.isLoggedIn && allow_tokopedia)
                {
                    instance_tokped.Sync(listSingleProduct, getAllProduct(), isNewItems || syncProductFilter.Count > 0);
                }
                
                if (isSyncStopped()) return;
                if (profile.shopee_on && instance_shopee != null && instance_shopee.isLoggedIn && allow_shopee)
                {
                    instance_shopee.Sync(listSingleProduct, getAllProduct(), syncProductFilter);
                }

                if (isSyncStopped()) return;
                if (profile.bukalapak_on && instance_bukalapak != null && instance_bukalapak.isLoggedIn && allow_bukalapak)
                {
                    instance_bukalapak.Sync(listSingleProduct);
                }

                if (isSyncStopped()) return;
                if (profile.lazada_on && instance_lazada != null && instance_lazada.isLoggedIn && allow_lazada)
                {
                    instance_lazada.Sync(listSingleProduct);
                }

                if (isSyncStopped()) return;
                if (profile.blibli_on && instance_blibli != null && instance_blibli.isLoggedIn && allow_blibli)
                {
                    instance_blibli.Sync(listSingleProduct);
                }

                while (
                    (profile.tokped_on && instance_tokped != null && instance_tokped.isLoggedIn && allow_tokopedia && !instance_tokped.isSyncDone) || 
                    (profile.shopee_on && instance_shopee != null && instance_shopee.isLoggedIn && allow_shopee && !instance_shopee.isSyncDone) ||
                    (profile.bukalapak_on && instance_bukalapak != null && instance_bukalapak.isLoggedIn && allow_bukalapak && !instance_bukalapak.isSyncDone) ||
                    (profile.lazada_on && instance_lazada != null && instance_lazada.isLoggedIn && allow_lazada && !instance_lazada.isSyncDone) ||
                    (profile.blibli_on && instance_blibli != null && instance_blibli.isLoggedIn && allow_blibli && !instance_blibli.isSyncDone))
                {
                    Console.WriteLine("Waiting for Market process");
                    if (command.Equals("skip"))
                    {
                        Log("Melewati proses Marketplace secara paksa...");
                        command = "";
                        break;
                    }
                    await Task.Delay(1).ConfigureAwait(false);
                    if (isSyncStopped()) return;
                }

                //mengambil data id marketplace untuk kepentingan monitoring
                if (listMonitored.Count > 0)
                {
                    foreach (var item in listMonitored)
                    {
                        if (instance_tokped != null)
                        {
                            if (instance_tokped.cache != null && instance_tokped.cache.marketID != null && instance_tokped.cache.marketID.ContainsKey(item.Value.sku))
                            {
                                item.Value.tokopedia_id = instance_tokped.cache.marketID[item.Value.sku];
                                if (item.Value.tokopedia_status == -1)
                                {
                                    item.Value.tokopedia_status = 1;
                                }
                            }
                            else
                            {
                                item.Value.tokopedia_id = "";
                            }
                        }
                        if (instance_shopee != null)
                        {
                            if (instance_shopee.cache != null && instance_shopee.cache.marketID != null && instance_shopee.cache.marketID.ContainsKey(item.Value.sku))
                            {
                                item.Value.shopee_id = instance_shopee.cache.marketID[item.Value.sku];
                                if (item.Value.shopee_status == -1)
                                {
                                    item.Value.shopee_status = 1;
                                }
                            }
                            else
                            {
                                item.Value.shopee_id = "";
                            }
                        }
                        if (instance_bukalapak != null)
                        {
                            if (instance_bukalapak.cache != null && instance_bukalapak.cache.marketID != null && instance_bukalapak.cache.marketID.ContainsKey(item.Value.sku))
                            {
                                item.Value.bukalapak_id = instance_bukalapak.cache.marketID[item.Value.sku];
                                if (item.Value.bukalapak_status == -1)
                                {
                                    item.Value.bukalapak_status = 1;
                                }
                            }
                            else
                            {
                                item.Value.bukalapak_id = "";
                            }
                        }
                        if (instance_lazada != null)
                        {
                            if (instance_lazada.cache != null && instance_lazada.cache.marketID != null && instance_lazada.cache.marketID.ContainsKey(item.Value.sku))
                            {
                                item.Value.lazada_id = instance_lazada.cache.marketID[item.Value.sku];
                                if (item.Value.lazada_status == -1)
                                {
                                    item.Value.lazada_status = 1;
                                }
                            }
                            else
                            {
                                item.Value.lazada_id = "";
                            }
                        }
                        if (instance_blibli != null)
                        {
                            if (instance_blibli.cache != null && instance_blibli.cache.marketID != null && instance_blibli.cache.marketID.ContainsKey(item.Value.sku))
                            {
                                item.Value.blibli_id = instance_blibli.cache.marketID[item.Value.sku];
                                if (item.Value.blibli_status == -1)
                                {
                                    item.Value.blibli_status = 1;
                                }
                            }
                            else
                            {
                                item.Value.blibli_id = "";
                            }
                        }
                    }
                }

                if (isSyncStopped()) return;
                if (allow_monitoring && profile.main_send_product)
                {
                    if (deletedProducts != "")
                    {
                        //setServicePoint();
                        Log("Menghapus data produk dari situs monitoring..");

                        try
                        {
                            url = Program.config.monitorURL
                                    .AppendPathSegment("/api/product_sync");
                            resp = await url
                            .PostUrlEncodedAsync(new
                            {
                                id_branch = Program.config.branchID,
                                id_profile = profile.id,
                                deleted_data = deletedProducts
                            }).ReceiveString().ConfigureAwait(false);
                        }
                        catch (Exception e)
                        {
                            resp = e.Message;
                        }

                        if (profile.Equals(Program.devPassword))
                        {
                            Log("Respons penghapusan produk : " + resp);
                        }

                        obj = SimpleJSON.JSON.Parse(resp);
                        if (obj != null)
                        {
                            if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                            {
                                Log("Permintaan sukses..");
                            }
                            else
                            {
                                Log(resp);
                            }
                        }
                        else
                        {
                            Log(resp);
                        }
                    }

                    if (listMonitored.Count > 0)
                    {
                        var data = "";
                        var no = 1;
                        var limit = 1000;

                        //setServicePoint();
                        if (Program.config.branchID != "")
                        {
                            client = new RestClient(Program.config.monitorURL);
                            Log("Mengirim data produk ke situs monitoring...");
                            foreach (var product in listMonitored)
                            {
                                if (!data.Equals(""))
                                {

                                    data += "#R#";
                                }

                                var pdt = "NULL";
                                if (cache_woo.list_id.ContainsKey(product.Value.id))
                                {
                                    var wooid = cache_woo.list_id[product.Value.id];
                                    if (listWooPublish.ContainsKey(wooid))
                                    {
                                        var splitted = listWooPublish[wooid].Split('|');
                                        pdt = product.Value.woocommerce_publish_at + "|pdt|" + splitted[1] + "|pdt|" + splitted[2];
                                    }
                                }

                                var dt = product.Value.woocommerce_at.HasValue ? product.Value.woocommerce_at.Value.ToString("yyyy-MM-dd HH:mm:ss") : "NULL";
                                var p = product.Value.id + "|C|" +
                                       Program.config.branchID + "|C|" +
                                       profile.id + "|C|" +
                                       dt + "|C|" +
                                       Program.Base64Encode(product.Value.sku) + "|C|" +
                                       Program.Base64Encode(product.Value.name) + "|C|" +
                                       product.Value.prices + "|C|" +
                                       product.Value.stock + "|C|" +
                                       product.Value.stockdetail + "|C|" +
                                       (product.Value.weight == -1 || product.Value.weight.Equals("") ? 0 : product.Value.weight) + "|C|" +
                                       "?" + Program.Base64Encode(product.Value.description) + "|C|" + //tambah ? biar ngk dianggap kosong sama explode
                                       pdt + "|C|" +
                                       product.Value.tokopedia_sync + "|C|" +
                                       product.Value.tokopedia_id + "|C|" +
                                       product.Value.tokopedia_price + "|C|" +
                                       product.Value.tokopedia_stock + "|C|" +
                                       product.Value.tokopedia_status + "|C|" +
                                       product.Value.shopee_sync + "|C|" +
                                       product.Value.shopee_id + "|C|" +
                                       product.Value.shopee_price + "|C|" +
                                       product.Value.shopee_stock + "|C|" +
                                       product.Value.shopee_status + "|C|" +
                                       product.Value.bukalapak_sync + "|C|" +
                                       product.Value.bukalapak_id + "|C|" +
                                       product.Value.bukalapak_price + "|C|" +
                                       product.Value.bukalapak_stock + "|C|" +
                                       product.Value.bukalapak_status + "|C|" +
                                       product.Value.lazada_sync + "|C|" +
                                       product.Value.lazada_id + "|C|" +
                                       product.Value.lazada_price + "|C|" +
                                       product.Value.lazada_stock + "|C|" +
                                       product.Value.lazada_status + "|C|" +
                                       product.Value.blibli_sync + "|C|" +
                                       product.Value.blibli_id + "|C|" +
                                       product.Value.blibli_price + "|C|" +
                                       product.Value.blibli_stock + "|C|" +
                                       product.Value.blibli_status + "|C|" +
                                       product.Value.unit;

                                //Log(p);

                                data = data + p;


                                if (no % limit == 0 || no >= listMonitored.Count)
                                {

                                    Log("Mengirim data produk " + no + " / " + listMonitored.Count);

                                    try
                                    {
                                        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                        // | SecurityProtocolType.Tls11
                                        // | SecurityProtocolType.Tls12;
                                        url = Program.config.monitorURL
                                        .AppendPathSegment("/api/product_sync");
                                        File.WriteAllText(monitoring_file, data);
                                        resp = await url
                                            .PostMultipartAsync((mp) =>
                                            {
                                                mp.AddString("id_branch", Program.config.branchID)
                                                .AddString("id_profile", profile.id.ToString())
                                                .AddFile("new_data", monitoring_file);
                                            }).ReceiveString().ConfigureAwait(false);
                                    }
                                    catch (Exception e)
                                    {
                                        resp = e.InnerException + " | " + e.Message;
                                    }

                                    if (profile.Equals(Program.devPassword))
                                    {
                                        Log("Respons monitoring produk : " + resp);
                                    }

                                    obj = SimpleJSON.JSON.Parse(resp);
                                    if (obj != null)
                                    {
                                        if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                                        {
                                            Log("Permintaan sukses..");
                                        }
                                        else
                                        {
                                            Log(resp);
                                        }
                                    }
                                    else
                                    {
                                        Log(resp);
                                    }

                                    data = "";
                                }
                                no++;

                                if (isSyncStopped()) return;
                            }
                        }
                        else
                        {
                            Log("Gagal mengirim ke situs monitoring karena branch tidak diketahui.",Color.Red);
                        }
                    }
                }


                if (isSyncStopped()) return;
                if (profile.main_send_customer && deletedCustomers != "")
                {
                    Log("Menghapus data pelanggan dari situs monitoring..");
                    try {
                        url = Program.config.monitorURL
                                .AppendPathSegment("/api/customer_sync");
                        resp = await url
                        .PostUrlEncodedAsync(new
                        {
                            id_branch = Program.config.branchID,
                            id_profile = profile.id,
                            deleted_data = deletedCustomers
                        }).ReceiveString().ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                        resp = e.Message;
                    }

                    if (profile.main_dev.Equals(Program.devPassword))
                    {
                        Log("Respons penghapusan pelanggan : " + resp);
                    }
                    obj = SimpleJSON.JSON.Parse(resp);
                    if (obj != null)
                    {
                        if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                        {
                            Log("Permintaan sukses..");
                        }
                        else
                        {
                            Log(resp);
                        }
                    }
                    else
                    {
                        Log(resp);
                    }
                }


                if (isSyncStopped()) return;
                if (profile.main_send_customer && listMonitoredCustomers.Count > 0)
                {
                    var data = "";
                    var no = 1;
                    var limit = 1000;

                    //setServicePoint();
                    client = new RestClient(Program.config.monitorURL);
                    Log("Mengirim data pelanggan ke situs monitoring...");
                    foreach (var customer in listMonitoredCustomers)
                    {
                        if (!data.Equals(""))
                        {

                            data += "#R#";
                        }

                        var c = customer.Value.id + "|C|" +
                               Program.config.branchID + "|C|" +
                               profile.id + "|C|" +
                               Program.Base64Encode(customer.Value.code) + "|C|" +
                               Program.Base64Encode(customer.Value.name) + "|C|" +
                               Program.Base64Encode(customer.Value.address) + "|C|" +
                               customer.Value.id_monitoring.ToString();

                        data = data + c;


                        if (no % limit == 0 || no >= listMonitoredCustomers.Count)
                        {
                            Log("Mengirim data pelanggan " + no + " / " + listMonitoredCustomers.Count);
                            try {
                                url = Program.config.monitorURL
                                .AppendPathSegment("/api/customer_sync");
                                File.WriteAllText(monitoring_cust_file, data);
                                resp = await url
                                    .PostMultipartAsync((mp) =>
                                    {
                                        mp.AddString("id_branch", Program.config.branchID)
                                        .AddString("id_profile", profile.id.ToString())
                                        .AddFile("new_data", monitoring_cust_file);
                                    }).ReceiveString().ConfigureAwait(false);
                            }
                            catch (Exception e)
                            {
                                resp = e.Message;
                            }

                            if (profile.main_dev.Equals(Program.devPassword))
                            {
                                Log("Respons monitoring pelanggan : " + resp);
                            }

                            obj = SimpleJSON.JSON.Parse(resp);
                            if (obj != null)
                            {
                                if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                                {
                                    Log("Permintaan sukses..");
                                }
                                else
                                {
                                    Log(resp);
                                }
                            }
                            else
                            {
                                Log(resp);
                            }
                            data = "";
                        }
                        no++;
                    }
                }


                if (isSyncStopped()) return;
                if (profile.main_get_customer)
                {
                    if (instance_ipos != null)
                    {
                        await instance_ipos.GetCustomer().ConfigureAwait(false);
                    }
                }


                if (isSyncStopped()) return;
                if (profile.main_get_sales)
                {
                    if (instance_ipos != null)
                    {
                        await instance_ipos.GetSales().ConfigureAwait(false);
                    }
                }

                cache_product.lastupdate = DateTime.Now;
                SaveProductCache();
                cache_customers.lastupdate = DateTime.Now;
                SaveCustomerCache();
                cache_sales.lastupdate = DateTime.Now;
                SaveSalesCache();
                SaveWooCache();


                Log("Proses memakan waktu " + (Program.watch.ElapsedMilliseconds - startSecs).ToString() + " ms");
                Log("========== SINKRONISASI SELESAI ==========");

                listProduct.Clear();
                listSingleProduct.Clear();
                listCustomer.Clear();
                listSales.Clear();
                listMonitoredCustomers.Clear();
                listMonitoredSales.Clear();
                listWooPublish.Clear();
                syncProductFilter.Clear();
                items = null;
                request = null;
                client = null;
                obj = null;
                response = null;
                content = null;
                url = null;
                resp = null;

                allow_monitoring = true;
                allow_shopee = true;
                allow_tokopedia = true;
                allow_woocommerce = true;
                allow_bukalapak = true;
                allow_lazada = true;
                allow_blibli = true;
            }
            catch (Exception e)
            {
                Log(e.Message);
                Log(e.StackTrace);
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Log(line.ToString());
            }
            if (isDelay)
            {
                Log("Jeda selama " + txtInterval.Value + " detik..");
                isTimer = true;
                elapsedSecs = 0;
            }
        }

        public void AppendLog(string text, Color color, bool addNewLine = false)
        {
            txtLog.SuspendLayout();
            txtLog.SelectionColor = color;
            txtLog.AppendText(addNewLine
                ? $"{text}{Environment.NewLine}"
                : text);
            txtLog.ScrollToCaret();
            txtLog.ResumeLayout();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (logQueue.Count > 0)
            {
                if(txtLog.Lines.Length > 1000)
                {
                    txtLog.Clear();
                }
                while(logQueue.Count > 0)
                {
                    if (logQueue[0] != null)
                    {
                        AppendLog(logQueue[0].Item1, logQueue[0].Item2, true);
                    }
                    logQueue.RemoveAt(0);
                }
            }


            if (instance_tokped != null && instance_tokped.isLoggedIn && instance_tokped.logControl == null)
            {
                instance_tokped.initLog();
            }
            if (instance_shopee != null && instance_shopee.isLoggedIn && instance_shopee.logControl == null)
            {
                instance_shopee.initLog();
            }
            if (instance_bukalapak != null && instance_bukalapak.isLoggedIn && instance_bukalapak.logControl == null)
            {
                instance_bukalapak.initLog();
            }
            if (instance_lazada != null && instance_lazada.isLoggedIn && instance_lazada.logControl == null)
            {
                instance_lazada.initLog();
            }
            if (instance_blibli != null && instance_blibli.isLoggedIn && instance_blibli.logControl == null)
            {
                instance_blibli.initLog();
            }

            if (isTimer && isDelay)
            {
                elapsedSecs++;
                if (elapsedSecs % profile.interval == 0)
                {
                    isTimer = false;
                    BeginMainSync();
                }
            }
        }

        void SaveFrontConfig()
        {
            profile = Program.config.profiles[idxProfile];
            profile.warehouse = "";
            foreach (var item in cbWarehouse.CheckBoxItems)
            {
                if (item.Checked)
                {
                    if(profile.warehouse == "")
                    {
                        profile.warehouse = "'"+item.Text+"'";
                    }
                    else
                    {
                        profile.warehouse = profile.warehouse + "," + "'" + item.Text + "'";
                    }
                }
            }
            profile.interval = (int)txtInterval.Value;
            Program.SaveConfig();
        }

        private void btnToogleTimer_Click(object sender, EventArgs e)
        {
            profile = Program.config.profiles[idxProfile];
            SaveFrontConfig();
            if (btnToogleTimer.Tag.Equals("0"))
            {
                btnToogleTimer.Tag = "1";
                btnToogleTimer.Text = "Matikan Sinkron";
                cbWarehouse.Enabled = false;
                txtInterval.Enabled = false;
                isDelay = true;
                btnConfig.Enabled = false;
                btnProducts.Enabled = false;
                BeginMainSync();
                btnSyncMore.Enabled = false;
            }
            else
            {
                if (MarketInitialization != null && MarketInitialization.IsCompleted)
                {
                    if (bWorker.IsBusy || (SyncWorker != null && !SyncWorker.IsCompleted))
                    {
                        if (MessageBox.Show("Menghentikan sinkronisasi ditengah proses akan menyebabkan data tidak sepenuhnya terbaca / tidak lengkap. Lanjutkan?", "Peringatan", MessageBoxButtons.YesNo) == DialogResult.No) return;
                    }
                    btnToogleTimer.Tag = "0";
                    btnToogleTimer.Text = "Mulai Sinkron";
                    isDelay = false;
                    isTimer = false;
                    isSynchronizing = false;
                    cbWarehouse.Enabled = true;
                    txtInterval.Enabled = true;
                    btnConfig.Enabled = true;
                    btnProducts.Enabled = true;
                    bWorker.CancelAsync();
                    btnSyncMore.Enabled = true;
                    Log("========= SINKRONISASI DIHENTIKAN ========");
                }
                else
                {
                    MessageBox.Show("Silahkan tunggu sampai proses di log selesai.");
                }
            }
            try
            {
                var url = Program.config.monitorURL
                        .AppendPathSegment("/api/update_profile_status");
                var resp = url
                .PostUrlEncodedAsync(new
                {
                    id_branch = Program.config.branchID,
                    id_profile = profile.id,
                    sync_status = btnToogleTimer.Tag
                }).ReceiveString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            if (isNoRunningProcess())
            {
                profileHash = profile.tokped_email.GetHashCode() +
                              profile.tokped_password.GetHashCode() +
                              profile.tokped_on.GetHashCode() +
                              profile.shopee_email.GetHashCode() +
                              profile.shopee_password.GetHashCode() +
                              profile.shopee_on.GetHashCode() +
                              profile.bukalapak_email.GetHashCode() +
                              profile.bukalapak_password.GetHashCode() +
                              profile.bukalapak_on.GetHashCode() +
                              profile.lazada_email.GetHashCode() +
                              profile.lazada_password.GetHashCode() +
                              profile.lazada_on.GetHashCode() +
                              profile.blibli_email.GetHashCode() +
                              profile.blibli_password.GetHashCode() +
                              profile.blibli_on.GetHashCode();
                var cfg = new frmProfileConfig(idxProfile);
                if (cfg.ShowDialog() == DialogResult.OK)
                {
                    if (Program.module != Program.Appmodule.Ipos || instance_ipos.ConnectDB())
                    {
                        initConfig();
                    }
                }
            }
            else
            {
                MessageBox.Show("Silahkan tunggu sampai proses di log selesai.");
            }
        }
        private void frmMain_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                
            }
        }

        private void log_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            LoadWooCache();
            LoadProductCache();
            LoadCustomerCache();
            LoadSalesCache();
        }

        private void cbWarehouse_SelectedValueChanged(object sender, EventArgs e)
        {
          
        }


        public bool isForceClosing = false;
        public void frmProfileSync_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveFrontConfig();
            if(!isForceClosing)
            {
                e.Cancel = true;
                Hide();
            }
            else
            {
                stopMarketplaceBrowser();
            }
        }

        public void stopMarketplaceBrowser()
        {
            if (instance_shopee != null)
            {
                instance_shopee.stop();
            }
            if (instance_tokped != null)
            {
                instance_tokped.stop();
            }
            if (instance_bukalapak != null)
            {
                instance_bukalapak.stop();
            }
            if (instance_lazada != null)
            {
                instance_lazada.stop();
            }
            if (instance_blibli != null)
            {
                instance_blibli.stop();
            }
        }

        private void frmProfileSync_FormClosed(object sender, FormClosedEventArgs e)
        {
            profile = Program.config.profiles[idxProfile];
            try
            {
                var url = Program.config.monitorURL
                        .AppendPathSegment("/api/update_profile_status");
                var resp = url
                .PostUrlEncodedAsync(new
                {
                    id_branch = Program.config.branchID,
                    id_profile = profile.id,
                    sync_status = 0
                }).ReceiveString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbWarehouse_SelectionChangeCommitted(object sender, EventArgs e)
        {
       
        }

        private void cbWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        async Task ConfigureMarketplace(string markets = "", Action callback = null)
        {
            profile = Program.config.profiles[idxProfile];
      
            try
            {
                if (markets.Contains("tokopedia") || markets.Equals(""))
                {
                    if (profile.tokped_on)
                    {
                        Log("Konfigurasi Tokopedia, harap menunggu sampai selesai...");
                        if (instance_tokped != null)
                        {
                            instance_tokped.stop();
                        }
                        instance_tokped = new TokopediaCore(idxProfile);
                        await instance_tokped.LoginAsync().ConfigureAwait(false);
                    }
                    else if (!markets.Equals(""))
                    {
                        Log("Marketplace Tokopedia belum diaktifkan.");
                        return;
                    }
                }

                if (markets.Contains("shopee") || markets.Equals(""))
                {
                    if (profile.shopee_on)
                    {
                        Log("Konfigurasi Shopee, harap menunggu sampai selesai...");
                        if (instance_shopee != null)
                        {
                            instance_shopee.stop();
                        }
                        instance_shopee = new ShopeeCore(idxProfile);
                        await instance_shopee.LoginAsync().ConfigureAwait(false);
                    }
                    else if (!markets.Equals(""))
                    {
                        Log("Marketplace Shopee belum diaktifkan.");
                        return;
                    }
                }

                if (markets.Contains("bukalapak") || markets.Equals("")) 
                { 
                    if (profile.bukalapak_on)
                    {
                        Log("Konfigurasi Bukalapak, harap menunggu sampai selesai...");
                        if (instance_bukalapak != null)
                        {
                            instance_bukalapak.stop();
                        }
                        instance_bukalapak = new BukalapakCore(idxProfile);
                        await instance_bukalapak.LoginAsync().ConfigureAwait(false);
                    }
                    else if (!markets.Equals(""))
                    {
                        Log("Marketplace Bukalapak belum diaktifkan.");
                        return;
                    }
                }

                if (markets.Contains("lazada") || markets.Equals(""))
                {
                    if (profile.lazada_on)
                    {
                        Log("Konfigurasi Lazada, harap menunggu sampai selesai...");
                        if (instance_lazada != null)
                        {
                            instance_lazada.stop();
                        }
                        instance_lazada = new LazadaCore(idxProfile);
                        await instance_lazada.LoginAsync().ConfigureAwait(false);
                    }
                    else if (!markets.Equals(""))
                    {
                        Log("Marketplace Lazada belum diaktifkan.");
                        return;
                    }
                }

                if (markets.Contains("blibli") || markets.Equals(""))
                {
                    if (profile.blibli_on)
                    {
                        Log("Konfigurasi blibli, harap menunggu sampai selesai...");
                        if (instance_blibli != null)
                        {
                            instance_blibli.stop();
                        }
                        instance_blibli = new BlibliCore(idxProfile);
                        await instance_blibli.LoginAsync().ConfigureAwait(false);
                    }
                    else if (!markets.Equals(""))
                    {
                        Log("Marketplace blibli belum diaktifkan.");
                        return;
                    }
                }
                if (callback != null) callback();
            }
            catch (Exception e)
            {
                Program.WriteDevLog("main", e.Message, e.StackTrace);
                Log(e.Message);
            }
        }

        private void resetMarketplaceToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

  

        private void txtCommand_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                command = txtCommand.Text;
                txtCommand.Clear();
                Log(command, Color.SkyBlue);

                var param = command.Split(' ');
                if (param[0].Equals("restart"))
                {
                    if (param.Length > 1)
                    {
                        if (MarketInitialization != null && !MarketInitialization.IsCompleted)
                        {
                            Log("Silahkan tunggu sampai proses inisialisasi Marketplace selesai.");
                            return;
                        }

                        if (SyncWorker != null && !SyncWorker.IsCompleted)
                        {
                            Log("Silahkan tunggu sampai proses sinkronisasi Marketplace selesai.");
                            return;
                        }
                        restartMarketplaceModule(param[1]);
                    }
                }
            }
        }

        public void restartMarketplaceModule(string name)
        {
            DeleteMarketplaceTab(name);
            Task.Run(() => MarketInitialization = ConfigureMarketplace(name));
        }
            
        private void semuaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                var url = Program.config.monitorURL
                                    .AppendPathSegment("/api/product_sync");
                var resp = url
                    .PostMultipartAsync((mp) =>
                    {
                        mp.AddString("id_branch", Program.config.branchID)
                        .AddString("id_profile", "4")
                        .AddFile("new_data", "I:/Business/Comical Studio/Projects.co.id/sandigma/iPOS Stock Sync/iPOS Stock Sync/bin/Debug/monitoring_product_0.log");
                    }).ReceiveString();
                MessageBox.Show(resp.Result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException + " | " + ex.Message);
                throw;
            }
        }

        private void txtLog_KeyPress(object sender, KeyPressEventArgs e)
        {
            ActiveControl = txtCommand;
        }

        private void bMarketInit_DoWork(object sender, DoWorkEventArgs e)
        {
            Task.Run(() => MarketInitialization = ConfigureMarketplace());
        }

        private void bMarketInit_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            if(cache_product == null || cache_product.data.Count == 0)
            {
                MessageBox.Show("Silahkan jalankan sinkronisasi terlebih dahulu sebelum mengakses daftar produk.");
                return;
            }
            var frm = new frmProfileProduct(cache_product);
            var result = frm.ShowDialog();
            SaveProductCache();
        }

        public Boolean verifyCommand(string desired)
        {
            var result = desired.Equals(command);
            command = "";
            return result;
        }

        private void HandleMarketplaceSyncCache(object sender, EventArgs e)
        {
            if (!isNoRunningProcess())
            {
                MessageBox.Show("Silahkan tunggu sampai proses yang lain selesai.");
                return;
            }
            var tag = ((ucMarketplaceLog)sender).Tag.ToString().ToLower();
            if (MessageBox.Show("Data sinkronisasi "+tag+" akan disamakan statusnya dengan data produk sumber. Ini akan menyebabkan perubahan data baru saat ini tidak akan tersinkronisasi, lanjutkan?", "Peringatan", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (tag.Equals("tokopedia"))
                {
                    instance_tokped.SyncCache();
                    MessageBox.Show("Data sinkronisasi Tokopedia berhasil disamakan.");
                }
                else if (tag.Equals("bukalapak"))
                {
                    instance_bukalapak.SyncCache();
                    MessageBox.Show("Data sinkronisasi Bukalapak berhasil disamakan.");
                }
                else if (tag.Equals("shopee"))
                {
                    instance_shopee.SyncCache();
                    MessageBox.Show("Data sinkronisasi Shopee berhasil disamakan.");
                }
                else if (tag.Equals("lazada"))
                {
                    instance_lazada.SyncCache();
                    MessageBox.Show("Data sinkronisasi Lazada berhasil disamakan.");
                }
                else if (tag.Equals("blibli"))
                {
                    instance_blibli.SyncCache();
                    MessageBox.Show("Data sinkronisasi Blibli berhasil disamakan.");
                }
            }
        }

        void HandleSalesSyncMonitoring(object sender, EventArgs e)
        {
            if (!isNoRunningProcess())
            {
                MessageBox.Show("Silahkan tunggu sampai proses yang lain selesai.");
                return;
            }
            var tag = ((ucMarketplaceLog)sender).Tag.ToString().ToLower();
            if (MessageBox.Show("Data penjualan " + tag + " di monitoring akan disamakan dengan data penjualan terakhir, lanjutkan?", "Peringatan", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (tag.Equals("tokopedia"))
                {
                    instance_tokped.SyncSalesToMonitoring();
                }
                else if (tag.Equals("bukalapak"))
                {
                    MessageBox.Show("Bukalapak belum support pengambilan penjualan.");
                }
                else if (tag.Equals("shopee"))
                {
                    instance_shopee.SyncSalesToMonitoring();
                }
                else if (tag.Equals("lazada"))
                {
                    MessageBox.Show("Lazada belum support pengambilan penjualan.");
                }
                else if (tag.Equals("blibli"))
                {
                    MessageBox.Show("Blibli belum support pengambilan penjualan.");
                }
            }
        }

        void HandleSalesImportToMarketplace(object sender, EventArgs e)
        {
            var tag = ((ucMarketplaceLog)sender).Tag.ToString().ToLower();
            if (tag.Equals("tokopedia"))
            {
                odf.Filter = "Excel 2007 File|*.xlsx";
                odf.Multiselect = false;
                if (odf.ShowDialog() == DialogResult.OK)
                {
                    FileInfo excel = new FileInfo(odf.FileName);
                    instance_tokped.TriggerImportSalesToIPOS(excel);
                }
            }
            else if (tag.Equals("bukalapak"))
            {
                MessageBox.Show("Impor penjualan belum bisa digunakan untuk Bukalapak.");
            }
            else if (tag.Equals("shopee"))
            {
                MessageBox.Show("Impor penjualan belum bisa digunakan untuk Shopee.");
            }
            else if (tag.Equals("lazada"))
            {
                MessageBox.Show("Impor penjualan belum bisa digunakan untuk Lazada.");
            }
            else if (tag.Equals("blibli"))
            {
                MessageBox.Show("Impor penjualan belum bisa digunakan untuk Blibli.");
            }
        }

        public ucMarketplaceLog CreateMarketplaceTab(int imageIndex, string name)
        {
            TabPage page = new TabPage(name);
            var log = new ucMarketplaceLog();
            log.Tag = name;
            log.Dock = DockStyle.Fill;
            log.SyncCacheClick += HandleMarketplaceSyncCache;
            log.SyncSalesMonitoringClick += HandleSalesSyncMonitoring;
            log.ImportSalesClick += HandleSalesImportToMarketplace;
            page.Name = name;
            page.ImageIndex = imageIndex;
            page.Controls.Add(log);
            tcProcess.TabPages.Add(page);
            return log;
        }

        private void txtCommand_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            cmsSync.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        bool isNoRunningProcess()
        {
            return (MarketInitialization == null || MarketInitialization.IsCompleted) && (SyncWorker == null || SyncWorker.IsCompleted);
        }

        private void sinkronSpesifikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isNoRunningProcess()) 
            {
                var frm = new frmSpecifiedSync();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    allow_monitoring = frm.chbMonitoring.Checked;
                    allow_shopee = frm.chbShopee.Checked;
                    allow_tokopedia = frm.chbTokped.Checked;
                    allow_bukalapak = frm.chbBukalapak.Checked;
                    allow_lazada = frm.chbLazada.Checked;
                    allow_blibli = frm.chbBlibli.Checked;
                    allow_woocommerce = frm.chbWoo.Checked;
                    btnToogleTimer_Click(this, null);
                }
            }
            else
            {
                MessageBox.Show("Silahkan tunggu sampai proses yang lain selesai.");
            }
        }

        private void resetSinkronisasiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isNoRunningProcess())
            {
                var frm = new frmClearCache();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    if (MessageBox.Show("Apakah anda yakin ingin melakukan reset data? Seluruh proses sinkronisasi mungkin akan diulang dari 0 untuk memastikan seluruh data tersinkronisasi.", "Peringatan", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                    if (frm.chbProduct.Checked)
                    {
                        cache_product = new LastProductCache();
                        SaveProductCache();
                    }
                    if (frm.chbWoo.Checked)
                    {
                        cache_woo = new WooCache();
                        SaveWooCache();
                    }
                    if (frm.chbTokped.Checked)
                    {
                        if (instance_tokped != null)
                        {
                            instance_tokped.ResetCache();
                        }
                    }
                    if (frm.chbShopee.Checked)
                    {
                        if (instance_shopee != null)
                        {
                            instance_shopee.ResetCache();
                        }
                    }
                    if (frm.chbBukalapak.Checked)
                    {
                        if (instance_bukalapak != null)
                        {
                            instance_bukalapak.ResetCache();
                        }
                    }
                    if (frm.chbLazada.Checked)
                    {
                        if (instance_lazada != null)
                        {
                            instance_lazada.ResetCache();
                        }
                    }
                    if (frm.chbBlibli.Checked)
                    {
                        if (instance_blibli != null)
                        {
                            instance_blibli.ResetCache();
                        }
                    }
                    MessageBox.Show("Data sinkronisasi berhasil direset.");
                }
            }
            else
            {
                MessageBox.Show("Silahkan tunggu sampai proses yang lain selesai.");
            }
        }

        private void sinkronBerdasarkanProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input = "";
            Program.ShowInputDialog(ref input,"Input SKU Produk");
            if (!input.Equals(""))
            {
                syncProductFilter.Clear();
                var splitted = input.Split(',');
                foreach (var item in splitted)
                {
                    syncProductFilter.Add(item);
                }
                if(syncProductFilter.Count > 0)
                {
                    btnToogleTimer_Click(this, null);
                }
            }
            
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_4(object sender, EventArgs e)
        {
           
        }

        public void DeleteMarketplaceTab(string name)
        {
            for (int i = tcProcess.TabPages.Count-1; i >= 0; i--)
            {
                if (name.ToLower().Equals(tcProcess.TabPages[i].Text.ToLower()))
                {
                    tcProcess.TabPages.RemoveAt(i);
                }
            }
            
        }
    }
}
