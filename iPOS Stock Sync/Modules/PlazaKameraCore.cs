﻿using Flurl;
using Flurl.Http;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;

namespace iPOS_Stock_Sync.Modules
{
    public class PlazaKameraCore
    {
        int idxProfile;

        const string key_name = "PK-API-KEY";
        const string key_value = "oopip2::c4ks0kgkooo0s44ckgo8oswkk80swk0kgc48o0og";
        const string host_url = "https://search.plazakamera.com/index.php/ipos2woo/api";
        const string api_stock = "/stock";
        const string api_lastupdate = "/stock/lastupdate/";
        frmProfileSync mainWindow;

        public PlazaKameraCore(int idx)
        {
            idxProfile = idx;
            

            var config = Program.config.profiles[idxProfile];
            mainWindow = Program.Syncers[config.name];
        }

        public async Task Fetch(string[] product_blacklist)
        {
            var resp = "";
            var profile = Program.config.profiles[idxProfile];

            try
            {
                var url = host_url
                        .AppendPathSegment(api_stock).WithHeader(key_name,key_value);
                resp = await url.GetStringAsync();
            }
            catch (Exception e)
            {
                resp = e.Message;
            }

            //mainWindow.Log(api_stock + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace(" ", "%20"));
            var obj = SimpleJSON.JSON.Parse(resp);
            if (obj != null && obj.AsArray.Count > 0)
            {
                Product p = new Product();
                var count = 0;
                var activeProducts = 0;
                foreach (SimpleJSON.JSONNode item in obj.AsArray)
                {
                    if (!profile.warehouse.Contains("'" + Program.StripQuotes(item["gudang"]) + "'"))
                    {
                        continue;
                    }
                    if (!mainWindow.listProduct.ContainsKey(Program.StripQuotes(item["sku"])))
                    {
                        if(mainWindow.syncProductFilter.Count > 0)
                        {
                            if (!mainWindow.syncProductFilter.Contains(Program.StripQuotes(item["sku"])))
                            {
                                continue;
                            }
                        }
                        p = new Product();
                        //mainWindow.Log(Program.StripQuotes(item["sku"]));
                        p.id = Program.StripQuotes(item["sku"]);
                        //mainWindow.Log("id : "+p.id.ToString());
                        p.code = Program.StripQuotes(item["sku"]);
                        p.name = Program.StripQuotes(item["nama"]);

                        p.weight = -1;
                        p.description = "";
                        p.price_main = 0;

                        //mainWindow.Log("a"+Program.StripQuotes(item["harga"])+"a");
                        p.price1 = long.Parse(Program.StripQuotes(item["harga"]));
                        
                        p.stock = 0;
                        //mainWindow.Log("sudah");

                        p.publish_date = "NULL";
                        if (mainWindow.cache_product.data.ContainsKey(p.id))
                        {
                            p.publish_date = mainWindow.cache_product.data[p.id].publish_date;
                        }
                        if (mainWindow.cache_woo.list_id.ContainsKey(p.id))
                        {
                            var wooid = mainWindow.cache_woo.list_id[p.id];
                            if (mainWindow.listWooPublish.ContainsKey(wooid))
                            {
                                var splitted = mainWindow.listWooPublish[wooid].Split('|');
                                p.publish_date = splitted[0];
                            }
                        }

                        if (mainWindow.firstTime)
                        {
                            if (count <= Program.productLimit() || Program.productLimit() == 0)
                            {
                                p.active = true;
                            }
                            else
                            {
                                p.active = false;
                            }
                        }
                        else
                        {
                            if (mainWindow.cache_product.data.ContainsKey(p.id))
                            {
                                p.active = mainWindow.cache_product.data[p.id].active;
                            }
                            else
                            {
                                p.active = false;
                            }
                        }

                        if (p.active)
                        {
                            activeProducts++;
                            if (activeProducts > Program.productLimit() && Program.productLimit() > 0)
                            {
                                p.active = false;
                                if (mainWindow.cache_product.data.ContainsKey(p.id))
                                {
                                    mainWindow.cache_product.data[p.id].active = false;
                                }
                                activeProducts--;
                            }
                        }

                        mainWindow.listProduct.Add(p.id, p);
                    }
                    else
                    {
                        p = mainWindow.listProduct[Program.StripQuotes(item["sku"])];
                    }

                    if (p.stockdetail == null)
                    {
                        p.stockdetail = new Dictionary<string, WarehouseStock>();
                    }
                    var warehouse = new WarehouseStock
                    {
                        name = Program.StripQuotes(item["gudang"]),
                        stock = int.Parse(Program.StripQuotes(item["qty"]))
                    };
                    if (p.stockdetail.ContainsKey(warehouse.name))
                    {
                        p.stockdetail[warehouse.name] = warehouse;
                    }
                    else
                    {
                        p.stockdetail.Add(warehouse.name, warehouse);
                    }

                    p.stock = p.stock + warehouse.stock;
                }
                //mainWindow.Log(mainWindow.listProduct.Count.ToString());
            }
        }

        public List<String> GetWarehouses()
        {
            List<String> result = new List<String>();
            var profile = Program.config.profiles[idxProfile];
            var client = new RestClient(host_url);
            var request = new RestRequest(api_stock, Method.GET);
            request.AddHeader(key_name, key_value);
            var response = client.Execute(request);
            var obj = SimpleJSON.JSON.Parse(response.Content);
            if (obj != null)
            {
                foreach (SimpleJSON.JSONNode item in obj.AsArray)
                {
                    if (!result.Contains(Program.StripQuotes(item["gudang"])))
                    {
                        result.Add(Program.StripQuotes(item["gudang"]));
                    }
                }
            }
            return result;
        }
    }
}

    
