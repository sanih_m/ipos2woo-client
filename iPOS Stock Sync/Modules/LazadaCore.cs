﻿using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using SeleniumExtras.WaitHelpers;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync.Modules
{
    public class LazadaCore
    {
        public const string cachename = "/cache_lazada_{IDX}.json";

        IWebDriver driver;
        ChromeDriverService service;
        string cache_file = Application.StartupPath + "/cache_lazada_{IDX}.json";
        public CacheLazadaProduct cache;
        WebDriverWait wait;

        const int maxLoginTry = 5;
        int currentLoginTry = 0;

        const int maxMainTry = 5;
        int currentMainTry = 0;

        const int maxMainWait = 10;

        const int marketIndex = 3;
        const int clearCacheInterval = 1;
        public const string marketName = "Lazada";

        int idxProfile = 0;

        public bool isDone = false;
        public bool isSyncDone = false;
        public bool isLoggedIn = false;
        public bool isStuck = false;
        public string loginResponse = "";

        
        frmProfileSync mainWindow;
        public ucMarketplaceLog logControl;
        frmLogViewer logWindow;

        bool isLogInit = false;
        public LazadaCore(int idx)
        {
            try
            {
                idxProfile = idx;
                var config = Program.config.profiles[idxProfile];
                mainWindow = Program.Syncers[config.name];

                var chromeProfileFolder = "session_" + config.id + "_lazada";
                //var chromeDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Google\\Chrome\\User Data\\");
                var chromeDir = Application.StartupPath + "\\";
                if (!Directory.Exists(chromeDir + chromeProfileFolder))
                {
                    Directory.CreateDirectory(chromeDir + chromeProfileFolder);
                }

                var options = new ChromeOptions();
                options.BinaryLocation = Application.StartupPath + "\\engine_chrome\\chrome.exe";
                if (config.lazada_dev != Program.devPassword)
                {
                    options.AddArgument("--window-position=-32000,-32000");
                }
                options.AddArgument("disable-popup-blocking");
                options.AddArgument("disable-notifications");
                //options.AddArgument("disable-extensions");
                options.AddArgument("disable-infobars");
                options.AddArgument("--start-maximized");
                options.AddArgument("user-data-dir=" + chromeDir + chromeProfileFolder);
                options.AddExcludedArguments(new List<string>() { "enable-automation" });
                options.PageLoadStrategy = PageLoadStrategy.Eager;

                if (config.lazada_proxy_enabled)
                {
                    var p_scheme = "";
                    var p_address = "";
                    var p_port = "";
                    var p_username = "";
                    var p_password = "";
                    var proxy = new Proxy();
                    var isSSL = false;
                    if (config.lazada_proxy_mode == 0)
                    {
                        mainWindow.Log("Mengambil proxy untuk Lazada dari server.");
                        var success = false;
                        var proxyAddress = "";
                        while (!success)
                        {

                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            RestClient client = new RestClient("https://app.ipos2woo.com/proxy/index.php");

                            RestRequest request = new RestRequest();
                            IRestResponse response = client.Execute(request);
                            proxyAddress = response.Content;
                            try
                            {
                                var splitted = proxyAddress.Split(':');
                                var testProxy = new WebProxy(splitted[0], int.Parse(splitted[1]));
                                response = new RestClient
                                {
                                    BaseUrl = new System.Uri("https://seller.lazada.com/"),
                                    Proxy = testProxy,
                                    Timeout = 5000,
                                    ReadWriteTimeout = 5000,
                                }.Execute(new RestRequest
                                {
                                    Method = Method.GET,
                                    Timeout = 5000,
                                    ReadWriteTimeout = 5000,
                                });
                                if (response.ErrorException != null)
                                {
                                    throw response.ErrorException;
                                }
                                success = (!response.Content.Equals(""));
                               // mainWindow.Log(response.Content);
                            }
                            catch (Exception ex)
                            {
                                mainWindow.Log(ex.Message);
                                success = false;
                            }

                            if (!success)
                            {
                                proxyAddress = "";
                            }
                        }


                        p_scheme = "http";
                        p_address = proxyAddress.Split(':')[0];
                        p_port = proxyAddress.Split(':')[1];
                        p_username = "";
                        p_password = "";
                    }
                    else
                    {
                        isSSL = config.lazada_proxy_address.Contains("https:");
                        p_scheme = isSSL ? "https" : "http";
                        p_address = config.lazada_proxy_address.Replace("http", "").Replace("https", "").Replace("://", "");
                        p_port = config.lazada_proxy_port;
                        p_username = config.lazada_proxy_username;
                        p_password = config.lazada_proxy_password;
                    }

                    if (File.Exists("lazada_proxy.zip")) File.Delete("lazada_proxy.zip");
                    File.WriteAllText("lazada_background.js",
                        Properties.Resources.background_js
                        .Replace("YOUR_PROXY_SCHEME", p_scheme)
                        .Replace("YOUR_PROXY_ADDRESS", p_address)
                        .Replace("YOUR_PROXY_PORT", p_port)
                        .Replace("YOUR_PROXY_USERNAME", p_username)
                        .Replace("YOUR_PROXY_PASSWORD", p_password)
                    );
                    File.WriteAllText("lazada_manifest.json",
                        Properties.Resources.manifest_json);
                    using (ZipArchive zip = ZipFile.Open("lazada_proxy.zip", ZipArchiveMode.Create))
                    {
                        zip.CreateEntryFromFile(@"lazada_background.js", "background.js");
                        zip.CreateEntryFromFile(@"lazada_manifest.json", "manifest.json");
                    }
                    options.AddExtension("lazada_proxy.zip");
                    File.Delete("lazada_background.js");
                    File.Delete("lazada_manifest.json");
                }

                service = ChromeDriverService.CreateDefaultService();
                service.HideCommandPromptWindow = true;

                driver = new ChromeDriver(service, options);
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                cache_file = cache_file.Replace("{IDX}", idxProfile.ToString());
                LoadCache();
            }
            catch (Exception ex)
            {
                mainWindow.Log("Gagal inisialisasi Lazada.", Color.Red);
                mainWindow.Log(ex.Message);
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);
                isDone = true;
            }
        }

        void handleStuck(object sender, EventArgs e)
        {
            isStuck = true;
            isSyncDone = true;
            mainWindow.Log("Terjadi gangguan pada sinkronisasi " + marketName + ". Modul ini akan dimulai ulang...");
        }

        public void initLog()
        {
            if (isLogInit)
            {
                return;
            }
            isLogInit = true;
            logControl = mainWindow.CreateMarketplaceTab(marketIndex, marketName);
            logControl.WorkTimeout += handleStuck;
            //logWindow = new frmLogViewer(config.name + " : Log Lazada");
        }

        bool isDialogPresent(IWebDriver driver)
        {
            IAlert alert = ExpectedConditions.AlertIsPresent().Invoke(driver);
            return (alert != null);
        }

        public void stop()
        {
            try
            {
                driver.Quit();
                mainWindow.DeleteMarketplaceTab(marketName);
                //create list of process id
                var driverProcessIds = new List<int> { service.ProcessId };

                //Get all the childs generated by the driver like conhost, chrome.exe...
                var mos = new System.Management.ManagementObjectSearcher($"Select * From Win32_Process Where ParentProcessID={service.ProcessId}");
                foreach (var mo in mos.Get())
                {
                    var pid = Convert.ToInt32(mo["ProcessID"]);
                    driverProcessIds.Add(pid);
                }
                //Kill all
                foreach (var id in driverProcessIds)
                {
                    var p = System.Diagnostics.Process.GetProcessById(id);
                    if (p != null) p.Kill();
                }
            }
            catch (Exception e)
            {
                mainWindow.Log(e.Message, Color.Red);
            }
        }

        public async Task LoginAsync()
        {
            mainWindow.Log("Otorisasi Lazada...");
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("window.key = \"blahblah\";");
            var config = Program.config.profiles[idxProfile];
            bool isOtp = false;

            sendEmailWhenLogin:
            try
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                driver.Navigate().GoToUrl("https://sellercenter.lazada.co.id");
                driver.FindElement(By.Name("TPL_username")).SendKeys(config.lazada_email);
                driver.FindElement(By.Name("TPL_password")).SendKeys(config.lazada_password);
            }
            catch (Exception ex)
            {
                goto checkUserInfo;
            }

            try
            {
                driver.FindElement(By.ClassName("next-btn-normal")).Click();
                var slider = driver.FindElements(By.ClassName("btn_slide"));
                if (slider.Count > 0)
                {
                    //Using Action Class
                    Actions move = new Actions(driver);
                    OpenQA.Selenium.Interactions.IAction action = move.DragAndDropToOffset(slider[0], 352, 0).Build();
                    action.Perform();
                }

                driver.FindElement(By.ClassName("next-btn-normal")).Click();

                currentLoginTry = 0;
                await Task.Delay(4000).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                mainWindow.Log(ex.Message);
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);

                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {

                    mainWindow.Log("Login gagal 1, Silahkan cek koneksi internet anda.", System.Drawing.Color.Red);
                    isDone = true;
                    return;
                }
            }
            #region "OTP"
            bool isFirstTime = true;
            needOtp:
            isOtp = false;
            try
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                isOtp = driver.FindElement(By.ClassName("js-tfa-form")) != null;
                if (isOtp)
                {
                    if (isFirstTime) driver.FindElement(By.ClassName("js-tfa-request__button")).Click();
                    isFirstTime = false;
                    driver.FindElement(By.ClassName("js-tfa-submit"));
                    mainWindow.Log("Silahkan ketikan kode otentikasi yang masuk ke perangkat anda disini.");
                    mainWindow.Log("Ketikan 'skip' untuk melanjutkan proses tanpa sinkronisasi tokopedia.");
                    mainWindow.Log("Ketikan 'resend' untuk mengirim ulang kode otentikasi.");
                    mainWindow.Log("");
                    mainWindow.command = "";
                    while (mainWindow.command.Equals(""))
                    {
                        Console.WriteLine("Waiting for command");
                        //mainWindow.Log("waiting for command : "+mainWindow.command);
                        await Task.Delay(1000).ConfigureAwait(false);
                    }
                    if (mainWindow.command.Equals("skip"))
                    {
                        mainWindow.Log("Login Lazada gagal, kode otentikasi dibatalkan.", System.Drawing.Color.Red);
                        driver.Navigate().GoToUrl("https://seller.Lazada.com");
                        isDone = true;
                        return;
                    }
                    else if (mainWindow.command.Equals("resend"))
                    {
                        mainWindow.Log("Kode otentikasi akan dikirimkan ulang.");
                        driver.FindElement(By.ClassName("js-tfa-re-request__button")).Click();
                        goto needOtp;
                    }
                    else
                    {
                        var el = driver.FindElement(By.Id("otp"));
                        wait.Until(ExpectedConditions.ElementToBeClickable(el));
                        el.Clear();
                        el.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                        el.SendKeys(OpenQA.Selenium.Keys.Delete);
                        await Task.Delay(500).ConfigureAwait(false);
                        el.SendKeys(mainWindow.command);
                        driver.FindElement(By.ClassName("js-tfa-submit")).Click();
                        mainWindow.Log("Mengirim kode otentikasi...");
                    }
                    mainWindow.command = "";
                }
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                Console.WriteLine("Login berhasil");
            }


            try
            {
                if (isOtp)
                {
                    mainWindow.Log("Kode otentikasi sedang diproses...");
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                    var finishEls = driver.FindElements(By.ClassName("js-tfa-step--finish"));
                    if (finishEls.Count > 0 && finishEls[1].GetCssValue("display") != "none")
                    {
                        finishEls[1].Click();
                    }
                    else
                    {
                        driver.FindElement(By.ClassName("js-tfa-form-field__status"));
                        mainWindow.Log("Kode otentikasi salah.", System.Drawing.Color.Red);
                        Console.WriteLine("OTP Denied");
                        goto needOtp;
                    }
                }
            }
            catch (Exception ex)
            {
                mainWindow.Log(ex.Message);
                Console.WriteLine("OTP Received");
                //driver.Navigate().GoToUrl("https://sellercenter.lazada.co.id");
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);

            #endregion
            checkUserInfo:
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
            try
            {
                driver.FindElement(By.ClassName("profile-icon"));
                mainWindow.Log("Proses login " + marketName + " berhasil.", System.Drawing.Color.Green);
                currentLoginTry = 0;

                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                await Task.Delay(2000).ConfigureAwait(false);
                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                var checkbox = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog').querySelector('iron-pages > div > settings-checkbox:nth-child(3)').shadowRoot.querySelector('cr-checkbox')");
                if (checkbox.GetAttribute("checked") != null)
                {
                    checkbox.Click();
                }
                button.Click();

                isDone = true;
            }
            catch (Exception ex)
            {
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    isDone = true;
                    mainWindow.Log("Login gagal 2, Silahkan cek koneksi internet anda.", System.Drawing.Color.Red);
                    return;
                }
            }

            loginResponse = "";
            isLoggedIn = true;
        }

        void AcceptDialog()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                alert.Accept();
            }
            catch (Exception ex)
            {
                //logControl.Log("dialog error : " + ex.Message.ToLower());
            }
        }

        public void RemoveProductFromCache(string sku, bool isSave = false)
        {
            cache.products.Remove(sku);
            if (isSave) SaveCache();
        }

        void SaveCache()
        {
            cache.lastupdate = DateTime.Now;
            var json = JsonConvert.SerializeObject(cache);
            File.WriteAllText(cache_file, json);
            File.Copy(cache_file, cache_file + ".backup", true);
        }

        void LoadCache()
        {
            try
            {
                var data = File.ReadAllText(cache_file);
                cache = JsonConvert.DeserializeObject<CacheLazadaProduct>(data);
                if (cache == null)
                {
                    cache = new CacheLazadaProduct();
                }
                else
                {
                    if (cache.products == null)
                    {
                        cache.products = new Dictionary<string, LazadaProduct>();
                    }
                    if (cache.marketID == null)
                    {
                        cache.marketID = new Dictionary<string, string>();
                    }
                    
                }
            }
            catch (Exception)
            {
                cache = new CacheLazadaProduct();
            }
        }

        public void ResetCache()
        {
            cache = new CacheLazadaProduct();
            SaveCache();
        }

        public void SyncCache()
        {
            var config = Program.config.profiles[idxProfile];
            foreach (var source in mainWindow.cache_product.data)
            {
                if (cache.marketID.ContainsKey(source.Value.sku))
                {
                    LazadaProduct p = new LazadaProduct();
                    p.id_ipos = source.Value.id;
                    p.sku = source.Value.sku;
                    p.level_index = config.lazada_price;
                    if (p.level_index == 1)
                    {
                        p.level_price = source.Value.price1;
                    }
                    else if (p.level_index == 2)
                    {
                        p.level_price = source.Value.price2;
                    }
                    else if (p.level_index == 3)
                    {
                        p.level_price = source.Value.price3;
                    }
                    else if (p.level_index == 4)
                    {
                        p.level_price = source.Value.price4;
                    }
                    p.stock = source.Value.stock;
                    if (cache.products.ContainsKey(p.sku))
                    {
                        cache.products[p.sku] = p;
                    }
                    else
                    {
                        cache.products.Add(p.sku, p);
                    }
                }
            }
            SaveCache();
        }

        public async Task Sync(Dictionary<string, SingleProduct> listProducts)
        {
            LoadCache();
            while (logControl == null)
            {
                await Task.Delay(1000).ConfigureAwait(false);
            }
            logControl.isClearLog = true;
            logControl.isWorking = true;
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            if (mainWindow.isSyncStopped()) return;
            mainWindow.Log("Memulai pengecekan sinkronisasi " + marketName + ".", System.Drawing.Color.Yellow);
            mainWindow.Log("Silahkan lihat tab '" + marketName + "' untuk melihat laporan sinkronisasi.");
            isSyncDone = false;
            isStuck = false;
            var config = Program.config.profiles[idxProfile];

            driver.Navigate().GoToUrl("https://sellercenter.lazada.co.id");
            await Task.Delay(100).ConfigureAwait(false);
            LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            if (driver.Url.Contains("/apps/seller/login"))
            {
                mainWindow.Log("Melakukan proses login ulang...");
                await LoginAsync().ConfigureAwait(false);
            }

            Dictionary<String, LazadaProduct> UpdatingProducts = new Dictionary<String, LazadaProduct>();
            try
            {
                foreach (var source in new Dictionary<string, SingleProduct>(listProducts))
                {
                    logControl.timeoutSecs = 0;
                    if (!source.Value.active)
                    {
                        continue;
                    }
                    var same = false;
                    if (cache != null && cache.products != null && cache.products.ContainsKey(source.Value.sku))
                    {
                        //mainWindow.Log("exist");
                        same = Program.CompareLazadaProduct(source.Value, cache.products[source.Value.sku], config);
                    }

                    if (!same)
                    {
                        LazadaProduct p = new LazadaProduct();
                        p.id_ipos = source.Value.id;
                        p.sku = source.Value.sku;
                        p.level_index = config.lazada_price;
                        if (p.level_index == 1)
                        {
                            p.level_price = source.Value.price1;
                            //mainWindow.Log("Harga "+source.Value.price1.ToString());
                        }
                        else if (p.level_index == 2)
                        {
                            p.level_price = source.Value.price2;
                            //mainWindow.Log("Harga " + source.Value.price2.ToString());
                        }
                        else if (p.level_index == 3)
                        {
                            p.level_price = source.Value.price3;
                            //mainWindow.Log("Harga " + source.Value.price3.ToString());
                        }
                        else if (p.level_index == 4)
                        {
                            p.level_price = source.Value.price4;
                            //mainWindow.Log("Harga " + source.Value.price4.ToString());
                        }
                        else
                        {
                            p.level_price = -1;
                        }
                        p.stock = source.Value.stock;
                        UpdatingProducts.Add(p.sku, p);
                    }
                    if (mainWindow.isSyncStopped()) return;
                }
            }
            catch (Exception e)
            {
                mainWindow.Log(e.Message+e.StackTrace);
            }
            if (UpdatingProducts.Count == 0)
            {
                logControl.Log("Belum ada produk untuk sinkronisasi.", System.Drawing.Color.Yellow);
                logControl.isWorking = false;
                isStuck = false;
                isSyncDone = true;
                return;
            }
            logControl.Log("Terdapat " + UpdatingProducts.Count + " produk untuk sinkronisasi.");
            var allCount = UpdatingProducts.Count;

            List<string> successProducts = new List<string>();
            bool needFetchID = false;
            if (logControl.CheckID)
            {
                foreach (var item in UpdatingProducts)
                {
                    logControl.timeoutSecs = 0;
                    if (!cache.marketID.ContainsKey(item.Value.sku))
                    {
                        //cache.marketID.Add(item.Value.sku, "394713133");
                        needFetchID = true;
                        break;
                    }
                    if (mainWindow.isSyncStopped()) return;
                }
            }
            if (needFetchID)
            {
                logControl.Log("Mengambil id produk...");
                var page = 1;
                var next = "https://sellercenter.lazada.co.id/product/portal/search?title=&sellerSku=&sellerName=&shopSku=&itemId=&brandName=&selectCategory=&orderBy=&desc=&currentTab=live&size=50&currentPage=1";
                do
                {
                    loopProducts:
                    logControl.timeoutSecs = 0;
                    if (mainWindow.command.Contains("skip"))
                    {
                        break;
                    }
                    try
                    {
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                        var slider = driver.FindElements(By.ClassName("btn_slide"));
                        if (slider.Count > 0)
                        {
                            //Using Action Class
                            Actions move = new Actions(driver);
                            OpenQA.Selenium.Interactions.IAction action = move.DragAndDropToOffset(slider[0], 352, 0).Build();
                            action.Perform();
                        }
                        else
                        {
                            driver.Navigate().GoToUrl(next);
                        }
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);

                        next = "";
                        
                        var html = driver.FindElement(By.TagName("body")).GetAttribute("innerHTML");
                        var obj = SimpleJSON.JSON.Parse(html);
                        if (obj != null && Program.StripQuotes(obj["success"].ToString()) == "true")
                        {
                            var arr = obj["data"]["tableData"].AsArray;
                            logControl.Log("Memeriksa " + arr.Count.ToString() + " data di halaman " + page.ToString() + "...");
                            foreach (JSONNode item in arr)
                            {
                                var sku = Program.StripQuotes(item["sku"].ToString());
                                var rdo = item["active"]["readOnly"].AsBool;
                                var act = item["active"]["active"].AsBool;
                                if (!sku.Equals("") && !rdo && act)
                                {
                                    if (cache.marketID.ContainsKey(sku))
                                    {
                                        cache.marketID[sku] = Program.StripQuotes(item["itemId"].ToString());
                                    }
                                    else
                                    {
                                        cache.marketID.Add(sku, Program.StripQuotes(item["itemId"].ToString()));
                                    }

                                }
                            }
                            var pageinfo = obj["data"]["pagination"];
                            if (pageinfo["total"].AsInt > (pageinfo["pageSize"].AsInt * (pageinfo["current"].AsInt)))
                            {
                                next = "https://sellercenter.lazada.co.id/product/portal/search?title=&sellerSku=&sellerName=&shopSku=&itemId=&brandName=&selectCategory=&orderBy=&desc=&currentTab=live&size=50&currentPage=" + (pageinfo["current"].AsInt + 1).ToString();
                            }
                            else
                            {
                                next = "";
                            }
                        }
                        else
                        {
                            logControl.Log("Tidak ada data di halaman " + page.ToString() + "...");
                        }
                    }
                    catch (Exception ex)
                    {
                        logControl.Log(ex.Message + ex.StackTrace);
                        currentMainTry++;
                        if (currentMainTry < maxMainTry)
                        {
                            logControl.Log("Gagal, Mencoba mengambil ulang id produk...", System.Drawing.Color.Yellow);
                            goto loopProducts;
                        }
                        else
                        {
                            logControl.Log("Gagal dalam proses mengambil id produk.", System.Drawing.Color.Red);
                            goto skipfindid;
                        }
                    }
                    if (mainWindow.isSyncStopped()) return;
                    page++;
                } while (!next.Equals(""));
                mainWindow.command = "";
            }

            var deletedUpdates = new List<string>();
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!cache.marketID.ContainsKey(item.Key))
                {
                    deletedUpdates.Add(item.Key);
                }
                else
                {
                    if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos))
                    {
                        var monitored = mainWindow.listMonitored[item.Value.id_ipos];
                        monitored.lazada_sync = 1;
                        monitored.lazada_price = item.Value.level_price;
                        monitored.lazada_stock = item.Value.stock;
                    }


                    if (cache.products.ContainsKey(item.Key))
                    {
                        cache.products[item.Key] = item.Value;
                    }
                    else
                    {
                        cache.products.Add(item.Key, item.Value);
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }
            if (deletedUpdates.Count > 0)
            {
                foreach (var item in deletedUpdates)
                {
                    logControl.timeoutSecs = 0;
                    UpdatingProducts.Remove(item);
                }
            }
            if (allCount != UpdatingProducts.Count)
            {
                logControl.Log("Terdapat " + (allCount - UpdatingProducts.Count) + " produk yang belum terdeteksi. Silahkan input SKU terlebih dahulu.", Color.Yellow);
            }


            skipfindid:


            try
            {
                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                await Task.Delay(2000).ConfigureAwait(false);
                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                button.Click();
            }
            catch (Exception)
            {
                logControl.Log("Cannot clear cache.");
            }

            currentMainTry = 0;
            var noitem = 1;
            var processCounter = 1;
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!logControl.SyncMarketplace)
                {
                    if (!logControl.SendMonitoring)
                    {
                        if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos))
                        {
                            mainWindow.listMonitored[item.Value.id_ipos].lazada_sync = 0;
                        }
                    }
                    continue;
                }
                if (mainWindow.isSyncStopped()) return;
                loopUpdate:
                try
                {
                    logControl.Log("Memulai sinkronisasi untuk sku " + item.Key + " (" + noitem + "/" + UpdatingProducts.Count + ")");
                    if (cache.marketID.ContainsKey(item.Key))
                    {
                        //logControl.Log("Test shopee 2");
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                        var slider = driver.FindElements(By.ClassName("btn_slide"));
                        if (slider.Count > 0)
                        {
                            logControl.Log("Menyelesaikan verifikasi lazada...", System.Drawing.Color.Yellow);
                            //Using Action Class
                            Actions move = new Actions(driver);
                            OpenQA.Selenium.Interactions.IAction action = move.DragAndDropToOffset(slider[0], 352, 0).Build();
                            action.Perform();
                        }
                        else
                        {
                            driver.Navigate().GoToUrl("https://sellercenter.lazada.co.id/apps/product/publish?productId=" + cache.marketID[item.Key]);
                        }

                        LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                        LetsWait().Until(driver => driver.FindElements(By.ClassName("props-sku-table")).Count > 0);
                        //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                        AcceptDialog();

                        var table = driver.FindElement(By.ClassName("props-sku-table"));
                        executor.ExecuteScript("arguments[0].scrollIntoView(false);", table);
                        await Task.Delay(500).ConfigureAwait(false);
                        var cols = table.FindElements(By.CssSelector(".next-table-header .next-table-header-inner thead tr > th"));
                        var idxHarga = 3;
                        var idxStok = 5;
                        var idxSKU = 6;
                        var curIdx = 0;
                        foreach (var col in cols)
                        {
                            
                            try
                            {
                                var str = col.FindElement(By.CssSelector(":scope > *")).Text.ToLower().Trim();
                                if (str.Equals("harga"))
                                {
                                    idxHarga = curIdx;

                                }
                                else if (str.Equals("jumlah stok"))
                                {
                                    idxStok = curIdx;
                                }
                                else if (str.Equals("seller sku"))
                                {
                                    idxSKU = curIdx;
                                }
                            }
                            catch (Exception e)
                            {
                                logControl.Log(e.Message);
                            }
                            if (curIdx == 3)
                            {
                                executor.ExecuteScript("var nexttablebody = document.querySelector('.next-table-body');" +
                                               "var maxScrollLeft = nexttablebody.scrollWidth - nexttablebody.clientWidth;" +
                                "nexttablebody.scrollLeft = maxScrollLeft;");
                                await Task.Delay(500).ConfigureAwait(false);
                            }
                            curIdx++;
                        }
                        var rows = table.FindElements(By.ClassName("next-table-row"));
                        if (rows.Count > 0)
                        {
                            var foundSku = false;
                            foreach (var row in rows)
                            {
                                cols = row.FindElements(By.CssSelector(":scope > td"));
                                if(idxSKU >= cols.Count)
                                {
                                    continue;
                                }
                                var inputSKU = cols[idxSKU].FindElements(By.CssSelector("input"));
                                if (inputSKU.Count > 0 && inputSKU[0].GetAttribute("value").ToLower().Equals(item.Key.ToLower()))
                                {
                                    //logControl.Log(inputSKU[0].GetAttribute("value").ToLower() + " vs " + item.Key.ToLower());
                                    foundSku = true;
                                    var inputStock = cols[idxStok].FindElement(By.CssSelector("input"));
                                    wait.Until(ExpectedConditions.ElementToBeClickable(inputStock));
                                    executor.ExecuteScript("arguments[0].scrollIntoView(false);", inputStock);
                                    inputStock.Clear();
                                    await Task.Delay(100).ConfigureAwait(false);
                                    inputStock.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                    await Task.Delay(100).ConfigureAwait(false);
                                    inputStock.SendKeys(OpenQA.Selenium.Keys.Delete);
                                    await Task.Delay(100).ConfigureAwait(false);
                                    executor.ExecuteScript("arguments[0].value = ''", inputStock);
                                    foreach (char c in item.Value.stock.ToString())
                                    {
                                        inputStock.SendKeys(c.ToString());
                                        await Task.Delay(100).ConfigureAwait(false);
                                    }
                                    //executor.ExecuteScript("arguments[0].value = " + item.Value.stock.ToString() + ";", inputStock);
                                    //executor.ExecuteScript("arguments[0].dispatchEvent(new Event('blur', { bubbles: true }));", inputStock);

                                    if (item.Value.level_price > 0)
                                    {
                                        var inputHarga = cols[idxHarga].FindElement(By.CssSelector("input"));
                                        wait.Until(ExpectedConditions.ElementToBeClickable(inputHarga));
                                        executor.ExecuteScript("arguments[0].scrollIntoView(false);", inputHarga);
                                        inputHarga.Clear();
                                        await Task.Delay(100).ConfigureAwait(false);
                                        inputHarga.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                        await Task.Delay(100).ConfigureAwait(false);
                                        inputHarga.SendKeys(OpenQA.Selenium.Keys.Delete);
                                        await Task.Delay(100).ConfigureAwait(false);
                                        executor.ExecuteScript("arguments[0].value = ''", inputHarga);
                                        foreach (char c in item.Value.level_price.ToString())
                                        {
                                            inputHarga.SendKeys(c.ToString());
                                            await Task.Delay(100).ConfigureAwait(false);
                                        }
                                        //executor.ExecuteScript("arguments[0].value = "+ item.Value.level_price.ToString() + ";", inputHarga);
                                        //executor.ExecuteScript("arguments[0].dispatchEvent(new Event('blur', { bubbles: true }));", inputHarga);

                                    }
                                    else if (item.Value.level_price == 0)
                                    {
                                        logControl.Log("Harga belum di isi untuk sku " + item.Key, System.Drawing.Color.Red);
                                    }
                                }
                            }
                            if (!foundSku)
                            {
                                logControl.Log("Gagal menemukan SKU produk ini : " + item.Key, Color.Red);
                                reportFailedSync(item);
                                continue;
                            }
                        }
                        else
                        {
                            logControl.Log("Tidak ada komponen SKU yang terdeteksi : " + item.Key, Color.Red);
                            reportFailedSync(item);
                            continue;
                        }

                        //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);

                        //hapus chat lazada yg ngalangin
                        executor.ExecuteScript("return (document.querySelector('.lzd-im-init-container') ? document.querySelector('.lzd-im-init-container').remove() : null)");

                        try
                        {
                            executor.ExecuteScript("return (document.evaluate('//button[contains(text(),\"Simpan&Tampilkan\")]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
                            executor.ExecuteScript("return (document.evaluate('//button[contains(text(),\"Simpan\")]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).click();");
                        }
                        catch (Exception)
                        {

                        }
                        await Task.Delay(500).ConfigureAwait(false);
                        //var btnSimpan = driver.FindElement(By.ClassName("next-btn-primary"));
                        //Actions actions = new Actions(driver);
                        //actions.MoveToElement(btnSimpan).Click().Build().Perform();

                        //cek ada error inputan
                        var other_errors_1 = driver.FindElements(By.ClassName("next-message-error"));
                        var other_errors_2 = driver.FindElements(By.ClassName("error-item"));
                        if (other_errors_1.Count > 0 || other_errors_2.Count > 0)
                        {
                            logControl.Log("Ada kesalahan pada data produk sehingga tidak bisa menyimpan sku " + item.Key, System.Drawing.Color.Red);
                            reportFailedSync(item);
                            continue;
                        }

                        LetsWait().Until(driver => driver.FindElements(By.ClassName("next-icon-success")).Count > 0);
                   
                        successProducts.Add(item.Key);
                        logControl.Log("Sinkronisasi berhasil.", System.Drawing.Color.Green);

                        processCounter++;
                        if (processCounter % clearCacheInterval == 0)
                        {
                            try
                            {
                                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                                await Task.Delay(2000).ConfigureAwait(false);
                                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                                button.Click();
                            }
                            catch (Exception)
                            {
                                logControl.Log("Cannot clear cache.");
                            }
                        }
                    }
                    else
                    {
                        logControl.Log("SKU belum ditentukan untuk produk ini.", System.Drawing.Color.Yellow);
                        if (cache.products.ContainsKey(item.Key))
                        {
                            var product = cache.products[item.Key];
                            if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                            {
                                mainWindow.listMonitored[product.id_ipos].lazada_id = "0";
                                mainWindow.listMonitored[product.id_ipos].lazada_status = 0;
                            }
                        }
                        Console.WriteLine("no cache 1");
                    }
                    noitem++;
                }
                catch (Exception e)
                {
                    logControl.Log(e.Message+" | "+e.StackTrace,Color.Red);
                    Console.WriteLine(e.Message.ToLower());
                    if (e.Message.ToLower().Contains("alert"))
                    {
                        logControl.Log(e.Message.ToLower());
                        Console.WriteLine("Dismissing alert");
                        AcceptDialog();
                        noitem++;
                        continue;
                    }

                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        logControl.Log("Gagal menginput perubahan, Mencoba menginput ulang perubahan data...", System.Drawing.Color.Yellow);
                        goto loopUpdate;
                    }
                    else
                    {
                        reportFailedSync(item);
                        continue;
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }

            //logControl.Log("Test shopee 4");
            foreach (var item in successProducts)
            {
                logControl.timeoutSecs = 0;
                if (cache.products.ContainsKey(item) && cache.marketID.ContainsKey(item))
                {
                    var product = cache.products[item];
                    var id = cache.marketID[item];
                    if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                    {
                        mainWindow.listMonitored[product.id_ipos].lazada_id = id;
                        mainWindow.listMonitored[product.id_ipos].lazada_status = 1;
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }
            Console.WriteLine("Done deleting");
            logControl.Log("Sinkronisasi produk selesai.", System.Drawing.Color.Green);
            mainWindow.Log("Sinkronisasi " + marketName + " selesai.", System.Drawing.Color.Green);
            logControl.isWorking = false;
            isSyncDone = true;
            isStuck = false;
            SaveCache();
            
        }

        void reportFailedSync(System.Collections.Generic.KeyValuePair<string, LazadaProduct> item)
        {
            TakeScreenshot(item.Key);
            logControl.Log("Sinkronisasi gagal untuk sku " + item.Key, System.Drawing.Color.Red);
            if (cache.products.ContainsKey(item.Key) && cache.marketID.ContainsKey(item.Key))
            {
                var product = cache.products[item.Key];
                var id = cache.marketID[item.Key];
                if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                {
                    mainWindow.listMonitored[product.id_ipos].lazada_id = id;
                    mainWindow.listMonitored[product.id_ipos].lazada_status = 2;
                }
            }
            AcceptDialog();
        }


        void TakeScreenshot(string name)
        {
            try
            {
                Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                var folder = Application.StartupPath + "/ss/Lazada/";
                Directory.CreateDirectory(folder);
                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                {
                    name = name.Replace(c, '_');
                }
                ss.SaveAsFile(folder + name + ".jpg", ScreenshotImageFormat.Jpeg);
            }
            catch (Exception e)
            {
                mainWindow.Log("Gagal screenshot, proses telah ditutup / tidak ditemukan.");
            }
        }

        public void showLog()
        {
            if (logWindow != null)
            {
                mainWindow.Log("Membuka log Lazada...", Color.Green);
                logControl.Show();
                logControl.BringToFront();
            }
        }

        WebDriverWait LetsWait(int waitSec = -1)
        {
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, driver, TimeSpan.FromSeconds(waitSec == -1 ? maxMainWait : waitSec), TimeSpan.FromMilliseconds(100));
            wait.PollingInterval = TimeSpan.FromMilliseconds(100);
            return wait;
        }
    }
}
