﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using OfficeOpenXml;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using SeleniumExtras.WaitHelpers;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync.Modules
{
    public class TokopediaCore
    {
        public const string cachename = "/cache_tokopedia_{IDX}.json";
        IWebDriver driver;
        ChromeDriverService service;
        string shopId;
        string cache_file = Application.StartupPath + "/cache_tokopedia_{IDX}.json";
        string log_sales_file = Application.StartupPath + "/" + "sales_tokopedia_{IDX}.log";
        Dictionary<string, Product> liveProducts;

        public CacheTokopediaProduct cache;
        public List<TokopediaSale> newSales = new List<TokopediaSale>();

        const int maxLoginTry = 3;
        int currentLoginTry = 0;

        const int maxMainTry = 3;
        int currentMainTry = 0;

        const int maxMainWait = 60;

        const int marketIndex = 0;
        const int clearCacheInterval = 1;
        public const string marketName = "Tokopedia";

        int idxProfile = 0;

        public bool isSyncDone = false;
        public bool isDone = false;
        public bool isLoggedIn = false;
        public bool isStuck = false;
        public string loginResponse = "";

        public DateTime lastCheckID = DateTime.Now;
        public DateTime nextCheckID = DateTime.Now;

        frmProfileSync mainWindow;
        public ucMarketplaceLog logControl;
        frmLogViewer logWindow;

        bool isLogInit = false;


        public TokopediaCore(int idx)
        {
            try
            {
                idxProfile = idx;
                var config = Program.config.profiles[idxProfile];
                mainWindow = Program.Syncers[config.name];

                var chromeProfileFolder = "session_" + config.id + "_tokopedia";
                //var chromeDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Google\\Chrome\\User Data\\");
                var chromeDir = Application.StartupPath + "\\";
                if (!Directory.Exists(chromeDir + chromeProfileFolder))
                {
                    Directory.CreateDirectory(chromeDir + chromeProfileFolder);
                }

                var options = new ChromeOptions();
                options.BinaryLocation = Application.StartupPath + "\\engine_chrome\\chrome.exe";
                options.AddArgument("disable-popup-blocking");
                options.AddArgument("disable-notifications");
                //options.AddArgument("disable-extensions");
                options.AddArgument("disable-infobars");
                //options.AddArgument("disable-dev-shm-usage");
                options.AddArgument("start-maximized");
                //options.AddArgument("--no-sandbox");
                options.AddArgument("user-data-dir=" + chromeDir + chromeProfileFolder);
                options.AddArgument("ignore-certificate-errors");
                options.AddExcludedArguments(new List<string>() { "enable-automation" });
                if (config.tokped_dev != Program.devPassword)
                {
                    options.AddArgument("--window-position=-32000,-32000");
                }
                options.PageLoadStrategy = PageLoadStrategy.Eager;


                if (config.tokped_proxy_enabled)
                {


                    var p_scheme = "";
                    var p_address = "";
                    var p_port = "";
                    var p_username = "";
                    var p_password = "";
                    var proxy = new Proxy();
                    var isSSL = false;
                    if (config.tokped_proxy_mode == 0)
                    {
                        mainWindow.Log("Mengambil proxy untuk Tokopedia dari server.");
                        var success = false;
                        var proxyAddress = "";
                        while (!success)
                        {

                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            RestClient client = new RestClient("https://app.ipos2woo.com/proxy/index.php");

                            RestRequest request = new RestRequest();
                            IRestResponse response = client.Execute(request);
                            proxyAddress = response.Content;
                            try
                            {
                                var splitted = proxyAddress.Split(':');
                                var testProxy = new WebProxy(splitted[0], int.Parse(splitted[1]));
                                response = new RestClient
                                {
                                    BaseUrl = new System.Uri("https://seller.tokopedia.com/"),
                                    Proxy = testProxy,
                                    Timeout = 5000,
                                    ReadWriteTimeout = 5000,
                                }.Execute(new RestRequest
                                {
                                    Method = Method.GET,
                                    Timeout = 5000,
                                    ReadWriteTimeout = 5000,
                                });
                                if (response.ErrorException != null)
                                {
                                    throw response.ErrorException;
                                }
                                success = (!response.Content.Equals(""));
                                mainWindow.Log(response.Content);
                            }
                            catch (Exception ex)
                            {
                                mainWindow.Log(ex.Message);
                                success = false;
                            }

                            if (!success)
                            {
                                proxyAddress = "";
                            }
                        }


                        p_scheme = "http";
                        p_address = proxyAddress.Split(':')[0];
                        p_port = proxyAddress.Split(':')[1];
                        p_username = "";
                        p_password = "";
                    }
                    else
                    {
                        isSSL = config.tokped_proxy_address.Contains("https:");
                        p_scheme = isSSL ? "https" : "http";
                        p_address = config.tokped_proxy_address.Replace("http", "").Replace("https", "").Replace("://", "");
                        p_port = config.tokped_proxy_port;
                        p_username = config.tokped_proxy_username;
                        p_password = config.tokped_proxy_password;
                    }


                    if(File.Exists("tokopedia_proxy.zip")) File.Delete("tokopedia_proxy.zip");
                    File.WriteAllText("tokopedia_background.js",
                        Properties.Resources.background_js
                        .Replace("YOUR_PROXY_SCHEME", p_scheme)
                        .Replace("YOUR_PROXY_ADDRESS", p_address)
                        .Replace("YOUR_PROXY_PORT", p_port)
                        .Replace("YOUR_PROXY_USERNAME", p_username)
                        .Replace("YOUR_PROXY_PASSWORD", p_password)
                    );
                    File.WriteAllText("tokopedia_manifest.json",
                        Properties.Resources.manifest_json);
                    using (ZipArchive zip = ZipFile.Open("tokopedia_proxy.zip", ZipArchiveMode.Create))
                    {
                        zip.CreateEntryFromFile(@"tokopedia_background.js", "background.js");
                        zip.CreateEntryFromFile(@"tokopedia_manifest.json", "manifest.json");
                    }
                    options.AddExtension("tokopedia_proxy.zip");
                    File.Delete("tokopedia_background.js");
                    File.Delete("tokopedia_manifest.json");
                }


                service = ChromeDriverService.CreateDefaultService();
                service.HideCommandPromptWindow = true;

                driver = new ChromeDriver(service, options);
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                //wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

                cache_file = cache_file.Replace("{IDX}", idxProfile.ToString());
                log_sales_file = log_sales_file.Replace("{IDX}", idxProfile.ToString());

                calculateNextCheckID(config);
                LoadCache();
            }
            catch (Exception ex)
            {
                mainWindow.Log("Gagal inisialisasi Tokopedia.", Color.Red);
                mainWindow.Log(ex.Message);
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);
                isDone = true;
            }
        }

        void handleStuck(object sender, EventArgs e)
        {
            if (isSyncDone) return;
            isStuck = true;
            isSyncDone = true;
            mainWindow.Log("Terjadi gangguan pada sinkronisasi " + marketName + ". Modul ini akan dimulai ulang...");
        }

        public void calculateNextCheckID(ProfileConfig config)
        {
            if (config.tokped_checkid_schedule != "" && config.tokped_checkid_schedule != null)
            {
                var time = config.tokped_checkid_schedule.Split(config.tokped_checkid_schedule.Substring(2, 1)[0]);
                nextCheckID = lastCheckID.Date.AddHours(int.Parse(time[0])).AddMinutes(int.Parse(time[1])).AddSeconds(0);
                if (nextCheckID <= lastCheckID)
                {
                    nextCheckID = nextCheckID.AddDays(1);
                }
            }
        }

        public void initLog()
        {
            if (isLogInit)
            {
                return;
            }
            isLogInit = true;
            logControl = mainWindow.CreateMarketplaceTab(marketIndex, marketName);
            logControl.CheckID = false;
            logControl.WorkTimeout += handleStuck;
            //logWindow = new frmLogViewer(config.name + " : Log Tokopedia");
        }

        public void stop()
        {
            try
            {
                mainWindow.Log("Cek proses Tokopedia yang aktif...");
                driver.Quit();
                mainWindow.DeleteMarketplaceTab(marketName);
                //create list of process id
                var driverProcessIds = new List<int> { service.ProcessId };

                //Get all the childs generated by the driver like conhost, chrome.exe...
                var mos = new System.Management.ManagementObjectSearcher($"Select * From Win32_Process Where ParentProcessID={service.ProcessId}");
                foreach (var mo in mos.Get())
                {
                    var pid = Convert.ToInt32(mo["ProcessID"]);
                    driverProcessIds.Add(pid);
                }
                //Kill all
                foreach (var id in driverProcessIds)
                {
                    var p = System.Diagnostics.Process.GetProcessById(id);
                    if (p != null) p.Kill();
                }
            }
            catch (Exception e)
            {
                mainWindow.Log(e.Message, Color.Red);
            }
        }

        public async Task LoginAsync()
        {
            mainWindow.Log("Otorisasi Tokopedia...");
            var config = Program.config.profiles[idxProfile];
            bool isOtp = false;


        sendEmailWhenLogin:
            try
            {
                driver.Navigate().GoToUrl("https://seller.tokopedia.com/home");
                LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                if (!driver.Url.Contains("seller.tokopedia.com"))
                {
                    LetsWait().Until(driver => driver.FindElements(By.CssSelector("a[id='link-page']")).Count > 0);
                    driver.FindElement(By.CssSelector("a[id='link-page']")).Click();
                    LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                }
                LetsWait(5).Until(driver => driver.FindElements(By.XPath("//*[@name='email']")).Count > 0);
                driver.FindElement(By.XPath("//*[@name='email']")).SendKeys(config.tokped_email);
                await Task.Delay(1000).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                goto checkBannerAfterLogin;
            }


            try
            {
                Actions action = new Actions(driver);
                action.MoveToElement(driver.FindElement(By.ClassName("unf-user-btn"))).Click().Perform();

                LetsWait(5).Until(driver => driver.FindElements(By.XPath("//*[@name='password']")).Count > 0);
                LetsWait(5).Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@name='password']")));
                driver.FindElement(By.XPath("//*[@name='password']")).SendKeys(config.tokped_password);

                LetsWait(5).Until(ExpectedConditions.ElementToBeClickable(By.Id("login-submit")));
                driver.FindElement(By.Id("login-submit")).Click();
                currentLoginTry = 0;
            }
            catch (Exception e)
            {
                mainWindow.Log(e.Message+e.StackTrace);
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    mainWindow.Log("Login tokopedia gagal, Mencoba ulang...", System.Drawing.Color.Yellow);
                    goto sendEmailWhenLogin;
                }
                else
                {
                    mainWindow.Log("Login tokopedia gagal, Silahkan cek data login / koneksi internet anda.", System.Drawing.Color.Red);
                    isDone = true;
                    return;
                }
            }

        selectMethod:
            try
            {
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                LetsWait(5).Until(driver => driver.FindElements(By.ClassName("cotp__full")).Count > 0);
                if (driver.FindElement(By.ClassName("cotp__full")) != null)
                {
                    mainWindow.Log("Login tokopedia membutuhkan verifikasi dengan kode OTP, silahkan pilih metode verifikasi berikut :");
                    mainWindow.Log("1. Melalui Whatsapp");
                    mainWindow.Log("2. Melalui SMS");
                    mainWindow.Log("Ketikan 'skip' untuk melanjutkan proses tanpa sinkronisasi tokopedia.");
                    mainWindow.Log("Silahkan ketik angka pilihan anda.");

                    selectOTPmethod:
                    mainWindow.command = "";
                    while (mainWindow.command.Equals(""))
                    {
                        Console.WriteLine("Waiting for command");
                        await Task.Delay(1).ConfigureAwait(false);
                    }
                    if (mainWindow.command.Equals("1"))
                    {
                        var elements = driver.FindElements(By.Id("cotp__method--wa"));
                        if (elements.Count > 0)
                        {
                            elements[0].Click();
                        }
                        else
                        {
                            mainWindow.Log("Pengiriman melalui whatsapp gagal, Silahkan pilih metode lain.");
                            goto selectOTPmethod;
                        }
                    }
                    else if (mainWindow.command.Equals("2"))
                    {
                        var elements = driver.FindElements(By.Id("cotp__method--sms"));
                        if (elements.Count > 0)
                        {
                            elements[0].Click();
                        }
                        else
                        {
                            mainWindow.Log("Pengiriman melalui sms gagal, Silahkan pilih metode lain.");
                            goto selectOTPmethod;
                        }
                    }
                    else if (mainWindow.command.Equals("skip"))
                    {
                        mainWindow.Log("Login tokopedia gagal, kode otp dibatalkan.", System.Drawing.Color.Red);
                        driver.Navigate().GoToUrl("https://seller.tokopedia.com/home");
                        isDone = true;
                        return;
                    }
                    else
                    {
                        mainWindow.Log("Silahkan ketik angka pilihan anda dengan benar.");
                        goto selectOTPmethod;
                    }
                }
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                currentLoginTry = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Login berhasil");
            }

        needOtp:
            isOtp = false;
            try
            {
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                LetsWait(5).Until(driver => driver.FindElements(By.ClassName("cotp__full")).Count > 0);
                isOtp = driver.FindElement(By.ClassName("cotp__full")) != null;
                if (isOtp)
                {
                    mainWindow.Log("Silahkan ketikan 4 digit KODE OTP yang masuk ke perangkat anda disini.");
                    mainWindow.Log("Ketikan 'skip' untuk melanjutkan proses tanpa sinkronisasi tokopedia.");
                    mainWindow.Log("Ketikan 'resend' untuk mengirim ulang kode OTP.");
                    mainWindow.Log("Ketika 'back' untuk kembali ke pemilihan metode verifikasi.");
                    mainWindow.Log("");
                    mainWindow.command = "";
                    while (mainWindow.command.Equals(""))
                    {
                        Console.WriteLine("Waiting for command");
                        //mainWindow.Log("waiting for command : "+mainWindow.command);
                        await Task.Delay(1000).ConfigureAwait(false);
                    }
                    if (mainWindow.command.Equals("skip"))
                    {
                        mainWindow.command = "";
                        mainWindow.Log("Login tokopedia gagal, kode otp dibatalkan.", System.Drawing.Color.Red);
                        driver.Navigate().GoToUrl("https://seller.tokopedia.com/home");
                        isDone = true;
                        return;
                    }
                    else if (mainWindow.command.Equals("resend"))
                    {
                        mainWindow.command = "";
                        mainWindow.Log("OTP akan dikirimkan ulang.");
                        driver.FindElement(By.ClassName("cotp__text--resend")).Click();
                        goto needOtp;
                    }
                    else if (mainWindow.command.Equals("back"))
                    {
                        mainWindow.command = "";
                        driver.FindElement(By.ClassName("back-arrow")).Click();
                        await Task.Delay(1).ConfigureAwait(false);
                        goto selectMethod;
                    }
                    else
                    {
                        if (mainWindow.command.Length != 4)
                        {
                            mainWindow.Log("Kode OTP harus 4 digit angka.", System.Drawing.Color.Red);
                            goto needOtp;
                        }
                        var els = driver.FindElements(By.ClassName("otp-number-input"));
                        for (int i = 0; i < els.Count; i++)
                        {
                            els[i].Clear();
                            els[i].SendKeys(mainWindow.command[i].ToString());
                        }
                        driver.FindElement(By.Id("confirm")).Click();
                        mainWindow.Log("Mengirim kode OTP...");
                        mainWindow.command = "";
                    }
                }
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                Console.WriteLine("Login berhasil");
            }


            try
            {
                if (isOtp)
                {
                    await Task.Delay(3000).ConfigureAwait(false);
                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                    var noerrors = driver.FindElements(By.ClassName("invisible"));
                    if (noerrors.Count == 0)
                    {
                        mainWindow.Log("Kode OTP salah.", System.Drawing.Color.Red);
                        Console.WriteLine("OTP Denied");
                        goto needOtp;
                    }
                    else
                    {
                        driver.FindElement(By.ClassName("unf-user-toaster__body--error"));
                        mainWindow.Log("Pengisian Kode OTP bermasalah.", System.Drawing.Color.Red);
                        Console.WriteLine("OTP Denied");
                        goto needOtp;
                    }
                }
            }
            catch (Exception)
            {
                mainWindow.Log("Kode OTP sedang diproses...");
                Console.WriteLine("OTP Received");
            }


            driver.Navigate().GoToUrl("https://seller.tokopedia.com/home");
            LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));

        checkBannerAfterLogin:
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
            try
            {
                LetsWait().Until(driver => driver.PageSource.Contains("shopID"));
                shopId = driver.PageSource.Split(new[] { "\"shopID\":\"" }, StringSplitOptions.RemoveEmptyEntries)[1].Split(new[] { "\"" }, StringSplitOptions.RemoveEmptyEntries)[0];
                //mainWindow.Log(shopId);
                if (!Regex.IsMatch(shopId, @"^\d+$")) {
                    try
                    {
                        LetsWait().Until(driver => driver.FindElements(By.CssSelector("img[data-testid='imgSellerSidebarProfile']")).Count > 0);
                        shopId = driver.FindElement(By.CssSelector("img[data-testid='imgSellerSidebarProfile']")).GetAttribute("src").Split('/')[10];
                        //mainWindow.Log(shopId);
                    }
                    catch (Exception e)
                    {
                        shopId = "";
                        mainWindow.Log(e.Message);
                    }
                    if (!Regex.IsMatch(shopId, @"^\d+$"))
                    {
                        isDone = true;
                        mainWindow.Log("Koneksi tokopedia gagal, gagal menemukan id toko.", System.Drawing.Color.Red);
                        return;
                    }
                           
                }

                currentLoginTry = 0;

                IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                await Task.Delay(2000).ConfigureAwait(false);
                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                var checkbox = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog').querySelector('iron-pages > div > settings-checkbox:nth-child(3)').shadowRoot.querySelector('cr-checkbox')");
                if (checkbox.GetAttribute("checked") != null)
                {
                    checkbox.Click();
                }
                button.Click();

                isDone = true;
                mainWindow.Log("Proses login tokopedia berhasil.", System.Drawing.Color.Green);
            }
            catch (Exception e)
            {
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    isDone = true;
                    mainWindow.Log(e.Message + e.StackTrace, Color.Red);
                    mainWindow.Log("Login tokopedia gagal, Silahkan cek koneksi internet anda.", System.Drawing.Color.Red);
                    return;
                }
            }

            loginResponse = "";
            isLoggedIn = true;
        }

        public void RemoveProductFromCache(string sku, bool isSave = false)
        {
            logControl.Log("removing sku " + sku);
            cache.products.Remove(sku);
            if (isSave) SaveCache();
        }

        void SaveCache()
        {
            cache.lastupdate = DateTime.Now;
            var json = JsonConvert.SerializeObject(cache);
            File.WriteAllText(cache_file, json);
            File.Copy(cache_file, cache_file + ".backup", true);
        }

        void LoadCache()
        {
            try
            {
                var data = File.ReadAllText(cache_file);
                cache = JsonConvert.DeserializeObject<CacheTokopediaProduct>(data);
                if (cache == null)
                {
                    cache = new CacheTokopediaProduct();
                }
                else
                {
                    if (cache.products == null)
                    {
                        cache.products = new Dictionary<string, TokopediaProduct>();
                    }
                    if (cache.marketID == null)
                    {
                        cache.marketID = new Dictionary<string, string>();
                    }
                    if (cache.sales == null)
                    {
                        cache.sales = new Dictionary<string, TokopediaSale>();
                    }
                }
            }
            catch (Exception)
            {
                cache = new CacheTokopediaProduct();
            }
        }

        public void ResetCache()
        {
            cache.products.Clear();
            SaveCache();
        }

        public void ResetSaleCache()
        {
            cache.sales.Clear();
            SaveCache();
        }

        async Task SendSalesToMonitoringAsync(List<TokopediaSale> sales)
        {
            logControl.Log("Mengirim data penjualan Tokopedia ke monitoring...");
            var config = Program.config.profiles[idxProfile];
            var data = "";
            foreach (var item in sales)
            {
                logControl.timeoutSecs = 0;
                if (!data.Equals(""))
                {

                    data += "#R#";
                }

                var c = Program.config.branchID + "|C|" +
                        config.id + "|C|" +
                        Program.Base64Encode(item.invoice) + "|C|" +
                        Program.Base64Encode(item.date.ToString("yyyy-MM-dd HH:mm")) + "|C|" +
                        Program.Base64Encode(item.buyer) + "|C|" +
                        Program.Base64Encode(item.address) + "|C|" +
                        (item.products) + "|C|" +
                        Program.Base64Encode(item.shipname) + "|C|" +
                        Program.Base64Encode(item.total.ToString()) + "|C|" +
                        Program.Base64Encode(item.link) + "|C|" +
                        item.type + "|C|" +
                        (item.is_pass ? 1 : 0);
                data = data + c;
            }
            try
            {
                
                var url = Program.config.monitorURL.AppendPathSegment("/api/marketplace_sales_sync");
                File.WriteAllText(log_sales_file, data);
                var resp = await url
                .PostMultipartAsync((mp) =>
                {
                    mp.AddString("id_branch", Program.config.branchID)
                    .AddString("id_profile", config.id.ToString())
                    .AddString("market_name", marketName.ToLower())
                    .AddFile("new_data", log_sales_file);
                }).ReceiveString().ConfigureAwait(false);

                var obj = SimpleJSON.JSON.Parse(resp);
                if (obj != null)
                {
                    if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                    {
                        logControl.Log("Pengiriman sukses..");
                    }
                    else
                    {
                        logControl.Log(resp, Color.Red);
                    }
                }
                else
                {
                    logControl.Log(resp, Color.Red);
                }
            }
            catch (Exception e)
            {
                logControl.Log(e.Message + " " + e.StackTrace);
            }
        }

        public void SyncCache()
        {
            var config = Program.config.profiles[idxProfile];
            foreach (var source in mainWindow.cache_product.data)
            {
                if (cache.marketID.ContainsKey(source.Value.sku))
                {
                    TokopediaProduct p = new TokopediaProduct();
                    p.id_ipos = source.Value.id;
                    p.sku = source.Value.sku;
                    p.level_index = config.tokped_price;
                    if (p.level_index == 1)
                    {
                        p.level_price = source.Value.price1;
                    }
                    else if (p.level_index == 2)
                    {
                        p.level_price = source.Value.price2;
                    }
                    else if (p.level_index == 3)
                    {
                        p.level_price = source.Value.price3;
                    }
                    else if (p.level_index == 4)
                    {
                        p.level_price = source.Value.price4;
                    }

                    p.stock = source.Value.stock;

                    if (cache.products.ContainsKey(p.sku))
                    {
                        cache.products[p.sku] = p;
                    }
                    else
                    {
                        cache.products.Add(p.sku, p);
                    }
                }
            }
            SaveCache();
        }

        public void SyncSalesToMonitoring()
        {
            logControl.Log("Melakukan proses penyamaan data penjualan ke monitoring...");
            if (cache.sales.Count == 0)
            {
                logControl.Log("Tidak ada data penjualan yang bisa dikirim ke monitoring.");
                return;
            }
            List<TokopediaSale> sales = new List<TokopediaSale>();
            foreach (var item in cache.sales)
            {
                sales.Add(item.Value);
            }
            Task.Run(() => SendSalesToMonitoringAsync(sales)).Wait();
        }

        public void TriggerImportSalesToIPOS(FileInfo excel)
        {
            var config = Program.config.profiles[idxProfile];
            logControl.isClearLog = true;
            logControl.isWorking = true;
            Task.Run(() => ImportSalesToIPOS(config,liveProducts, excel)).Wait();
            logControl.isClearLog = false;
            logControl.isWorking = false;
            MessageBox.Show("File penjualan sudah diproses.");
        }

        async Task ImportSalesToIPOS(ProfileConfig config, Dictionary<string, Product> ALiveProducts, FileInfo excel)
        {
            newSales.Clear();
            using (ExcelPackage xlPackage = new ExcelPackage(excel))
            {
                var myWorksheet = xlPackage.Workbook.Worksheets.First(); //select sheet here
                var totalRows = myWorksheet.Dimension.End.Row;
                var totalColumns = myWorksheet.Dimension.End.Column;
                var realRows = totalRows - 5;

                var sb = new StringBuilder(); //this is your data
                var nos = 0;
                var parent_invoice = "";
                var parent_sale = new TokopediaSale();
                for (int rowNum = 5; rowNum <= totalRows; rowNum++) //select starting row here
                {
                    var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString()).ToArray<string>();

                    nos++;

                    logControl.timeoutSecs = 0;
                    if (mainWindow.isSyncStopped()) return;


                    var col_invoice = System.Web.HttpUtility.HtmlDecode(row[2].Trim());
                    var col_date = System.Web.HttpUtility.HtmlDecode(row[3].Trim());
                    var col_status = System.Web.HttpUtility.HtmlDecode(row[4].Trim());
                    var col_id = System.Web.HttpUtility.HtmlDecode(row[5].Trim());
                    var col_name = System.Web.HttpUtility.HtmlDecode(row[6].Trim());
                    var col_qty = System.Web.HttpUtility.HtmlDecode(row[7].Trim());
                    var col_sku = System.Web.HttpUtility.HtmlDecode(row[8].Trim());
                    var col_note = System.Web.HttpUtility.HtmlDecode(row[9].Trim());
                    var col_price = System.Web.HttpUtility.HtmlDecode(row[10].Trim());
                    var col_cust_name = System.Web.HttpUtility.HtmlDecode(row[13].Trim());
                    var col_cust_phone = System.Web.HttpUtility.HtmlDecode(row[14].Trim());
                    var col_rec_name = System.Web.HttpUtility.HtmlDecode(row[15].Trim());
                    var col_rec_phone = System.Web.HttpUtility.HtmlDecode(row[16].Trim());
                    var col_rec_address = System.Web.HttpUtility.HtmlDecode(row[17].Trim());
                    var col_courier = System.Web.HttpUtility.HtmlDecode(row[18].Trim());
                    var col_ship_price = System.Web.HttpUtility.HtmlDecode(row[21].Trim());
                    var col_total = System.Web.HttpUtility.HtmlDecode(row[22].Trim());
                    var col_awb = System.Web.HttpUtility.HtmlDecode(row[23].Trim());
                    //logControl.Log(String.Join("\n", row));
                    //logControl.Log("");

                    if (col_id.Equals(""))
                    {
                        continue;
                    }

                    if (!col_invoice.Equals("") && col_invoice != null)
                    {
                        if (parent_invoice != col_invoice)
                        {
                            parent_sale = new TokopediaSale();

                            var numbers = col_invoice.Split('/');
                            parent_sale.invoice = config.tokped_sale_prefix + numbers[numbers.Length - 1];
                            parent_sale.link = "";
                            parent_sale.date = DateTime.Now;
                            parent_sale.type = config.tokped_jual_tujuan;
                            parent_sale.status = col_status;
                            parent_sale.buyer = col_rec_name.ToUpper().Replace("'","''");
                            parent_sale.address = col_rec_address.ToUpper().Replace("'", "''");
                            parent_sale.shipname = col_courier.ToUpper().Replace("'", "''");
                            parent_sale.shiptrack = col_awb.ToUpper().Replace("'", "''");
                            long.TryParse(col_ship_price.Replace("Rp", "").Replace(".", "").Trim(), out parent_sale.shipfee);
                            long.TryParse(col_total.Replace("Rp", "").Replace(".", "").Trim(), out parent_sale.total);
                            parent_sale.products = "";
                            parent_sale.is_pass = true;
                            parent_sale.subtotal = 0;
                        }
                        parent_invoice = col_invoice;
                        //logControl.Log("Number row : " + parent_invoice);
                        //logControl.Log(JsonConvert.SerializeObject(parent_sale));
                    }
                    else
                    {
                        //logControl.Log("Item row : "+parent_invoice);
                        //logControl.Log(JsonConvert.SerializeObject(parent_sale));
                    }

                    var tempProduct = "";
                    var is_pass = true;
                    int qty = 0;
                    long price = 0;
                    var name = col_name;
                    int.TryParse(col_qty, out qty);
                    long.TryParse(col_price.Replace("Rp", "").Replace(".", "").Trim(), out price);
                    parent_sale.subtotal += qty * price;
                    var sku = col_sku;
                    if (!sku.Equals(""))
                    {
                        is_pass = liveProducts.ContainsKey(sku);
                        if (!is_pass)
                        {
                            parent_sale.is_pass = false;
                        }
                        else
                        {
                            if (config.tokped_jual_tujuan > 0)
                            {
                                var cp = liveProducts[sku];
                                if (cp != null && cp.stockdetail != null && cp.stockdetail.ContainsKey(config.tokped_sale_warehousecode))
                                {
                                    if (qty > cp.stockdetail[config.tokped_sale_warehousecode].stock && config.tokped_jual_stokminus == 0)
                                    {
                                        is_pass = false;
                                        parent_sale.is_pass = false;
                                    }
                                }
                                else
                                {
                                    is_pass = false;
                                    parent_sale.is_pass = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        sku = "";
                        parent_sale.is_pass = false;
                        is_pass = false;
                    }

                    tempProduct = col_name + "|#|" + sku + "|#|" + qty + "|#|" + price + "|#|" + (is_pass ? 1 : 0) + "|#|" + Program.Base64Encode(col_note);
                    if (parent_sale.products != "")
                    {
                        parent_sale.products = parent_sale.products + ";";
                    }
                    parent_sale.products = parent_sale.products + tempProduct;

                    var isEndInvoice = false;
                    if (rowNum < totalRows)
                    {
                        var rowNext = myWorksheet.Cells[rowNum + 1, 1, rowNum + 1, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString()).ToArray<string>();
                        if ((parent_invoice != rowNext[2] && !rowNext[2].Trim().Equals("") && rowNext[2] != null) || rowNext[5].Trim().Equals(""))
                        {
                            isEndInvoice = true;
                        }
                    }
                    else
                    {
                        isEndInvoice = true;
                    }

                    if (isEndInvoice)
                    {
                        if (config.tokped_sale_tab.Equals("confirm_shipping"))
                        {
                            if (!parent_sale.status.ToLower().Trim().Contains("pemesanan sedang diproses oleh penjual."))
                            {
                                continue;
                            }
                        }
                        else if (config.tokped_sale_tab.Equals("new_order"))
                        {
                            if (!parent_sale.status.ToLower().Trim().Contains("Pembayaran telah diterima Tokopedia dan pesanan sudah diteruskan ke penjual."))
                            {
                                continue;
                            }
                        }

                        newSales.Add(parent_sale);
                        if (!cache.sales.ContainsKey(parent_sale.invoice))
                        {
                            logControl.Log(parent_sale.invoice + " berhasil diambil.", Color.Green);
                            
                            if (parent_sale.is_pass)
                            {
                                cache.sales.Add(parent_sale.invoice, parent_sale);
                            }
                            else
                            {
                                logControl.Log("Input data penjualan ke aplikasi Ipos tidak memungkinkan. (Stok kurang/SKU tidak ada).", Color.Red);
                            }
                        }
                        else
                        {
                            logControl.Log(parent_sale.invoice + " sudah diambil sebelumnya.", Color.Yellow);
                            continue;
                        }
                    }
                    if(newSales.Count >= 25)
                    {
                        await SendSalesByPart(config).ConfigureAwait(false);
                    }
                }
                await SendSalesByPart(config).ConfigureAwait(false);
            }
        }

        WebDriverWait LetsWait(int waitSec = -1)
        {
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, driver, TimeSpan.FromSeconds(waitSec == -1 ? maxMainWait : waitSec), TimeSpan.FromMilliseconds(100));
            wait.PollingInterval = TimeSpan.FromMilliseconds(100);
            return wait;
        }

        async Task SendSalesByPart(ProfileConfig config)
        {
            if (newSales.Count > 0)
            {
                logControl.Log(newSales.Count + " data penjualan sedang dicoba dikirimkan...", System.Drawing.Color.Yellow);

                if (mainWindow.isSyncStopped()) return;

                if (mainWindow.instance_ipos != null)
                {
                    List<String> successSales;
                    if (config.tokped_jual_tujuan == 0)
                    {
                        IPOSOptionSalesOrder options = new IPOSOptionSalesOrder();
                        options.customer = config.tokped_sale_custcode;
                        options.seller = config.tokped_sale_sellercode;
                        options.warehouse = config.tokped_sale_warehousecode;
                        options.userid = config.tokped_sale_userid;
                        successSales = await mainWindow.instance_ipos.SyncSalesToOrder(getIposSalesOrder(), options).ConfigureAwait(false);
                        if (successSales != null)
                        {
                            logControl.Log(successSales.Count + " dari " + newSales.Count + " data penjualan telah masuk ke pesanan penjualan IPOS.");
                        }
                        else
                        {
                            logControl.Log(newSales.Count + " data penjualan gagal masuk ke pesanan penjualan IPOS.",Color.Red);
                        }
                    }
                    else if (config.tokped_jual_tujuan == 1)
                    {
                        IPOSOptionOfficeSale options = new IPOSOptionOfficeSale();
                        options.customer = config.tokped_sale_custcode;
                        options.seller = config.tokped_sale_sellercode;
                        options.warehouse = config.tokped_sale_warehousecode;
                        options.paymethod = config.tokped_jual_tipebayar;
                        options.acc_tunai = config.tokped_sale_acctunai;
                        options.acc_kredit = config.tokped_sale_acckredit;
                        options.acc_pot = config.tokped_sale_accpot;
                        options.acc_blain = config.tokped_sale_accblain;
                        options.userid = config.tokped_sale_userid;
                        successSales = await mainWindow.instance_ipos.SyncSalesToOfficeSale(getIposOfficeSale(), options).ConfigureAwait(false);
                        if (successSales != null)
                        {
                            logControl.Log(successSales.Count + " dari " + newSales.Count + " data penjualan telah masuk ke penjualan IPOS.");
                        }
                        else
                        {
                            logControl.Log(newSales.Count + " data penjualan gagal masuk ke penjualan IPOS.", Color.Red);
                        }
                    }
                    else
                    {
                        IPOSOptionCashierSale options = new IPOSOptionCashierSale();
                        options.customer = config.tokped_sale_custcode;
                        options.seller = config.tokped_sale_sellercode;
                        options.warehouse = config.tokped_sale_warehousecode;
                        options.paymethod = config.tokped_jual_tipebayar;
                        options.acc_tunai = config.tokped_sale_acctunai;
                        options.acc_kredit = config.tokped_sale_acckredit;
                        options.acc_pot = config.tokped_sale_accpot;
                        options.acc_blain = config.tokped_sale_accblain;
                        options.userid = config.tokped_sale_userid;
                        successSales = await mainWindow.instance_ipos.SyncSalesToCashierSale(getIposCashierSale(), options).ConfigureAwait(false);
                        if (successSales != null)
                        {
                            logControl.Log(successSales.Count + " dari " + newSales.Count + " data penjualan kasir telah masuk ke penjualan IPOS.");
                        }
                        else
                        {
                            logControl.Log(newSales.Count + " data penjualan kasir gagal masuk ke penjualan kasir IPOS.", Color.Red);
                        }
                    }

                    for (int i = newSales.Count-1; i >= 0; i--)
                    {
                        var item = newSales[i];
                        if (successSales == null || !successSales.Contains(item.invoice))
                        {
                            //if(cache.sales.ContainsKey(item.invoice)) cache.sales.Remove(item.invoice);
                            newSales.RemoveAt(i);
                        }
                    }
                }
                if (newSales.Count > 0)
                {
                    await SendSalesToMonitoringAsync(newSales).ConfigureAwait(false);
                }
                SaveCache();
                newSales.Clear();
            }
            else
            {
                logControl.Log("Tidak ada lagi data penjualan yang akan dikirimkan pada hari ini.");
            }
            return;
        }


        public async Task Sync(Dictionary<string, SingleProduct> listProducts, Dictionary<string, Product> ALiveProducts = null, bool forceCheckId = false)
        {
            liveProducts = ALiveProducts;
            mainWindow.Log("Memulai pengecekan sinkronisasi Tokopedia.", System.Drawing.Color.Yellow);
            mainWindow.Log("Silahkan lihat tab 'Tokopedia' untuk melihat laporan sinkronisasi.");
            LoadCache();
            isSyncDone = false;
            isStuck = false;
            while (logControl == null)
            {
                await Task.Delay(1000).ConfigureAwait(false);
            }
            logControl.isClearLog = true;
            logControl.isWorking = true;
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            var config = Program.config.profiles[idxProfile];

            driver.Navigate().GoToUrl("https://seller.tokopedia.com/settings/info");
            await Task.Delay(100).ConfigureAwait(false);
            LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            if (driver.Url.Contains("accounts.tokopedia.com"))
            {
                mainWindow.Log("Melakukan proses login ulang...");
                await LoginAsync().ConfigureAwait(false);
            }
            if (config.tokped_jual_on)
            {
                newSales.Clear();
                //mainWindow.Log("cache sales : " + cache.sales.Count.ToString());
                logControl.Log("Memulai pengecekan data penjualan Tokopedia.", System.Drawing.Color.Yellow);
                driver.Navigate().GoToUrl("https://seller.tokopedia.com/myshop_order?status=" + config.tokped_sale_tab);
                LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));

                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                var idx = 0;
                var curPage = "1";
                loopSales:
                try
                {
                    logControl.timeoutSecs = 0;
                    await Task.Delay(3000).ConfigureAwait(false);
                    LetsWait(60).Until(driver => (driver.FindElements(By.CssSelector("[class*='unf-emptystate']")).Count > 0 || driver.FindElements(By.ClassName("empty-new-orders")).Count > 0 || driver.FindElements(By.CssSelector("[data-testid='somOrderList'] section")).Count > 0));
                    if (true)
                    {
                        //try
                        //{
                        //    LetsWait(10).Until(ExpectedConditions.ElementExists(By.CssSelector("div[aria-label='unf-overlay']")));
                        //    executor.ExecuteScript("document.querySelector('div[aria-label=\"unf-overlay\"]').remove();");
                        //    executor.ExecuteScript("document.querySelector('div[class*=\"-unf-modal\"]').remove();");
                        //    logControl.Log("Popup ditutup.");
                        //}
                        //catch (Exception e)
                        //{
                        //    logControl.Log("Tidak ada gangguan popup.");
                        //}
                        LetsWait(60).Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".right-section #download button")));
                        var buttons = driver.FindElements(By.CssSelector(".right-section #download button"));
                        if (buttons.Count > 0)
                        {
                            string userPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                            string downloadPath = Path.Combine(userPath, "Downloads");
                            DirectoryInfo dirInfo = new DirectoryInfo(downloadPath);
                            if (!dirInfo.Exists)
                            {
                                dirInfo.Create();
                            }
                            int directoryFiles = dirInfo.EnumerateFiles().Count();

                            //logControl.Log("Click button");
                            LetsWait(5).Until(ExpectedConditions.ElementToBeClickable(buttons[0]));
                            executor.ExecuteScript("arguments[0].click();", buttons[0]);
                            //logControl.Log("wait for sibling");
                            //LetsWait(60).Until(driver => (buttons[0].FindElements(By.XPath("following-sibling::*")).Count > 0));
                            LetsWait().Until(driver => driver.FindElements(By.CssSelector("div[data-testid='divSOMDatePicker']")).Count > 0);
                            //LetsWait().Until(driver => ExpectedConditions.ElementToBeClickable(buttons[0].FindElement(By.XPath("following-sibling::*"))));
                            //var modal = buttons[0].FindElement(By.XPath("following-sibling::*"));
                            var modal = driver.FindElement(By.CssSelector("div[data-testid='divSOMDatePicker']"));

                            //logControl.Log("select datemode and click");
                            //var datemode = modal.FindElement(By.CssSelector(".section .small"));
                            //datemode.Click();
                            //logControl.Log("wait for sibling");
                            //LetsWait(5).Until(driver => (datemode.FindElements(By.XPath("following-sibling::*")).Count > 0));
                            //var list = datemode.FindElement(By.XPath("following-sibling::*"));
                            //logControl.Log("click option 2");
                            //list.FindElement(By.CssSelector("li:nth-child(2)")).Click();
                            var now = DateTime.Now.ToString("MMM dd yyyy");
                            var yesterday = DateTime.Now.AddDays(-1).ToString("MMM dd yyyy");
                            //logControl.Log(now);
                            //logControl.Log(yesterday);
                            executor.ExecuteScript("arguments[0].click();", modal.FindElement(By.CssSelector(".DayPicker [aria-label$='" + now + "']")));
                            await Task.Delay(1000).ConfigureAwait(false);

                            var elYesterday = modal.FindElements(By.CssSelector(".DayPicker [aria-label$='" + yesterday + "']"));
                            if (elYesterday.Count == 0)
                            {
                                try
                                {

                                    executor.ExecuteScript("arguments[0].click();", modal.FindElement(By.CssSelector("div[data-testid='divSOMDatePicker']")));
                                    //executor.ExecuteScript("arguments[0].click();", modal.FindElement(By.CssSelector(".DayPicker .left")).FindElement(By.XPath("..")));
                                    //executor.ExecuteScript("arguments[0].click();", modal.FindElement(By.CssSelector(".DayPicker .DayPicker-NavButton--prev")));
                                }
                                catch (Exception e)
                                {
                                    logControl.Log(e.Message);
                                    logControl.Log("Gagal mencari tanggal sebelumnya, menggunakan tanggal sekarang...");
                                    yesterday = now;
                                }
                            }
                            executor.ExecuteScript("arguments[0].click();", modal.FindElement(By.CssSelector(".DayPicker [aria-label$='" + yesterday + "']")));
                            await Task.Delay(1000).ConfigureAwait(false);

                            //logControl.Log("wait for clickable");
                             
                            LetsWait(5).Until(ExpectedConditions.ElementToBeClickable(driver.FindElement(By.XPath("//span[contains(text(),'Minta Laporan')]"))));
                            logControl.Log("Mengunduh data penjualan...");
                            executor.ExecuteScript("arguments[0].click();", driver.FindElement(By.XPath("//span[contains(text(),'Minta Laporan')]")).FindElement(By.XPath("..")));
                            //logControl.Log("comparing files");
                            int currentFiles = dirInfo.EnumerateFiles().Count();
                            int countTimeout = 0;
                            while(currentFiles <= directoryFiles)
                            {
                                dirInfo = new DirectoryInfo(downloadPath);
                                currentFiles = dirInfo.EnumerateFiles().Count();
                                await Task.Delay(1000).ConfigureAwait(false);
                                countTimeout++;
                                if(countTimeout >= 10)
                                {
                                    logControl.Log("Gagal dalam proses mengambil penjualan tokopedia.", System.Drawing.Color.Red);
                                    goto skipSales;
                                }
                            }
                            //logControl.Log("getting excel "+ dirInfo.GetFiles().Length);
                            var excel = dirInfo.GetFiles()
                             .Where(s => s.Extension.Equals(".xlsx"))
                             .OrderByDescending(f => f.LastWriteTime)
                             .First();
                            logControl.Log(excel.ToString());
                            logControl.Log("Membaca data penjualan...");
                            await ImportSalesToIPOS(config, liveProducts, excel).ConfigureAwait(false);
                            excel.Delete();
                        }
                        else
                        {
                            logControl.Log("Data penjualan tidak ditemukan.");
                        }
                    }
                    else {
                        #region "Old Sale Scrape"
                        var pContainer = driver.FindElements(By.ClassName("bulk-action-container"));
                        if (pContainer.Count > 0)
                        {
                            var paginations = pContainer[0].FindElements(By.ClassName("css-guql3t"));
                            if (paginations.Count > 0)
                            {
                                while (idx < paginations.Count)
                                {
                                    logControl.timeoutSecs = 0;
                                    if (mainWindow.isSyncStopped()) return;

                                    var overlays = driver.FindElements(By.CssSelector("[aria-label='unf-overlay']"));
                                    if (overlays.Count > 0)
                                    {
                                        executor.ExecuteScript("arguments[0].nextSibling.remove();", overlays[0]);
                                        executor.ExecuteScript("arguments[0].remove();", overlays[0]);
                                    }

                                    if (idx > 0)
                                    {
                                        pContainer = driver.FindElements(By.ClassName("bulk-action-container"));
                                        if (pContainer.Count > 0)
                                        {
                                            paginations = pContainer[0].FindElements(By.ClassName("css-guql3t"));
                                        }
                                        if (idx <= paginations.Count - 1)
                                        {
                                            logControl.Log("Halaman selanjutnya...");
                                            executor.ExecuteScript("arguments[0].scrollIntoView(false);", paginations[idx]);
                                            Actions action = new Actions(driver);
                                            var isChangeIdx = paginations[idx].FindElements(By.TagName("i")).Count > 0;
                                            if (!isChangeIdx)
                                            {
                                                curPage = paginations[idx].Text;
                                            }
                                            try
                                            {
                                                action.MoveToElement(paginations[idx]).Click().Perform();
                                                LetsWait(5).Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("product-list")));
                                            }
                                            catch (Exception e)
                                            {
                                                pContainer = driver.FindElements(By.ClassName("bulk-action-container"));
                                                if (pContainer.Count == 0)
                                                {
                                                    goto skipSales;
                                                }
                                                var newCount = pContainer[0].FindElements(By.ClassName("css-guql3t")).Count;
                                                if (idx > (newCount - 1) && newCount < paginations.Count)
                                                {
                                                    goto skipSales;
                                                }
                                            }
                                            LetsWait(60).Until(driver => (driver.FindElements(By.CssSelector("[class*='unf-emptystate']")).Count > 0 || driver.FindElements(By.ClassName("empty-new-orders")).Count > 0 || driver.FindElements(By.ClassName("product-list")).Count > 0));
                                            logControl.timeoutSecs = 0;
                                            if (isChangeIdx)
                                            {
                                                logControl.Log("New page list...");
                                                //jika ganti list page, maka cek kembali jumlahnya
                                                idx = 1;
                                                pContainer = driver.FindElements(By.ClassName("bulk-action-container"));
                                                paginations = pContainer[0].FindElements(By.ClassName("css-guql3t"));
                                            }
                                        }
                                        else
                                        {
                                            goto skipSales;
                                        }
                                    }

                                    overlays = driver.FindElements(By.CssSelector("[aria-label='unf-overlay']"));
                                    if (overlays.Count > 0)
                                    {
                                        executor.ExecuteScript("arguments[0].nextSibling.remove();", overlays[0]);
                                        executor.ExecuteScript("arguments[0].remove();", overlays[0]);
                                    }

                                    await Task.Delay(1000).ConfigureAwait(false);

                                    //try
                                    //{
                                    //    ((IJavaScriptExecutor)driver).ExecuteScript("window.open();");
                                    //    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    //    driver.SwitchTo().DefaultContent();
                                    //    driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                                    //    await Task.Delay(2000).ConfigureAwait(false);
                                    //    var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                                    //    button.Click();
                                    //    ((IJavaScriptExecutor)driver).ExecuteScript("window.close();");
                                    //    driver.SwitchTo().Window(driver.WindowHandles.First());
                                    //    driver.SwitchTo().DefaultContent();
                                    //}
                                    //catch (Exception)
                                    //{
                                    //    logControl.Log("Cannot clear cache.");
                                    //}

                                    var sales = driver.FindElements(By.CssSelector("section[class*='-unf-card']"));
                                    if (sales.Count > 0)
                                    {
                                        logControl.Log("Mengambil data penjualan halaman " + curPage + "...");
                                        //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                                        var nos = 0;
                                        foreach (var sale in sales)
                                        {
                                            if (sale.FindElements(By.ClassName("product-list")).Count == 0)
                                            {
                                                continue;
                                            }
                                            nos++;
                                            logControl.timeoutSecs = 0;
                                            if (mainWindow.isSyncStopped()) return;
                                            if (sale.GetAttribute("class").Contains("error")) continue;
                                            executor.ExecuteScript("arguments[0].scrollIntoView(false);", sale);

                                            TokopediaSale objSale = new TokopediaSale();
                                            var summary = sale.FindElement(By.ClassName("order-summary-wrapper"));

                                            var invoice = summary.FindElement(By.TagName("a"));
                                            var numbers = invoice.Text.Split('/');
                                            objSale.invoice = config.tokped_sale_prefix + numbers[numbers.Length - 1];

                                            if (!cache.sales.ContainsKey(objSale.invoice))
                                            {
                                                logControl.Log("(" + nos + "/" + (sales.Count - 1) + ") " + invoice.Text + " berhasil diambil.", Color.Green);
                                            }
                                            else
                                            {
                                                logControl.Log("(" + nos + "/" + (sales.Count - 1) + ") " + invoice.Text + " sudah diambil sebelumnya.", Color.Yellow);
                                                continue;
                                            }

                                            objSale.link = invoice.GetAttribute("href");

                                            var date = summary.FindElement(By.ClassName("order-date"));
                                            var datetext = date.Text.Substring(0, date.Text.Length - 4);
                                            //objSale.date = DateTime.ParseExact(datetext, "dd MMM yyyy HH:mm",
                                            //    System.Globalization.CultureInfo.InvariantCulture);
                                            objSale.date = DateTime.Now;

                                            //var buyer = summary.FindElement(By.ClassName("buyer-name"));
                                            //objSale.buyer = buyer.Text.ToUpper();

                                            objSale.type = config.tokped_jual_tujuan;

                                            var status = summary.FindElement(By.ClassName("label")).FindElement(By.TagName("span"));
                                            objSale.status = status.Text;

                                            var address = sale.FindElement(By.ClassName("col--address__destination")).Text.Split('\n');
                                            objSale.buyer = address[0].Trim().Replace("\r", "").ToUpper();
                                            objSale.address = address[1] + (address.Length > 2 ? address[2] : "");

                                            var shipping = sale.FindElement(By.ClassName("col--shipping")).FindElements(By.TagName("span"));
                                            //logControl.Log("shipping : "+shipping.Count.ToString());
                                            if (shipping.Count > 0)
                                            {
                                                var shipinfo = shipping[1].Text.Split('\n');
                                                objSale.shipname = shipinfo[0].Replace(shipping[2].Text.Trim(), "").Trim().ToUpper();
                                                var ongkir = shipping[2].Text.Trim().Replace(".", "");
                                                ongkir = ongkir.Substring(4, ongkir.Length - 5);
                                                long.TryParse(ongkir, out objSale.shipfee);
                                                if (shipping.Count > 3)
                                                {
                                                    objSale.shiptrack = shipping[4].Text.ToUpper();
                                                }
                                            }

                                            var total = sale.FindElement(By.ClassName("total-price"));
                                            long.TryParse(total.Text.Substring(3).Trim().Replace(".", ""), out objSale.total);

                                            var moreProduct = sale.FindElements(By.ClassName("chevron-down-circle"));
                                            if (moreProduct.Count > 0 && !moreProduct[0].GetAttribute("class").Contains("active"))
                                            {
                                                var moreProductToggle = moreProduct[0].FindElement(By.XPath("..")).FindElement(By.TagName("a"));
                                                Actions action = new Actions(driver);
                                                action.Click(moreProductToggle).Perform();
                                                executor.ExecuteScript("arguments[0].scrollIntoView(false);", moreProduct[0]);
                                                LetsWait(10).Until(driver => moreProduct[0].GetAttribute("class").Contains("active"));
                                                logControl.timeoutSecs = 0;
                                            }

                                            objSale.products = "";
                                            objSale.is_pass = true;
                                            objSale.subtotal = 0;
                                            var products = sale.FindElements(By.ClassName("product-list"));
                                            if (products.Count > 0)
                                            {
                                                var tempProducts = "";
                                                foreach (var p in products)
                                                {
                                                    var is_pass = true;
                                                    var name = p.FindElement(By.TagName("a")).Text;

                                                    int qty = 0;
                                                    long price = 0;
                                                    var productCount = p.FindElement(By.ClassName("product-list__count")).FindElements(By.TagName("span"));
                                                    if (productCount.Count > 0)
                                                    {
                                                        int.TryParse(productCount[0].Text.Split(new string[] { " " }, StringSplitOptions.None)[0], out qty);
                                                        long.TryParse(productCount[2].Text.Split(new string[] { " " }, StringSplitOptions.None)[1].Replace(".", ""), out price);
                                                    }
                                                    objSale.subtotal += qty * price;

                                                    var csku = p.FindElements(By.ClassName("product-list__sku"));
                                                    var sku = "";
                                                    if (csku.Count > 0)
                                                    {
                                                        sku = csku[0].Text.Split(new string[] { " - " }, StringSplitOptions.None)[1];
                                                        is_pass = liveProducts.ContainsKey(sku);
                                                        if (!is_pass)
                                                        {
                                                            objSale.is_pass = false;
                                                        }
                                                        else
                                                        {
                                                            if (config.tokped_jual_tujuan > 0)
                                                            {
                                                                var cp = liveProducts[sku];
                                                                if (cp.stockdetail.ContainsKey(config.tokped_sale_warehousecode))
                                                                {
                                                                    if (qty > cp.stockdetail[config.tokped_sale_warehousecode].stock)
                                                                    {
                                                                        is_pass = false;
                                                                        objSale.is_pass = false;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    is_pass = false;
                                                                    objSale.is_pass = false;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sku = "";
                                                        objSale.is_pass = false;
                                                        is_pass = false;
                                                    }


                                                    var enote = p.FindElement(By.ClassName("product-list__note"));
                                                    var note = "";
                                                    if (enote.FindElements(By.TagName("span")).Count == 0)
                                                    {
                                                        note = enote.Text.Substring(1, enote.Text.Length - 2);
                                                    }

                                                    if (tempProducts != "")
                                                    {
                                                        tempProducts = tempProducts + ";";
                                                    }
                                                    tempProducts = tempProducts + name + "|#|" + sku + "|#|" + qty + "|#|" + price + "|#|" + (is_pass ? 1 : 0) + "|#|" + Program.Base64Encode(note);
                                                }
                                                objSale.products = tempProducts;
                                            }
                                            else
                                            {
                                                objSale.is_pass = false;
                                            }

                                            if (!cache.sales.ContainsKey(objSale.invoice))
                                            {
                                                newSales.Add(objSale);
                                                if (objSale.is_pass)
                                                {
                                                    cache.sales.Add(objSale.invoice, objSale);
                                                }
                                                else
                                                {
                                                    logControl.Log("Input data penjualan ke aplikasi Ipos tidak memungkinkan. (Stok kurang/SKU tidak ada).", Color.Red);
                                                }
                                            }
                                        }
                                    }
                                    await SendSalesByPart(config).ConfigureAwait(false);
                                    idx++;
                                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                                }
                                //mainWindow.Log("cache sales : " + cache.sales.Count.ToString());
                            }
                            else
                            {
                                logControl.Log("Tidak ada data penjualan yang bisa diambil.");
                            }
                        }
                        else
                        {
                            logControl.Log("Data penjualan tidak ditemukan.");
                        }
                        #endregion;
                    }
                    logControl.timeoutSecs = 0;
                }
                catch (Exception e)
                {
                    logControl.Log(e.Message + e.StackTrace);
                    AcceptDialog();
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        logControl.Log("Gagal, Mencoba mengambil ulang penjualan tokopedia...", System.Drawing.Color.Yellow);
                        goto loopSales;
                    }
                    else
                    {
                        logControl.Log("Gagal dalam proses mengambil penjualan tokopedia.", System.Drawing.Color.Red);
                        
                        goto skipSales;
                    }
                }
                skipSales:
                if(newSales.Count > 0)
                {
                    await SendSalesByPart(config).ConfigureAwait(false);
                }
                if (mainWindow.isSyncStopped()) return;
            }


            currentMainTry = 0;
            if (!config.tokped_sync_on)
            {
                logControl.Log("Proses dilewati, sinkronisasi Tokopedia tidak aktif.", System.Drawing.Color.Yellow);
                isSyncDone = true;
                logControl.isWorking = false;
                isStuck = false;
                return;
            }

            if (mainWindow.isSyncStopped()) return;

            bool checkID = (config.tokped_checkid_schedule != "") && (nextCheckID != lastCheckID) && (DateTime.Now > nextCheckID && lastCheckID < nextCheckID);
            
            Dictionary<String, TokopediaProduct> UpdatingProducts = new Dictionary<String, TokopediaProduct>();
            foreach (var source in new Dictionary<string, SingleProduct>(listProducts))
            {
                logControl.timeoutSecs = 0;
                if (!source.Value.active) continue;
                var same = false;
                if (cache.products.ContainsKey(source.Value.sku))
                {
                    //logControl.Log("exist : " + source.Value.sku);
                    //mainWindow.Log("exist");
                    same = Program.CompareTokopediaProduct(source.Value, cache.products[source.Value.sku], config);
                }

                if (!same)
                {
                    TokopediaProduct p = new TokopediaProduct();
                    p.id_ipos = source.Value.id;
                    p.sku = source.Value.sku;
                    p.level_index = config.tokped_price;
                    if (p.level_index == 1)
                    {
                        p.level_price = source.Value.price1;
                        //mainWindow.Log("Harga " + source.Value.price1.ToString());
                    }
                    else if (p.level_index == 2)
                    {
                        p.level_price = source.Value.price2;
                        //mainWindow.Log("Harga " + source.Value.price2.ToString());
                    }
                    else if (p.level_index == 3)
                    {
                        p.level_price = source.Value.price3;
                        //mainWindow.Log("Harga " + source.Value.price3.ToString());
                    }
                    else if (p.level_index == 4)
                    {
                        p.level_price = source.Value.price4;
                        //mainWindow.Log("Harga " + source.Value.price4.ToString());
                    }
                    else
                    {
                        p.level_price = -1;
                    }

                    if (config.tokped_stock > 0)
                    {
                        p.stock = source.Value.stock;
                    }
                    else
                    {
                        p.stock = -1;
                    }
                    

                    UpdatingProducts.Add(p.sku, p);
                }
                if (mainWindow.isSyncStopped()) return;
            }
            if (UpdatingProducts.Count == 0)
            {
                logControl.Log("Belum ada produk untuk sinkronisasi tokopedia.", System.Drawing.Color.Yellow);
                isSyncDone = true;
                logControl.isWorking = false;
                isStuck = false;
                return;
            }
            logControl.Log("Terdapat " + UpdatingProducts.Count + " produk untuk sinkronisasi tokopedia.");
            var allCount = UpdatingProducts.Count;

            List<string> successProducts = new List<string>();
            bool needFetchID = false;

            if (mainWindow.isSyncStopped()) return;
            if (logControl.CheckID || mainWindow.command.Equals("check_id") || checkID || cache.products == null || cache.products.Count == 0 || forceCheckId)
            {
                if (checkID)
                {
                    logControl.Log("Melakukan pengecekan ID produk sesuai jadwal...");
                }
                if (forceCheckId)
                {
                    logControl.Log("Melakukan pengecekan ID produk secara spesifik...");
                }
                foreach (var item in UpdatingProducts)
                {
                    logControl.timeoutSecs = 0;
                    if (!cache.marketID.ContainsKey(item.Key))
                    { 
                        needFetchID = true;
                        break;
                    }
                    if (mainWindow.isSyncStopped()) return;
                }
                mainWindow.command = "";
                lastCheckID = DateTime.Now;
                calculateNextCheckID(config);
            }
            if (mainWindow.isSyncStopped()) return;
            if (needFetchID) {
                logControl.Log("Mencocokan data produk tokopedia...");
                //startFetchID:
                driver.Navigate().GoToUrl("https://seller.tokopedia.com/settings/info");
                LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                var page = 1;
                do
                {
                    loopProducts:
                    logControl.timeoutSecs = 0;
                    try
                    {
                        var scriptRes = Properties.Resources.tokopedia_productlist_js;
                        scriptRes = scriptRes.Replace("[SHOP_ID]", shopId);
                        scriptRes = scriptRes.Replace("[CURRENT_PAGE]", page.ToString());
                        //logControl.Log(scriptRes);
                        object response = executor.ExecuteAsyncScript(scriptRes);
                        var obj = SimpleJSON.JSON.Parse((String)response);
                        /*if (config.tokped_dev.Equals(Program.devPassword))
                        {
                            logControl.Log("shopid : "+shopId.ToString());
                            logControl.Log(response.ToString());
                        }*/
                        if (obj != null && obj[0] != null && obj[0]["data"] != null && obj[0]["data"]["ProductList"] != null && (obj[0]["data"]["ProductList"]["header"]["errorCode"] == null || obj[0]["data"]["ProductList"]["header"]["reason"].ToString().Contains("max limit")))
                        {   
                            var arr = obj[0]["data"]["ProductList"]["data"].AsArray;
                            if (arr.Count > 0)
                            {
                                logControl.Log("Memeriksa "+arr.Count.ToString()+" data di halaman " + page.ToString() + "...");
                                foreach (JSONNode item in arr)
                                {
                                    if (forceCheckId)
                                    {
                                        foreach (var found in cache.marketID.Where(kvp => kvp.Value == Program.StripQuotes(item["id"].ToString())).ToList())
                                        {
                                            cache.marketID.Remove(found.Key);
                                        }
                                    }

                                    //if(item["sku"] != null)
                                    //{
                                    //    logControl.Log(item["sku"].ToString());
                                    //}
                                    if (item["isVariant"].AsBool == false)
                                    {
                                        //logControl.Log("Non variant");
                                        var sku = Program.StripQuotes(item["sku"].ToString());
                                        if (!sku.Trim().Equals(""))
                                        {
                                            if (cache.marketID.ContainsKey(sku))
                                            {
                                                cache.marketID[sku] = Program.StripQuotes(item["id"].ToString());
                                            }
                                            else
                                            {
                                                cache.marketID.Add(sku, Program.StripQuotes(item["id"].ToString()));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //logControl.Log("variant");
                                        scriptRes = Properties.Resources.tokopedia_variant_js;
                                        scriptRes = scriptRes.Replace("[PRODUCT_ID]", Program.StripQuotes(item["id"].ToString()));
                                        //logControl.Log(scriptRes);
                                        response = executor.ExecuteAsyncScript(scriptRes);
                                        var objVar = SimpleJSON.JSON.Parse((String)response);
                                        if (objVar != null && objVar[0] != null && objVar[0]["data"] != null && objVar[0]["data"]["getProductV3"] != null && objVar[0]["data"]["getProductV3"]["variant"]["products"] != null)
                                        {
                                            var variants = objVar[0]["data"]["getProductV3"]["variant"]["products"].AsArray;
                                            foreach (JSONNode vars in variants)
                                            {
                                                var sku = Program.StripQuotes(vars["sku"].ToString());
                                                if (!sku.Equals(""))
                                                {
                                                    if (cache.marketID.ContainsKey(sku))
                                                    {
                                                        cache.marketID[sku] = Program.StripQuotes(item["id"].ToString());
                                                    }
                                                    else
                                                    {
                                                        cache.marketID.Add(sku, Program.StripQuotes(item["id"].ToString()));
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            logControl.Log("Data variant produk "+ Program.StripQuotes(item["id"].ToString()) + " gagal diambil.", System.Drawing.Color.Yellow);
                                            logControl.Log((String)response);
                                        }
                                    }
                                }
                                page++;                              
                            }
                            else
                            {
                                logControl.Log("Tidak ada data di halaman " + page.ToString() + "...");
                                page = 0;
                            }
                        }
                        else
                        {
                            logControl.Log("Gagal mengambil data produk.", System.Drawing.Color.Yellow);
                            logControl.Log((String)response);
                            page = 0;
                        }
                    }
                    catch (Exception e)
                    {
                        
                        logControl.Log(e.Message);
                        AcceptDialog();
                        currentMainTry++;
                        if (currentMainTry < maxMainTry)
                        {
                            logControl.Log("Gagal, Mencoba mencocokan ulang produk tokopedia...", System.Drawing.Color.Yellow);
                            goto loopProducts;
                        }
                        else
                        {
                            page = 0;
                            logControl.Log("Proses pencocokan produk gagal. Melanjutkan proses sinkronisasi.", System.Drawing.Color.Red);
                            goto skiptosync;
                        }
                    }
                    if (mainWindow.isSyncStopped()) return;
                } while (page > 0);
            }
          

            skiptosync:
            if (mainWindow.isSyncStopped()) return;
            currentMainTry = 0;

            var deletedUpdates = new List<string>();
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!cache.marketID.ContainsKey(item.Key))
                {
                    deletedUpdates.Add(item.Key);
                }
                else
                {
                    if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos)){
                        var monitored = mainWindow.listMonitored[item.Value.id_ipos];
                        monitored.tokopedia_sync = 1;
                        monitored.tokopedia_price = item.Value.level_price;
                        monitored.tokopedia_stock = item.Value.stock;
                    }

                    if (cache.products.ContainsKey(item.Key))
                    {
                        cache.products[item.Key] = item.Value;
                    }
                    else
                    {

                        cache.products.Add(item.Key, item.Value);
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }

            if(deletedUpdates.Count > 0)
            {
                foreach (var item in deletedUpdates)
                {
                    logControl.timeoutSecs = 0;
                    UpdatingProducts.Remove(item);
                }
            }
            if(allCount != UpdatingProducts.Count)
            {
                logControl.Log("Terdapat " + (allCount - UpdatingProducts.Count) + " produk yang belum terdapat di Tokopedia. Silahkan input SKU terlebih dahulu.",Color.Yellow);
            }      

            
            //update products
             try
            {
                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                await Task.Delay(2000).ConfigureAwait(false);
                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                button.Click();
            }
            catch (Exception)
            {
                logControl.Log("Cannot clear cache.");
            }
            var noitem = 1;
            var processCounter = 1;
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!logControl.SyncMarketplace)
                {
                    if (!logControl.SendMonitoring)
                    {
                        if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos))
                        {
                            mainWindow.listMonitored[item.Value.id_ipos].tokopedia_sync = 0;
                        }
                    }
                    continue;
                }
                if (mainWindow.isSyncStopped()) return;
                currentMainTry = 0;
                loopUpdate:
                try
                {
                    logControl.Log("Memulai sinkronisasi tokopedia untuk sku " + item.Key + " (" + noitem + "/" + UpdatingProducts.Count + ")");
                    if (cache.marketID.ContainsKey(item.Key))
                    {
                        driver.Navigate().GoToUrl("https://seller.tokopedia.com/edit-product/" + cache.marketID[item.Key]);
                        LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                        LetsWait().Until(driver => driver.FindElements(By.CssSelector("input[data-testid='txtAEPProductName']")).Count > 0);
                        
                        var maxWait = 10;
                        var currentWait = 0;
                        while(driver.FindElements(By.CssSelector("button[data-testid='btnAEPSave']")).Count == 0)
                        {
                            executor.ExecuteScript("if(document.body != null){window.scrollTo(document.body.scrollWidth, document.body.scrollHeight);}");
                            await Task.Delay(500).ConfigureAwait(false);
                            if(currentWait >= maxWait)
                            {
                                throw new Exception("Core attribute not loaded.");
                            }
                            currentWait++;
                        }

                        LetsWait().Until(driver => driver.FindElement(By.CssSelector("input[data-testid='txtAEPProductName']")).GetAttribute("value") != "");

                        var errors = driver.FindElements(By.ClassName("errorDialog"));
                        if (errors.Count > 0)
                        {
                            foreach (var found in cache.marketID.Where(kvp => kvp.Value == cache.marketID[item.Key]).ToList())
                            {
                                cache.marketID.Remove(found.Key);
                            }
                            logControl.Log("Terdapat error pada produk " + item.Key + " kemungkinan karena sudah dihapus", System.Drawing.Color.Red);
                            reportFailedSync(item);
                            continue;
                        }

                        var varTable = driver.FindElements(By.CssSelector("#tableVarSection"));
                        if (varTable.Count == 0)
                        {
                            if (!driver.FindElement(By.CssSelector("input[data-testid='txtAEPSKU']")).GetAttribute("value").ToLower().Equals(item.Key.ToLower()))
                            {
                                foreach (var found in cache.marketID.Where(kvp => kvp.Value == item.Key.ToLower()).ToList())
                                {
                                    cache.marketID.Remove(found.Key);
                                }
                                logControl.Log("Gagal menemukan SKU produk ini." + item.Key, Color.Red);
                                reportFailedSync(item);
                                continue;
                            }

                            if (item.Value.level_price > 0)
                            {
                                var elharga = driver.FindElement(By.CssSelector("input[data-testid='txtAEPPrice']"));
                                executor.ExecuteScript("arguments[0].scrollIntoView(false);", elharga);
                                elharga.Clear();
                                elharga.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                elharga.SendKeys(OpenQA.Selenium.Keys.Delete);
                                executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elharga);
                                await Task.Delay(300).ConfigureAwait(false);
                                elharga.SendKeys(item.Value.level_price.ToString());
                                executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elharga);
                                await Task.Delay(300).ConfigureAwait(false);
                            }
                            else if (item.Value.level_price == 0)
                            {
                                logControl.Log("Harga belum di isi untuk sku " + item.Key);
                            }

                            if (item.Value.stock > -1)
                            {
                                if (item.Value.stock > 0)
                                {
                                    var tgStock = driver.FindElement(By.CssSelector("label[data-testid='tglAEPProductStatus'] input"));
                                    executor.ExecuteScript("arguments[0].scrollIntoView(false);", tgStock);
                                    executor.ExecuteScript("if(!arguments[0].checked){arguments[0].click();var evt = new Event('click', {'bubbles': true,'cancelable': false});arguments[0].dispatchEvent(evt);}", tgStock);

                                    var elstock = driver.FindElement(By.CssSelector("input[data-testid='txtAEPProductStock']"));
                                    executor.ExecuteScript("arguments[0].scrollIntoView(false);", elstock);
                                    LetsWait(5).Until(ExpectedConditions.ElementToBeClickable(elstock));
                                    elstock.Clear();
                                    elstock.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                    elstock.SendKeys(OpenQA.Selenium.Keys.Delete);
                                    executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elstock);
                                    await Task.Delay(300).ConfigureAwait(false);
                                    elstock.SendKeys(item.Value.stock.ToString());
                                    executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elstock);
                                    await Task.Delay(300).ConfigureAwait(false);
                                }
                                else
                                {
                                    var tgStock = driver.FindElement(By.CssSelector("label[data-testid='tglAEPProductStatus'] input"));
                                    executor.ExecuteScript("arguments[0].scrollIntoView(false);", tgStock);
                                    executor.ExecuteScript("if(arguments[0].checked){arguments[0].click();var evt = new Event('click', {'bubbles': true,'cancelable': false});arguments[0].dispatchEvent(evt);}", tgStock);
                                }
                                //var radios = driver.FindElements(By.ClassName("unf-radio"));
                                //foreach (var radio in radios)
                                //{
                                //    logControl.timeoutSecs = 0;
                                //    var text = radio.FindElement(By.ClassName("unf-radio-label__text"));
                                //    Console.WriteLine(text.GetAttribute("innerHTML"));
                                //    var html = text.GetAttribute("innerHTML");
                                //    if ((html.Contains("Stok Terbatas") || html.Contains("Limited Stock")) && item.Value.stock > 0)
                                //    {
                                //        radio.FindElement(By.ClassName("unf-radio-button")).Click();
                                //        var elstock = driver.FindElement(By.XPath("//*[@name='stock']"));
                                //        executor.ExecuteScript("arguments[0].scrollIntoView(false);", elstock);
                                //        elstock.Clear();
                                //        elstock.SendKeys(item.Value.stock.ToString());
                                //        break;
                                //    }
                                //    else if ((html.Contains("Stok Kosong") || html.Contains("No Stock")) && item.Value.stock <= 0)
                                //    {
                                //        radio.FindElement(By.ClassName("unf-radio-button")).Click();
                                //        break;
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            const string COL_HARGA = "HARGA";
                            const string COL_STOK = "STOK";
                            const string COL_SKU = "SKU";
                            const string COL_STATUS = "STATUS";
                            var idx_harga = -1;
                            var idx_stok = -1;
                            var idx_sku = -1;
                            var idx_status = -1;
                            var table = driver.FindElement(By.CssSelector("#tableVarSection > div:nth-child(3)"));
                            executor.ExecuteScript("arguments[0].scrollIntoView(false);", table);
                            var rows = table.FindElements(By.CssSelector(":scope > div"));
                            var skufound = false;
                            if (rows.Count > 1) //termasuk header
                            {
                                var no = 1;
                                foreach (var row in rows)
                                {
                                    logControl.timeoutSecs = 0;
                                    if (no == 1) //test
                                    {
                                        var cols = row.FindElements(By.CssSelector(":scope > div"));
                                        for (int i = 0; i < cols.Count; i++)
                                        {
                                            var inner = cols[i].Text.Trim().ToUpper();
                                            if (inner.Equals(COL_HARGA))
                                            {
                                                idx_harga = i;
                                            }
                                            else if (inner.Equals(COL_STOK))
                                            {
                                                idx_stok = i;
                                            }
                                            else if (inner.Equals(COL_SKU))
                                            {
                                                idx_sku = i;
                                            }
                                            else if (inner.Equals(COL_STATUS))
                                            {
                                                idx_status = i;
                                            }
                                        }
                                        no++;
                                        continue;
                                    }
                                    if (idx_harga > -1 && idx_stok > -1 && idx_sku > -1 && idx_status > -1)
                                    {
                                        var cols = row.FindElements(By.CssSelector(":scope > div"));
                                        var inputSKU = cols[idx_sku].FindElement(By.TagName("input"));
                                        if (inputSKU != null && inputSKU.GetAttribute("value").ToLower().Equals(item.Key.ToLower()))
                                        {   
                                            skufound = true;

                                            if (item.Value.level_price > -1)
                                            {
                                                if (driver.FindElements(By.CssSelector("button[data-testid='txtAEPDeleteWholesale']")).Count == 0)
                                                {
                                                    var inputHarga = cols[idx_harga].FindElement(By.TagName("input"));
                                                    if (item.Value.level_price > 0)
                                                    {
                                                        LetsWait().Until(ExpectedConditions.ElementToBeClickable(inputHarga));
                                                        inputHarga.Clear();
                                                        inputHarga.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                                        inputHarga.SendKeys(OpenQA.Selenium.Keys.Delete);
                                                        executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", inputHarga);
                                                        await Task.Delay(300).ConfigureAwait(false);
                                                        inputHarga.SendKeys(item.Value.level_price.ToString());
                                                        executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", inputHarga);
                                                        await Task.Delay(300).ConfigureAwait(false);
                                                    }
                                                    else if (item.Value.level_price == 0)
                                                    {
                                                        logControl.Log("Harga belum di isi untuk sku " + item.Key, System.Drawing.Color.Red);
                                                    }
                                                }
                                                else
                                                {
                                                    logControl.Log("SKU " + item.Key + " sedang menggunakan harga grosir", System.Drawing.Color.Yellow);
                                                }
                                            }

                                            if (item.Value.stock > -1)
                                            {
                                                var elstock = cols[idx_stok].FindElement(By.TagName("input"));
                                                executor.ExecuteScript("arguments[0].scrollIntoView(false);", elstock);
                                                LetsWait(5).Until(ExpectedConditions.ElementToBeClickable(elstock));
                                                elstock.Clear();
                                                elstock.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                                elstock.SendKeys(OpenQA.Selenium.Keys.Delete);
                                                executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elstock);
                                                await Task.Delay(300).ConfigureAwait(false);
                                                elstock.SendKeys(item.Value.stock.ToString());
                                                executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elstock);                                            
                                                var tgStock = cols[idx_status].FindElement(By.TagName("input"));
                                                if (item.Value.stock == 0)
                                                {
                                                    executor.ExecuteScript("arguments[0].scrollIntoView(false);", tgStock);
                                                    executor.ExecuteScript("if(arguments[0].checked){arguments[0].click();var evt = new Event('click', {'bubbles': true,'cancelable': false});arguments[0].dispatchEvent(evt);}", tgStock);
                                                }
                                                else
                                                {
                                                    executor.ExecuteScript("arguments[0].scrollIntoView(false);", tgStock);
                                                    executor.ExecuteScript("if(!arguments[0].checked){arguments[0].click();var evt = new Event('click', {'bubbles': true,'cancelable': false});arguments[0].dispatchEvent(evt);}", tgStock);
                                                }
                                                await Task.Delay(300).ConfigureAwait(false);
                                            }

                                             //if (item.Value.stock > 0)
                                            //{
                                            //    var tgStock = cols[6].FindElement(By.TagName("input"));
                                            //    executor.ExecuteScript("arguments[0].scrollIntoView(false);", tgStock);
                                            //    executor.ExecuteScript("if(!arguments[0].checked){var evt = new Event('click', {'bubbles': true,'cancelable': false});arguments[0].dispatchEvent(evt);}", tgStock);

                                            //    var elstock = cols[3].FindElement(By.TagName("input"));
                                            //    executor.ExecuteScript("arguments[0].scrollIntoView(false);", elstock);
                                            //    LetsWait(5).Until(ExpectedConditions.ElementToBeClickable(elstock));
                                            //    elstock.Clear();
                                            //    elstock.SendKeys(item.Value.stock.ToString());
                                            //}
                                            //else if (item.Value.stock == 0)
                                            //{
                                            //    var tgStock = cols[6].FindElement(By.TagName("input"));
                                            //    executor.ExecuteScript("arguments[0].scrollIntoView(false);", tgStock);
                                            //    executor.ExecuteScript("if(arguments[0].checked){var evt = new Event('click', {'bubbles': true,'cancelable': false});arguments[0].dispatchEvent(evt);}", tgStock);
                                            //}

                                            
                                        }

                                    }
                                    else
                                    {
                                        logControl.Log("Terdapat perubahan pada struktur data variant. Harap hubungi developer : " + item.Key, Color.Red);
                                        reportFailedSync(item);
                                        break;
                                    }
                                    no++;
                                }
                            }
                            if (!skufound)
                            {
                                foreach (var found in cache.marketID.Where(kvp => kvp.Value == item.Key.ToLower()).ToList())
                                {
                                    cache.marketID.Remove(found.Key);
                                }
                                logControl.Log("Gagal menemukan SKU variant produk ini." + item.Key, Color.Red);
                                reportFailedSync(item);
                                continue;
                            }
                        }

                        //var btnPrimaries = driver.FindElements(By.ClassName("unf-btn__primary"));
                        //executor.ExecuteScript("arguments[0].scrollIntoView(false);", btnPrimaries[btnPrimaries.Count-1]);
                        //btnPrimaries[btnPrimaries.Count - 1].Click();
                        executor.ExecuteScript("document.body.click();");
                        //await Task.Delay(100).ConfigureAwait(false);
                        var lastURL = driver.Url;
                        var btnsave = driver.FindElement(By.CssSelector("button[data-testid='btnAEPSave']"));
                        executor.ExecuteScript("arguments[0].scrollIntoView(false);", btnsave);
                        executor.ExecuteScript("arguments[0].click();", btnsave);
                        var triedsave = false;
                        trysave:
                        btnsave.Click();   
                        
                        try
                        {
                            logControl.Log("Menyimpan data...", System.Drawing.Color.YellowGreen);
                            LetsWait(3).Until(driver => !driver.Url.Equals(lastURL));
                        }
                        catch (Exception e)
                        {
                            logControl.Log(e.Message + e.StackTrace);
                            //cek gambar lengkap atau tidak
                            var images_error = driver.FindElements(By.XPath("//div[contains(text(),'Produk harus memiliki minimum 1 gambar')]"));
                            if (images_error.Count > 0)
                            {
                                logControl.Log("Gambar produk belum di lengkapi untuk sku " + item.Key, System.Drawing.Color.Red);
                                reportFailedSync(item);
                                continue;
                            }

                            //cek ada error inputan
                            var other_errors = driver.FindElements(By.XPath("//div[contains(text(),'harus diisi')]"));
                            if (other_errors.Count > 0)
                            {
                                if (triedsave)
                                {
                                    logControl.Log("Ada data utama produk yang harus diisi untuk sku " + item.Key, System.Drawing.Color.Red);
                                    reportFailedSync(item);
                                    continue;
                                }
                                else
                                {
                                    triedsave = true;
                                    goto trysave;
                                }
                            }
                        }
                        
                        //LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                        //LetsWait().Until(driver => driver.FindElements(By.CssSelector("img[data-testid='imgSellerSidebarProfile']")).Count > 0);

                        successProducts.Add(item.Key);
                        logControl.Log("Sinkronisasi berhasil.", System.Drawing.Color.Green);

                        processCounter++;
                        if (processCounter % clearCacheInterval == 0)
                        {
                            try
                            {
                                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                                await Task.Delay(2000).ConfigureAwait(false);
                                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                                button.Click();
                            }
                            catch (Exception)
                            {
                                logControl.Log("Cannot clear cache.");
                            }
                        }
                    }
                    else
                    {
                        logControl.Log("SKU Tokopedia belum ditentukan untuk barang ini.", System.Drawing.Color.Yellow);
                        if (cache.products.ContainsKey(item.Key))
                        {
                            var product = cache.products[item.Key];
                            if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                            {
                                mainWindow.listMonitored[product.id_ipos].tokopedia_id = "0";
                                mainWindow.listMonitored[product.id_ipos].tokopedia_status = 0;
                            }
                        }
                    }
                    logControl.Log("");
                }
                catch (Exception e)
                {
                    
                    Console.WriteLine(e.Message);
                    if (e.Message.ToLower().Contains("alert"))
                    {
                        Console.WriteLine("Dismissing alert");
                        AcceptDialog();
                        continue;
                    }

                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        logControl.Log(e.Message+e.StackTrace);
                        logControl.Log("Gagal, Mencoba sinkronisasi ulang produk tersebut...", System.Drawing.Color.Yellow);
                        goto loopUpdate;
                    }
                    else
                    {
                        reportFailedSync(item);
                        continue;
                    }
                }
                noitem++;
                if (mainWindow.isSyncStopped()) return;
            }
            foreach (var item in successProducts)
            {
                logControl.timeoutSecs = 0;
                if (cache.products.ContainsKey(item) && cache.marketID.ContainsKey(item))
                {
                    var product = cache.products[item];
                    var id = cache.marketID[item];
                    if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                    {
                        mainWindow.listMonitored[product.id_ipos].tokopedia_id = id;
                        mainWindow.listMonitored[product.id_ipos].tokopedia_status = 1;
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }
            logControl.Log("Sinkronisasi tokopedia selesai.");
            mainWindow.Log("Sinkronisasi Tokopedia selesai.", System.Drawing.Color.Green);
            logControl.isWorking = false;
            isSyncDone = true;
            isStuck = false;
            SaveCache();
        }

        void AcceptDialog()
        {
            try
            {
                LetsWait().Until(ExpectedConditions.AlertIsPresent());
                IAlert alert = driver.SwitchTo().Alert();
                if (alert != null) alert.Accept();
            }
            catch (Exception ex)
            {
                //logControl.Log("dialog error : " + ex.Message.ToLower());
            }
        }

        void reportFailedSync(System.Collections.Generic.KeyValuePair<string, TokopediaProduct> item)
        {
            TakeScreenshot(item.Key);
            logControl.Log("Sinkronisasi tokopedia gagal untuk sku " + item.Key, System.Drawing.Color.Red);
            if (cache.products.ContainsKey(item.Key) && cache.marketID.ContainsKey(item.Key))
            {
                var product = cache.products[item.Key];
                var id = cache.marketID[item.Key];
                if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                {
                    mainWindow.listMonitored[product.id_ipos].tokopedia_id = id;
                    mainWindow.listMonitored[product.id_ipos].tokopedia_status = 2;
                }
            }
        }


        void TakeScreenshot(string name)
        {
            try
            {
                Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                var folder = Application.StartupPath + "/ss/tokopedia/";
                Directory.CreateDirectory(folder);
                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                {
                    name = name.Replace(c, '_');
                }
                ss.SaveAsFile(folder + name + ".jpg", ScreenshotImageFormat.Jpeg);
            }
            catch (Exception e)
            {
                logControl.Log("Gagal screenshot, proses telah ditutup / tidak ditemukan.", System.Drawing.Color.Red);
            }
        }

        public void showLog()
        {
            if(logWindow != null)
            {
                mainWindow.Log("Membuka log tokopedia...", Color.Green);
                logControl.Show();
                logControl.BringToFront();
            }
        }

        public Dictionary<String,IPOSSalesOrder> getIposSalesOrder()
        {
            Dictionary<String, IPOSSalesOrder> result = new Dictionary<String, IPOSSalesOrder>();
            foreach (var item in newSales)
            {
                if (!item.is_pass || result.ContainsKey(item.invoice)) continue;
                IPOSSalesOrder sale = new IPOSSalesOrder();
                sale.number = item.invoice;
                sale.address = Program.CleanSQLString(item.address);
                sale.buyer = Program.CleanSQLString(item.buyer);
                sale.date = item.date;
                sale.products = item.products;
                sale.otherfee = item.shipfee;
                sale.cut_value = 0;
                sale.cut_pct = 0;
                sale.shipinfo = Program.CleanSQLString(item.shipname + "\n" + item.shiptrack);
                sale.total = (item.subtotal + sale.otherfee) - sale.cut_value;
                result.Add(sale.number,sale);
            }
            return result;
        }

        public Dictionary<String, IPOSOfficeSale> getIposOfficeSale()
        {
            Dictionary<String, IPOSOfficeSale> result = new Dictionary<String, IPOSOfficeSale>();
            foreach (var item in newSales)
            {
                if (!item.is_pass || result.ContainsKey(item.invoice)) continue;
                IPOSOfficeSale sale = new IPOSOfficeSale();
                sale.number = item.invoice;
                sale.address = Program.CleanSQLString(item.address);
                sale.buyer = Program.CleanSQLString(item.buyer);
                sale.date = item.date;
                sale.products = item.products;
                sale.otherfee = item.shipfee;
                sale.cut_value = 0;
                sale.cut_pct = 0;
                sale.shipinfo = Program.CleanSQLString(item.shipname + "\n" + item.shiptrack);
                sale.total = (item.subtotal + sale.otherfee) - sale.cut_value;
                result.Add(sale.number,sale);
            }
            return result;
        }

        public Dictionary<String, IPOSCashierSale> getIposCashierSale()
        {
            Dictionary<String, IPOSCashierSale> result = new Dictionary<String, IPOSCashierSale>();
            foreach (var item in newSales)
            {
                if (!item.is_pass || result.ContainsKey(item.invoice)) continue;
                IPOSCashierSale sale = new IPOSCashierSale();
                sale.number = item.invoice;
                sale.address = Program.CleanSQLString(item.address);
                sale.buyer = Program.CleanSQLString(item.buyer);
                sale.date = item.date;
                sale.products = item.products;
                sale.otherfee = item.shipfee;
                sale.cut_value = 0;
                sale.cut_pct = 0;
                sale.shipinfo = Program.CleanSQLString(item.shipname + "\n" + item.shiptrack);
                sale.total = (item.subtotal + sale.otherfee) - sale.cut_value;
                result.Add(sale.number,sale);
            }
            return result;
        }
    }
}
