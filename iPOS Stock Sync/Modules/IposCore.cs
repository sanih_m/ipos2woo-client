﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using SimpleJSON;

namespace iPOS_Stock_Sync.Modules
{
    public class IposCore
    {
        //string defConnString = "Host=localhost;Username=postgres;Password=;Database=postgres;port=5444;Persist Security Info=True;";
        public NpgsqlConnection conn = null;
        NpgsqlTransaction trans;
        frmProfileSync mainWindow;
        int idxProfile;
        public Dictionary<string, Product> allProducts = new Dictionary<string, Product>();

        public IposCore(int idx)
        {
            idxProfile = idx;
            if (conn != null && conn.State == ConnectionState.Closed)
                conn.Open();

            var config = Program.config.profiles[idxProfile];
            mainWindow = Program.Syncers[config.name];
        }

        public bool ConnectDB()
        {
            var profile = Program.config.profiles[idxProfile];
            var connString = "Host=" + profile.host + ";Port=" + profile.port + ";Username=" + profile.username + ";Password=" + profile.password + ";Database=" + profile.database + "";
            try
            {
                conn = new NpgsqlConnection(connString);
                conn.Open();
                return true;
            }
            catch (Exception e)
            {
                var cfg = new frmProfileConfig(idxProfile);
                cfg.ShowDialog();
                ConnectDB();
                return false;
            }
        }

        public List<string> GetWarehouses()
        {
            List<string> result = new List<string>();
            using (var cmd = new NpgsqlCommand("SELECT kodekantor from tbl_kantor", conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                }
            }
            return result;
        }


        public void Fetch(string[] product_blacklist)
        {
            if (conn.State != ConnectionState.Open) ConnectDB();
            var sql = "";
            var profile = Program.config.profiles[idxProfile];

            //check oid exist
            if (trans != null) trans.Dispose();
            trans = conn.BeginTransaction();
            try
            {
                sql = "select oid from tbl_item limit 1";
                using (var cmd = new NpgsqlCommand(sql, conn, trans))
                {
                    cmd.ExecuteScalar();
                }
                trans.Commit();
            }
            catch (Exception e)
                
            {
                if (!trans.IsCompleted) trans.Rollback();
                if (e.Message.Substring(0, 5).Equals("42703"))
                {
                    mainWindow.Log("Konfigurasi OID pada tabel produk iPos..");
                    sql = "ALTER TABLE tbl_item SET WITH OIDS";
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.ExecuteScalar();
                    }
                }
            }
            trans.Dispose();

            trans = conn.BeginTransaction();
            try
            {
                sql = "select oid from tbl_supel limit 1";
                using (var cmd = new NpgsqlCommand(sql, conn, trans))
                {
                    cmd.ExecuteScalar();
                }
                trans.Commit();
            }
            catch (Exception e)

            {
                if (!trans.IsCompleted) trans.Rollback();
                if (e.Message.Substring(0, 5).Equals("42703"))
                {
                    mainWindow.Log("Konfigurasi OID pada tabel pelanggan iPos..");
                    sql = "ALTER TABLE tbl_supel SET WITH OIDS";
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.ExecuteScalar();
                    }
                }
            }
            trans.Dispose();

            trans = conn.BeginTransaction();
            try
            {
                sql = "ALTER TABLE tbl_supel add column id_monitoring Integer default 0;";
                using (var cmd = new NpgsqlCommand(sql, conn, trans))
                {
                    cmd.ExecuteScalar();
                }
                trans.Commit();
            }
            catch (Exception)
            {
                if (!trans.IsCompleted) trans.Rollback();
            }
            trans.Dispose();

            trans = conn.BeginTransaction();
            try
            {
                sql = "select oid from tbl_pesanhd limit 1";
                using (var cmd = new NpgsqlCommand(sql, conn, trans))
                {
                    cmd.ExecuteScalar();
                }
                trans.Commit();
            }
            catch (Exception e)
            {
                if (!trans.IsCompleted) trans.Rollback();
                if (e.Message.Substring(0, 5).Equals("42703"))
                {
                    mainWindow.Log("Konfigurasi OID pada tabel pesanan penjualan iPos..");
                    sql = "ALTER TABLE tbl_pesanhd SET WITH OIDS";
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.ExecuteScalar();
                    }
                }
            }
            trans.Dispose();


            trans = conn.BeginTransaction();
            try
            {
                sql = "ALTER TABLE tbl_pesanhd add column id_monitoring Integer default 0;";
                using (var cmd = new NpgsqlCommand(sql, conn, trans))
                {
                    cmd.ExecuteScalar();
                }
                trans.Commit();
            }
            catch (Exception)
            {
                if (!trans.IsCompleted) trans.Rollback();
            }
            trans.Dispose();

            
            #region "Ambil Item Ipos";
            allProducts.Clear();
            var whereblacklist = "";
            if (product_blacklist.Length > 0)
            {
                foreach (var item in product_blacklist)
                {
                    if (item.Equals("")) continue;
                    if (!whereblacklist.Equals(""))
                    {
                        whereblacklist += "|";
                    }
                    whereblacklist += item.ToUpper();
                }
                if (whereblacklist.Trim() != "")
                {
                    whereblacklist = "where UPPER(namaitem) not similar to '%(" + whereblacklist + ")%'";
                }
                else
                {
                    whereblacklist = "";
                }
            }
            sql = "select oid, kodeitem, namaitem, rak, keterangan, hargapokok, satuan from tbl_item " + whereblacklist;
            if (mainWindow.syncProductFilter.Count > 0 && profile.primary_key == ProfileConst.PK_CODE)
            {
                var data = "";
                foreach (var item in mainWindow.syncProductFilter)
                {
                    if (!data.Equals(""))
                    {
                        data += ",";
                    }
                    data += "'" + item + "'";
                }
                var where = " and ";
                if (whereblacklist.Equals(""))
                {
                    where = "where ";
                }
                where += "kodeitem in (" + data + ")";
                sql += where;
            }
            if (profile.Equals(Program.devPassword))
            {
                mainWindow.Log("");
                mainWindow.Log("sql item : " + sql);
                mainWindow.Log("");
            }

            var activeProducts = 0;
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                Product p = new Product();
                var count = 0;
                while (reader.Read())
                {
                    count++;
                    p = new Product();
                    p.id = reader.GetValue(0).ToString();
                    p.code = reader.GetString(1);
                    p.name = reader.GetString(2);

                    p.stock = 0;
                    if (profile.woo_sync_weight)
                    {
                        if (!float.TryParse(reader.IsDBNull(3) ? "0" : reader.GetString(3), out p.weight))
                        {
                            p.weight = -1;
                        }
                    }
                    else
                    {
                        p.weight = -1;
                    }
                    p.description = reader.IsDBNull(4) ? "" : reader.GetString(4);
                    p.price_main = reader.IsDBNull(5) ? 0 : long.Parse(Math.Round(reader.GetDouble(5), MidpointRounding.AwayFromZero).ToString()); ;
                    p.publish_date = "NULL";
                    p.unit = reader.IsDBNull(6) ? "" : reader.GetString(6);

                    if (mainWindow.cache_product.data.ContainsKey(p.id))
                    {
                        p.publish_date = mainWindow.cache_product.data[p.id].publish_date;
                    }
                    if (mainWindow.cache_woo.list_id.ContainsKey(p.id))
                    {
                        var wooid = mainWindow.cache_woo.list_id[p.id];
                        if (mainWindow.listWooPublish.ContainsKey(wooid))
                        {
                            var splitted = mainWindow.listWooPublish[wooid].Split('|');
                            p.publish_date = splitted[0];
                        }
                    }

                    //jika pertama kali
                    if (mainWindow.firstTime)
                    {
                        if (count <= Program.productLimit() || Program.productLimit() == 0)
                        {
                            if (!p.interacted)
                            {
                                p.active = true;
                            }
                        }
                        else
                        {
                            p.active = false;
                        }
                    }
                    else
                    {
                        if (mainWindow.cache_product.data.ContainsKey(p.id))
                        {
                            p.active = mainWindow.cache_product.data[p.id].active;
                        }
                        else
                        {
                            if (count <= Program.productLimit() || Program.productLimit() == 0)
                            {
                                if (!p.interacted)
                                {
                                    p.active = true;
                                }
                            }
                            else
                            {
                                p.active = false;
                            }
                        }
                    }

                    if (p.active)
                    {
                        activeProducts++;
                        if (activeProducts > Program.productLimit() && Program.productLimit() > 0)
                        {
                            p.active = false;
                            if (mainWindow.cache_product.data.ContainsKey(p.id))
                            {
                                mainWindow.cache_product.data[p.id].active = false;
                            }
                            activeProducts--;
                        }
                    }

                    mainWindow.listProduct.Add(p.id, p);
                }
            }
            #endregion

            #region "Ambil Stok Ipos";
            sql = "select i.oid, s.kantor as gudang, s.stok from tbl_item i, tbl_itemstok s where i.kodeitem = s.kodeitem and s.kantor in (" + profile.warehouse + ") order by s.kantor";
            if (profile.main_dev.Equals(Program.devPassword))
            {
                mainWindow.Log("");
                mainWindow.Log("sql stok ipos: " + sql);
                mainWindow.Log("");
            }
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0) && mainWindow.listProduct.ContainsKey(reader.GetValue(0).ToString()))
                    {
                        if (reader.IsDBNull(1)) continue;
                        var p = mainWindow.listProduct[reader.GetValue(0).ToString()];
                        if (p.stockdetail == null)
                        {
                            p.stockdetail = new Dictionary<string, WarehouseStock>();
                        }

                        var warehouse = new WarehouseStock
                        {
                            name = reader.GetString(1),
                            stock = !reader.IsDBNull(2) ? reader.GetFloat(2) : 0
                        };

                        if (p.stockdetail.ContainsKey(reader.GetString(1)))
                        {
                            p.stockdetail[reader.GetString(1)].stock += warehouse.stock;
                        }
                        else
                        {
                            p.stockdetail.Add(reader.GetString(1), warehouse);
                        }

                        p.stock += !reader.IsDBNull(2) ? reader.GetFloat(2) : 0;
                        //listProduct[uint.Parse(reader.GetValue(0).ToString())] = p;
                    }
                }
            }
            #endregion

            #region "Ambil Harga Ipos";
            sql = "select i.oid, j.level, cast(j.hargajual as double precision) as hargajual from tbl_item i, tbl_itemhj j where i.kodeitem = j.kodeitem";
            if (profile.Equals(Program.devPassword))
            {
                mainWindow.Log("");
                mainWindow.Log("sql harga ipos : " + sql);
                mainWindow.Log("");
            }
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (mainWindow.listProduct.ContainsKey(reader.GetValue(0).ToString()))
                    {
                        var p = mainWindow.listProduct[reader.GetValue(0).ToString()];
                        if (reader.GetInt32(1).Equals(1))
                        {
                            p.price1 = long.Parse(Math.Round(reader.GetDouble(2), MidpointRounding.AwayFromZero).ToString());
                        }
                        else if (reader.GetInt32(1).Equals(2))
                        {
                            p.price2 = long.Parse(Math.Round(reader.GetDouble(2), MidpointRounding.AwayFromZero).ToString());
                        }
                        else if (reader.GetInt32(1).Equals(3))
                        {
                            p.price3 = long.Parse(Math.Round(reader.GetDouble(2), MidpointRounding.AwayFromZero).ToString());
                        }
                        else if (reader.GetInt32(1).Equals(4))
                        {
                            p.price4 = long.Parse(Math.Round(reader.GetDouble(2), MidpointRounding.AwayFromZero).ToString());
                        }
                    }
                }
            }
            #endregion

            #region "Ambil Satuan Ipos"
            //tbl_itemsatuanjml
            sql = "select i.oid, s.satuan, s.kodebarcode, s.jumlahkonv, s.hargapokok from tbl_item i, tbl_itemsatuanjml s where i.kodeitem = s.kodeitem";
            if (mainWindow.syncProductFilter.Count > 0 && profile.primary_key == ProfileConst.PK_BARCODE)
            {
                var data = "";
                foreach (var item in mainWindow.syncProductFilter)
                {
                    if (!data.Equals(""))
                    {
                        data += ",";
                    }
                    data += "'" + item + "'";
                }
                var where = " and ";
                if (whereblacklist.Equals(""))
                {
                    where = "where ";
                }
                where += "s.kodebarcode in (" + data + ")";
                sql += where;
            }
            if (profile.Equals(Program.devPassword))
            {
                mainWindow.Log("");
                mainWindow.Log("sql satuan item : " + sql);
                mainWindow.Log("");
            }
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0) && mainWindow.listProduct.ContainsKey(reader.GetValue(0).ToString()))
                    {
                        if (reader.IsDBNull(1)) continue;
                        var p = mainWindow.listProduct[reader.GetValue(0).ToString()];
                        if (p.units == null)
                        {
                            p.units = new Dictionary<string, ProductUnit>();
                        }

                        var unit = new ProductUnit
                        {
                            name = reader.GetString(1),
                            barcode = !reader.IsDBNull(2) ? reader.GetString(2) : "",
                            conversion = reader.GetDecimal(3),
                            cost = reader.GetDecimal(4),
                        };

                        if (!p.units.ContainsKey(reader.GetString(1)))
                        {
                            p.units.Add(reader.GetString(1), unit);
                        }
                        else
                        {
                            p.units[reader.GetString(1)] = unit;
                        }
                    }
                }
            }
            #endregion

            #region "Ambil Harga Ipos Satuan lain";
            sql = "select i.oid, j.satuan, j.level, cast(j.hargajual as double precision) as hargajual from tbl_item i, tbl_itemhj j where i.kodeitem = j.kodeitem";
            if (profile.Equals(Program.devPassword))
            {
                mainWindow.Log("");
                mainWindow.Log("sql unit price : " + sql);
                mainWindow.Log("");
            }
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0) && mainWindow.listProduct.ContainsKey(reader.GetValue(0).ToString()))
                    {
                        if (reader.IsDBNull(1)) continue;
                        var p = mainWindow.listProduct[reader.GetValue(0).ToString()];
                        var unit = reader.GetString(1);
                        if (p.units != null && p.units.ContainsKey(unit))
                        {
                            if (p.units[unit].prices == null)
                            {
                                p.units[unit].prices = new Dictionary<string, ProductPrice>();
                            }
                            var price = new ProductPrice
                            {
                                level = reader.GetInt32(2),
                                price = long.Parse(Math.Round(reader.GetDouble(3), MidpointRounding.AwayFromZero).ToString()),
                            };
                            if (!p.units[unit].prices.ContainsKey(price.level.ToString()))
                            {
                                p.units[unit].prices.Add(price.level.ToString(), price);
                            }
                            else
                            {
                                p.units[unit].prices[price.level.ToString()] = price;
                            }
                        }
                    }

                }
            }
            #endregion

            if (profile.main_send_customer)
            {
                #region "Ambil Pelanggan Ipos";
                sql = "select oid,kode,nama,alamat,id_monitoring from tbl_supel where tipe = 'PL'";
                if (profile.main_dev.Equals(Program.devPassword))
                {
                    mainWindow.Log("");
                    mainWindow.Log("sql pelanggan : " + sql);
                    mainWindow.Log("");
                }

                using (var cmd = new NpgsqlCommand(sql, conn))
                using (var reader = cmd.ExecuteReader())
                {
                    Customer c;
                    while (reader.Read())
                    {
                        c = new Customer();
                        c.id = reader.GetValue(0).ToString();
                        c.code = reader.GetString(1);
                        c.name = reader.GetString(2);
                        c.address = reader.GetString(3);
                        c.id_monitoring = reader.GetInt32(4);

                        mainWindow.listCustomer.Add(c.id, c);

                        if (mainWindow.cache_customers.data.ContainsKey(c.id))
                        {
                            mainWindow.cache_customers.data[c.id] = c;
                        }
                        else
                        {
                            mainWindow.cache_customers.data.Add(c.id, c);
                        }
                    }
                }
                #endregion
            }

            foreach (var item in mainWindow.listProduct)
            {
                var p = item.Value;       
                if (profile.primary_key == ProfileConst.PK_CODE)
                {
                    if (allProducts.ContainsKey(p.code))
                    {
                        allProducts[p.code] = (Product)p.Clone();
                    }
                    else
                    {
                        allProducts.Add(p.code, (Product)p.Clone());
                    }
                }
                else if(profile.primary_key == ProfileConst.PK_BARCODE)
                {
                    if(p.units != null)
                    {
                        foreach (var unit in p.units)
                        {
                            if (!unit.Value.barcode.Equals(""))
                            {
                                if (allProducts.ContainsKey(unit.Value.barcode))
                                {
                                    allProducts[unit.Value.barcode] = (Product)p.Clone();
                                }
                                else
                                {
                                    allProducts.Add(unit.Value.barcode, (Product)p.Clone());
                                }
                            }
                        }
                    }
                }
            }
        }

        public async Task GetCustomer()
        {
            #region "Tarik data pelanggan dari monitoring"
            var profile = Program.config.profiles[idxProfile];
            mainWindow.Log("Cek data pelanggan dari situs monitoring...");
            var url = "";
            var resp = "";
            var sql = "";
            try
            {
                url = Program.config.monitorURL.AppendPathSegment("/api/customer_pull");
                resp = await url
                .PostUrlEncodedAsync(new
                {
                    id_branch = Program.config.branchID,
                    id_profile = profile.id,
                }).ReceiveString();
            }
            catch (Exception e)
            {
                resp = e.Message;
            }

            if (profile.main_dev.Equals(Program.devPassword))
            {
                mainWindow.Log("Respons tarik pelanggan : " + resp);
            }

            var obj = SimpleJSON.JSON.Parse(resp);
            if (obj != null)
            {
                if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                {
                    var arr = obj["data"].AsArray;
                    if (arr.Count > 0)
                    {
                        mainWindow.Log("Terdapat " + arr.Count + " data pelanggan baru");

                        var urut = 0;
                        sql = "select COALESCE(CAST(substring(max(kode),3,6) as integer),0) as code from tbl_supel where tipe = 'PL' and substring(kode,0,3) = 'PM' and length(kode) = 8 limit 1";
                        using (var cmd = new NpgsqlCommand(sql, conn))
                        {
                            var code = cmd.ExecuteScalar();
                            int.TryParse(code.ToString(), out urut);
                        }

                        var rows = "";
                        foreach (JSONNode item in arr)
                        {
                            var code = item["code"].ToString().Replace("\"", "");
                            if (code.Equals("Auto"))
                            {
                                urut++;
                                code = "PM" + urut.ToString("D6");
                            }
                            var name = item["name"].ToString().Replace("\"", "");
                            var address = item["address"].ToString().Replace("\"", "");
                            var id_monitoring = item["id_monitoring"].ToString().Replace("\"", "");
                            var insert = "('" + code + "','PL','" + name + "','" + address + "','IDR','DI','General'," + id_monitoring + ")";

                            if (!rows.Equals(""))
                            {
                                rows += ",";
                            }
                            rows += insert;
                        }
                        sql = "INSERT INTO tbl_supel(kode, tipe, nama, alamat, matauang, tipepot, kgrup, id_monitoring) VALUES" + rows;
                        if (profile.Equals(Program.devPassword))
                        {
                            mainWindow.Log("Sql new pelanggan : " + sql);
                        }
                        using (var cmd = new NpgsqlCommand(sql, conn))
                        {
                            cmd.ExecuteScalar();
                        }
                    }

                }
                else
                {
                    mainWindow.Log(resp);
                }
            }
            else
            {
                mainWindow.Log(resp);
            }
            #endregion
        }

        public async Task GetSales()
        {
            #region "Tarik data penjualan dari monitoring"
            var profile = Program.config.profiles[idxProfile];
            mainWindow.Log("Cek data penjualan dari situs monitoring...");
            var url = "";
            var resp = "";
            var sql = "";
            try
            {
                url = Program.config.monitorURL.AppendPathSegment("/api/sales_pull");
                resp = await url
                .PostUrlEncodedAsync(new
                {
                    id_branch = Program.config.branchID,
                    id_profile = profile.id,
                }).ReceiveString();
            }
            catch (Exception e)
            {
                resp = e.Message;
            }

            if (profile.Equals(Program.devPassword))
            {

            }
            mainWindow.Log("Respons tarik penjualan : " + resp);

            var obj = SimpleJSON.JSON.Parse(resp);
            if (obj != null)
            {
                if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                {
                    var arr = obj["data"].AsArray;
                    if (arr.Count > 0)
                    {
                        mainWindow.Log("Terdapat " + arr.Count + " data penjualan baru");

                        var urut = 0;
                        sql = "select COALESCE(CAST(substring(max(notransaksi),1,4) as integer),0) as code from tbl_pesanhd where tipe = 'OJ' and jenis = 'JL' and substring(notransaksi,6,5) = 'PJI2W' limit 1";
                        using (var cmd = new NpgsqlCommand(sql, conn))
                        {
                            var code = cmd.ExecuteScalar();
                            if (code != null)
                            {
                                int.TryParse(code.ToString(), out urut);
                            }
                        }
                        //Log("test1");

                        var rows = "";
                        var rowsItem = "";
                        foreach (JSONNode item in arr)
                        {
                            var sold_at = item["sold_at"].ToString().Replace("\"", "");
                            var send_at = item["send_at"].ToString().Replace("\"", "");
                            var warehouse = item["warehouse"].ToString().Replace("\"", "");

                            var number = item["number"].ToString().Replace("\"", "");
                            if (number.Equals("Auto"))
                            {
                                urut++;
                                var dt = DateTime.ParseExact(send_at, "yyyy-MM-dd HH:mm:ss",
                                   System.Globalization.CultureInfo.InvariantCulture);
                                number = urut.ToString("D4") + "/PJI2W/" + dt.ToString("MMyy");
                            }
                            var note = item["note"].ToString().Replace("\"", "");
                            var cut_pct = item["cut_pct"].ToString().Replace("\"", "");
                            var cut_value = item["cut_value"].ToString().Replace("\"", "");
                            var add_cost = item["add_cost"].ToString().Replace("\"", "");
                            var is_inc_cost = item["is_inc_cost"].ToString().Replace("\"", "") == "0" ? "false" : "true";
                            var total = item["total"].ToString().Replace("\"", "");

                            var customer = item["code_customer"].ToString().Replace("\"", "");
                            long subtotal = 0;
                            var nodet = 0;
                            foreach (JSONNode detail in item["detail"].AsArray)
                            {
                                nodet++;
                                var md = "";
                                using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
                                {
                                    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(number + item["created_at"].ToString().Replace("\"", ""));
                                    byte[] hashBytes = md5.ComputeHash(inputBytes);

                                    // Convert the byte array to hexadecimal string
                                    StringBuilder sb = new StringBuilder();
                                    for (int i = 0; i < hashBytes.Length; i++)
                                    {
                                        sb.Append(hashBytes[i].ToString("X2"));
                                    }
                                    md = sb.ToString();
                                }
                                var code = detail["code"].ToString().Replace("\"", "");
                                var qty = detail["qty"].ToString().Replace("\"", "");
                                var unit = detail["unit"].ToString().Replace("\"", "");
                                var price = detail["price"].ToString().Replace("\"", "");
                                var cut = detail["cut"].ToString().Replace("\"", "");
                                var totaldet = detail["total"].ToString().Replace("\"", "");
                                subtotal = subtotal + long.Parse(totaldet);
                                var insertdet = "('" + number + "-" + md + "-" + nodet.ToString() + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp,1)";
                                if (profile.db_type == 0)
                                {
                                    insertdet = "('" + number + "-" + md + "-" + nodet.ToString() + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp)";
                                }
                                if (!rowsItem.Equals(""))
                                {
                                    rowsItem += ",";
                                }
                                rowsItem += insertdet;
                            }

                            var total_item = nodet;

                            var id_monitoring = item["id_monitoring"].ToString().Replace("\"", "");


                            var insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + sold_at + "','OJ','" + send_at + "','JL','" + customer + "','IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + is_inc_cost + ",'IPOS2WOO',now()::timestamp,'Non',false,'2-3100','1-1110'," + id_monitoring + ")";
                            if (profile.db_type == 0)
                            {
                                insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + sold_at + "','OJ','" + send_at + "','JL','" + customer + "','IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + is_inc_cost + ",'IPOS2WOO',now()::timestamp,'2-3100','1-1110'," + id_monitoring + ")";
                            }
                            if (!rows.Equals(""))
                            {
                                rows += ",";
                            }
                            rows += insert;
                        }
                        //Log("test2");
                        sql = "INSERT INTO tbl_pesanhd (notransaksi, kodekantor, kantortujuan, tanggal, tipe, tanggalkirim, jenis, kodesupel,matauang, rate, keterangan, totalitem, totalterima, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, biaya_msk_total, user1, dateupd, ppn, bc_trf_sts, acc_dppesanan, acc_dpkas, id_monitoring) VALUES" + rows;
                        if (profile.db_type == 0)
                        {
                            sql = "INSERT INTO tbl_pesanhd (notransaksi, kodekantor, kantortujuan, tanggal, tipe, tanggalkirim, jenis, kodesupel,matauang, rate, keterangan, totalitem, totalterima, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, biaya_msk_total, user1, dateupd, acc_dppesanan, acc_dpkas, id_monitoring) VALUES" + rows;
                        }
                        if (profile.Equals(Program.devPassword))
                        {

                        }
                        //Log("Sql sales master : " + sql);
                        using (var cmd = new NpgsqlCommand(sql, conn))
                        {
                            cmd.ExecuteScalar();
                        }

                        sql = "INSERT INTO tbl_pesandt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlterima, satuan, harga, potongan, total, dateupd, jmlkonversi) VALUES" + rowsItem;
                        if (profile.db_type == 0)
                        {
                            sql = "INSERT INTO tbl_pesandt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlterima, satuan, harga, potongan, total, dateupd) VALUES" + rowsItem;
                        }
                        if (profile.Equals(Program.devPassword))
                        {

                        }
                        //mainWindow.Log("Sql sales detail : " + sql);
                        using (var cmd = new NpgsqlCommand(sql, conn))
                        {
                            cmd.ExecuteScalar();
                        }
                    }

                }
                else
                {
                    mainWindow.Log(resp);
                }
            }
            else
            {
                mainWindow.Log(resp);
            }
            #endregion
        }


        public async Task<List<String>> SyncSalesToOrder(Dictionary<string,IPOSSalesOrder> sales, IPOSOptionSalesOrder options)
        {
            List<String> result = new List<string>();
            var removed = 0;
            var sql = "select notransaksi from tbl_pesanhd";
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (sales.ContainsKey(reader.GetString(0)))
                    {
                        sales.Remove(reader.GetString(0));
                        removed++;
                    }
                }
            }
            if(removed > 0)
            {
                mainWindow.Log("Melewati "+removed+" pesanan penjualan yang telah berada di aplikasi Ipos.");
            }

            if (trans != null) trans.Dispose();
            trans = conn.BeginTransaction();
            try
            {
                if (sales.Count == 0) return result;
                var profile = Program.config.profiles[idxProfile];
                var rows = "";
                var rowsItem = "";
                foreach (var s in sales)
                {
                    var sale = s.Value;
                    var number = sale.number;
                    var warehouse = options.warehouse;
                    var sold_at = sale.date.ToString("yyyy-MM-dd HH:mm:ss");
                    var send_at = sale.date.ToString("yyyy-MM-dd HH:mm:ss");
                    var note = sale.buyer+" / "+sale.shipinfo;
                    var cut_pct = sale.cut_pct.ToString().Replace(',', '.');
                    var cut_value = sale.cut_value;
                    var add_cost = sale.otherfee;
                    var is_inc_cost = "true";
                    var total = sale.total;
                    var customer = options.customer;
                    var seller = options.seller == "" ? "null" : "'"+options.seller+"'";
                    long subtotal = 0;
                    var nodet = 0;

                    var products = sale.products.Split(';');
                    
                    foreach (var p in products)
                    {
                        nodet++;
                        
                        var product = p.Split(new string[] { "|#|" },StringSplitOptions.None);
                        var code = product[1];
                        var qty = int.Parse(product[2]);
                        var price = long.Parse(product[3]);
                        var unit = allProducts.ContainsKey(code) ? allProducts[code].unit : "PCS";
                        var cut = "0";
                        var totaldet = qty * price;
                        subtotal = subtotal + totaldet;
                        var insertdet = "('" + number + "-" + code + "-" + nodet.ToString() + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp,1)";
                        if(options.iposver == 4)
                        {
                            insertdet = "('" + number + "-" + code + "-" + nodet.ToString() + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp)";
                        }

                        if (!rowsItem.Equals(""))
                        {
                            rowsItem += ",";
                        }
                        rowsItem += insertdet;
                    }

                    var total_item = nodet;

                    var id_monitoring = "0";


                    var insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + sold_at + "','OJ','" + send_at + "','JL','" + customer + "'," + seller + ",'IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + is_inc_cost + ",'" + options.userid + "',now()::timestamp,'Non',false,'2-3100','1-1110'," + id_monitoring + ")";
                    if(options.iposver == 4)
                    {
                        insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + sold_at + "','OJ','" + send_at + "','JL','" + customer + "'," + seller + ",'IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + is_inc_cost + ",'" + options.userid + "',now()::timestamp,'2-3100','1-1110'," + id_monitoring + ")";
                    }


                    if (!rows.Equals(""))
                    {
                        rows += ",";
                    }
                    rows += insert;
                }
                
                sql = "INSERT INTO tbl_pesanhd (notransaksi, kodekantor, kantortujuan, tanggal, tipe, tanggalkirim, jenis, kodesupel,kodesales,matauang, rate, keterangan, totalitem, totalterima, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, biaya_msk_total, user1, dateupd, ppn, bc_trf_sts, acc_dppesanan, acc_dpkas, id_monitoring) VALUES" + rows;
                if(options.iposver == 4)
                {
                    sql = "INSERT INTO tbl_pesanhd (notransaksi, kodekantor, kantortujuan, tanggal, tipe, tanggalkirim, jenis, kodesupel,kodesales,matauang, rate, keterangan, totalitem, totalterima, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, biaya_msk_total, user1, dateupd, acc_dppesanan, acc_dpkas, id_monitoring) VALUES" + rows;
                }
                
                //mainWindow.Log("Sql sales master : " + sql);
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteScalar();
                }

                sql = "INSERT INTO tbl_pesandt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlterima, satuan, harga, potongan, total, dateupd, jmlkonversi) VALUES" + rowsItem;
                if (options.iposver == 4)
                {
                    sql = "INSERT INTO tbl_pesandt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlterima, satuan, harga, potongan, total, dateupd) VALUES" + rowsItem;
                }

                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteScalar();
                }
                trans.Commit();

                sql = "select notransaksi from tbl_pesanhd";
                using (var cmd = new NpgsqlCommand(sql, conn))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (sales.ContainsKey(reader.GetString(0)))
                        {
                            result.Add(reader.GetString(0));
                        }
                    }
                }

                await Task.Delay(1000).ConfigureAwait(false);
                return result;
            }
            catch (Exception e)
            {
                if(!trans.IsCompleted) trans.Rollback();
                mainWindow.Log(e.Message + e.StackTrace);
                await Task.Delay(1000).ConfigureAwait(false);
                return null;
            }
        }

        public async Task<List<String>> SyncSalesToOfficeSale(Dictionary<String, IPOSOfficeSale> sales, IPOSOptionOfficeSale options)
        {
            List<String> result = new List<string>();
            var removed = 0;
            var sql = "select notransaksi from tbl_ikhd where tipe = 'JL'";
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (sales.ContainsKey(reader.GetString(0)))
                    {
                        sales.Remove(reader.GetString(0));
                        removed++;
                    }
                }
                cmd.Dispose();
            }

            Dictionary<string, object[]> items = new Dictionary<string, object[]>();
            sql = "select kodeitem, hargapokok from tbl_item";
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    items.Add(reader.GetString(0), new object[] { reader.GetInt64(1) });
                }
                cmd.Dispose();
            }

            if (removed > 0)
            {
                mainWindow.Log("Melewati " + removed + " penjualan kantor yang telah berada di aplikasi Ipos.");
            }

            if (trans != null) trans.Dispose();
            trans = conn.BeginTransaction();
            try
            {
                if (sales.Count == 0) return result;
                var profile = Program.config.profiles[idxProfile];
                var rows = "";
                var rowsItem = "";
                var rowsJurnal = "";
                var rowsItemIK = "";
                var acc_tunai = options.acc_tunai;
                var acc_kredit = options.acc_kredit;
                var acc_pot = options.acc_pot;
                var acc_blain = options.acc_blain;
                var acc_persediaan = "1-2010";
                var acc_hpokokjual = "5-1300";
                var acc_pendjual = "4-1100";

                foreach (var s in sales)
                {
                    var sale = s.Value;
                    var number = sale.number;
                    var warehouse = options.warehouse;
                    var sold_at = sale.date.ToString("yyyy-MM-dd HH:mm:ss");
                    var send_at = sale.date.ToString("yyyy-MM-dd HH:mm:ss");
                    var jt_at = sale.date.AddDays(3).ToString("yyyy-MM-dd HH:mm:ss");
                    var note = sale.buyer + " / " + sale.shipinfo;
                    var cut_pct = sale.cut_pct.ToString().Replace(',', '.');
                    var cut_value = sale.cut_value;
                    var add_cost = sale.otherfee;
                    var is_inc_cost = "true";
                    var total = sale.total;
                    long tunai = 0;
                    long kredit = 0;
                    var customer = options.customer;
                    var seller = options.seller == "" ? "null" : "'" + options.seller + "'";
                    long subtotal = 0;
                    var acc_bayar = "";
                    if(options.paymethod == 0)
                    {
                        kredit = total;
                        acc_bayar = acc_kredit;
                    }
                    else
                    {
                        tunai = total;
                        acc_bayar = acc_tunai;
                    }
                    var nodet = 0;

                    var products = sale.products.Split(';');
                    long persediaan = 0;

                    foreach (var p in products)
                    {
                        nodet++;

                        var product = p.Split(new string[] { "|#|" }, StringSplitOptions.None);
                        if (product == null || product.Length <= 1) continue;
                        var code = product[1];
                        var itemdata = items[code];
                        var hpok = itemdata != null ? long.Parse(itemdata[0].ToString()) : 0;
                        var qty = int.Parse(product[2]);
                        persediaan = persediaan + (hpok * qty);
                        var price = long.Parse(product[3]);
                        var unit = allProducts.ContainsKey(code) ? allProducts[code].unit : "PCS";
                        var cut = "0";
                        var totaldet = qty * price;
                        subtotal = subtotal + totaldet;
                        var iddet = number + "-" + code + "-" + nodet.ToString();
                        var insertdet = "('" + iddet + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp,1,'',null,'',null,null,'',null,null)";
                        if (options.iposver == 4)
                        {
                            insertdet = "('" + iddet + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp,null,null,null)";
                        }
                        if (!rowsItem.Equals(""))
                        {
                            rowsItem += ",";
                        }
                        rowsItem += insertdet;

                        //siapin insert tbl_item_ik
                        /*insertdet = "('" + iddet + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp,1,null,null,null,null,0,null,null,null)";
                        if (!rowsItemIK.Equals(""))
                        {
                            rowsItemIK += ",";
                        }
                        rowsItemIK += insertdet;*/

                        sql = "UPDATE tbl_itemstok set stok = stok - " + qty + " where kodeitem = '" + code + "' and kantor = '" + warehouse + "'";
                        using (var cmd = new NpgsqlCommand(sql, conn))
                        {
                            cmd.ExecuteScalar();
                        }

                        if(options.iposver == 5)
                        {
                            mainWindow.Log("test hpp");
                            sql = "SELECT hpp_ik_v3('"+iddet+"-1','"+number+"','" + sold_at + "','" + warehouse + "','JL','"+code+"',"+qty+",'"+ unit + "',3,0,1)";
                            mainWindow.Log(sql);
                            using (var cmd = new NpgsqlCommand(sql, conn))
                            {
                                cmd.ExecuteScalar();
                            }
                        }
                    }

                    var total_item = nodet;


                    var insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + jt_at + "','" + sold_at + "','JL','','" + customer + "'," + seller + ",null,null,null,'IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + tunai + "," + kredit + ",0," + is_inc_cost + ",'" + System.Environment.MachineName + "','','"+options.userid+ "','" + options.userid + "','',now()::timestamp,'Include',false,null,null,'"+acc_pot+ "','2-4110','" + acc_blain + "','" + acc_tunai + "','" + acc_kredit + "','6-3020',null,'1-1120','1-1220','2-3101','2-1300','1-1110','2-3100',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,false,'1-122X01')";
                    if(options.iposver == 4)
                    {
                        insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + jt_at + "','" + sold_at + "','JL',null,'" + customer + "'," + seller + ",null,null,null,'IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + tunai + "," + kredit + ",0," + is_inc_cost + ",'" + System.Environment.MachineName + "','" + options.userid + "','" + options.userid + "',now()::timestamp,null,null,'" + acc_pot + "','2-4110','" + acc_blain + "','" + acc_tunai + "','" + acc_kredit + "','5-1300',null,'1-1120','1-1220','','1-1110','2-3100',null,null,null,null,null,null,null,null)";
                    }
                    //mainWindow.Log(insert);

                    if (!rows.Equals(""))
                    {
                        rows += ",";
                    }
                    rows += insert;

                    if(options.iposver == 5)
                    {   
                        insert = "('"+ number + "-IPOS2WOO-" + acc_persediaan + "-1" +"',1,'R','"+number+ "',now()::timestamp,'"+ acc_persediaan+"','Jurnal','','IDR',1,"+persediaan+ ",'K',0," + persediaan + ",'"+options.warehouse+"','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;

                        insert = "('" + number + "-IPOS2WOO-" + acc_hpokokjual + "-2" + "',2,'R','" + number + "',now()::timestamp,'" + acc_hpokokjual + "','Jurnal','','IDR',1," + persediaan + ",'D'," + persediaan + ",0,'" + options.warehouse + "','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;

                        insert = "('" + number + "-IPOS2WOO-" + acc_pendjual + "-3" + "',3,'R','" + number + "',now()::timestamp,'" + acc_pendjual + "','Jurnal','','IDR',1," + total + ",'K',0," + total + ",'" + options.warehouse + "','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;

                        insert = "('" + number + "-IPOS2WOO-" + acc_bayar + "-4" + "',4,'R','" + number + "',now()::timestamp,'" + acc_bayar + "','Jurnal','','IDR',1," + total + ",'D'," + total + ",0,'" + options.warehouse + "','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;
                    }
                }

                sql = "INSERT INTO tbl_ikhd (notransaksi, kodekantor, kantordari, byr_krd_jt, tanggal, tipe, notrsorder, kodesupel, kodesales, kodesales2, kodesales3, kodesales4, matauang, rate, keterangan, totalitem, totalitempesan, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, jmltunai, jmlkredit, krd_jml_byr, biaya_msk_total, compname, compname_online, user1, user2, user_online, dateupd, ppn, bc_trf_sts, notrsretur, carabayar, acc_potongan, acc_pajak, acc_biayalain, acc_tunai, acc_kredit, acc_sales, acc_hpp, acc_debit, acc_kk, acc_deposit, acc_sales_hut, acc_biaya_pot, acc_dppesanan, acc_beda_cab, byr_krd_no, byr_debit_bank, byr_kk_bank, byr_debit_no, byr_kk_no,tanggal_sa,shiftkerja,nofp,byr_komisi1,byr_komisi2,byr_komisi3,byr_komisi4,point_notrans,ak_kotatujuan,ambilnomor,status_online,acc_emoney) VALUES" + rows;
                if(options.iposver == 4)
                {
                    sql = "INSERT INTO tbl_ikhd (notransaksi, kodekantor, kantordari, byr_krd_jt, tanggal, tipe, notrsorder, kodesupel, kodesales, kodesales2, kodesales3, kodesales4, matauang, rate, keterangan, totalitem, totalitempesan, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, jmltunai, jmlkredit, krd_jml_byr, biaya_msk_total, compname, user1, user2, dateupd, notrsretur, carabayar, acc_potongan, acc_pajak, acc_biayalain, acc_tunai, acc_kredit, acc_sales, acc_hpp, acc_debit, acc_kk, acc_deposit, acc_biaya_pot, acc_dppesanan, byr_krd_no, byr_debit_bank, byr_kk_bank, byr_debit_no, byr_kk_no,tanggal_sa,shiftkerja,nofp) VALUES" + rows;
                }
                //mainWindow.Log("Sql sales master : " + sql);
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteScalar();
                }

                sql = "INSERT INTO tbl_ikdt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlpesan, satuan, harga, potongan, total, dateupd, jmlkonversi, idorder, idtrsretur, detinfo, notrsretur, potpiutang, sistemhargajual, itempromo, satuanpromo) VALUES" + rowsItem;
                if (options.iposver == 4)
                {
                    sql = "INSERT INTO tbl_ikdt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlpesan, satuan, harga, potongan, total, dateupd, idorder, idtrsretur, detinfo) VALUES" + rowsItem;
                }
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteScalar();
                }

                if(options.iposver == 5)
                {
                    sql = "INSERT INTO tbl_accjurnal (iddetail, nourut, tipeinput, notransaksi, tanggal, kodeacc, jenis, keterangan, matauang, rate, jumlah, posisi, debet, kredit, kantor, modul) VALUES" + rowsJurnal;
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.ExecuteScalar();
                    }
                }

                trans.Commit();

                sql = "select notransaksi from tbl_ikhd where tipe = 'JL'";
                using (var cmd = new NpgsqlCommand(sql, conn))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (sales.ContainsKey(reader.GetString(0)))
                        {
                            result.Add(reader.GetString(0));
                        }
                    }
                }

                await Task.Delay(1000).ConfigureAwait(false);
                return result;
            }
            catch (Exception e)
            {
                if (!trans.IsCompleted) trans.Rollback();
                mainWindow.Log(e.Message + e.StackTrace);
                await Task.Delay(1000).ConfigureAwait(false);
                return null;
            }    
        }

        public async Task<List<String>> SyncSalesToCashierSale(Dictionary<String, IPOSCashierSale> sales, IPOSOptionCashierSale options)
        {
            List<String> result = new List<string>();
            var removed = 0;
            var sql = "select notransaksi from tbl_ikhd where tipe = 'KSR'";
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (sales.ContainsKey(reader.GetString(0)))
                    {
                        sales.Remove(reader.GetString(0));
                        removed++;
                    }
                }
            }

            Dictionary<string, object[]> items = new Dictionary<string, object[]>();
            sql = "select kodeitem, hargapokok from tbl_item";
            using (var cmd = new NpgsqlCommand(sql, conn))
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    items.Add(reader.GetString(0), new object[] { reader.GetInt64(1) });
                }
            }

            if (removed > 0)
            {
                mainWindow.Log("Melewati " + removed + " penjualan kasir yang telah berada di aplikasi Ipos.");
            }

            if (trans != null) trans.Dispose();
            trans = conn.BeginTransaction();
            try
            {
                if (sales.Count == 0) return result;
                var profile = Program.config.profiles[idxProfile];
                var rows = "";
                var rowsItem = "";
                var rowsJurnal = "";
                var acc_tunai = options.acc_tunai;
                var acc_kredit = options.acc_kredit;
                var acc_pot = options.acc_pot;
                var acc_blain = options.acc_blain;
                var acc_persediaan = "1-2010";
                var acc_hpokokjual = "5-1300";
                var acc_pendjual = "4-1100";

                foreach (var s in sales)
                {
                    var sale = s.Value;
                    var number = sale.number;
                    var warehouse = options.warehouse;
                    var sold_at = sale.date.ToString("yyyy-MM-dd HH:mm:ss");
                    var send_at = sale.date.ToString("yyyy-MM-dd HH:mm:ss");
                    var jt_at = sale.date.AddDays(3).ToString("yyyy-MM-dd HH:mm:ss");
                    var note = sale.buyer + " / " + sale.shipinfo;
                    var cut_pct = sale.cut_pct.ToString().Replace(',', '.');
                    var cut_value = sale.cut_value;
                    var add_cost = sale.otherfee;
                    var is_inc_cost = "true";
                    var total = sale.total;
                    long tunai = 0;
                    long kredit = 0;
                    var customer = options.customer;
                    var seller = options.seller == "" ? "null" : "'" + options.seller + "'";
                    long subtotal = 0;
                    var acc_bayar = "";
                    if (options.paymethod == 0)
                    {
                        kredit = total;
                        acc_bayar = acc_kredit;
                    }
                    else
                    {
                        tunai = total;
                        acc_bayar = acc_tunai;
                    }
                    var nodet = 0;

                    var products = sale.products.Split(';');
                    long persediaan = 0;

                    foreach (var p in products)
                    {
                        nodet++;

                        var product = p.Split(new string[] { "|#|" }, StringSplitOptions.None);
                        var code = product[1];
                        var itemdata = items[code];
                        var hpok = itemdata != null ? long.Parse(itemdata[0].ToString()) : 0;
                        var qty = int.Parse(product[2]);
                        persediaan = persediaan + (hpok * qty);
                        var price = long.Parse(product[3]);
                        var unit = allProducts.ContainsKey(code) ? allProducts[code].unit : "PCS";
                        var cut = "0";
                        var totaldet = qty * price;
                        subtotal = subtotal + totaldet;
                        var insertdet = "('" + number + "-" + code + "-" + nodet.ToString() + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp,1,null,null,null,null,0,null,null,null)";
                        if (options.iposver == 4)
                        {
                            insertdet = "('" + number + "-" + code + "-" + nodet.ToString() + "'," + nodet.ToString() + ",'" + number + "','" + code + "'," + qty + ",0,'" + unit + "'," + price + "," + cut + "," + totaldet + ",now()::timestamp,null,null,null)";
                        }
                        if (!rowsItem.Equals(""))
                        {
                            rowsItem += ",";
                        }
                        rowsItem += insertdet;

                        sql = "UPDATE tbl_itemstok set stok = stok - " + qty + " where kodeitem = '" + code + "' and kantor = '" + warehouse + "'";
                        using (var cmd = new NpgsqlCommand(sql, conn))
                        {
                            cmd.ExecuteScalar();
                        }
                    }

                    var total_item = nodet;

     
                    var insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + jt_at + "','" + sold_at + "','KSR',null,'" + customer + "'," + seller + ",null,null,null,'IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + tunai + "," + kredit + ",0," + is_inc_cost + ",'" + System.Environment.MachineName + "','','" + options.userid + "','" + options.userid + "','',now()::timestamp,'Include',false,null,null,'" + acc_pot + "','2-4110','" + acc_pot + "','" + acc_tunai + "','" + acc_kredit + "','6-3020',null,'1-1120','1-1220','2-3101','2-1300','1-1110','2-3100',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)";
                    if (options.iposver == 4)
                    {
                        insert = "('" + number + "','" + warehouse + "','" + warehouse + "','" + jt_at + "','" + sold_at + "','KSR',null,'" + customer + "'," + seller + ",null,null,null,'IDR',1,'" + note + "'," + total_item + ",0," + subtotal.ToString() + "," + cut_pct + "," + cut_value + "," + add_cost + "," + total + "," + tunai + "," + kredit + ",0," + is_inc_cost + ",'" + System.Environment.MachineName + "','" + options.userid + "','" + options.userid + "',now()::timestamp,null,null,'" + acc_pot + "','2-4110','" + acc_pot + "','" + acc_tunai + "','" + acc_kredit + "','5-1300',null,'1-1120','1-1220','','1-1110','2-3100',null,null,null,null,null,null,null,null)";
                    }

                    if (!rows.Equals(""))
                    {
                        rows += ",";
                    }
                    rows += insert;

                    if (options.iposver == 5)
                    {
                        insert = "('" + number + "-IPOS2WOO" + acc_persediaan + "-1" + "',1,'R','" + number + "',now()::timestamp,'" + acc_persediaan + "','Jurnal','','IDR',1," + persediaan + ",'K',0," + persediaan + ",'" + options.warehouse + "','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;

                        insert = "('" + number + "-IPOS2WOO" + acc_hpokokjual + "-2" + "',2,'R','" + number + "',now()::timestamp,'" + acc_hpokokjual + "','Jurnal','','IDR',1," + persediaan + ",'D'," + persediaan + ",0,'" + options.warehouse + "','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;

                        insert = "('" + number + "-IPOS2WOO" + acc_pendjual + "-3" + "',3,'R','" + number + "',now()::timestamp,'" + acc_pendjual + "','Jurnal','','IDR',1," + total + ",'K',0," + total + ",'" + options.warehouse + "','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;

                        insert = "('" + number + "-IPOS2WOO" + acc_bayar + "-4" + "',4,'R','" + number + "',now()::timestamp,'" + acc_bayar + "','Jurnal','','IDR',1," + total + ",'D'," + total + ",0,'" + options.warehouse + "','JUA')";
                        if (!rowsJurnal.Equals(""))
                        {
                            rowsJurnal += ",";
                        }
                        rowsJurnal += insert;
                    }
                }

                sql = "INSERT INTO tbl_ikhd (notransaksi, kodekantor, kantordari, byr_krd_jt, tanggal, tipe, notrsorder, kodesupel, kodesales, kodesales2, kodesales3, kodesales4, matauang, rate, keterangan, totalitem, totalitempesan, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, jmltunai, jmlkredit, krd_jml_byr, biaya_msk_total, compname, compname_online, user1, user2, user_online, dateupd, ppn, bc_trf_sts, notrsretur, carabayar, acc_potongan, acc_pajak, acc_biayalain, acc_tunai, acc_kredit, acc_sales, acc_hpp, acc_debit, acc_kk, acc_deposit, acc_sales_hut, acc_biaya_pot, acc_dppesanan, acc_beda_cab, byr_krd_no, byr_debit_bank, byr_kk_bank, byr_debit_no, byr_kk_no,tanggal_sa,shiftkerja,nofp,byr_komisi1,byr_komisi2,byr_komisi3,byr_komisi4,point_notrans,ak_kotatujuan,ambilnomor) VALUES" + rows;
                if (options.iposver == 4)
                {
                    sql = "INSERT INTO tbl_ikhd (notransaksi, kodekantor, kantordari, byr_krd_jt, tanggal, tipe, notrsorder, kodesupel, kodesales, kodesales2, kodesales3, kodesales4, matauang, rate, keterangan, totalitem, totalitempesan, subtotal, potfaktur, potnomfaktur, biayalain ,totalakhir, jmltunai, jmlkredit, krd_jml_byr, biaya_msk_total, compname, user1, user2, dateupd, notrsretur, carabayar, acc_potongan, acc_pajak, acc_biayalain, acc_tunai, acc_kredit, acc_sales, acc_hpp, acc_debit, acc_kk, acc_deposit, acc_biaya_pot, acc_dppesanan, byr_krd_no, byr_debit_bank, byr_kk_bank, byr_debit_no, byr_kk_no,tanggal_sa,shiftkerja,nofp) VALUES" + rows;
                }

                //mainWindow.Log("Sql sales master : " + sql);
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteScalar();
                }

                sql = "INSERT INTO tbl_ikdt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlpesan, satuan, harga, potongan, total, dateupd, jmlkonversi, idorder, idtrsretur, detinfo, notrsretur, potpiutang, sistemhargajual, itempromo, satuanpromo) VALUES" + rowsItem;
                if (options.iposver == 4)
                {
                    sql = "INSERT INTO tbl_ikdt (iddetail, nobaris, notransaksi, kodeitem, jumlah, jmlpesan, satuan, harga, potongan, total, dateupd, idorder, idtrsretur, detinfo) VALUES" + rowsItem;
                }
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    cmd.ExecuteScalar();
                }

                if (options.iposver == 5)
                {
                    sql = "INSERT INTO tbl_accjurnal (iddetail, nourut, tipeinput, notransaksi, tanggal, kodeacc, jenis, keterangan, matauang, rate, jumlah, posisi, debet, kredit, kantor, modul) VALUES" + rowsJurnal;
                    using (var cmd = new NpgsqlCommand(sql, conn))
                    {
                        cmd.ExecuteScalar();
                    }
                }

                trans.Commit();

                sql = "select notransaksi from tbl_ikhd where tipe = 'KSR'";
                using (var cmd = new NpgsqlCommand(sql, conn))
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (sales.ContainsKey(reader.GetString(0)))
                        {
                            result.Add(reader.GetString(0));
                        }
                    }
                }

                await Task.Delay(1000).ConfigureAwait(false);
                return result;
            }
            catch (Exception e)
            {
                if (!trans.IsCompleted) trans.Rollback();
                mainWindow.Log(e.Message + e.StackTrace);
                mainWindow.Log(sql);
                await Task.Delay(1000).ConfigureAwait(false);
                return null;
            }
        }
    }

}
