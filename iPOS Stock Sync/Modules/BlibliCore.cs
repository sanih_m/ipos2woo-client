﻿using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using SeleniumExtras.WaitHelpers;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync.Modules
{
    public class BlibliCore
    {
        public const string cachename = "/cache_blibli_{IDX}.json";

        IWebDriver driver;
        ChromeDriverService service_chrome;
        FirefoxDriverService service_firefox;
        string cache_file = Application.StartupPath + "/cache_blibli_{IDX}.json";
        public CacheBlibliProduct cache;
        WebDriverWait wait;

        const int maxLoginTry = 5;
        int currentLoginTry = 0;

        const int maxMainTry = 5;
        int currentMainTry = 0;

        const int maxMainWait = 60;

        const int marketIndex = 4;

        const int clearCacheInterval = 1;

        public const string marketName = "Blibli";

        int idxProfile = 0;

        public bool isDone = false;
        public bool isSyncDone = false;
        public bool isLoggedIn = false;
        public bool isStuck = false;
        public string loginResponse = "";

        JSONNode authData = null;

        frmProfileSync mainWindow;
        public ucMarketplaceLog logControl;
        frmLogViewer logWindow;

        bool isLogInit = false;
        public BlibliCore(int idx)
        {
            try
            {
                idxProfile = idx;
                var config = Program.config.profiles[idxProfile];
                mainWindow = Program.Syncers[config.name];

                if (config.blibli_browser == 0)
                {
                    var chromeProfileFolder = "session_" + config.id + "_blibli";
                    var chromeDir = Application.StartupPath + "\\";
                    if (!Directory.Exists(chromeDir + chromeProfileFolder))
                    {
                        Directory.CreateDirectory(chromeDir + chromeProfileFolder);
                    }
                    var options = new ChromeOptions();
                    options.BinaryLocation = Application.StartupPath + "\\engine_chrome\\chrome.exe";
                    if (config.blibli_dev != Program.devPassword)
                    {
                        options.AddArgument("--window-position=-32000,-32000");
                    }
                    options.AddArgument("disable-popup-blocking");
                    options.AddArgument("disable-notifications");
                    //options.AddArgument("disable-extensions");
                    options.AddArgument("disable-infobars");
                    options.AddArgument("--start-maximized");
                    options.AddArgument("user-data-dir=" + chromeDir + chromeProfileFolder);
                    options.PageLoadStrategy = PageLoadStrategy.Eager;
                    options.AddExcludedArguments(new List<string>() { "enable-automation" });

                    if (config.blibli_proxy_enabled)
                    {
                        var p_scheme = "";
                        var p_address = "";
                        var p_port = "";
                        var p_username = "";
                        var p_password = "";
                        var proxy = new Proxy();
                        var isSSL = false;
                        if (config.blibli_proxy_mode == 0)
                        {
                            mainWindow.Log("Mengambil proxy untuk Blibli dari server.");
                            var success = false;
                            var proxyAddress = "";
                            while (!success)
                            {

                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                RestClient client = new RestClient("https://app.ipos2woo.com/proxy/index.php");

                                RestRequest request = new RestRequest();
                                IRestResponse response = client.Execute(request);
                                proxyAddress = response.Content;
                                try
                                {
                                    var splitted = proxyAddress.Split(':');
                                    var testProxy = new WebProxy(splitted[0], int.Parse(splitted[1]));
                                    response = new RestClient
                                    {
                                        BaseUrl = new System.Uri("https://seller.blibli.com/"),
                                        Proxy = testProxy,
                                        Timeout = 5000,
                                        ReadWriteTimeout = 5000,
                                    }.Execute(new RestRequest
                                    {
                                        Method = Method.GET,
                                        Timeout = 5000,
                                        ReadWriteTimeout = 5000,
                                    });
                                    if (response.ErrorException != null)
                                    {
                                        throw response.ErrorException;
                                    }
                                    success = (!response.Content.Equals(""));
                                    mainWindow.Log(response.Content);
                                }
                                catch (Exception ex)
                                {
                                    mainWindow.Log(ex.Message);
                                    success = false;
                                }

                                if (!success)
                                {
                                    proxyAddress = "";
                                }
                            }


                            p_scheme = "http";
                            p_address = proxyAddress.Split(':')[0];
                            p_port = proxyAddress.Split(':')[1];
                            p_username = "";
                            p_password = "";
                        }
                        else
                        {
                            isSSL = config.blibli_proxy_address.Contains("https:");
                            p_scheme = isSSL ? "https" : "http";
                            p_address = config.blibli_proxy_address.Replace("http", "").Replace("https", "").Replace("://", "");
                            p_port = config.blibli_proxy_port;
                            p_username = config.blibli_proxy_username;
                            p_password = config.blibli_proxy_password;
                        }


                        if (File.Exists("blibli_proxy.zip")) File.Delete("blibli_proxy.zip");
                        File.WriteAllText("blibli_background.js",
                            Properties.Resources.background_js
                            .Replace("YOUR_PROXY_SCHEME", p_scheme)
                            .Replace("YOUR_PROXY_ADDRESS", p_address)
                            .Replace("YOUR_PROXY_PORT", p_port)
                            .Replace("YOUR_PROXY_USERNAME", p_username)
                            .Replace("YOUR_PROXY_PASSWORD", p_password)
                        );
                        File.WriteAllText("blibli_manifest.json",
                            Properties.Resources.manifest_json);
                        using (ZipArchive zip = ZipFile.Open("blibli_proxy.zip", ZipArchiveMode.Create))
                        {
                            zip.CreateEntryFromFile(@"blibli_background.js", "background.js");
                            zip.CreateEntryFromFile(@"blibli_manifest.json", "manifest.json");
                        }
                        options.AddExtension("blibli_proxy.zip");
                        File.Delete("blibli_background.js");
                        File.Delete("blibli_manifest.json");
                    }

                    service_chrome = ChromeDriverService.CreateDefaultService();
                    service_chrome.HideCommandPromptWindow = true;
                    driver = new ChromeDriver(service_chrome, options);
                }
                else if (config.blibli_browser == 1)
                {
                    var profileFolder = "ff_session_" + config.id + "_blibli";
                    var path = Application.StartupPath + "\\";
                    //if (!Directory.Exists(path + profileFolder))
                    //{
                    //    Directory.CreateDirectory(path + profileFolder);
                    //}
                    var options = new FirefoxOptions();
                    if (config.blibli_dev != Program.devPassword)
                    {
                        //options.AddArgument("--window-position=-32000,-32000");
                    }
                    //options.SetPreference("dom.webnotifications.enabled", false);
                    //options.SetPreference("dom.push.enabled", false);
                    //options.SetPreference("dom.disable_beforeunload", true);
                    options.AddArguments(new string[] { "-profile", path + profileFolder });
                    service_firefox = FirefoxDriverService.CreateDefaultService();
                    service_firefox.HideCommandPromptWindow = false;

                    //mainWindow.Log("1");
                    driver = new FirefoxDriver(service_firefox, options);
                    //mainWindow.Log("2");
                    driver.Manage().Window.Maximize();
                }

                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                cache_file = cache_file.Replace("{IDX}", idxProfile.ToString());
                LoadCache();
            }
            catch (Exception ex)
            {
                mainWindow.Log("Gagal inisialisasi Blibli : " + ex.Message, Color.Red);
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);
                isDone = true;
            }
        }

        void handleStuck(object sender, EventArgs e)
        {
            isStuck = true;
            isSyncDone = true;
            mainWindow.Log("Terjadi gangguan pada sinkronisasi " + marketName + ". Modul ini akan dimulai ulang...");
        }

        public void initLog()
        {
            if (isLogInit)
            {
                return;
            }
            isLogInit = true;
            logControl = mainWindow.CreateMarketplaceTab(marketIndex, marketName);
            logControl.WorkTimeout += handleStuck;
            //logWindow = new frmLogViewer(config.name + " : Log Blibli");
        }

        bool isDialogPresent(IWebDriver driver)
        {
            IAlert alert = ExpectedConditions.AlertIsPresent().Invoke(driver);
            return (alert != null);
        }

        public void stop()
        {
            try
            {
                var config = Program.config.profiles[idxProfile];
                driver.Quit();
                mainWindow.DeleteMarketplaceTab(marketName);
                var bpid = config.blibli_browser == 0 ? service_chrome.ProcessId : service_firefox.ProcessId;
                //create list of process id
                var driverProcessIds = new List<int> { bpid };

                //Get all the childs generated by the driver like conhost, chrome.exe...
                var mos = new System.Management.ManagementObjectSearcher($"Select * From Win32_Process Where ParentProcessID={bpid}");
                foreach (var mo in mos.Get())
                {
                    var pid = Convert.ToInt32(mo["ProcessID"]);
                    driverProcessIds.Add(pid);
                }
                //Kill all
                foreach (var id in driverProcessIds)
                {
                    var p = System.Diagnostics.Process.GetProcessById(id);
                    if (p != null) p.Kill();
                }
            }
            catch (Exception e)
            {
                mainWindow.Log(e.Message);
            }
        }


        public async Task LoginAsync()
        {
            mainWindow.Log("Otorisasi Blibli...");
            var config = Program.config.profiles[idxProfile];
            var lastUrl = "https://seller.blibli.com/";
            bool isOtp = false;
            sendEmailWhenLogin:
            try
            {
                driver.Navigate().GoToUrl("https://seller.blibli.com/sign-in");
                LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.FindElement(By.Id("email")).SendKeys(config.blibli_email);
                driver.FindElement(By.Id("password")).SendKeys(config.blibli_password);
                lastUrl = driver.Url;
            }
            catch (Exception ex)
            {
                goto checkUserInfo;
            }

            

        relogin:
            try
            {
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                LetsWait(10).Until(driver => (driver.FindElements(By.Id("sign-in")).Count > 0));
                driver.FindElement(By.Id("sign-in")).Click();
                currentLoginTry = 0;
            }
            catch (Exception ex)
            {
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);
                //mainWindow.Log(ex.StackTrace);
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    mainWindow.Log("Login gagal, Silahkan cek koneksi internet anda. 1", System.Drawing.Color.Red);
                    isDone = true;
                    return;
                }
            }

            

        #region "OTP"
        /*needOtp:
            isOtp = false;
            try
            {
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                LetsWait(5).Until(driver => (driver.FindElements(By.ClassName("js-tfa-form")).Count > 0));
                isOtp = driver.FindElement(By.ClassName("js-tfa-form")) != null;
                if (isOtp)
                {
                    driver.FindElement(By.ClassName("js-tfa-submit"));
                    mainWindow.Log("Silahkan ketikan kode otentikasi yang masuk ke perangkat anda disini.");
                    mainWindow.Log("Ketikan 'skip' untuk melanjutkan proses tanpa sinkronisasi tokopedia.");
                    mainWindow.Log("Ketikan 'resend' untuk mengirim ulang kode otentikasi.");
                    mainWindow.Log("");
                    mainWindow.command = "";
                    while (mainWindow.command.Equals(""))
                    {
                        Console.WriteLine("Waiting for command");
                        //mainWindow.Log("waiting for command : "+mainWindow.command);
                        await Task.Delay(1000).ConfigureAwait(false);
                    }
                    if (mainWindow.command.Equals("skip"))
                    {
                        mainWindow.Log("Login blibli gagal, kode otentikasi dibatalkan.", System.Drawing.Color.Red);
                        driver.Navigate().GoToUrl("https://seller.blibli.com");
                        isDone = true;
                        return;
                    }
                    else if (mainWindow.command.Equals("resend"))
                    {
                        mainWindow.Log("Kode otentikasi akan dikirimkan ulang.");
                        driver.FindElement(By.ClassName("js-tfa-re-request__button")).Click();
                        goto needOtp;
                    }
                    else if (mainWindow.command.Equals("back"))
                    {
                        mainWindow.command = "";
                        driver.FindElement(By.CssSelector("button.mfp-close")).Click();
                        await Task.Delay(1000).ConfigureAwait(false);
                        goto relogin;
                    }
                    else
                    {
                        var el = driver.FindElement(By.Id("otp"));
                        wait.Until(ExpectedConditions.ElementToBeClickable(el));
                        el.Clear();
                        el.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                        el.SendKeys(OpenQA.Selenium.Keys.Delete);
                        await Task.Delay(500).ConfigureAwait(false);
                        el.SendKeys(mainWindow.command);
                        driver.FindElement(By.ClassName("js-tfa-submit")).Click();
                        mainWindow.Log("Mengirim kode otentikasi...");
                    }
                    mainWindow.command = "";
                }
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                Console.WriteLine("Login berhasil");
            }


            try
            {
                if (isOtp)
                {
                    mainWindow.Log("Kode otentikasi sedang diproses...");
                    await Task.Delay(3000).ConfigureAwait(false);
                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                    var finishEls = driver.FindElements(By.ClassName("js-tfa-step--finish"));
                    if (finishEls.Count > 0 && finishEls[1].GetCssValue("display") != "none")
                    {
                        finishEls[1].Click();
                    }
                    else
                    {
                        driver.FindElement(By.ClassName("js-tfa-form-field__status"));
                        mainWindow.Log("Kode otentikasi salah.", System.Drawing.Color.Red);
                        Console.WriteLine("OTP Denied");
                        goto needOtp;
                    }
                }
            }
            catch (Exception ex)
            {
                //mainWindow.Log(ex.Message);
                Console.WriteLine("OTP Received");
                driver.Navigate().GoToUrl("https://seller.blibli.com");

            }
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);

           
            try
            {
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                LetsWait(5).Until(driver => (driver.FindElements(By.ClassName("js-tfa-form")).Count > 0));
                mainWindow.Log("Login Blibli membutuhkan verifikasi dengan kode OTP, silahkan pilih metode verifikasi berikut :");
                mainWindow.Log("1. Melalui SMS");
                mainWindow.Log("Ketikan 'skip' untuk melanjutkan proses tanpa sinkronisasi tokopedia.");
                mainWindow.Log("Silahkan ketik angka pilihan anda.");

            selectOTPmethod:
                mainWindow.command = "";
                while (mainWindow.command.Equals(""))
                {
                    Console.WriteLine("Waiting for command");
                    await Task.Delay(1).ConfigureAwait(false);
                }
                var buttons = driver.FindElements(By.ClassName("js-tfa-request__button"));
                if (mainWindow.command.Equals("1"))
                {
                    buttons[0].Click();
                }
                else if (mainWindow.command.Equals("2"))
                {
                    buttons[1].Click();
                }
                else if (mainWindow.command.Equals("skip"))
                {
                    mainWindow.Log("Login Blibli gagal, kode otp dibatalkan.", System.Drawing.Color.Red);
                    driver.Navigate().GoToUrl("https://seller.blibli.com");
                    isDone = true;
                    return;
                }
                else
                {
                    mainWindow.Log("Silahkan ketik angka pilihan anda dengan benar.");
                    goto selectOTPmethod;
                }
                currentLoginTry = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Login berhasil");
            }*/
        #endregion

        checkUserInfo:
            
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
            try
            {
                try
                {
                    //mainWindow.Log(driver.Url);
                    LetsWait(10).Until(driver => !driver.Url.Equals(lastUrl));
                }
                catch (Exception)
                {
                    //mainWindow.Log(driver.Url);
                    if(driver.FindElements(By.CssSelector("#remind-me-later-button")).Count > 0){
                       // mainWindow.Log("clicking");
                        driver.FindElement(By.CssSelector("#remind-me-later-button")).Click();
                    }
                    LetsWait().Until(driver => !driver.Url.Equals(lastUrl));
                }

                /*
                 * #close-announcement-button
                   #close-dashboard-onboarding-button
                 */
                LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                LetsWait().Until(driver => (driver.FindElements(By.CssSelector(".user__name")).Count > 0));
                //driver.Navigate().GoToUrl("https://seller.blibli.com/api/authenticate");
                //LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                //var html = driver.FindElement(By.TagName("body")).GetAttribute("innerText");
                //authData = SimpleJSON.JSON.Parse(html);
                mainWindow.Log("Proses login " + marketName + " berhasil.", System.Drawing.Color.Green);
                currentLoginTry = 0;

                if (config.blibli_browser == 0)
                {
                    IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
                    driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                    LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));

                    await Task.Delay(2000).ConfigureAwait(false);
                    var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                    var checkbox = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog').querySelector('iron-pages > div > settings-checkbox:nth-child(3)').shadowRoot.querySelector('cr-checkbox')");
                    if (checkbox.GetAttribute("checked") != null)
                    {
                        checkbox.Click();
                    }
                    button.Click();
                }

                isDone = true;
            }
            catch (Exception ex)
            {
                mainWindow.Log(ex.Message + ex.StackTrace);
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);

                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    isDone = true;
                    mainWindow.Log("Login gagal, Silahkan cek koneksi internet anda. 2", System.Drawing.Color.Red);
                    return;
                }
            }

            loginResponse = "";
            isLoggedIn = true;
        }

        void AcceptDialog()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                alert.Accept();
            }
            catch (Exception ex)
            {
                //logControl.Log("dialog error : " + ex.Message.ToLower());
            }
        }

        public void RemoveProductFromCache(string sku, bool isSave = false)
        {
            cache.products.Remove(sku);
            if (isSave) SaveCache();
        }
        void SaveCache()
        {
            cache.lastupdate = DateTime.Now;
            var json = JsonConvert.SerializeObject(cache);
            File.WriteAllText(cache_file, json);
            File.Copy(cache_file, cache_file + ".backup", true);
        }

        void LoadCache()
        {
            try
            {
                var data = File.ReadAllText(cache_file);
                cache = JsonConvert.DeserializeObject<CacheBlibliProduct>(data);
                if (cache == null)
                {
                    cache = new CacheBlibliProduct();
                }
                else
                {
                    if (cache.products == null)
                    {
                        cache.products = new Dictionary<string, BlibliProduct>();
                    }
                    if (cache.marketID == null)
                    {
                        cache.marketID = new Dictionary<string, string>();
                    }
                }
            }
            catch (Exception)
            {
                cache = new CacheBlibliProduct();
            }
        }

        public void ResetCache()
        {
            cache.products.Clear();
            SaveCache();
        }

        public void SyncCache()
        {
            var config = Program.config.profiles[idxProfile];
            foreach (var source in mainWindow.cache_product.data)
            {
                if (cache.marketID.ContainsKey(source.Value.sku))
                {
                    BlibliProduct p = new BlibliProduct();
                    p.id_ipos = source.Value.id;
                    p.sku = source.Value.sku;
                    p.level_index = config.blibli_price;
                    if (p.level_index == 1)
                    {
                        p.level_price = source.Value.price1;
                    }
                    else if (p.level_index == 2)
                    {
                        p.level_price = source.Value.price2;
                    }
                    else if (p.level_index == 3)
                    {
                        p.level_price = source.Value.price3;
                    }
                    else if (p.level_index == 4)
                    {
                        p.level_price = source.Value.price4;
                    }
                    p.stock = source.Value.stock;
                    if (cache.products.ContainsKey(p.sku))
                    {
                        cache.products[p.sku] = p;
                    }
                    else
                    {
                        cache.products.Add(p.sku, p);
                    }
                }
            }
            SaveCache();
        }

        WebDriverWait LetsWait(int waitSec = -1)
        {
            IClock clock = new SystemClock();
            return new WebDriverWait(clock, driver, TimeSpan.FromSeconds(waitSec == -1 ? maxMainWait : waitSec), TimeSpan.FromSeconds(0.1));
        }

        public async Task Sync(Dictionary<string, SingleProduct> listProducts)
        {
            LoadCache();
            while (logControl == null)
            {
                await Task.Delay(1).ConfigureAwait(false);
            }
            logControl.isClearLog = true;
            logControl.isWorking = true;
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            if (mainWindow.isSyncStopped()) return;
            mainWindow.Log("Memulai pengecekan sinkronisasi " + marketName + ".", System.Drawing.Color.Yellow);
            mainWindow.Log("Silahkan lihat tab '" + marketName + "' untuk melihat laporan sinkronisasi.");
            isSyncDone = false;
            isStuck = false;
            var config = Program.config.profiles[idxProfile];

            driver.Navigate().GoToUrl("https://seller.blibli.com/MTA/view/external/settings");
            await Task.Delay(100).ConfigureAwait(false);
            LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            if (driver.Url.Contains("/sign-in"))
            {
                mainWindow.Log("Melakukan proses login ulang...");
                await LoginAsync().ConfigureAwait(false);
            }

            Dictionary<String, BlibliProduct> UpdatingProducts = new Dictionary<String, BlibliProduct>();
            foreach (var source in new Dictionary<string, SingleProduct>(listProducts))
            {
                logControl.timeoutSecs = 0;
                if (!source.Value.active) continue;
                var same = false;
                if (cache != null && cache.products != null && cache.products.ContainsKey(source.Value.sku))
                {
                    //mainWindow.Log("exist");
                    same = Program.CompareBlibliProduct(source.Value, cache.products[source.Value.sku], config);
                }

                if (!same)
                {
                    BlibliProduct p = new BlibliProduct();
                    p.id_ipos = source.Value.id;
                    p.sku = source.Value.sku;
                    p.level_index = config.blibli_price;
                    if (p.level_index == 1)
                    {
                        p.level_price = source.Value.price1;
                        //mainWindow.Log("Harga "+source.Value.price1.ToString());
                    }
                    else if (p.level_index == 2)
                    {
                        p.level_price = source.Value.price2;
                        //mainWindow.Log("Harga " + source.Value.price2.ToString());
                    }
                    else if (p.level_index == 3)
                    {
                        p.level_price = source.Value.price3;
                        //mainWindow.Log("Harga " + source.Value.price3.ToString());
                    }
                    else if (p.level_index == 4)
                    {
                        p.level_price = source.Value.price4;
                        //mainWindow.Log("Harga " + source.Value.price4.ToString());
                    }
                    else
                    {
                        p.level_price = -1;
                    }
                    p.stock = source.Value.stock;
                    UpdatingProducts.Add(p.sku, p);
                }
                if (mainWindow.isSyncStopped()) return;
            }
            if (UpdatingProducts.Count == 0)
            {
                logControl.Log("Belum ada produk untuk sinkronisasi.", System.Drawing.Color.Yellow);
                logControl.isWorking = false;
                isSyncDone = true;
                isStuck = false;
                return;
            }
            logControl.Log("Terdapat " + UpdatingProducts.Count + " produk untuk sinkronisasi.");
            var allCount = UpdatingProducts.Count;

            List<string> successProducts = new List<string>();
            bool needFetchID = false;
            if (logControl.CheckID)
            {
                foreach (var item in UpdatingProducts)
                {
                    logControl.timeoutSecs = 0;
                    if (!cache.marketID.ContainsKey(item.Value.sku))
                    {
                        //cache.marketID.Add(item.Value.sku, "394713133");
                        needFetchID = true;
                        break;
                    }
                    if (mainWindow.isSyncStopped()) return;
                }
            }
            if (needFetchID)
            {
                logControl.Log("Mencocokan data produk blibli...");
                //startFetchID:
                driver.Navigate().GoToUrl("https://seller.blibli.com/MTA/view/external/settings");
                LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                var page = 0;
                do
                {
                    loopProducts:
                    logControl.timeoutSecs = 0;
                    try
                    {
                        var ajaxRes = Properties.Resources.blibli_products_request;
                        ajaxRes = ajaxRes.Replace("[PAGE]", page.ToString());
                        object response = executor.ExecuteAsyncScript(ajaxRes);
                        logControl.Log((String)response);
                        var obj = SimpleJSON.JSON.Parse((String)response);
                        if (obj != null && obj["success"].AsBool)
                        {
                            var arr = obj["content"].AsArray;
                            if (arr.Count > 0)
                            {
                                logControl.Log("Memeriksa " + arr.Count.ToString() + " data di halaman " + page.ToString() + "...");
                                foreach (JSONNode item in arr)
                                {
                                    var sku = Program.StripQuotes(item["merchantSku"].ToString());
                                    if (!sku.Trim().Equals(""))
                                    {
                                        if (cache.marketID.ContainsKey(sku))
                                        {
                                            cache.marketID[sku] = Program.StripQuotes(item["itemSku"].ToString());
                                        }
                                        else
                                        {
                                            cache.marketID.Add(sku, Program.StripQuotes(item["itemSku"].ToString()));
                                        }
                                    }
                                }
                                page++;
                            }
                            else
                            {
                                logControl.Log("Tidak ada data di halaman " + page.ToString() + "...");
                                page = -1;
                            }
                        }
                        else
                        {
                            logControl.Log("Gagal mengambil data produk.", System.Drawing.Color.Yellow);
                            logControl.Log((String)response);
                            page = -1;
                        }
                    }
                    catch (Exception e)
                    {

                        logControl.Log(e.Message);
                        AcceptDialog();
                        currentMainTry++;
                        if (currentMainTry < maxMainTry)
                        {
                            logControl.Log("Gagal, Mencoba mencocokan ulang produk tokopedia...", System.Drawing.Color.Yellow);
                            goto loopProducts;
                        }
                        else
                        {
                            page = -1;
                            logControl.Log("Proses pencocokan produk gagal. Melanjutkan proses sinkronisasi.", System.Drawing.Color.Red);
                            goto skipfindid;
                        }
                    }
                    if (mainWindow.isSyncStopped()) return;
                } while (page > -1);
            }

            var deletedUpdates = new List<string>();
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!cache.marketID.ContainsKey(item.Key))
                {
                    deletedUpdates.Add(item.Key);
                }
                else
                {
                    if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos))
                    {
                        var monitored = mainWindow.listMonitored[item.Value.id_ipos];
                        monitored.blibli_sync = 1;
                        monitored.blibli_price = item.Value.level_price;
                        monitored.blibli_stock = item.Value.stock;
                    }


                    if (cache.products.ContainsKey(item.Key))
                    {
                        cache.products[item.Key] = item.Value;
                    }
                    else
                    {
                        cache.products.Add(item.Key, item.Value);
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }
            if (deletedUpdates.Count > 0)
            {
                foreach (var item in deletedUpdates)
                {
                    UpdatingProducts.Remove(item);
                    if (mainWindow.isSyncStopped()) return;
                }
            }
            if (allCount != UpdatingProducts.Count)
            {
                logControl.Log("Terdapat " + (allCount - UpdatingProducts.Count) + " produk yang belum terdeteksi. Silahkan input SKU terlebih dahulu.", Color.Yellow);
            }



            skipfindid:

            if (config.blibli_browser == 0)
            {
                try
                {
                    driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                    await Task.Delay(2000).ConfigureAwait(false);
                    var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                    button.Click();
                }
                catch (Exception)
                {
                    logControl.Log("Cannot clear cache.");
                }
            }
            currentMainTry = 0;

            var noitem = 1;
            var processCounter = 1;
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!logControl.SyncMarketplace)
                {
                    if (!logControl.SendMonitoring)
                    {
                        if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos))
                        {
                            mainWindow.listMonitored[item.Value.id_ipos].blibli_sync = 0;
                        }
                    }
                    continue;
                }
                if (mainWindow.isSyncStopped()) return;
                loopUpdate:

                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                try
                {
                    logControl.Log("Memulai sinkronisasi untuk sku " + item.Key + " (" + noitem + "/" + UpdatingProducts.Count + ")");
                    if (cache.marketID.ContainsKey(item.Key))
                    {
                        //logControl.Log("Test shopee 2");
                        driver.Navigate().GoToUrl("https://seller.blibli.com/external/product/detail/active/" + cache.marketID[item.Key]);
                        AcceptDialog();

                        LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                        LetsWait(10).Until(driver => (driver.FindElements(By.CssSelector(".price__stock")).Count > 0));
                        executor.ExecuteScript("arguments[0].click();", driver.FindElement(By.CssSelector(".price__stock .btn-confirm")));
                        LetsWait(10).Until(driver => (driver.FindElements(By.CssSelector(".price__stock .edit__mode")).Count > 0));

                        var idx_price = -1;
                        var idx_stock = -1;
                        var headers = driver.FindElements(By.CssSelector(".price__stock table > tr th"));
                        var cur_idx = 0;
                        foreach (var col in headers)
                        {
                            if(col.Text.ToLower().Contains("harga normal"))
                            {
                                idx_price = cur_idx;
                            }
                            if (col.Text.ToLower().Contains("stok tersedia"))
                            {
                                idx_stock = cur_idx;
                            }
                            cur_idx++;
                        }

                        var attributes = driver.FindElements(By.CssSelector(".price__stock table tr:nth-child(2) td"));

                        if (attributes.Count > 0)
                        {
                            if (idx_price > -1)
                            {
                                if (item.Value.level_price > 0)
                                {
                                    var inputHarga = attributes[idx_price].FindElement(By.CssSelector("input"));
                                    LetsWait().Until(ExpectedConditions.ElementToBeClickable(inputHarga));
                                    inputHarga.Clear();
                                    inputHarga.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                    inputHarga.SendKeys(OpenQA.Selenium.Keys.Delete);
                                    executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", inputHarga);
                                    await Task.Delay(250).ConfigureAwait(false);
                                    inputHarga.SendKeys(item.Value.level_price.ToString());
                                    executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", inputHarga);
                                    await Task.Delay(250).ConfigureAwait(false);
                                }
                                else if (item.Value.level_price == 0)
                                {
                                    logControl.Log("Harga belum di isi untuk sku " + item.Key, Color.Red);
                                }
                            }
                            else
                            {
                                logControl.Log("Atribut harga tidak ditemukan untuk SKU " + item.Key, Color.Red);
                            }


                            if (idx_stock > -1)
                            {
                                if (item.Value.stock > -1)
                                {
                                    var elstock = attributes[idx_stock].FindElement(By.CssSelector("input"));
                                    LetsWait().Until(ExpectedConditions.ElementToBeClickable(elstock));
                                    elstock.Clear();
                                    elstock.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                    elstock.SendKeys(OpenQA.Selenium.Keys.Delete);
                                    executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elstock);
                                    await Task.Delay(250).ConfigureAwait(false);
                                    elstock.SendKeys(item.Value.stock.ToString());
                                    executor.ExecuteScript("var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", elstock);
                                }
                            }
                            else
                            {
                                logControl.Log("Atribut stok tidak ditemukan untuk SKU " + item.Key, Color.Red);
                            }
                        }
                        else
                        {
                            logControl.Log("Data atribut tidak ditemukan untuk SKU " + item.Key, Color.Red);
                        }

                        
                        executor.ExecuteScript("arguments[0].click();", driver.FindElement(By.CssSelector(".price__stock .btn-confirm")));
                        try
                        {
                            var alert = driver.FindElement(By.CssSelector(".sweetalert__content .sweetalert__content_btncls_okButton"));
                            alert.Click();
                        }
                        catch (Exception){}
                        LetsWait(10).Until(driver => (driver.FindElements(By.CssSelector(".toaster__success")).Count > 0));
                        //cek ada error inputan
                        //var other_errors = driver.FindElements(By.ClassName("vee-validate-dropdown"));
                        //if (other_errors.Count > 0)
                        //{
                        //    logControl.Log("Ada data utama produk yang belum di lengkapi untuk sku " + item.Key, System.Drawing.Color.Red);
                        //    reportFailedSync(item);
                        //    continue;
                        //}


                        successProducts.Add(item.Key);
                        logControl.Log("Sinkronisasi berhasil.", System.Drawing.Color.Green);
                        processCounter++;
                        if (processCounter % clearCacheInterval == 0)
                        {
                            if (config.blibli_browser == 0)
                            {
                                try
                                {
                                    driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                                    await Task.Delay(2000).ConfigureAwait(false);
                                    var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                                    button.Click();
                                }
                                catch (Exception)
                                {
                                    logControl.Log("Cannot clear cache.");
                                }
                            }
                        }
                    }
                    else
                    {
                        logControl.Log("SKU belum ditentukan untuk produk ini.", System.Drawing.Color.Yellow);
                        if (cache.products.ContainsKey(item.Key))
                        {
                            var product = cache.products[item.Key];
                            if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                            {
                                mainWindow.listMonitored[product.id_ipos].blibli_id = "0";
                                mainWindow.listMonitored[product.id_ipos].blibli_status = 0;
                            }
                        }
                        Console.WriteLine("no cache 1");
                    }
                    noitem++;

                }
                catch (Exception e)
                {
                    logControl.Log(e.Message + e.StackTrace);
                    Console.WriteLine(e.Message.ToLower());
                    if (e.Message.ToLower().Contains("alert"))
                    {
                        logControl.Log(e.Message.ToLower());
                        Console.WriteLine("Dismissing alert");
                        AcceptDialog();
                        noitem++;
                        continue;
                    }

                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        logControl.Log("Gagal menginput perubahan, Mencoba menginput ulang perubahan data...", System.Drawing.Color.Yellow);
                        goto loopUpdate;
                    }
                    else
                    {
                        reportFailedSync(item);
                        continue;
                    }
                }
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                if (mainWindow.isSyncStopped()) return;
            }

            //logControl.Log("Test shopee 4");
            foreach (var item in successProducts)
            {
                logControl.timeoutSecs = 0;
                if (cache.products.ContainsKey(item) && cache.marketID.ContainsKey(item))
                {
                    var product = cache.products[item];
                    var id = cache.marketID[item];
                    if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                    {
                        mainWindow.listMonitored[product.id_ipos].blibli_id = id;
                        mainWindow.listMonitored[product.id_ipos].blibli_status = 1;
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }
            Console.WriteLine("Done deleting");
            logControl.Log("Sinkronisasi produk selesai.", System.Drawing.Color.Green);
            mainWindow.Log("Sinkronisasi " + marketName + " selesai.", System.Drawing.Color.Green);
            logControl.isWorking = false;
            isSyncDone = true;
            isStuck = false;
            SaveCache();

        }

        void reportFailedSync(System.Collections.Generic.KeyValuePair<string, BlibliProduct> item)
        {
            TakeScreenshot(item.Key);
            logControl.Log("Sinkronisasi  gagal untuk sku " + item.Key, System.Drawing.Color.Red);
            if (cache.products.ContainsKey(item.Key) && cache.marketID.ContainsKey(item.Key))
            {
                var product = cache.products[item.Key];
                var id = cache.marketID[item.Key];
                if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                {
                    mainWindow.listMonitored[product.id_ipos].blibli_id = id;
                    mainWindow.listMonitored[product.id_ipos].blibli_status = 2;
                }
            }
            AcceptDialog();
        }


        void TakeScreenshot(string name)
        {
            try
            {
                Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                var folder = Application.StartupPath + "/ss/blibli/";
                Directory.CreateDirectory(folder);
                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                {
                    name = name.Replace(c, '_');
                }
                ss.SaveAsFile(folder + name + ".jpg", ScreenshotImageFormat.Jpeg);
            }
            catch (Exception e)
            {
                mainWindow.Log("Gagal screenshot, proses telah ditutup / tidak ditemukan.");
            }
        }

        public void showLog()
        {
            if (logWindow != null)
            {
                mainWindow.Log("Membuka log blibli...", Color.Green);
                logControl.Show();
                logControl.BringToFront();
            }
        }
    }
}
