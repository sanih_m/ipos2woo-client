﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using SeleniumExtras.WaitHelpers;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync.Modules
{
    public class ShopeeCore
    {
        public const string cachename = "/cache_shopee_{IDX}.json";

        IWebDriver driver;
        ChromeDriverService service;
        string cache_file = Application.StartupPath + "/cache_shopee_{IDX}.json";
        string log_sales_file = Application.StartupPath + "/" + "sales_shopee_{IDX}.log";

        public CacheShopeeProduct cache;
        public List<ShopeeSale> newSales = new List<ShopeeSale>();

        const int maxLoginTry = 5;
        int currentLoginTry = 0;

        const int maxMainTry = 3;
        int currentMainTry = 0;

        const int maxMainWait = 60;

        const int marketIndex = 1;
        const int clearCacheInterval = 1;
        public const string marketName = "Shopee";

        int idxProfile = 0;

        public bool isDone = false;
        public bool isSyncDone = false;
        public bool isLoggedIn = false;
        public bool isStuck = false;
        public string loginResponse = "";

        frmProfileSync mainWindow;
        public ucMarketplaceLog logControl;
        frmLogViewer logWindow;

        bool isLogInit = false;
        public ShopeeCore(int idx)
        {  
            try
            {
                idxProfile = idx;
                var config = Program.config.profiles[idxProfile];
                mainWindow = Program.Syncers[config.name];

                var chromeProfileFolder = "session_" + config.id + "_shopee";
                //var chromeDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Google\\Chrome\\User Data\\");
                var chromeDir = Application.StartupPath + "\\";
                if (!Directory.Exists(chromeDir + chromeProfileFolder))
                {
                    Directory.CreateDirectory(chromeDir + chromeProfileFolder);
                }

                var options = new ChromeOptions();
                if (config.shopee_engine == 0)
                {
                    options.BinaryLocation = Application.StartupPath + "\\engine_chrome\\chrome.exe";
                }
                if (config.shopee_dev != Program.devPassword)
                {
                    options.AddArgument("--window-position=-32000,-32000");
                }
                options.AddArgument("disable-popup-blocking");
                options.AddArgument("disable-notifications");
                //options.AddArgument("disable-extensions");
                options.AddArgument("disable-infobars");
                options.AddArgument("--start-maximized");
                if(config.shopee_profile == 0)
                {
                    options.AddArgument("user-data-dir=" + chromeDir + chromeProfileFolder);
                }
                else if(config.shopee_profile == 1)
                {
                    options.AddArgument("user-data-dir=/Users/User/AppData/Local/Google/Chrome/User Data");
                }
                options.AddExcludedArguments(new List<string>() { "enable-automation" });
                options.PageLoadStrategy = PageLoadStrategy.Eager;

                //mainWindow.Log(config.shopee_proxy_enabled.ToString());
                if (config.shopee_proxy_enabled)
                {
                    var p_scheme = "";
                    var p_address = "";
                    var p_port = "";
                    var p_username = "";
                    var p_password = "";
                    var proxy = new Proxy();
                    var isSSL = false;
                    //mainWindow.Log(config.shopee_proxy_enabled.ToString());
                    if (config.shopee_proxy_mode == 0)
                    {
                        mainWindow.Log("Mengambil proxy untuk Shopee dari server.");
                        var success = false;
                        var proxyAddress = "";
                        while (!success)
                        {

                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            RestClient client = new RestClient("https://app.ipos2woo.com/proxy/index.php");

                            RestRequest request = new RestRequest();
                            IRestResponse response = client.Execute(request);
                            proxyAddress = response.Content;
                            try
                            {
                                var splitted = proxyAddress.Split(':');
                                var testProxy = new WebProxy(splitted[0], int.Parse(splitted[1]));
                                response = new RestClient
                                {
                                    BaseUrl = new System.Uri("https://seller.shopee.co.id/"),
                                    Proxy = testProxy,
                                    Timeout = 5000,
                                    ReadWriteTimeout = 5000,
                                }.Execute(new RestRequest
                                {
                                    Method = Method.GET,
                                    Timeout = 5000,
                                    ReadWriteTimeout = 5000,
                                });
                                if (response.ErrorException != null)
                                {
                                    throw response.ErrorException;
                                }
                                success = (!response.Content.Equals(""));
                                mainWindow.Log(response.Content);
                            }
                            catch (Exception ex)
                            {
                                mainWindow.Log(ex.Message);
                                success = false;
                            }

                            if (!success)
                            {
                                proxyAddress = "";
                            }
                        }


                        p_scheme = "http";
                        p_address = proxyAddress.Split(':')[0];
                        p_port = proxyAddress.Split(':')[1];
                        p_username = "";
                        p_password = "";
                    }
                    else
                    {
                        isSSL = config.shopee_proxy_address.Contains("https:");
                        p_scheme = isSSL ? "https" : "http";
                        p_address = config.shopee_proxy_address.Replace("http", "").Replace("https", "").Replace("://", "");
                        p_port = config.shopee_proxy_port;
                        p_username = config.shopee_proxy_username;
                        p_password = config.shopee_proxy_password;
                    }


                    if (File.Exists("shopee_proxy.zip")) File.Delete("shopee_proxy.zip");
                    File.WriteAllText("shopee_background.js",
                        Properties.Resources.background_js
                        .Replace("YOUR_PROXY_SCHEME", p_scheme)
                        .Replace("YOUR_PROXY_ADDRESS", p_address)
                        .Replace("YOUR_PROXY_PORT", p_port)
                        .Replace("YOUR_PROXY_USERNAME", p_username)
                        .Replace("YOUR_PROXY_PASSWORD", p_password)
                    );
                    File.WriteAllText("shopee_manifest.json",
                        Properties.Resources.manifest_json);
                    using (ZipArchive zip = ZipFile.Open("shopee_proxy.zip", ZipArchiveMode.Create))
                    {
                        zip.CreateEntryFromFile(@"shopee_background.js", "background.js");
                        zip.CreateEntryFromFile(@"shopee_manifest.json", "manifest.json");
                    }
                    options.AddExtension("shopee_proxy.zip");
                    File.Delete("shopee_background.js");
                    File.Delete("shopee_manifest.json");

                }

                service = ChromeDriverService.CreateDefaultService();
                service.HideCommandPromptWindow = true;



                driver = new ChromeDriver(service, options);
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                cache_file = cache_file.Replace("{IDX}", idxProfile.ToString());
                LoadCache();
            }
            catch (Exception ex)
            {
                mainWindow.Log("Gagal inisialisasi Shopee.", Color.Red);
                mainWindow.Log(ex.Message);
                Program.WriteDevLog(marketName, ex.Message, ex.StackTrace);
                isDone = true;
            }
            
        }

        WebDriverWait LetsWait(int waitSec = -1)
        {
            IClock clock = new SystemClock();
            return new WebDriverWait(clock, driver, TimeSpan.FromSeconds(waitSec == -1 ? maxMainWait : waitSec), TimeSpan.FromSeconds(0.1));
        }

        void handleStuck(object sender, EventArgs e)
        {
            isStuck = true;
            isSyncDone = true;
            mainWindow.Log("Terjadi gangguan pada sinkronisasi " + marketName + ". Modul ini akan dimulai ulang...");
        }

        public void initLog()
        {
            if (isLogInit)
            {
                return;
            }
            isLogInit = true;
            logControl = mainWindow.CreateMarketplaceTab(marketIndex, marketName);
            logControl.WorkTimeout += handleStuck;
            //logWindow = new frmLogViewer(config.name + " : Log Shopee");
        }

        bool isDialogPresent(IWebDriver driver)
        {
            IAlert alert = ExpectedConditions.AlertIsPresent().Invoke(driver);
            return (alert != null);
        }

        public void stop()
        {
            try
            {
                driver.Quit();
                mainWindow.DeleteMarketplaceTab(marketName);
                //create list of process id
                var driverProcessIds = new List<int> { service.ProcessId };

                //Get all the childs generated by the driver like conhost, chrome.exe...
                var mos = new System.Management.ManagementObjectSearcher($"Select * From Win32_Process Where ParentProcessID={service.ProcessId}");
                foreach (var mo in mos.Get())
                {
                    var pid = Convert.ToInt32(mo["ProcessID"]);
                    driverProcessIds.Add(pid);
                }
                //Kill all
                foreach (var id in driverProcessIds)
                {
                    var p = System.Diagnostics.Process.GetProcessById(id);
                    if (p != null) p.Kill();
                }
            }
            catch (Exception e)
            {
                mainWindow.Log(e.Message, Color.Red);
            }
        }

        public async Task LoginAsync()
        {
            mainWindow.Log("Otorisasi Shopee...");
            var config = Program.config.profiles[idxProfile];
            bool isOtp = false;
            bool isResendOTP = false;

            sendEmailWhenLogin:
            try
            {
                driver.Navigate().GoToUrl("https://seller.shopee.co.id/");
                LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                LetsWait(10).Until(driver => driver.FindElements(By.XPath("//*[@placeholder='Email/Telepon/Username']")).Count > 0);
                driver.FindElement(By.XPath("//*[@placeholder='Email/Telepon/Username']")).SendKeys(config.shopee_email);
                driver.FindElement(By.XPath("//*[@type='password']")).SendKeys(config.shopee_password);
            }
            catch (Exception ex)
            {
                //mainWindow.Log(ex.Message + ex.StackTrace);
                goto checkUserInfo;
            }

            try
            {
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                driver.FindElement(By.ClassName("shopee-checkbox")).Click();
                driver.FindElement(By.ClassName("shopee-button--primary")).Click();
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    
                    mainWindow.Log("Login shopee gagal, Silahkan cek koneksi internet anda.", System.Drawing.Color.Red);
                    isDone = true;
                    return;
                }
            }

            
            needOtp:
            isOtp = false;
            isResendOTP = false;
            try
            {
               // driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                isOtp = driver.FindElement(By.ClassName("shopee-otp-verification")) != null;
                if (isOtp)
                { 
                    mainWindow.Log("Login shopee butuh kode OTP, silahkan ketikan KODE OTP yang masuk ke perangkat anda disini.");
                    mainWindow.Log("Ketikan 'skip' untuk melanjutkan proses tanpa sinkronisasi shopee.");
                    mainWindow.Log("Ketikan 'resend' untuk mengirim ulang kode OTP.");
                    while (mainWindow.command.Equals(""))
                    {
                        Console.WriteLine("Waiting for command");
                        await Task.Delay(1).ConfigureAwait(false);
                    }
                    if (mainWindow.command.Equals("skip"))
                    {
                        mainWindow.Log("Login shopee gagal, kode otp dibatalkan.", System.Drawing.Color.Red);
                        driver.Navigate().GoToUrl("https://seller.shopee.co.id/");
                        isDone = true;
                        return;
                    }else if (mainWindow.command.Equals("resend"))
                    {
                        isResendOTP = true;
                        mainWindow.Log("OTP akan dikirimkan ulang.");
                        driver.FindElement(By.ClassName("shopee-button--large")).Click();
                        goto needOtp;
                    }
                    else
                    {
                        var el = driver.FindElement(By.TagName("input"));
                        LetsWait().Until(ExpectedConditions.ElementToBeClickable(el));
                        el.Clear();
                        el.SendKeys(mainWindow.command);
                        driver.FindElement(By.ClassName("shopee-button--primary")).Click();
                        mainWindow.Log("Menginput kode OTP...");
                    }
                    mainWindow.command = "";
                }
                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                Console.WriteLine("Login berhasil");
            }



            try
            {
                if (isOtp)
                {
                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                    driver.FindElement(By.ClassName("shopee-toast__content"));
                    mainWindow.Log("Kode OTP ditolak.", System.Drawing.Color.Red);
                    Console.WriteLine("OTP Denied");
                    goto needOtp;
                }

            }
            catch (Exception)
            {
                mainWindow.Log("Kode OTP sedang diproses...");
                Console.WriteLine("OTP Received");
            }
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);


            try
            {
                if (isOtp)
                {
                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                    driver.FindElement(By.XPath("//*[@placeholder='Email/Telepon/Username']"));
                    mainWindow.Log("Kode OTP expired, melakukan login ulang shopee..");
                    Console.WriteLine("OTP expired, retry");
                    goto sendEmailWhenLogin;
                }
            }
            catch (Exception)
            {
                if (isOtp && isResendOTP)
                {
                    Console.WriteLine("Reinput OTP");
                    goto needOtp;
                }
                else
                {
                    Console.WriteLine("No problem");
                }
                
            }
            

            checkUserInfo:
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
            try
            {
                LetsWait().Until(driver => driver.FindElements(By.ClassName("account-name")).Count > 0);
                mainWindow.Log("Proses login shopee berhasil.", System.Drawing.Color.Green);
                currentLoginTry = 0;

                IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                await Task.Delay(2000).ConfigureAwait(false);
                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                var checkbox = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog').querySelector('iron-pages > div > settings-checkbox:nth-child(3)').shadowRoot.querySelector('cr-checkbox')");
                if (checkbox.GetAttribute("checked") != null)
                {
                    checkbox.Click();
                }
                button.Click();

                isDone = true;
            }
            catch (Exception)
            {
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    isDone = true;
                    mainWindow.Log("Login shopee gagal, Silahkan cek koneksi internet anda.", System.Drawing.Color.Red);
                    return;
                }
            }

            loginResponse = "";
            isLoggedIn = true;
        }

        void AcceptDialog()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                if (alert != null) alert.Accept();
            }
            catch (Exception ex)
            {
                //logControl.Log("dialog error : " + ex.Message.ToLower());
            }
        }

        public void RemoveProductFromCache(string sku, bool isSave = false)
        {
            cache.products.Remove(sku);
            if (isSave) SaveCache();
        }

        void SaveCache()
        {
            cache.lastupdate = DateTime.Now;
            var json = JsonConvert.SerializeObject(cache);
            File.WriteAllText(cache_file, json);
            File.Copy(cache_file, cache_file + ".backup", true);
        }

        void LoadCache()
        {
            try
            {
                var data = File.ReadAllText(cache_file);
                cache = JsonConvert.DeserializeObject<CacheShopeeProduct>(data);
                if (cache == null)
                {
                    cache = new CacheShopeeProduct();
                }
                else
                {
                    if (cache.products == null)
                    {
                        cache.products = new Dictionary<string, ShopeeProduct>();
                    }
                    if (cache.marketID == null)
                    {
                        cache.marketID = new Dictionary<string, string>();
                    }
                    if (cache.sales == null)
                    {
                        cache.sales = new Dictionary<string, ShopeeSale>();
                    }
                }
            }
            catch (Exception)
            {
                cache = new CacheShopeeProduct();
            }
        }

        public void ResetCache()
        {
            cache.products.Clear();
            SaveCache();
        }

        public void ResetSaleCache()
        {
            cache.sales.Clear();
            SaveCache();
        }

        async Task SendSalesToMonitoringAsync(List<ShopeeSale> sales)
        {
            logControl.Log("Mengirim data penjualan Shopee ke monitoring...");
            var config = Program.config.profiles[idxProfile];
            var data = "";
            foreach (var item in sales)
            {
                logControl.timeoutSecs = 0;
                if (!data.Equals(""))
                {

                    data += "#R#";
                }

                var c = Program.config.branchID + "|C|" +
                        config.id + "|C|" +
                        Program.Base64Encode(item.invoice) + "|C|" +
                        Program.Base64Encode(item.date.ToString("yyyy-MM-dd HH:mm")) + "|C|" +
                        Program.Base64Encode(item.buyer) + "|C|" +
                        Program.Base64Encode(item.address) + "|C|" +
                        (item.products) + "|C|" +
                        Program.Base64Encode(item.shipname) + "|C|" +
                        Program.Base64Encode(item.total.ToString()) + "|C|" +
                        Program.Base64Encode(item.link) + "|C|" +
                        item.type + "|C|" +
                        (item.is_pass ? 1 : 0);
                data = data + c;
            }
            try
            {
                
                var url = Program.config.monitorURL.AppendPathSegment("/api/marketplace_sales_sync");
                File.WriteAllText(log_sales_file, data);
                var resp = await url
                .PostMultipartAsync((mp) =>
                {
                    mp.AddString("id_branch", Program.config.branchID)
                    .AddString("id_profile", config.id.ToString())
                    .AddString("market_name", marketName.ToLower())
                    .AddFile("new_data", log_sales_file);
                }).ReceiveString().ConfigureAwait(false);

                var obj = SimpleJSON.JSON.Parse(resp);
                if (obj != null)
                {
                    if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                    {
                        logControl.Log("Pengiriman sukses..");
                    }
                    else
                    {
                        logControl.Log(resp, Color.Red);
                    }
                }
                else
                {
                    logControl.Log(resp, Color.Red);
                }
            }
            catch (Exception e)
            {
                logControl.Log(e.Message + " " + e.StackTrace);
            }
        }


        public void SyncCache()
        {
            var config = Program.config.profiles[idxProfile];
            foreach (var source in mainWindow.cache_product.data)
            {
                if (cache.marketID.ContainsKey(source.Value.sku))
                {
                    ShopeeProduct p = new ShopeeProduct();
                    p.id_ipos = source.Value.id;
                    p.sku = source.Value.sku;
                    p.level_index = config.shopee_price;
                    if (p.level_index == 1)
                    {
                        p.level_price = source.Value.price1;
                    }
                    else if (p.level_index == 2)
                    {
                        p.level_price = source.Value.price2;
                    }
                    else if (p.level_index == 3)
                    {
                        p.level_price = source.Value.price3;
                    }
                    else if (p.level_index == 4)
                    {
                        p.level_price = source.Value.price4;
                    }
                    p.stock = source.Value.stock;
                    if (cache.products.ContainsKey(p.sku))
                    {
                        cache.products[p.sku] = p;
                    }
                    else
                    {
                        cache.products.Add(p.sku, p);
                    }
                }
            }
            SaveCache();
        }

        public void SyncSalesToMonitoring()
        {
            logControl.Log("Melakukan proses penyamaan data penjualan ke monitoring...");
            if (cache.sales.Count == 0)
            {
                logControl.Log("Tidak ada data penjualan yang bisa dikirim ke monitoring.");
                return;
            }
            List<ShopeeSale> sales = new List<ShopeeSale>();
            foreach (var item in cache.sales)
            {
                sales.Add(item.Value);
            }
            Task.Run(() => SendSalesToMonitoringAsync(sales)).Wait();
        }



        async Task SendSalesByPart(ProfileConfig config)
        {
            if (newSales.Count > 0)
            {
                logControl.Log(newSales.Count + " data penjualan baru ditemukan.", System.Drawing.Color.Green);
                await SendSalesToMonitoringAsync(newSales).ConfigureAwait(false);
                if (mainWindow.instance_ipos != null)
                {
                    if (config.shopee_jual_tujuan == 0)
                    {
                        logControl.Log("Mengirim penjualan Shopee ke pesanan ipos...");
                        IPOSOptionSalesOrder options = new IPOSOptionSalesOrder();
                        options.customer = config.shopee_sale_custcode;
                        options.seller = config.shopee_sale_sellercode;
                        options.warehouse = config.shopee_sale_warehousecode;
                        options.userid = config.shopee_sale_userid;
                        options.iposver = config.db_type == 0 ? 4 : 5;
                        List<String> result = await mainWindow.instance_ipos.SyncSalesToOrder(getIposSalesOrder(), options).ConfigureAwait(false);
                        if (result != null) logControl.Log(result.Count+" penjualan telah masuk ke pesanan ipos");
                    }
                    else if (config.shopee_jual_tujuan == 1)
                    {
                        logControl.Log("Mengirim penjualan Shopee ke penjualan kantor ipos...");
                        IPOSOptionOfficeSale options = new IPOSOptionOfficeSale();
                        options.customer = config.shopee_sale_custcode;
                        options.seller = config.shopee_sale_sellercode;
                        options.warehouse = config.shopee_sale_warehousecode;
                        options.paymethod = config.shopee_jual_tipebayar;
                        options.acc_tunai = config.shopee_sale_acctunai;
                        options.acc_kredit = config.shopee_sale_acckredit;
                        options.acc_pot = config.shopee_sale_accpot;
                        options.acc_blain = config.shopee_sale_accblain;
                        options.userid = config.shopee_sale_userid;
                        options.iposver = config.db_type == 0 ? 4 : 5;
                        List<String> result = await mainWindow.instance_ipos.SyncSalesToOfficeSale(getIposOfficeSale(), options).ConfigureAwait(false);
                        if(result != null) logControl.Log(result.Count + " penjualan telah masuk ke penjualan kantor ipos");
                    }
                    else
                    {
                        logControl.Log("Mengirim penjualan Shopee ke penjualan kasir ipos...");
                        IPOSOptionCashierSale options = new IPOSOptionCashierSale();
                        options.customer = config.shopee_sale_custcode;
                        options.seller = config.shopee_sale_sellercode;
                        options.warehouse = config.shopee_sale_warehousecode;
                        options.paymethod = config.shopee_jual_tipebayar;
                        options.acc_tunai = config.shopee_sale_acctunai;
                        options.acc_kredit = config.shopee_sale_acckredit;
                        options.acc_pot = config.shopee_sale_accpot;
                        options.acc_blain = config.shopee_sale_accblain;
                        options.userid = config.shopee_sale_userid;
                        options.iposver = config.db_type == 0 ? 4 : 5;
                        List<String> result = await mainWindow.instance_ipos.SyncSalesToCashierSale(getIposCashierSale(), options).ConfigureAwait(false);
                        if (result != null) logControl.Log(result.Count + " penjualan telah masuk ke penjualan kasir ipos");
                    }
                }
                SaveCache();
                newSales.Clear();
                if (mainWindow.isSyncStopped()) return;
            }
            else
            {
                logControl.Log("Tidak ada data penjualan yang akan dikirimkan pada tahap ini.");
            }
            return;
        }

        public async Task Sync(Dictionary<string, SingleProduct> listProducts, Dictionary<string, Product> liveProducts = null, List<string> specificProducts = null)
        {
            LoadCache();
            while (logControl == null)
            {
                await Task.Delay(1000).ConfigureAwait(false);
            }
            logControl.isClearLog = true;
            logControl.isWorking = true;
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            if (mainWindow.isSyncStopped()) return;
            mainWindow.Log("Memulai pengecekan sinkronisasi Shopee.", System.Drawing.Color.Yellow);
            mainWindow.Log("Silahkan lihat tab 'Shopee' untuk melihat laporan sinkronisasi.");
            
            isSyncDone = false;
            isStuck = false;
            var config = Program.config.profiles[idxProfile];

            logControl.Log("Verifikasi sesi login...");
            var lastUrl = (string)executor.ExecuteScript("return window.location.href;");
            logControl.Log(lastUrl);
            if (lastUrl.Contains("seller.shopee.co.id/portal/settings/basic/shop"))
            {
                lastUrl = "";
                executor.ExecuteScript("return window.location.reload();");
            }
            else
            {
                driver.Navigate().GoToUrl("https://seller.shopee.co.id/portal/settings/basic/shop");
            }
            try
            {
                LetsWait(30).Until(driver => !lastUrl.Equals((string)executor.ExecuteScript("return window.location.href")));
                logControl.Log((string)executor.ExecuteScript("return window.location.href"));
                LetsWait(30).Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            }
            catch (Exception)
            {
                logControl.Log("Terdapat gangguan koneksi pada saat mengakses Shopee.", System.Drawing.Color.Red);
                logControl.isWorking = false;
                isSyncDone = true;
                isStuck = false;
                return;
            }
            
            if (driver.Url.Contains("/signin"))
            {
                mainWindow.Log("Melakukan proses login ulang...");
                await LoginAsync().ConfigureAwait(false);
            }
            
            if (config.shopee_jual_on)
            {
                loopSales:
                try
                {
                    List<string> salesLinks = new List<string>();
                    newSales.Clear();
                    //mainWindow.Log("cache sales : " + cache.sales.Count.ToString());
                    logControl.Log("Memulai pengecekan data penjualan Shopee.", System.Drawing.Color.Yellow);
                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                    var sale_type = "";
                    var sale_subtype = "";
                    var types = config.shopee_sale_tab.Split('-');
                    if(types.Length == 2)
                    {
                        sale_type = types[0];
                        if(sale_type.Equals("toship"))
                        {
                            sale_subtype = "source=" + types[1];
                        }
                        else
                        {
                            sale_subtype = "source=" + types[1];
                        }
                    }
                    else
                    {
                        sale_type = config.shopee_sale_tab;
                    }
                    driver.Navigate().GoToUrl("https://seller.shopee.co.id/portal/sale/?type=" + sale_type + (sale_subtype.Equals("") ? "" : "&"+sale_subtype));
                    LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                    await Task.Delay(5000).ConfigureAwait(false);
                    /*if(driver.FindElements(By.CssSelector(".filter-sort .shopee-tabs__nav-tab")).Count > 0)
                    {
                        var els = driver.FindElements(By.CssSelector(".filter-sort .shopee-tabs__nav-tab"));
                        if(els[els.Count - 1] != null) els[els.Count - 1].Click();
                        await Task.Delay(1000).ConfigureAwait(false);
                    }*/
                    if (driver.FindElements(By.CssSelector(".page.shipment .no-data")).Count == 0)
                    {
                        LetsWait(60).Until(driver => driver.FindElements(By.CssSelector("a.order-item")).Count > 0);
                        if (driver.FindElements(By.CssSelector("a.order-item")).Count > 0)
                        {
                            
                            var idx = 0;
                            var pContainer = driver.FindElements(By.ClassName("shopee-pager__pages"));
                            var total = 1;
                            if (pContainer.Count > 0)
                            {
                                var pages = pContainer[0].FindElements(By.ClassName("shopee-pager__page"));
                                total = int.Parse(pages[pages.Count - 1].Text);
                            }
                            var nextbutton = driver.FindElements(By.ClassName("shopee-pager__button-next"));
                            while (idx < total)
                            {
                                logControl.timeoutSecs = 0;
                                if (idx > 0)
                                {
                                    if (nextbutton.Count > 0 && nextbutton[0].GetAttribute("disabled") == null)
                                    {
                                        executor.ExecuteScript("arguments[0].click();", nextbutton[0]);
                                        LetsWait().Until(driver => (driver.FindElements(By.CssSelector(".shopee-pager__page.active")).Count > 0 && driver.FindElement(By.CssSelector(".shopee-pager__page.active")).Text.Equals((idx+1).ToString())));
                                    }
                                }
                                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                                var sales = driver.FindElements(By.ClassName("order-item"));
                                if (sales.Count > 0)
                                {
                                    logControl.Log("Mengambil data penjualan (step " + (idx + 1).ToString() + "/" + total.ToString() + ")");
                                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                                    foreach (var sale in sales)
                                    {
                                        var href = sale.GetAttribute("href").Split('/');
                                        if (href.Length > 0)
                                        {
                                            salesLinks.Add("https://seller.shopee.co.id/api/v3/order/get_one_order?SPC_CDS=89d34078-090d-4e1d-a873-a1caa698dc74&SPC_CDS_VER=2&order_id=" + href[href.Length - 1]);
                                        }
                                        //salesLinks.Add(sale.GetAttribute("href"));
                                    }
                                }
                                idx++;
                                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                                if(nextbutton.Count > 0) nextbutton = driver.FindElements(By.ClassName("shopee-pager__button-next"));

                            }
                            //mainWindow.Log("cache sales : " + cache.sales.Count.ToString());
                        }
                        else
                        {
                            logControl.Log("Data penjualan tidak bisa diambil.");
                        }
                    }
                    else
                    {
                        logControl.Log("Data penjualan tidak ditemukan.");
                    }

                    if (salesLinks.Count > 0)
                    {
                        //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                        var nos = 0;
                        foreach (var link in salesLinks)
                        {
                            nos++;
                            logControl.timeoutSecs = 0;
                            if (mainWindow.isSyncStopped()) return;
                            driver.Navigate().GoToUrl(link);
                            LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                            var html = driver.FindElement(By.TagName("pre")).GetAttribute("innerHTML");
                            var obj = SimpleJSON.JSON.Parse(html);
                            if (obj != null && Program.StripQuotes(obj["message"].ToString()) == "success")
                            {
                                var data = obj["data"];
                                ShopeeSale objSale = new ShopeeSale();
                                objSale.invoice = Program.StripQuotes(data["order_sn"]).ToString();
                                objSale.link = "https://seller.shopee.co.id/portal/sale/" + objSale.invoice;
                                objSale.invoice = config.shopee_sale_prefix + objSale.invoice;

                                if (cache.sales != null && cache.sales.ContainsKey(objSale.invoice))
                                {
                                    logControl.Log("(" + nos + "/" + (salesLinks.Count) + ") " + objSale.invoice + " sudah diambil sebelumnya.", Color.Yellow);
                                    continue;
                                }
                                else
                                {
                                    logControl.Log("(" + nos + "/" + (salesLinks.Count) + ") " + objSale.invoice + " berhasil diambil.", Color.Green);
                                }

                                objSale.date = DateTime.Now;
                                objSale.type = config.shopee_jual_tujuan;
                                objSale.status = Program.StripQuotes(data["status"].ToString());

                                objSale.buyer = Program.StripQuotes(data["buyer_address_name"].ToString()).ToUpper();
                                objSale.address = Program.StripQuotes(data["shipping_address"].ToString());

                                objSale.shipname = Program.StripQuotes(data["actual_carrier"].ToString());
                                if (data["shipping_traceno"] != null)
                                {
                                    objSale.shiptrack = Program.StripQuotes(data["shipping_traceno"].ToString());
                                }


                                objSale.products = "";
                                objSale.is_pass = true;
                                var arr = data["order_items"].AsArray;
                                objSale.subtotal = 0;
                                var tempProducts = "";
                                foreach (JSONNode p in arr)
                                {
                                    JSONNode model = null;
                                    var is_pass = true;
                                    var name = "";
                                    int qty = 0;
                                    long price = 0;
                                    var sku = "";
                                    var note = "";
                                    if (p["bundle_deal_id"] != null && Program.StripQuotes(p["bundle_deal_id"].ToString()) != "0")
                                    {
                                        var arrBundle = p["bundle_deal_model"] != null ? p["bundle_deal_model"] : p["bundle_deal_product"];
                                        foreach (JSONNode b in arrBundle.AsArray)
                                        {
                                            is_pass = true;
                                            name = "";
                                            qty = 0;
                                            price = 0;
                                            sku = "";
                                            note = "";

                                            name = Program.StripQuotes(b["name"].ToString());
                                            int.TryParse(Program.StripQuotes(p["amount"].ToString()), out qty);
                                            long.TryParse(Program.StripQuotes(b["price"].ToString()).Replace(".00", ""), out price);
                                            objSale.subtotal += (price * qty);
                                            sku = b["sku"];
                                            if (sku != null)
                                            {
                                                sku = Program.StripQuotes(sku.ToString());
                                                is_pass = !sku.Equals("") && liveProducts.ContainsKey(sku);
                                                if (!is_pass)
                                                {
                                                    objSale.is_pass = false;
                                                }
                                                else
                                                {
                                                    if (config.shopee_jual_tujuan > 0)
                                                    {
                                                        var cp = liveProducts[sku];
                                                        if (cp.stockdetail != null && cp.stockdetail.ContainsKey(config.shopee_sale_warehousecode))
                                                        {
                                                            if (qty > cp.stockdetail[config.shopee_sale_warehousecode].stock && config.shopee_jual_stokminus == 0)
                                                            {
                                                                is_pass = false;
                                                                objSale.is_pass = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            is_pass = false;
                                                            objSale.is_pass = false;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                sku = "";
                                                objSale.is_pass = false;
                                                is_pass = false;
                                            }
                                            note = "";
                                            if (tempProducts != "")
                                            {
                                                tempProducts = tempProducts + ";";
                                            }
                                            tempProducts = tempProducts + name + "|#|" + sku + "|#|" + qty + "|#|" + price + "|#|" + (is_pass ? 1 : 0) + "|#|" + Program.Base64Encode(note);
                                        }
                                    }
                                    else
                                    {
                                        model = p["item_model"] != null && p["item_model"]["sku"] != null ? p["item_model"] : p["product"];
                                        name = Program.StripQuotes(model["name"].ToString());
                                        int.TryParse(Program.StripQuotes(p["amount"].ToString()), out qty);
                                        long.TryParse(Program.StripQuotes(p["item_price"].ToString()).Replace(".00", ""), out price);
                                        objSale.subtotal += (price * qty);
                                        sku = model["sku"];
                                        if (sku != null)
                                        {
                                            sku = Program.StripQuotes(sku.ToString());
                                            is_pass = !sku.Equals("") && liveProducts.ContainsKey(sku);
                                            if (!is_pass)
                                            {
                                                objSale.is_pass = false;
                                            }
                                            else
                                            {
                                                if (config.shopee_jual_tujuan > 0)
                                                {
                                                    var cp = liveProducts != null ? liveProducts[sku] : null;
                                                    if (cp != null && cp.stockdetail != null && cp.stockdetail.ContainsKey(config.shopee_sale_warehousecode))
                                                    {
                                                        if (qty > cp.stockdetail[config.shopee_sale_warehousecode].stock && config.shopee_jual_stokminus == 0)
                                                        {
                                                            is_pass = false;
                                                            objSale.is_pass = false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        is_pass = false;
                                                        objSale.is_pass = false;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            sku = "";
                                            objSale.is_pass = false;
                                            is_pass = false;
                                        }
                                        if (tempProducts != "")
                                        {
                                            tempProducts = tempProducts + ";";
                                        }
                                        tempProducts = tempProducts + name + "|#|" + sku + "|#|" + qty + "|#|" + price + "|#|" + (is_pass ? 1 : 0) + "|#|" + Program.Base64Encode(note);
                                    }

                                }
                                objSale.products = tempProducts;

                                if (config.shopee_jual_ongkir == 1)
                                {
                                    long.TryParse(Program.StripQuotes(data["shipping_fee"].ToString()).Replace(".00", ""), out objSale.shipfee);
                                }
                                if (config.shopee_jual_badmin == 1)
                                {
                                    long comfee = 0;
                                    long.TryParse(Program.StripQuotes(data["comm_fee"].ToString()).Replace(".00", ""), out comfee);
                                    long servfee = 0;
                                    long.TryParse(Program.StripQuotes(data["seller_service_fee"].ToString()).Replace(".00", ""), out servfee);
                                    objSale.adminfee = comfee + servfee;
                                }

                                objSale.total = (objSale.subtotal - objSale.adminfee) + objSale.shipfee;
                                //long.TryParse(Program.StripQuotes(data["total_price"].ToString()).Replace(".00", ""), out objSale.total);

                                //foreach (var field in objSale.GetType().GetFields())
                                //{
                                //    mainWindow.Log(field.Name + ": " + field.GetValue(objSale));
                                //}
                                //logControl.Log(JsonConvert.SerializeObject(objSale));
                                newSales.Add(objSale);
                                if (!cache.sales.ContainsKey(objSale.invoice))
                                {
                                    if (objSale.is_pass)
                                    {
                                        cache.sales.Add(objSale.invoice, objSale);
                                    }
                                    else
                                    {
                                        logControl.Log("Kondisi pengambilan data penjualan tidak memungkinkan. (Stok kurang/SKU tidak ada).", Color.Red);
                                    }
                                }
                            }

                            //LetsWait(10).Until(driver => driver.FindElements(By.ClassName("od-shippin")).Count > 0);
                            //LetsWait(10).Until(driver => driver.FindElements(By.ClassName("od-log")).Count > 0);

                            //var info = driver.FindElement(By.ClassName("od-shippin"));
                            ////driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);

                            //ShopeeSale objSale = new ShopeeSale();

                            //var invoice = info.FindElement(By.ClassName("id")).FindElement(By.ClassName("detail"));
                            //objSale.invoice = config.shopee_sale_prefix+invoice.Text;
                            //if (!cache.sales.ContainsKey(objSale.invoice))
                            //{
                            //    logControl.Log("(" + nos + "/" + (salesLinks.Count - 1) + ") " + invoice.Text + " berhasil diambil.", Color.Green);
                            //}
                            //else
                            //{
                            //    logControl.Log("(" + nos + "/" + (salesLinks.Count - 1) + ") " + invoice.Text + " sudah diambil sebelumnya.", Color.Yellow);
                            //    continue;
                            //}

                            //objSale.link = link;

                            //var log = driver.FindElements(By.ClassName("od-log"));
                            //var date = log[log.Count - 1].FindElement(By.ClassName("time"));
                            ////objSale.date = DateTime.ParseExact(date.Text, "dd-MM-yyyy HH:mm",System.Globalization.CultureInfo.InvariantCulture);
                            //objSale.date = DateTime.Now;

                            ////var buyer = driver.FindElement(By.ClassName("username"));
                            ////objSale.buyer = buyer.Text;

                            //objSale.type = config.shopee_jual_tujuan;

                            //var status = driver.FindElement(By.ClassName("status")).FindElement(By.ClassName("name"));
                            //objSale.status = status.Text;

                            //var addressarea = info.FindElement(By.ClassName("address")).FindElement(By.ClassName("detail")).FindElements(By.TagName("div"));
                            //objSale.buyer = addressarea[0].Text.Split(',')[0].ToUpper();
                            //objSale.address = addressarea[1].Text;


                            //var shipping = driver.FindElements(By.ClassName("logistic-history-log"));
                            ////logControl.Log("shipping : "+shipping.Count.ToString());
                            //if (shipping.Count > 0)
                            //{
                            //    var shipinfo = shipping[0].FindElement(By.ClassName("log-info-header"));
                            //    objSale.shipname = shipinfo.FindElement(By.ClassName("carrier")).Text.Trim().ToUpper();

                            //    var track = shipinfo.FindElements(By.ClassName("label"));
                            //    if (track.Count > 0)
                            //    {
                            //        objSale.shiptrack = track[0].Text.Trim();
                            //    }
                            //}

                            //var calcs = driver.FindElements(By.ClassName("income-subtotal"));
                            //if (calcs.Count > 0)
                            //{
                            //    long subtotal = 0;
                            //    var subtotal_el = calcs[0].FindElement(By.ClassName("income-value"));
                            //    long.TryParse(subtotal_el.Text.Substring(2).Trim().Replace(".", ""), out subtotal);

                            //    objSale.shipfee = 0;
                            //    var ongkir_el = calcs[1].FindElement(By.ClassName("income-value"));
                            //    long.TryParse(ongkir_el.Text.Substring(2).Trim().Replace(".", ""), out objSale.shipfee);

                            //    objSale.total = subtotal + objSale.shipfee;
                            //}

                            //objSale.products = "";
                            //objSale.is_pass = true;
                            //var products = driver.FindElements(By.ClassName("product-list-item"));
                            //if (products.Count > 0)
                            //{
                            //    var tempProducts = "";
                            //    for (int i = 1; i < products.Count; i++)
                            //    {
                            //        var p = products[i];
                            //        var is_pass = true;

                            //        var name = p.FindElement(By.ClassName("product-name")).Text.Trim();

                            //        int qty = 0;
                            //        long price = 0;
                            //        //logControl.Log(p.Text);
                            //        //logControl.Log("test1");
                            //        int.TryParse(p.FindElement(By.ClassName("qty")).Text, out qty);
                            //        //logControl.Log("test2");
                            //        long.TryParse(p.FindElement(By.ClassName("price")).Text.Replace(".",""), out price);
                            //        //logControl.Log("test3");
                            //        var csku = p.FindElements(By.ClassName("product-meta"));
                            //        //logControl.Log("test4");
                            //        var sku = "";
                            //        if (csku.Count > 0)
                            //        {
                            //            var skuDiv = csku[0].FindElements(By.TagName("div"));
                            //            sku = skuDiv.Count > 0 ? skuDiv[0].Text.Substring(4).Trim() : "";
                            //            is_pass = !sku.Equals("") && liveProducts.ContainsKey(sku);
                            //            if (!is_pass)
                            //            {
                            //                objSale.is_pass = false;
                            //            }
                            //            else
                            //            {
                            //                if (config.shopee_jual_tujuan > 0)
                            //                {
                            //                    var cp = liveProducts[sku];
                            //                    if (cp.stockdetail.ContainsKey(config.shopee_sale_warehousecode))
                            //                    {
                            //                        if (qty > cp.stockdetail[config.shopee_sale_warehousecode].stock)
                            //                        {
                            //                            is_pass = false;
                            //                            objSale.is_pass = false;
                            //                        }
                            //                    }
                            //                    else
                            //                    {
                            //                        is_pass = false;
                            //                        objSale.is_pass = false;
                            //                    }
                            //                }
                            //            }
                            //        }
                            //        else
                            //        {
                            //            sku = "";
                            //            objSale.is_pass = false;
                            //            is_pass = false;
                            //        }


                            //        var note = "";

                            //        if (tempProducts != "")
                            //        {
                            //            tempProducts = tempProducts + ";";
                            //        }
                            //        tempProducts = tempProducts + name + "|#|" + sku + "|#|" + qty + "|#|" + price + "|#|" + (is_pass ? 1 : 0) + "|#|" + Program.Base64Encode(note);

                            //    }
                            //    objSale.products = tempProducts;
                            //}
                            //else
                            //{
                            //    objSale.is_pass = false;
                            //}


                            ////foreach (var field in objSale.GetType().GetFields())
                            ////{
                            ////    mainWindow.Log(field.Name + ": " + field.GetValue(objSale));
                            ////}

                            //if (!cache.sales.ContainsKey(objSale.invoice))
                            //{
                            //    newSales.Add(objSale);
                            //    if (objSale.is_pass)
                            //    {
                            //        cache.sales.Add(objSale.invoice, objSale);
                            //    }
                            //    else
                            //    {
                            //        logControl.Log("Kondisi pengambilan data penjualan tidak memungkinkan. (Stok kurang/SKU tidak ada).", Color.Red);
                            //    }
                            //}
                        }
                        await SendSalesByPart(config).ConfigureAwait(false);
                    } 
                }
                catch (Exception e)
                {
                    logControl.Log(e.Message + e.StackTrace);
                    AcceptDialog();
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        logControl.Log("Gagal, Mencoba mengambil ulang penjualan shopee...", System.Drawing.Color.Yellow);
                        goto loopSales;
                    }
                    else
                    {
                        logControl.Log("Gagal dalam proses mengambil penjualan shopee.", System.Drawing.Color.Red);
                        goto skipSales;
                    }
                }

                skipSales:
                if (mainWindow.isSyncStopped()) return;
                logControl.timeoutSecs = 0;
                if (newSales.Count > 0)
                {
                    await SendSalesByPart(config).ConfigureAwait(false);
                }
            }
            currentMainTry = 0;
            

            if (!config.shopee_sync_on)
            {
                logControl.Log("Proses dilewati, sinkronisasi Shopee tidak aktif.", System.Drawing.Color.Yellow);
                isSyncDone = true;
                logControl.isWorking = false;
                isStuck = false;
                return;
            }

            if(specificProducts != null && specificProducts.Count > 0)
            {
                //logControl.Log("Deleting cache");
                foreach (var item in specificProducts)
                {
                    if (cache.products.ContainsKey(item))
                    {
                        //logControl.Log("deleting " + item);
                        cache.products.Remove(item);
                    }
                }
            }

            Dictionary<String, ShopeeProduct> UpdatingProducts = new Dictionary<String, ShopeeProduct>();
            foreach (var source in new Dictionary<string, SingleProduct>(listProducts))
            {
                logControl.timeoutSecs = 0;
                if (!source.Value.active) continue;
                var same = false;
                if (cache.products.ContainsKey(source.Value.sku))
                {
                    //mainWindow.Log("exist");
                    same = Program.CompareShopeeProduct(source.Value, cache.products[source.Value.sku], config);
                    //logControl.Log(source.Value.sku + "," + source.Value.price1 + " = " + same);
                }

                if (!same)
                {
                    ShopeeProduct p = new ShopeeProduct();
                    p.id_ipos = source.Value.id;
                    p.sku = source.Value.sku;
                    p.level_index = config.shopee_price;
                    if (p.level_index == 1)
                    {
                        p.level_price = source.Value.price1;
                        //mainWindow.Log("Harga "+source.Value.price1.ToString());
                    }
                    else if (p.level_index == 2)
                    {
                        p.level_price = source.Value.price2;
                        //mainWindow.Log("Harga " + source.Value.price2.ToString());
                    }
                    else if (p.level_index == 3)
                    {
                        p.level_price = source.Value.price3;
                        //mainWindow.Log("Harga " + source.Value.price3.ToString());
                    }
                    else if (p.level_index == 4)
                    {
                        p.level_price = source.Value.price4;
                        //mainWindow.Log("Harga " + source.Value.price4.ToString());
                    }
                    else
                    {
                        p.level_price = -1;
                    }

                    if (config.shopee_stock > 0)
                    {
                        p.stock = source.Value.stock;
                    }
                    else
                    {
                        p.stock = -1;
                    }
                    
                    UpdatingProducts.Add(p.sku, p);
                }
                if (mainWindow.isSyncStopped()) return;
            }
            if (UpdatingProducts.Count == 0)
            {
                logControl.Log("Belum ada produk untuk sinkronisasi shopee.", System.Drawing.Color.Yellow);
                logControl.isWorking = false;
                isSyncDone = true;
                isStuck = false;
                return;
            }
            logControl.Log("Terdapat " + UpdatingProducts.Count + " produk untuk sinkronisasi shopee.");
            var allCount = UpdatingProducts.Count;
            if (mainWindow.isSyncStopped()) return;
            List<string> successProducts = new List<string>();
            bool needFetchID = false;
            if (logControl.CheckID)
            {
                foreach (var item in UpdatingProducts)
                {
                    logControl.timeoutSecs = 0;
                    if (!cache.marketID.ContainsKey(item.Value.sku))
                    {
                        //cache.marketID.Add(item.Value.sku, "394713133");
                        needFetchID = true;
                        break;
                    }
                    if (mainWindow.isSyncStopped()) return;
                }
            }

            if (needFetchID)
            {
                logControl.Log("Mengambil id produk shopee...");
                var type = ""; //active_only_and_sold_out
                var next = "https://seller.shopee.co.id/api/v3/product/page_product_list/?page_number=1&page_size=100&list_type=" + type;
                do
                {
                    loopProducts:
                    logControl.timeoutSecs = 0;
                    if (mainWindow.command.Contains("skip"))
                    {
                        break;
                    }
                    try
                    {
                        driver.Navigate().GoToUrl(next);
                        next = "";
                        var html = driver.FindElement(By.TagName("pre")).GetAttribute("innerHTML");
                        var obj = SimpleJSON.JSON.Parse(html);
                        if (obj != null && Program.StripQuotes(obj["message"].ToString()) == "success")
                        {
                            var arr = obj["data"]["list"].AsArray;
                            foreach (JSONNode item in arr)
                            {
                                if (item["model_list"] == null || item["model_list"].AsArray.Count <= 1)
                                {
                                    var sku = Program.StripQuotes(item["parent_sku"].ToString());
                                    if (!sku.Equals(""))
                                    {
                                        if (cache.marketID.ContainsKey(sku))
                                        {
                                            cache.marketID[sku] = Program.StripQuotes(item["id"].ToString());
                                        }
                                        else
                                        {
                                            cache.marketID.Add(sku, Program.StripQuotes(item["id"].ToString()));
                                        }

                                    }
                                }
                                else
                                {
                                    var variants = item["model_list"].Childs;
                                    foreach (JSONNode row in variants)
                                    {
                                        var sku = Program.StripQuotes(row["sku"].ToString());
                                        if (!sku.Equals(""))
                                        {
                                            if (cache.marketID.ContainsKey(sku))
                                            {
                                                cache.marketID[sku] = Program.StripQuotes(item["id"].ToString());
                                            }
                                            else
                                            {
                                                cache.marketID.Add(sku, Program.StripQuotes(item["id"].ToString()));
                                            }
                                        }
                                    }
                                }
                            }
                            var pageinfo = obj["data"]["page_info"];
                            if (pageinfo["total"].AsInt > (pageinfo["page_size"].AsInt * (pageinfo["page_number"].AsInt)))
                            {
                                next = "https://seller.shopee.co.id/api/v3/product/page_product_list/?page_number=" + (pageinfo["page_number"].AsInt + 1).ToString() + "&page_size=100&list_type=" + type;
                            }
                            else
                            {
                                if (type.Equals("live"))
                                {
                                    type = "soldout";
                                    next = "https://seller.shopee.co.id/api/v3/product/page_product_list/?page_number=1&page_size=100&list_type=" + type;
                                }
                                else
                                {
                                    next = "";
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        AcceptDialog();
                        currentMainTry++;
                        if (currentMainTry < maxMainTry)
                        {
                            logControl.Log("Gagal, Mencoba mengambil ulang id produk shopee...", System.Drawing.Color.Yellow);
                            goto loopProducts;
                        }
                        else
                        {
                            logControl.Log("Gagal dalam proses mengambil id produk shopee.", System.Drawing.Color.Red);
                            goto skipfindid;
                        }
                    }
                    if (mainWindow.isSyncStopped()) return;
                } while (!next.Equals(""));
                mainWindow.command = "";
            }
            currentMainTry = 0;


            var deletedUpdates = new List<string>();
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!cache.marketID.ContainsKey(item.Key))
                {
                    deletedUpdates.Add(item.Key);
                }
                else
                {
                    if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos)) {
                        var monitored = mainWindow.listMonitored[item.Value.id_ipos];
                        monitored.shopee_sync = 1;
                        monitored.shopee_price = item.Value.level_price;
                        monitored.shopee_stock = item.Value.stock;
                    }
                    

                    if (cache.products.ContainsKey(item.Key))
                    {
                        cache.products[item.Key] = item.Value;
                    }
                    else
                    {
                        cache.products.Add(item.Key, item.Value);
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }
            if (deletedUpdates.Count > 0)
            {
                foreach (var item in deletedUpdates)
                {
                    UpdatingProducts.Remove(item);
                    if (mainWindow.isSyncStopped()) return;
                }
            }
            if (allCount != UpdatingProducts.Count)
            {
                logControl.Log("Terdapat " + (allCount - UpdatingProducts.Count) + " produk yang belum terdapat di Shopee. Silahkan input SKU terlebih dahulu.", Color.Yellow);
            }




            skipfindid:

            try
            {
                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                await Task.Delay(2000).ConfigureAwait(false);
                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                button.Click();
            }
            catch (Exception)
            {
                logControl.Log("Cannot clear cache.");
            }

            currentMainTry = 0;
            var noitem = 1;
            var processCounter = 1;
            foreach (var item in UpdatingProducts)
            {
                logControl.timeoutSecs = 0;
                if (!logControl.SyncMarketplace)
                {
                    if (!logControl.SendMonitoring)
                    {
                        if (mainWindow.listMonitored.ContainsKey(item.Value.id_ipos))
                        {
                            mainWindow.listMonitored[item.Value.id_ipos].shopee_sync = 0;
                        }
                    }
                    continue;
                }
                if (mainWindow.isSyncStopped()) return;
                loopUpdate:

                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                try
                {
                    logControl.Log("Memulai sinkronisasi shopee untuk sku " + item.Key + " (" + noitem + "/" + UpdatingProducts.Count + ")");
                    if (cache.marketID.ContainsKey(item.Key))
                    {
                        //logControl.Log("Test shopee 2");
                        driver.Navigate().GoToUrl("https://seller.shopee.co.id/portal/product/" + cache.marketID[item.Key]);
                        AcceptDialog();

                     //   logControl.Log("test1");
                        LetsWait().Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));


                        //var inc = 0;
                        //while (driver.FindElements(By.ClassName("loading-text")).Count == 0 && inc < 50)
                        //{
                        //    await Task.Delay(100);
                        //    inc++;
                        //} 
                        //logControl.Log("test2");
                        LetsWait().Until(driver => (driver.FindElements(By.ClassName("loading-text")).Count > 0));
                        //do
                        //{
                        //    await Task.Delay(100);
                        //} while (driver.FindElements(By.ClassName("loading-text")).Count > 0);
                        //logControl.Log("test3");
                        LetsWait().Until(driver => (driver.FindElements(By.ClassName("loading-text")).Count == 0));
                        //await Task.Delay(500).ConfigureAwait(false);
                        //logControl.Log("starting");

                        try
                        {
                            LetsWait(3).Until(driver => (driver.FindElements(By.CssSelector(".category-input .cat-selected-item")).Count > 0));
                        }
                        catch (Exception e)
                        {
                            executor.ExecuteScript("return window.stop();");
                            currentMainTry++;
                            if (currentMainTry < 5)
                            {
                                logControl.Log("Gagal memuat data produk, Memuat ulang produk...", System.Drawing.Color.Yellow);
                                await Task.Delay((new Random()).Next(1000, 5000)*currentMainTry).ConfigureAwait(false);
                                goto loopUpdate;
                            }
                            else
                            {
                                logControl.Log(e.Message.ToLower() + e.StackTrace, Color.Red);
                                reportFailedSync(item);
                                AcceptDialog();
                                continue;
                            }
                        }
                        


                        var variationTable = driver.FindElements(By.ClassName("variation-model-table"));
                        if (variationTable.Count == 0) {
                            logControl.Log("Produk non-variant terdeteksi.");
                            var rows = driver.FindElements(By.ClassName("edit-row"));
                            var no = 0;
                            //logControl.Log(rows.Count.ToString());
                            foreach (var row in rows)
                            {
                                var label = row.FindElements(By.CssSelector(".edit-label"));
                                if (label.Count == 0) continue;
                                var str_label = label[0].Text.Replace("\r", "").Replace("\n","").Replace("*", "").Replace(" ", "").ToLower();
                                //logControl.Log(str_label);
                                if (str_label.Equals("harga") || str_label.Equals("price"))
                                {
                                    //logControl.Log("price match");
                                    if (item.Value.level_price > 0)
                                    {
                                        var text = row.FindElement(By.CssSelector(".edit-input input"));
                                        try
                                        {
                                            LetsWait(3).Until(ExpectedConditions.ElementToBeClickable(text));
                                            text.Clear();
                                            text.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                            text.SendKeys(OpenQA.Selenium.Keys.Delete);
                                            await Task.Delay(500).ConfigureAwait(false);
                                            text.SendKeys(item.Value.level_price.ToString());
                                        }
                                        catch (Exception)
                                        {
                                            logControl.Log("Harga tidak bisa diubah untuk sku " + item.Key, System.Drawing.Color.Yellow);
                                        }
                                    }
                                    else if (item.Value.level_price == 0)
                                    {
                                        logControl.Log("Harga belum di isi untuk sku " + item.Key, System.Drawing.Color.Red);
                                        continue;
                                    }
                                }
                                else if (str_label.Equals("stok") || str_label.Equals("totalstok") || str_label.Equals("stoktotal") || str_label.Equals("stock") || str_label.Equals("totalstock"))
                                {
                                    //logControl.Log("stock match");
                                    var text = row.FindElement(By.CssSelector(".edit-input input"));
                                    LetsWait().Until(ExpectedConditions.ElementToBeClickable(text));
                                    executor.ExecuteScript("arguments[0].value = '"+ item.Value.stock.ToString()+ "';var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", text);
                                    //text.Clear();
                                    //text.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                    //text.SendKeys(OpenQA.Selenium.Keys.Delete);
                                    //await Task.Delay(500).ConfigureAwait(false);
                                    //text.SendKeys(item.Value.stock.ToString());
                                }
                                else if (str_label.Equals("merk") || str_label.Equals("brand"))
                                {
                                    Console.WriteLine("merk match");
                                    //var sibling = label.FindElement(By.XPath("following-sibling::div[@class='shopee-form-item__control']"));
                                    //var selected = sibling.FindElements(By.ClassName("selected"));
                                    //if (selected.Count == 0)
                                    //{
                                    //    Console.WriteLine("no selected");
                                    //    var opener = sibling.FindElements(By.ClassName("dropdown-input__text"));
                                    //    if (opener.Count > 0)
                                    //    {
                                    //        Console.WriteLine("opener exist");
                                    //        executor.ExecuteScript("arguments[0].scrollIntoView(false);", opener[0]);
                                    //        executor.ExecuteScript("arguments[0].click();", opener[0]);
                                    //        await Task.Delay(500).ConfigureAwait(false);
                                    //        var merk = sibling.FindElements(By.ClassName("shopee-dropdown-item"));
                                    //        if (merk.Count > 0)
                                    //        {
                                    //            Console.WriteLine("clicking no merk");
                                    //            executor.ExecuteScript("arguments[0].scrollIntoView(false);", merk[0]);
                                    //            executor.ExecuteScript("arguments[0].click();", merk[0]);
                                    //            await Task.Delay(500);
                                    //        }
                                    //    }

                                    //}
                                }
                                else
                                {
                                    Console.WriteLine("No label match");
                                }
                                no++;
                            }
                        }
                        else
                        {
                            logControl.Log("Produk variant terdeteksi.");
                            executor.ExecuteScript("arguments[0].scrollIntoView(false);", variationTable[0]);
                            LetsWait().Until(driver => (variationTable[0].FindElements(By.CssSelector(".data-group")).Count > 0));
                            var rows = variationTable[0].FindElements(By.CssSelector(".data-group"));
                            //logControl.Log(rows.Count.ToString());
                            if (rows.Count > 0)
                            {
                                //logControl.Log(rows.Count.ToString());
                                foreach (var row in rows)
                                {
                                    logControl.timeoutSecs = 0;
                                    var inputSKU = row.FindElement(By.TagName("textarea"));
                                    //logControl.Log(inputSKU.GetAttribute("value").ToLower() + " vs " + item.Key.ToLower());
                                    if (inputSKU != null && inputSKU.GetAttribute("value").ToLower().Equals(item.Key.ToLower()))
                                    {
                                        var hargaObj = row.FindElements(By.CssSelector("[restrictiontype='value']"));
                                        if (hargaObj.Count > 0)
                                        {
                                            var inputHarga = hargaObj[0];
                                            if (driver.FindElements(By.ClassName("wholesale-item-name")).Count == 0)
                                            {
                                                if (item.Value.level_price > 0)
                                                {
                                                    try
                                                    {
                                                        LetsWait(3).Until(ExpectedConditions.ElementToBeClickable(inputHarga));
                                                        inputHarga.Clear();
                                                        inputHarga.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                                        inputHarga.SendKeys(OpenQA.Selenium.Keys.Delete);
                                                        await Task.Delay(500).ConfigureAwait(false);
                                                        inputHarga.SendKeys(item.Value.level_price.ToString());
                                                    }
                                                    catch (Exception)
                                                    {
                                                        logControl.Log("Harga tidak bisa diubah untuk sku " + item.Key, System.Drawing.Color.Yellow);
                                                    }
                                                }
                                                else if (item.Value.level_price == 0)
                                                {
                                                    logControl.Log("Harga belum di isi untuk SKU " + item.Key, System.Drawing.Color.Red);
                                                }
                                            }
                                            else
                                            {
                                                logControl.Log("SKU " + item.Key + " sedang menggunakan harga grosir", System.Drawing.Color.Yellow);
                                            }
                                        }

                                        var stockObj = row.FindElements(By.CssSelector("input[restrictiontype='input']"));
                                        if (stockObj.Count > 0)
                                        {
                                            var inputStock = stockObj[0];
                                            if (item.Value.stock > -1)
                                            {
                                                //logControl.Log("Changing stock to : " + item.Value.stock.ToString());
                                                LetsWait(10).Until(ExpectedConditions.ElementToBeClickable(inputStock));
                                                executor.ExecuteScript("arguments[0].value = '" + item.Value.stock.ToString() + "';var event=new Event('input',{bubbles:!0,cancelable:!0});arguments[0].dispatchEvent(event);", inputStock);

                                                //inputStock.Clear();
                                                //inputStock.SendKeys(OpenQA.Selenium.Keys.Control + "a");
                                                //inputStock.SendKeys(OpenQA.Selenium.Keys.Delete);
                                                //await Task.Delay(500).ConfigureAwait(false);
                                                //inputStock.SendKeys(item.Value.stock.ToString());
                                            }
                                        }
                                    }

                                }
                            }
                            else
                            {
                                logControl.Log("Gagal menemukan SKU variant produk ini." + item.Key, Color.Red);
                                reportFailedSync(item);
                                continue;
                            }
                        }

                        

                    }
                    else
                    {
                        logControl.Log("SKU Shopee belum ditentukan untuk barang ini.", System.Drawing.Color.Yellow);
                        if (cache.products.ContainsKey(item.Key))
                        {
                            var product = cache.products[item.Key];
                            if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                            {
                                mainWindow.listMonitored[product.id_ipos].shopee_id = "0";
                                mainWindow.listMonitored[product.id_ipos].shopee_status = 0;
                            }
                        }
                        Console.WriteLine("no cache 1");
                    }

                }
                catch (Exception e)
                {

                    logControl.Log(e.Message.ToLower() + e.StackTrace);
                    Console.WriteLine(e.Message.ToLower());
                    if (e.Message.ToLower().Contains("alert"))
                    {
                        Console.WriteLine("Dismissing alert");
                        AcceptDialog();
                        noitem++;
                        continue;
                    }

                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        logControl.Log("Gagal menginput perubahan, Mencoba menginput ulang perubahan data...", System.Drawing.Color.Yellow);
                        goto loopUpdate;
                    }
                    else
                    {
                        logControl.Log(e.Message.ToLower() + e.StackTrace, Color.Red);
                        reportFailedSync(item);
                        AcceptDialog();
                        continue;
                    }
                }
                currentMainTry = 0;

                //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                //logControl.Log("Test shopee 3");
                loopUpdate2:
                try
                {
                    if (cache.marketID.ContainsKey(item.Key))
                    {
                        Console.WriteLine("Clicking save");
                        var buttons = driver.FindElements(By.ClassName("shopee-button--primary"));
                        var found = false;
                        foreach (var button in buttons)
                        {
                            var span = button.FindElement(By.TagName("span"));
                            Console.WriteLine(span.Text);
                            if (span.Text.ToLower().Contains("update"))
                            {
                                found = true;
                                var lastURL = driver.Url;

                                executor.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);");
                                executor.ExecuteScript("arguments[0].click();", button);
                                //Actions actions = new Actions(driver);
                                //actions.MoveToElement(button).Click(button).Perform();
                                //button.Click();
                                //await Task.Delay(500);
                                //if (driver.FindElements(By.ClassName("shopee-toast__content")).Count == 0)
                                //{
                                //    driver.FindElement(By.ClassName("list-name-search"));
                                //}
                                LetsWait().Until(driver => (!driver.Url.Equals(lastURL)));

                                successProducts.Add(item.Key);
                                logControl.Log("Sinkronisasi berhasil.", System.Drawing.Color.Green);
                            }
                        }
                        if (!found)
                        {
                            continue;
                        }
                        processCounter++;
                        if (processCounter % clearCacheInterval == 0)
                        {
                            try
                            {
                                driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
                                await Task.Delay(2000).ConfigureAwait(false);
                                var button = (IWebElement)executor.ExecuteScript("return document.querySelector('settings-ui').shadowRoot.querySelector('settings-main').shadowRoot.querySelector('settings-basic-page').shadowRoot.querySelector('settings-privacy-page').shadowRoot.querySelector('settings-clear-browsing-data-dialog').shadowRoot.querySelector('cr-dialog #clearBrowsingDataConfirm')");
                                button.Click();
                            }
                            catch (Exception)
                            {
                                logControl.Log("Cannot clear cache.");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("no cache 2");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message.ToLower());
                    logControl.Log(e.Message.ToLower()+e.StackTrace);
                    if (e.Message.ToLower().Contains("alert"))
                    {
                        Console.WriteLine("Dismissing alert");

                        AcceptDialog();
                        noitem++;
                    }

                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        
                        logControl.Log("Gagal menyimpan perubahan, Mencoba menyimpan ulang produk tersebut...", System.Drawing.Color.Yellow);
                        goto loopUpdate2;
                    }
                    else
                    {
                        reportFailedSync(item);
                        AcceptDialog();
                        continue;
                    }
                }
                logControl.Log("");
                noitem++;
                Console.WriteLine("next");
                if (mainWindow.isSyncStopped()) return;
            }
            currentMainTry = 0;

            //logControl.Log("Test shopee 4");
            Console.WriteLine("Done start deleting");
            foreach (var item in successProducts)
            {
                logControl.timeoutSecs = 0;
                if (cache.products.ContainsKey(item) && cache.marketID.ContainsKey(item))
                {
                    var product = cache.products[item];
                    var id = cache.marketID[item];
                    if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                    {
                        mainWindow.listMonitored[product.id_ipos].shopee_id = id;
                        mainWindow.listMonitored[product.id_ipos].shopee_status = 1;
                    }
                }
                if (mainWindow.isSyncStopped()) return;
            }
            Console.WriteLine("Done deleting");
            logControl.Log("Sinkronisasi Shopee selesai.");
            mainWindow.Log("Sinkronisasi Shopee selesai.", System.Drawing.Color.Green);

            isSyncDone = true;
            isStuck = false;
            logControl.isWorking = false;
            SaveCache();
            
        }

        void reportFailedSync(System.Collections.Generic.KeyValuePair<string, ShopeeProduct> item)
        {
            TakeScreenshot(item.Key);
            logControl.Log("Sinkronisasi shopee gagal untuk sku " + item.Key, System.Drawing.Color.Red);
            if (cache.products.ContainsKey(item.Key) && cache.marketID.ContainsKey(item.Key))
            {
                var product = cache.products[item.Key];
                var id = cache.marketID[item.Key];
                if (mainWindow.listMonitored.ContainsKey(product.id_ipos))
                {
                    mainWindow.listMonitored[product.id_ipos].shopee_id = id;
                    mainWindow.listMonitored[product.id_ipos].shopee_status = 2;
                }
            }
        }

        void TakeScreenshot(string name)
        {
            try
            {
               
                Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                var folder = Application.StartupPath + "/ss/shopee/";
                Directory.CreateDirectory(folder);
                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                {
                    name = name.Replace(c, '_');
                }
                ss.SaveAsFile(folder + name + ".jpg", ScreenshotImageFormat.Jpeg);
                logControl.Log("Menyimpan screenshot error ke " + folder + name + ".jpg");
            }
            catch (Exception e)
            {
                logControl.Log("Gagal screenshot, proses telah ditutup / tidak ditemukan.");
            }
        }

        public void showLog()
        {
            if (logWindow != null)
            {
                mainWindow.Log("Membuka log shopee...", Color.Green);
                logControl.Show();
                logControl.BringToFront();
            }
        }

        public Dictionary<String, IPOSSalesOrder> getIposSalesOrder()
        {
            Dictionary<String, IPOSSalesOrder> result = new Dictionary<String, IPOSSalesOrder>();
            foreach (var item in newSales)
            {
                if (!item.is_pass || result.ContainsKey(item.invoice)) continue;
                IPOSSalesOrder sale = new IPOSSalesOrder();
                sale.number = item.invoice;
                sale.address = Program.CleanSQLString(item.address);
                sale.buyer = Program.CleanSQLString(item.buyer);
                sale.date = item.date;
                sale.products = item.products;
                sale.otherfee = item.shipfee;
                sale.cut_value = item.adminfee;
                sale.cut_pct = ((decimal)sale.cut_value / item.subtotal) * ((decimal)100);
                sale.shipinfo = Program.CleanSQLString(item.shipname + "\n" + item.shiptrack);
                sale.total = (item.subtotal + sale.otherfee) - sale.cut_value;
                result.Add(sale.number, sale);
            }
            return result;
        }

        public Dictionary<String, IPOSOfficeSale> getIposOfficeSale()
        {
            Dictionary<String, IPOSOfficeSale> result = new Dictionary<String, IPOSOfficeSale>();
            foreach (var item in newSales)
            {
                if (!item.is_pass || result.ContainsKey(item.invoice)) continue;
                IPOSOfficeSale sale = new IPOSOfficeSale();
                sale.number = item.invoice;
                sale.address = Program.CleanSQLString(item.address);
                sale.buyer = Program.CleanSQLString(item.buyer);
                sale.date = item.date;
                sale.products = item.products;
                sale.otherfee = item.shipfee;
                sale.cut_value = item.adminfee;
                sale.cut_pct = ((decimal)sale.cut_value / item.subtotal)*((decimal)100);
                sale.shipinfo = Program.CleanSQLString(item.shipname + "\n" + item.shiptrack);
                sale.total = (item.subtotal + sale.otherfee) - sale.cut_value;
                result.Add(sale.number, sale);
            }
            return result;
        }

        public Dictionary<String, IPOSCashierSale> getIposCashierSale()
        {
            Dictionary<String, IPOSCashierSale> result = new Dictionary<String, IPOSCashierSale>();
            foreach (var item in newSales)
            {
                if (!item.is_pass || result.ContainsKey(item.invoice)) continue;
                IPOSCashierSale sale = new IPOSCashierSale();
                sale.number = item.invoice;
                sale.address = Program.CleanSQLString(item.address);
                sale.buyer = Program.CleanSQLString(item.buyer);
                sale.date = item.date;
                sale.products = item.products;
                sale.otherfee = item.shipfee;
                sale.cut_value = item.adminfee;
                sale.cut_pct = ((decimal)sale.cut_value / item.subtotal) * ((decimal)100);
                sale.shipinfo = Program.CleanSQLString(item.shipname + "\n" + item.shiptrack);
                sale.total = (item.subtotal + sale.otherfee) - sale.cut_value;
                result.Add(sale.number, sale);
            }
            return result;
        }

    }



}
