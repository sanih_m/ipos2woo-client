﻿using Flurl;
using Flurl.Http;
using Npgsql;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows;

namespace iPOS_Stock_Sync.Modules
{
    public class OnglaiCore
    {
        int idxProfile;

        const string key_name = "Authorization";
        const string key_value = "token c64659a19603751:1043e2d4dea592a";
        const string host_url = "https://onglai.id/api";
        const string api_item = "/resource/Bin";
        const string api_warehouse = "/resource/Warehouse";
        const string api_price = "/resource/Item Price";
        frmProfileSync mainWindow;
        public Dictionary<string, Product> allProducts = new Dictionary<string, Product>();

        public OnglaiCore(int idx)
        {
            idxProfile = idx;
            var config = Program.config.profiles[idxProfile];
            mainWindow = Program.Syncers[config.name];
        }

        public async Task Fetch(string[] product_blacklist)
        {
            var resp = "";
            var profile = Program.config.profiles[idxProfile];

            mainWindow.Log("Mengelola data harga...");
            SimpleJSON.JSONNode obj = null;
            var page = 0;
            var inc = 10000;
            Dictionary<string, long[]> prices = new Dictionary<string, long[]>();
            do
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var client = new RestClient(host_url);
                var resource = api_price + "?fields=[\"item_code\",\"price_list\",\"currency\",\"price_list_rate\"]&[[\"selling\",\" = \",1]]";
                var request = new RestRequest(resource, Method.GET);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Content-Type", "application/json");
                request.AddQueryParameter("limit_start", (page * inc).ToString());
                request.AddQueryParameter("limit_page_length", inc.ToString());
                request.AddHeader(key_name, key_value);
                var response = client.Execute(request);
                if (response.ErrorMessage != null && !response.ErrorMessage.Equals(""))
                {
                    mainWindow.Log("get price : " + response.ErrorMessage);
                }


                obj = SimpleJSON.JSON.Parse(response.Content);
                if (obj != null && obj["data"] != null)
                {
                    foreach (SimpleJSON.JSONNode item in obj["data"].AsArray)
                    {
                        if (!prices.ContainsKey(Program.StripQuotes(item["item_code"])))
                        {
                            prices.Add(Program.StripQuotes(item["item_code"]), new long[4] { -1, -1, -1, -1 });
                        }

                        var idx = -1;
                        var level = Program.StripQuotes(item["price_list"]).ToLower();
                        if (level.Equals("hj 1")) idx = 0;
                        else if (level.Equals("hj 2")) idx = 1;
                        else if (level.Equals("hj 3")) idx = 2;
                        else if (level.Equals("hj 4")) idx = 3;

                        if (idx > -1) prices[Program.StripQuotes(item["item_code"])][idx] = (long)float.Parse(Program.StripQuotes(item["price_list_rate"]).Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                    }
                }
                page++;
                mainWindow.Log("Proses data halaman "+page.ToString());
            } while (obj != null && obj["data"] != null && obj["data"].AsArray.Count > 0);

            allProducts.Clear();
            mainWindow.Log("Mengelola data produk...");
            page = 0;
            inc = 1000;
            do
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var client = new RestClient(host_url);
                var resource = api_item + "?fields=[\"item_code\",\"name\",\"warehouse\",\"actual_qty\"]";
                var request = new RestRequest(resource, Method.GET);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Content-Type", "application/json");
                request.AddQueryParameter("limit_start", (page * inc).ToString());
                request.AddQueryParameter("limit_page_length", inc.ToString());
                request.AddHeader(key_name, key_value);
                var response = client.Execute(request);
                if (response.ErrorMessage != null && !response.ErrorMessage.Equals(""))
                {
                    mainWindow.Log("get item : " + response.ErrorMessage);
                }

                //mainWindow.Log(api_stock + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace(" ", "%20"));
                obj = SimpleJSON.JSON.Parse(response.Content);
                if (obj != null && obj["data"] != null)
                {
                    Product p = new Product();
                    var count = 0;
                    var activeProducts = 0;
                    foreach (SimpleJSON.JSONNode item in obj["data"].AsArray)
                    {
                        if (!profile.warehouse.Contains("'" + Program.StripQuotes(item["warehouse"]) + "'"))
                        {
                            continue;
                        }
                        if (!mainWindow.listProduct.ContainsKey(Program.StripQuotes(item["item_code"])))
                        {
                            if (mainWindow.syncProductFilter.Count > 0)
                            {
                                if (!mainWindow.syncProductFilter.Contains(Program.StripQuotes(item["item_code"])))
                                {
                                    continue;
                                }
                            }
                            p = new Product();
                            p.id = Program.StripQuotes(item["item_code"]);
                            p.code = Program.StripQuotes(item["item_code"]);
                            p.name = Program.StripQuotes(item["name"]);
                            p.weight = -1;
                            p.description = "";
                            p.price_main = 0;
                            //mainWindow.Log("a"+Program.StripQuotes(item["harga"])+"a");
                            if (prices.ContainsKey(p.code))
                            {
                                p.price1 = prices[p.code][0];
                                p.price2 = prices[p.code][1];
                                p.price3 = prices[p.code][2];
                                p.price4 = prices[p.code][3]; 
                            }

                            p.stock = 0;
                            
                            p.publish_date = "NULL";
                            if (mainWindow.cache_product.data.ContainsKey(p.id))
                            {
                                p.publish_date = mainWindow.cache_product.data[p.id].publish_date;
                            }
                            if (mainWindow.cache_woo.list_id.ContainsKey(p.id))
                            {
                                var wooid = mainWindow.cache_woo.list_id[p.id];
                                if (mainWindow.listWooPublish.ContainsKey(wooid))
                                {
                                    var splitted = mainWindow.listWooPublish[wooid].Split('|');
                                    p.publish_date = splitted[0];
                                }
                            }

                            if (mainWindow.firstTime)
                            {
                                if (count <= Program.productLimit() || Program.productLimit() == 0)
                                {
                                    p.active = true;
                                }
                                else
                                {
                                    p.active = false;
                                }
                            }
                            else
                            {
                                if (mainWindow.cache_product.data.ContainsKey(p.id))
                                {
                                    p.active = mainWindow.cache_product.data[p.id].active;
                                }
                                else
                                {
                                    p.active = false;
                                }
                            }

                            if (p.active)
                            {
                                activeProducts++;
                                if (activeProducts > Program.productLimit() && Program.productLimit() > 0)
                                {
                                    p.active = false;
                                    if (mainWindow.cache_product.data.ContainsKey(p.id))
                                    {
                                        mainWindow.cache_product.data[p.id].active = false;
                                    }
                                    activeProducts--;
                                }
                            }

                            mainWindow.listProduct.Add(p.id, p);
                            allProducts.Add(p.code, p);
                        }
                        else
                        {
                            p = mainWindow.listProduct[Program.StripQuotes(item["item_code"])];
                        }

                        if (p.stockdetail == null)
                        {
                            p.stockdetail = new Dictionary<string, WarehouseStock>();
                        }
                        var warehouse = new WarehouseStock
                        {
                            name = Program.StripQuotes(item["warehouse"]),
                            stock = float.Parse(Program.StripQuotes(item["actual_qty"]).Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator))
                        };
                        if (p.stockdetail.ContainsKey(warehouse.name))
                        {
                            p.stockdetail[warehouse.name] = warehouse;
                        }
                        else
                        {
                            p.stockdetail.Add(warehouse.name, warehouse);
                        }

                        p.stock = p.stock + warehouse.stock;
                    }
                }
                page++;
                mainWindow.Log("Proses data halaman " + page.ToString());
            } while (obj != null && obj["data"] != null && obj["data"].AsArray.Count > 0);
        }

        public List<String> GetWarehouses()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            List<String> result = new List<String>();
            var profile = Program.config.profiles[idxProfile];
            var client = new RestClient(host_url);
            var request = new RestRequest(api_warehouse, Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader(key_name, key_value);
            var response = client.Execute(request);
            if (response.ErrorMessage != null && !response.ErrorMessage.Equals(""))
            {
                mainWindow.Log("get warehouse : "+response.ErrorMessage);
            }
            var obj = SimpleJSON.JSON.Parse(response.Content);
            if (obj != null && obj["data"] != null)
            {
                foreach (SimpleJSON.JSONNode item in obj["data"].AsArray)
                {
                    if (!result.Contains(Program.StripQuotes(item["name"])))
                    {
                        result.Add(Program.StripQuotes(item["name"]));
                    }
                }
            }
            return result;
        }
    }
}

    
