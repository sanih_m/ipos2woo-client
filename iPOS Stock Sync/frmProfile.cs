﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flurl;
using Flurl.Http;
using System.Net;

namespace iPOS_Stock_Sync
{
    public partial class frmProfile : Form
    {
        List<string> deletedProfiles = new List<string>();
        int ActiveProfiles = 0;

        public frmProfile()
        {
            InitializeComponent();
        }

        void LoadProfiles()
        {
            lvProfile.Items.Clear();
            for (int i = 0; i < Program.config.profiles.Count; i++)
            {
                Program.config.profiles[i].id = i + 1;
                
                lvProfile.Items.Add(Program.config.profiles[i].name);
                lvProfile.Items[i].SubItems.Add(Program.config.profiles[i].active ? "Aktif" : "Non-Aktif");

                if (!Program.Syncers.ContainsKey(Program.config.profiles[i].name))
                {
                    frmProfileSync form = new frmProfileSync(i);
                    if (Program.config.profiles[i].active)
                    {
                        form.Show();
                        ActiveProfiles++;
                    }
                    Program.Syncers.Add(Program.config.profiles[i].name, form);
                }
                else
                {
                    Program.Syncers[Program.config.profiles[i].name].idxProfile = i;
                }
            }
            Program.SaveConfig();
            if (!bWorker.IsBusy) bWorker.RunWorkerAsync();
        }

        private void frmProfile_Load(object sender, EventArgs e)
        {
            if(Program.config.version != Assembly.GetExecutingAssembly().GetName().Version.ToString())
            {
                Program.config.version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                Program.SaveConfig();
                Application.Restart();
                Environment.Exit(0);
            }
            Visible = false;
            var login = new frmLogin();
            login.ShowDialog();
            if (Program.verified)
            {
                Visible = true;
            }
            else
            {
                Application.Exit();
            }
            if (Program.config.autohide)
            {
                this.WindowState = FormWindowState.Minimized;
            }
            CheckStatus();
            LoadProfiles();
            mnPackage.Text = Program.packageName();
            if(Program.module == Program.Appmodule.Onglai)
            {
                Text = Text + " - Onglai";
            }
            else if (Program.module == Program.Appmodule.PlazaKamera)
            {
                Text = Text + " - Plaza Kamera";
            }
        }

        void CheckAccess()
        {
            try
            {
                var client = new RestClient(Program.config.monitorURL);
                var request = new RestRequest("/api/check_access", Method.POST);
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                request.AddHeader("X-Authorization-Token", Program.CreateMD5(Program.secretKey + unixTimestamp.ToString() + System.Environment.OSVersion.VersionString));
                request.AddHeader("X-Authorization-Time", unixTimestamp.ToString());
                request.AddHeader("User-Agent", System.Environment.OSVersion.VersionString);

                request.AddParameter("id_user", Program.id_user);
                IRestResponse response = client.Execute(request);
                var obj = SimpleJSON.JSON.Parse(response.Content);
                if (obj != null)
                {
                    if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                    {

                        var data = obj["data"].AsArray;
                        if (data != null && data.Count > 0)
                        {
                            foreach (SimpleJSON.JSONNode item in data)
                            {
                                if (item["package"] != null)
                                {
                                    var package = item["package"];
                                    Program.access_user[package["level"].AsInt - 1] = SimpleJSON.JSON.Parse(Program.StripQuotes(package["access"]));
                                    Program.access_user[package["level"].AsInt - 1]["name"] = package["name"];
                                }
                            }
                            if (Program.access_user[0] == null)
                            {
                                Close();
                            }
                            else
                            {
                                //exist
                                mnPackage.Text = Program.packageName();
                                for (int i = lvProfile.Items.Count -1; i >= 0; i--)
                                {
                                    if (ActiveProfiles > Program.profileLimit() && Program.profileLimit() > 0 && Program.config.profiles[i].active)
                                    {
                                        lvProfile.Items[i].Selected = true;
                                        aktifkanNonaktifkanToolStripMenuItem_Click_1(lvProfile, null);
                                        ActiveProfiles--;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Close();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
            }
        }

        void CheckUpdate(bool manual = false)
        {
            lblVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            var resp = "";
            try
            {
                var url = Program.config.monitorURL.AppendPathSegment("/setup/update.json");
                resp = url.GetStringAsync().Result;
                var obj = SimpleJSON.JSON.Parse(resp);
                var version = obj["version"].ToString().Replace("\"", "");
                var version1 = new Version(lblVersion.Text);
                var version2 = new Version(version);
                if (version1.CompareTo(version2) < 0)
                {
                    timer1.Enabled = false;
                    if (manual)
                    {
                        MessageBox.Show("Terdapat update aplikasi versi " + version + ". Klik OK untuk mengunduh.");
                    }
                    // Create a new WebClient instance.
                    var file = "ipos2woo_client_"+version+".msi";
                    using (WebClient myWebClient = new WebClient())
                    {
                        string myStringWebResource = Program.config.monitorURL + "/setup/"+ file;
                        // Download the Web resource and save it into the current filesystem folder.
                        myWebClient.DownloadFile(myStringWebResource, file);
                    }
                    if (File.Exists(Application.StartupPath + "/" + file))
                    {
                        if (manual)
                        {
                            MessageBox.Show("Update berhasil diunduh. Klik OK untuk melakukan install dan tunggu sampai selesai.");
                        }
                        System.Diagnostics.Process.Start(Application.StartupPath + "/" + file, "/passive /norestart");
                        Close();
                    }
                }
                else
                {
                    if (manual)
                    {
                        MessageBox.Show("Anda sudah berada di versi terbaru");
                    }
                    var file = "ipos2woo_client_" + lblVersion.Text + ".msi";
                    if (File.Exists(Application.StartupPath + "/" + file))
                    {
                        File.Delete(Application.StartupPath + "/" + file);
                    }
                }
                
            }
            catch (Exception e)
            {
                resp = e.Message;
                if (manual)
                {
                    MessageBox.Show(resp);
                }
            }
        }

        void CheckStatus()
        {
            if (Program.config.branchID == "")
            {
                lblStatusd.Text = "Cabang belum terhubung.";
                lblStatusd.ForeColor = Color.Red;
                if(Program.config.monitorURL == "" || Program.config.monitorToken == "")
                {
                    MessageBox.Show("Silahkan isi Token di settings untuk menghubungkan dengan website Ipos2Woo.");
                }
                else
                {
                    if (!bWorker.IsBusy)
                        bWorker.RunWorkerAsync();
                }
            }
            else
            {
                lblStatusd.Text = "Cabang telah terhubung.";
                lblStatusd.ForeColor = Color.Blue;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
        }

        private void frmProfile_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                trayicon.Visible = true;
                trayicon.ShowBalloonTip(3000, "iPos Stock Sync", "Double-click on tray icon to reshow the app.", ToolTipIcon.Info);
                this.Hide();
            }
        }

        private void trayicon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            trayicon.Visible = false;
        }

        private void lvProfile_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lvProfile.SelectedIndices.Count > 0)
            {
                var profile = Program.config.profiles[lvProfile.SelectedIndices[0]];
                if (!profile.active)
                {
                    return;
                }
                if (Program.Syncers.ContainsKey(profile.name))
                {
                    Program.Syncers[profile.name].Show();
                    Program.Syncers[profile.name].WindowState = FormWindowState.Normal;
                    Program.Syncers[profile.name].BringToFront();
                }
            }
        }

        private void lvProfile_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            reportBranch();
            reportProfile();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!Program.config.branchID.Equals(""))
                CheckStatus();
        }

        private void lblStatus_Click(object sender, EventArgs e)
        {
            CheckStatus();
        }

        async void reportBranch()
        {
            try
            {
                var url = Program.config.monitorURL
                            .AppendPathSegment("/api/branch_connector");
                var resp = await url
                             .PostUrlEncodedAsync(new
                             {
                                 code = Program.config.monitorToken,
                                 device_name = System.Environment.MachineName,
                                 ip_address = new System.Net.WebClient().DownloadString("https://api.ipify.org")
                             }).ReceiveString().ConfigureAwait(false);
                var obj = SimpleJSON.JSON.Parse(resp);
                if (obj != null)
                {
                    if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                    {
                        Program.config.branchID = obj["id"].ToString().Replace("\"", "");
                        Program.SaveConfig();
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
                System.Diagnostics.Debug.WriteLine(exp.Message);
            }
        }

        
        void reportProfile()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var client = new RestClient(Program.config.monitorURL);
                var request = new RestRequest("/api/profile_sync", Method.POST);
                request.AddParameter("id_branch", Program.config.branchID);

                var data = "";
                for (int i = 0; i < Program.config.profiles.Count; i++)
                {
                    var profile = Program.config.profiles[i];
                    if (!data.Equals(""))
                    {
                        data += ",";
                    }
                    data += "[" +
                           profile.id + "," +
                           Program.config.branchID + "," +
                           profile.name.SurroundWithSingleQuotes().SurroundWithDoubleQuotes() + "," +
                           profile.host.SurroundWithSingleQuotes().SurroundWithDoubleQuotes() + "," +
                           profile.database.SurroundWithSingleQuotes().SurroundWithDoubleQuotes() + "," +
                           profile.woo_url.SurroundWithSingleQuotes().SurroundWithDoubleQuotes() + "," +
                           (Program.Syncers.ContainsKey(profile.name) ? 1 : 0) + "]";
                }
                data = "[" + data + "]";
                request.AddParameter("data", data);
                
                IRestResponse response = client.Execute(request);
                var obj = SimpleJSON.JSON.Parse(response.Content);
                if (obj != null)
                {
                    if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                    {

                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Profile respon : "+response.Content);
                        return;
                    }
                }
            }
            catch (Exception exp)
            {
                System.Diagnostics.Debug.WriteLine(exp.Message);
            }
        }

        private void tambahProfilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.profileLimit() > 0 && ActiveProfiles >= Program.profileLimit())
            {
                MessageBox.Show("Silahkan nonaktifkan profile lain / tingkatkan paket langganan anda untuk menambah jumlah profil IPOS2WOO. MAX : "+ Program.profileLimit().ToString());
                return;
            }
            var profile = new frmProfileConfig(-1);
            profile.ShowDialog();
            LoadProfiles();
        }

        private void hapusProfilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void pengaturanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var cfg = new frmConfig();
            cfg.ShowDialog();
            CheckStatus();
        }

        private void frmProfile_Shown(object sender, EventArgs e)
        {
            CheckUpdate();
        }

        private void keluarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (System.Collections.Generic.KeyValuePair<string,frmProfileSync> item in Program.Syncers)
            {
                item.Value.isForceClosing = true;
                item.Value.Close();
            }
            Program.Syncers.Clear();
            Visible = false;
            Program.verified = false;
            Program.config.email = "";
            Program.config.password = "";
            Program.SaveConfig();
            frmProfile_Load(this,null);
        }

      
        private void tokopediaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            var frm = new TokopediaTest();
            frm.ShowDialog();
        }

        private void shopeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new ShopeeTest();
            frm.ShowDialog();
        }

        private void frmProfile_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var item in Program.Syncers)
            {
                if(item.Value != null)
                {
                    item.Value.stopMarketplaceBrowser();
                }
            }
            foreach (var process in Process.GetProcessesByName("chromedriver"))
            {
                process.Kill();
            }
        }

        private void frmProfile_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cekUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CheckUpdate(true);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            CheckUpdate();
            CheckAccess();
        }

        private void hapusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvProfile.SelectedIndices.Count > 0)
            {
                if (MessageBox.Show("Yakin akan menghapus profil", "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var idx = lvProfile.SelectedIndices[0];
                    lvProfile.Items.RemoveAt(idx);
                    if (!Program.config.branchID.Equals(""))
                    {
                        Program.Syncers[Program.config.profiles[idx].name].isForceClosing = true;
                        Program.Syncers[Program.config.profiles[idx].name].Close();
                        Program.Syncers.Remove(Program.config.profiles[idx].name);

                        ProfileConfig profile = Program.config.profiles[idx];
                        deletedProfiles.Add(profile.id + "|" + Program.config.branchID);
                        Program.config.profiles.RemoveAt(idx);
                        Program.SaveConfig();
                        LoadProfiles();
                        ActiveProfiles--;
                    }
                }
            }
        }

        private void aktifkanNonaktifkanToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cmsLvProfile_Opening(object sender, CancelEventArgs e)
        {

        }

        private void aktifkanNonaktifkanToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (lvProfile.SelectedIndices.Count > 0)
            {
                var idx = lvProfile.SelectedIndices[0];
                var profile = Program.config.profiles[idx];
                profile.active = !profile.active;
                if (profile.active)
                {
                    if (Program.profileLimit() > 0 && ActiveProfiles >= Program.profileLimit())
                    {
                        MessageBox.Show("Silahkan nonaktifkan profile lain / tingkatkan paket langganan anda untuk menambah jumlah profil IPOS2WOO. MAX : " + Program.profileLimit().ToString());
                        profile.active = false;
                        return;
                    }
                    frmProfileSync form = new frmProfileSync(idx);
                    form.Show();
                    Program.Syncers[profile.name] = form;
                    lvProfile.Items[idx].SubItems[1].Text = "Aktif";
                    ActiveProfiles++;
                }
                else
                {
                    ActiveProfiles--;
                    Program.Syncers[profile.name].isForceClosing = true;
                    Program.Syncers[profile.name].Close();
                    lvProfile.Items[idx].SubItems[1].Text = "Non-Aktif";
                }
            }
        }

        private void bukaUlangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lvProfile.SelectedIndices.Count > 0)
            {
                var idx = lvProfile.SelectedIndices[0];
                var profile = Program.config.profiles[idx];
                Program.Syncers[profile.name].isForceClosing = true;
                Program.Syncers[profile.name].Close();
                frmProfileSync form = new frmProfileSync(idx);
                form.Show();
                Program.Syncers[profile.name] = form;
            }
        }

        private void mnPackage_Click(object sender, EventArgs e)
        {
            MessageBox.Show(System.Web.HttpUtility.HtmlDecode("Hello &amp;"));
        }
    }
}
