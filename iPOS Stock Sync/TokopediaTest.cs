﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class TokopediaTest : Form
    {
        IWebDriver driver;
        string shopId;
        List<string> ListSKU = new List<string>();
        Dictionary<string, string> ListMarketID = new Dictionary<string, string>();
        Dictionary<string, Product> UpdatingProducts = new Dictionary<string, Product>();

        const int maxLoginTry = 5;
        int currentLoginTry = 0;

        const int maxMainTry = 5;
        int currentMainTry = 0;

        public TokopediaTest()
        {
            InitializeComponent();
            var options = new ChromeOptions();
            options.AddArguments("headless");
            driver = new ChromeDriver(options);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        void login()
        {
            driver.Navigate().GoToUrl("https://seller.tokopedia.com/manage-product");

            sendEmailWhenLogin:
            try
            {

                driver.FindElement(By.XPath("//*[@name='email']")).SendKeys(txtemail.Text);
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    return;
                }
            }
            

            driver.FindElement(By.ClassName("unf-user-btn")).Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.FindElement(By.XPath("//*[@name='password']")).SendKeys(txtpassword.Text);

            driver.FindElement(By.Id("login-submit")).Click();

            checkBannerAfterLogin:
            try
            {
                driver.Navigate().GoToUrl("https://seller.tokopedia.com/manage-product");
                shopId = driver.FindElement(By.Id("shop-id")).GetAttribute("value");
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto checkBannerAfterLogin;
                }
                else
                {
                    return;
                }
            }
        }

        void getProducts()
        {
            var next = "https://tome.tokopedia.com/v2/shop/" + shopId + "/product_list?perpage=80&page=1";
            do
            {
                loopProducts:
                try
                {
                    driver.Navigate().GoToUrl(next);
                    next = "";
                    var html = driver.FindElement(By.TagName("pre")).GetAttribute("innerHTML");
                    Console.WriteLine(html);
                    var obj = SimpleJSON.JSON.Parse(html);
                    if (obj != null && Program.StripQuotes(obj["status"].ToString()) == "OK")
                    {
                        Console.WriteLine("ok");
                        var arr = obj["data"].AsArray;
                        foreach (JSONNode item in arr)
                        {
                            Console.WriteLine("loop");
                            ListSKU.Add(Program.StripQuotes(item["product_id"].ToString()));
                        }
                        Console.WriteLine("next");
                        next = obj["links"]["next"].Equals(null) ? "" : Program.StripQuotes(obj["links"]["next"].ToString());
                    }
                }
                catch (Exception)
                {
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        goto loopProducts;
                    }
                    else
                    {
                        return;
                    }
                }
                
            } while (!next.Equals(""));

            foreach (var item in ListSKU)
            {
                loopSku:
                try
                {
                    driver.Navigate().GoToUrl("https://tome.tokopedia.com/v3/product/" + item + "?opt=edit");
                    var html = driver.FindElement(By.TagName("pre")).GetAttribute("innerHTML");
                    Console.WriteLine(html);
                    var obj = SimpleJSON.JSON.Parse(html);
                    if (obj != null)
                    {
                        var sku = Program.StripQuotes(obj["data"]["sku"].ToString());
                        if (!sku.Equals(""))
                        {
                            if (ListMarketID.ContainsKey(sku))
                            {
                                ListMarketID[sku] = item;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        goto loopSku;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            login();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);


            driver.Navigate().GoToUrl("https://seller.tokopedia.com/edit-product/"+txtIDproduct.Text);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var elharga = driver.FindElement(By.XPath("//*[@name='price']"));
            elharga.Clear();
            elharga.SendKeys(numHarga.Value.ToString());

            var elstock = driver.FindElement(By.XPath("//*[@name='stock']"));
            elstock.Clear();
            elstock.SendKeys(numStok.Value.ToString());

            driver.FindElement(By.ClassName("unf-btn__primary")).Click();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            ListMarketID.Add("A020", "");
            var p = new Product();
            p.price1 = 200000;
            p.stock = 153;
            UpdatingProducts.Add("A020", p);

            ListMarketID.Add("A021", "");
            p = new Product();
            p.price1 = 150000;
            p.stock = 22;
            UpdatingProducts.Add("A021", p);


            login();
            getProducts();
            EditAll();
        }

        void EditAll()
        {
            currentMainTry = 0;
            foreach (var item in UpdatingProducts)
            {
                loopUpdate:
                try
                {
                    if (ListMarketID.ContainsKey(item.Key))
                    {
                        driver.Navigate().GoToUrl("https://seller.tokopedia.com/edit-product/" + ListMarketID[item.Key]);

                        var elharga = driver.FindElement(By.XPath("//*[@name='price']"));
                        elharga.Clear();
                        elharga.SendKeys(item.Value.price1.ToString());

                        var radios = driver.FindElements(By.ClassName("unf-radio"));
                        foreach (var radio in radios)
                        {
                            var text = radio.FindElement(By.ClassName("unf-radio-label__text"));
                            Console.WriteLine(text.GetAttribute("innerHTML"));
                            if (text.GetAttribute("innerHTML").Contains("Stok Terbatas"))
                            {
                                radio.FindElement(By.ClassName("unf-radio-button")).Click();
                                var elstock = driver.FindElement(By.XPath("//*[@name='stock']"));
                                elstock.Clear();
                                elstock.SendKeys(item.Value.stock.ToString());
                                break;
                            }    
                        }

                        driver.FindElement(By.ClassName("unf-btn__primary")).Click();

                    }
                }
                catch (Exception)
                {
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        goto loopUpdate;
                    }
                    else
                    {
                        return;
                    }
                }
      
            }
            MessageBox.Show("Done");
        }

        private void TokopediaTest_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //var obj = SimpleJSON.JSON.Parse(("{'header':{'process_time':0.077149008,'messages':[],'reason':'','error_code':''},'data':{'id':468395382,'name':'21','price':150000,'price_currency':'IDR','stock':0,'status':'UNLIMITED','description':'','min_order':1,'max_order':0,'weight':50,'weight_unit':'GR','condition':'NEW','last_update_price':'2019-05-10T00:29:50Z','is_must_insurance':false,'is_kreasi_lokal':false,'is_free_return':null,'is_eligible_cod':true,'alias':'21','sku':'ITEM1','gtin':'','url':'https://www.tokopedia.com/comical/21','category':{'id':1771,'name':'Fashion Wanita - Atasan Wanita - Blouse Wanita','title':'Fashion Wanita - Atasan Wanita - Blouse Wanita','breadcrumb_url':'https://www.tokopedia.com/p/fashion-wanita/atasan-wanita/blouse-wanita','detail':[{'id':1758,'name':'Fashion Wanita','breadcrumb_url':'https://www.tokopedia.com/p/fashion-wanita'},{'id':1768,'name':'Atasan Wanita','breadcrumb_url':'https://www.tokopedia.com/p/fashion-wanita/atasan-wanita'},{'id':1771,'name':'Blouse Wanita','breadcrumb_url':'https://www.tokopedia.com/p/fashion-wanita/atasan-wanita/blouse-wanita'}]},'menu':{'id':9100660,'name':'Peralatan'},'pictures':[{'id':1163400225,'description':'','file_path':'product-1/2019/5/9/2587719','file_name':'2587719_58f706c8-1fd6-47f4-a542-307e3b765f7c_1161_1161.jpg','x':1161,'y':1161,'is_from_ig':false,'url_original':'https://ecs7.tokopedia.net/img/cache/700/product-1/2019/5/9/2587719/2587719_58f706c8-1fd6-47f4-a542-307e3b765f7c_1161_1161.jpg','url_thumbnail':'https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/5/9/2587719/2587719_58f706c8-1fd6-47f4-a542-307e3b765f7c_1161_1161.jpg'}],'position':{'position':23,'is_swap':null},'shop':{'id':2587719,'name':'Comical Store','domain':'comical','url':'https://www.tokopedia.com/comical'},'wholesale':null,'variant':{'selection':[{'id':1,'name':'Warna','unit_id':0,'unit_name':'','identifier':'colour','options':[{'unit_value_id':1,'value':'Putih','hex_code':'#ffffff'},{'unit_value_id':2,'value':'Hitam','hex_code':'#000000'}]}],'products':[{'combination':[1],'is_primary':false,'price':150000,'sku':'ITEM1X','status':'UNLIMITED','stock':0,'pictures':[]},{'combination':[0],'is_primary':true,'price':150000,'sku':'ITEM1Z','status':'UNLIMITED','stock':0,'pictures':[]}],'sizecharts':[]},'video':null,'lock':{'full':false,'partial':{'price':false,'status':false,'stock':false,'wholesale':false,'name':false}},'tx_stats':{'sold':0,'tx_success':0,'tx_reject':0}}}").Replace("'","\""));
            var obj = SimpleJSON.JSON.Parse(("{'header':{'process_time':0.065915722,'messages':[],'reason':'','error_code':''},'data':{'id':468383523,'name':'a','price':10000,'price_currency':'IDR','stock':280,'status':'LIMITED','description':'','min_order':1,'max_order':0,'weight':450,'weight_unit':'GR','condition':'NEW','last_update_price':'2019-05-10T02:46:02Z','is_must_insurance':false,'is_kreasi_lokal':false,'is_free_return':null,'is_eligible_cod':true,'alias':'a','sku':'ITEM1C','gtin':'','url':'https://www.tokopedia.com/comical/a','category':{'id':1129,'name':'Kesehatan - Lainnya','title':'Kesehatan - Kesehatan Lainnya','breadcrumb_url':'https://www.tokopedia.com/p/kesehatan/lainnya','detail':[{'id':715,'name':'Kesehatan','breadcrumb_url':'https://www.tokopedia.com/p/kesehatan'},{'id':1129,'name':'Lainnya','breadcrumb_url':'https://www.tokopedia.com/p/kesehatan/lainnya'}]},'menu':{'id':9100660,'name':'Peralatan'},'pictures':[{'id':1163365085,'description':'','file_path':'product-1/2019/5/9/2587719','file_name':'2587719_fa904f3e-64a0-49e6-8aa9-15b9aa593a67_1161_1161.jpg','x':1161,'y':1161,'is_from_ig':false,'url_original':'https://ecs7.tokopedia.net/img/cache/700/product-1/2019/5/9/2587719/2587719_fa904f3e-64a0-49e6-8aa9-15b9aa593a67_1161_1161.jpg','url_thumbnail':'https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/5/9/2587719/2587719_fa904f3e-64a0-49e6-8aa9-15b9aa593a67_1161_1161.jpg'}],'position':{'position':4,'is_swap':null},'shop':{'id':2587719,'name':'Comical Store','domain':'comical','url':'https://www.tokopedia.com/comical'},'wholesale':null,'video':null,'lock':{'full':false,'partial':{'price':false,'status':false,'stock':false,'wholesale':false,'name':false}},'tx_stats':{'sold':0,'tx_success':0,'tx_reject':0}}}").Replace("'", "\""));
            MessageBox.Show(obj.ToString());
            MessageBox.Show(obj["data"]["sku"].ToString());
            MessageBox.Show((obj["data"]["variant"] == null).ToString());
            MessageBox.Show((obj["data"]["variant"].Equals("")).ToString());
           
        }
    }
}
