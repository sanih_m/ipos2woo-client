﻿using Microsoft.Win32;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class frmLogin : Form
    {
        int remaining = 5;
        bool extractDone = false;
        string status = "Menghubungkan ke server...";

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            
        }


        void Login()
        {
            btnLogin.Enabled = false;
            pnlConnect.Visible = true;
            
            if (!txtEmail.Text.Equals("") && !txtPassword.Text.Equals(""))
            {
                var client = new RestClient(Program.config.monitorURL);
                var request = new RestRequest("/api/mobile_login", Method.POST);
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                request.AddHeader("X-Authorization-Token", Program.CreateMD5(Program.secretKey + unixTimestamp.ToString() + System.Environment.OSVersion.VersionString));
                request.AddHeader("X-Authorization-Time", unixTimestamp.ToString());
                request.AddHeader("User-Agent", System.Environment.OSVersion.VersionString);
                
                request.AddParameter("email", txtEmail.Text);
                request.AddParameter("password", txtPassword.Text);
                IRestResponse response = client.Execute(request);
                var obj = SimpleJSON.JSON.Parse(response.Content);
                if (obj != null)
                {
                    if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                    {
                        Program.id_user = int.Parse(Program.StripQuotes(obj["id"].ToString()));
                        Program.config.email = txtEmail.Text;
                        Program.config.password = txtPassword.Text;
                        Program.SaveConfig();
                        check_access();
                        return;
                    }
                    else
                    {
                        Program.verified = false;
                        MessageBox.Show("Email / password salah.");
                    }
                }
                else
                {
                    MessageBox.Show("Terjadi error. Harap cek konfigurasi sistem anda.");
                }
            }
            pnlConnect.Visible = false;
            btnLogin.Enabled = true;
        }

        void check_access()
        {
            try
            {
                var client = new RestClient(Program.config.monitorURL);
                var request = new RestRequest("/api/check_access", Method.POST);
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                request.AddHeader("X-Authorization-Token", Program.CreateMD5(Program.secretKey + unixTimestamp.ToString() + System.Environment.OSVersion.VersionString));
                request.AddHeader("X-Authorization-Time", unixTimestamp.ToString());
                request.AddHeader("User-Agent", System.Environment.OSVersion.VersionString);

                request.AddParameter("id_user", Program.id_user);
                IRestResponse response = client.Execute(request);
                var obj = SimpleJSON.JSON.Parse(response.Content);
                if (obj != null)
                {
                    if (obj["api_status"].ToString().Replace("\"", "").Equals("1"))
                    {
                        
                        var data = obj["data"].AsArray;
                        if(data != null && data.Count > 0)
                        {
                            Program.access_user.Clear();
                            Program.access_user.Add(null);
                            Program.access_user.Add(null);
                            Program.access_user.Add(null);
                            Program.access_user.Add(null);
                            Program.access_user.Add(null);
                            foreach (SimpleJSON.JSONNode item in data)
                            {
                                if(item["package"] != null)
                                {
                                    var package = item["package"];
                                    Program.access_user[package["level"].AsInt-1] = SimpleJSON.JSON.Parse(Program.StripQuotes(package["access"]));
                                    Program.access_user[package["level"].AsInt - 1]["name"] = package["name"];
                                }
                            }
                            if(Program.access_user[0] == null)
                            {
                                Program.verified = false;
                                MessageBox.Show("Anda tidak mempunyai paket utama IPOS2WOO yang aktif.");
                            }
                            else
                            {
                                Program.verified = true;
                                Program.module = Program.Appmodule.Ipos;
                                try
                                {
                                    var module = Program.access_user[0]["module"];
                                    switch (Program.StripQuotes(module.ToString()))
                                    {
                                        case "pk":
                                            Program.module = Program.Appmodule.PlazaKamera;
                                            break;
                                        case "onglai":
                                            Program.module = Program.Appmodule.Onglai;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                catch (Exception)
                                {
                                }
                                this.Close();
                            }
                            
                        }
                        else
                        {
                            Program.access_user.Clear();
                            Program.verified = false;
                            MessageBox.Show("Anda tidak mempunyai paket langganan IPOS2WOO yang aktif.");
                        }
                    }
                    else
                    {
                        Program.access_user.Clear();
                        Program.verified = false;
                        MessageBox.Show("Gagal mengambil informasi langganan.");
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            pnlConnect.Visible = false;
            btnLogin.Enabled = true;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            
            Login();
        }

        private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
  
        }

        string getChromeVersion()
        {
            var path = Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe", "", null);
            FileVersionInfo version = null;
            if (path != null)
            {
                version = FileVersionInfo.GetVersionInfo(path.ToString());
            }
            if (version == null)
            {
                path = Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe", "", null);
                if (path != null)
                {
                    version = FileVersionInfo.GetVersionInfo(path.ToString());
                }
            }

            var result = "";
            if (version != null)
            {
                result = version.FileVersion;
            }
            else
            {
                result = "";
            }
            return result;
        }

        void downloader_ProgressChanged(object sender, DownloadEventArgs e)
        {
            status = "Mengunduh engine ipos2woo (" + e.PercentDone.ToString() + "%)...";
        }

        void downloader_DownloadedComplete(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => {
                status = "Menyiapkan engine ipos2woo...";
                System.IO.Compression.ZipFile.ExtractToDirectory(Application.StartupPath + "/engine_chrome.zip", Application.StartupPath);
                extractDone = true;
                //File.Delete(Application.StartupPath + "/engine_chrome.zip");
                status = "Menghubungkan ke server...";
            });
            thread.Start();
        }


        private void frmLogin_Shown(object sender, EventArgs e)
        {
            
            //var version = getChromeVersion();
            //if (!version.Equals(""))
            //{
            //    if (version.Substring(0, 2) != "77")
            //    {
            //        MessageBox.Show("Instalasi Google Chrome saat ini (" + version + ") dan harus menggunakan versi 77.* agar Ipos2Woo bisa berjalan dengan baik. Silahkan hapus instalasi Chrome terlebih dahulu.");
            //        Close();
            //    }
            //    else
            //    {
            //        var path = Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe", "", null);
            //        if (path == null || path.ToString().Equals(""))
            //        {
            //            path = Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe", "", null);
            //        }
            //        var dir = Path.GetDirectoryName(path.ToString());
            //        dir = Directory.GetParent(Directory.GetParent(dir).FullName).FullName + "\\Update\\";
            //        if(File.Exists(dir + "GoogleUpdate.exe"))
            //        {
            //            System.IO.File.Move(dir + "GoogleUpdate.exe", dir + "GoogleUpdate.disable_update");
            //        }
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Instalasi Google Chrome tidak ditemukan, Klik OK untuk mengunduh.");
            //    var file = "chrome_installer_32.exe";
            //    if(IntPtr.Size == 8)
            //    {
            //        file = "chrome_installer_64.exe";
            //    }
            //    using (WebClient myWebClient = new WebClient())
            //    {
            //        string myStringWebResource = Program.config.monitorURL + "/setup/" + file;
            //        // Download the Web resource and save it into the current filesystem folder.
            //        myWebClient.DownloadFile(myStringWebResource, file);
            //    }
            //    if (File.Exists(Application.StartupPath + "/" + file))
            //    {
            //        MessageBox.Show("Google Chrome berhasil diunduh. Klik OK untuk melakukan install dan jalankan kembali Ipos2woo setelah instalasi selesai.");
            //        System.Diagnostics.Process.Start(Application.StartupPath + "/" + file);
            //        Close();
            //    }
            //}
            
            txtEmail.Text = Program.config.email;
            txtPassword.Text = Program.config.password;

            foreach (var process in Process.GetProcessesByName("chromedriver"))
            {
                process.Kill();
            }
        }

        void launch()
        {
            Program.disableAutoStart = chbAutoStart.Checked;
            Login();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (remaining <= 0)
            {
                timer1.Enabled = false;
                if (!File.Exists(Application.StartupPath + "/engine_chrome/chrome.exe"))
                {
                    if (!File.Exists(Application.StartupPath + "/engine_chrome.zip")){
                        Thread thread = new Thread(() => {
                            FileDownloader downloader = new FileDownloader();
                            downloader.DownloadComplete += new EventHandler(downloader_DownloadedComplete);
                            downloader.ProgressChanged += new DownloadProgressHandler(downloader_ProgressChanged);
                            downloader.Download("https://app.ipos2woo.com/setup/engine_chrome.zip", Application.StartupPath);
                            status = "Mengunduh engine ipos2woo...";
                        });
                        thread.Start();
                    }
                    else
                    {
                        downloader_DownloadedComplete(sender,e);
                    }
                }
                else
                {
                    launch();
                }
            }
            else
            {
                remaining--;
                lblCountdown.Text = "(" + remaining.ToString() + ")";
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            lblStatus.Text = status;
            if (extractDone)
            {
                timer2.Enabled = false;
                launch();
            }
        }
    }
}
