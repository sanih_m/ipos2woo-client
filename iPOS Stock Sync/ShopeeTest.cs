﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class ShopeeTest : Form
    {
        IWebDriver driver;
        Dictionary<string, string> ListMarketID = new Dictionary<string, string>();
        Dictionary<string, Product> UpdatingProducts = new Dictionary<string, Product>();
        WebDriverWait wait;

        const int maxLoginTry = 5;
        int currentLoginTry = 0;

        const int maxMainTry = 5;
        int currentMainTry = 0;

        const int maxMainWait = 10;

        public ShopeeTest()
        {
            InitializeComponent();
            var options = new ChromeOptions();
            // options.AddArguments("headless");
            driver = new ChromeDriver(options);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        }

        void login()
        {
            sendEmailWhenLogin:
            try
            {
                driver.Navigate().GoToUrl("https://seller.shopee.co.id/");
                driver.FindElement(By.XPath("//*[@placeholder='Email/Telepon/Username']")).SendKeys(txtemail.Text);
                driver.FindElement(By.XPath("//*[@placeholder='Password']")).SendKeys(txtpassword.Text);
                driver.FindElement(By.ClassName("shopee-checkbox")).Click();

                driver.FindElement(By.ClassName("shopee-button--primary")).Click();
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    goto sendEmailWhenLogin;
                }
                else
                {
                    return;
                }
            }

            try
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
                if (driver.FindElement(By.ClassName("shopee-otp-verification")) != null){
                    Console.WriteLine("Kami tidak support otp");
                    driver.Navigate().GoToUrl("https://seller.shopee.co.id/");
                    return;
                }
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                Console.WriteLine("Login berhasil");
            }


            checkUserInfo:
            try
            {
                driver.FindElement(By.ClassName("user-info"));
                currentLoginTry = 0;
            }
            catch (Exception)
            {
                currentLoginTry++;
                if (currentLoginTry < maxLoginTry)
                {
                    driver.Navigate().GoToUrl("https://seller.shopee.co.id/");
                    goto checkUserInfo;
                }
                else
                {
                    return;
                }
            }


            //checkBannerAfterLogin:
            //try
            //{
            //    driver.Navigate().GoToUrl("https://seller.shopee.co.id/portal/product/list/all");
            //    shopId = driver.FindElement(By.Id("shop-id")).GetAttribute("value");
            //    currentLoginTry = 0;
            //}
            //catch (Exception)
            //{
            //    currentLoginTry++;
            //    if (currentLoginTry < maxLoginTry)
            //    {
            //        goto checkBannerAfterLogin;
            //    }
            //    else
            //    {
            //        return;
            //    }
            //}


            var next = "https://seller.shopee.co.id/api/v3/product/page_product_list/?page_number=1&page_size=48";
            do
            {
                loopProducts:
                try
                {
                    driver.Navigate().GoToUrl(next);
                    next = "";
                    var html = driver.FindElement(By.TagName("pre")).GetAttribute("innerHTML");
                    Console.WriteLine(html);
                    var obj = SimpleJSON.JSON.Parse(html);
                    if (obj != null && Program.StripQuotes(obj["message"].ToString()) == "success")
                    {
                        Console.WriteLine("success");
                        var arr = obj["data"]["list"].AsArray;
                        foreach (JSONNode item in arr)
                        {
                            Console.WriteLine(item);
                            var sku = Program.StripQuotes(item["parent_sku"].ToString());
                            if (!sku.Equals(""))
                            {
                                if (ListMarketID.ContainsKey(sku))
                                {
                                    ListMarketID[sku] = Program.StripQuotes(item["id"].ToString());
                                }
                                else
                                {
                                    ListMarketID.Add(sku, Program.StripQuotes(item["id"].ToString()));
                                }

                            }
                        }
                        Console.WriteLine("next");
                        var pageinfo = obj["data"]["page_info"];
                        if (pageinfo["total"].AsInt > (pageinfo["page_size"].AsInt * (pageinfo["page_number"].AsInt)))
                        {
                            next = "https://seller.shopee.co.id/api/v3/product/page_product_list/?page_number=" + (pageinfo["page_size"].AsInt + 1).ToString() + "&page_size=48";
                        }
                        else
                        {
                            next = "";
                        }
                    }
                }
                catch (Exception)
                {
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        goto loopProducts;
                    }
                    else
                    {
                        return;
                    }
                }

            } while (!next.Equals(""));
        }


        private void button1_Click(object sender, EventArgs e)
        {

            
        }

        private void button2_Click(object sender, EventArgs e)
        {

            
        }

        void EditAll()
        {
            currentMainTry = 0;
            foreach (var item in UpdatingProducts)
            {
                loopUpdate:
                try
                {
                    if (ListMarketID.ContainsKey(item.Key))
                    {
                        Console.WriteLine("https://seller.shopee.co.id/portal/product/" + ListMarketID[item.Key]);
                        driver.Navigate().GoToUrl("https://seller.shopee.co.id/portal/product/" + ListMarketID[item.Key]);

                        wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("loading-text")));

                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
                        var forms = driver.FindElements(By.ClassName("shopee-product-form-item"));
                        foreach (var form in forms)
                        {
                            var label = form.FindElement(By.TagName("label"));
                            if (label.GetAttribute("for").Equals("price"))
                            {
                                Console.WriteLine("Found price");
                                var text = form.FindElement(By.TagName("input"));
                                text.Clear();
                                text.SendKeys(item.Value.price1.ToString());
                            }
                            else if(label.GetAttribute("for").Equals("stock"))
                            {
                                Console.WriteLine("Found stock");
                                var text = form.FindElement(By.TagName("input"));
                                text.Clear();
                                text.SendKeys(item.Value.stock.ToString());
                            }
                            else
                            {
                                Console.WriteLine("Not found");
                            }
                        }
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    }
                }
                catch (Exception)
                {
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        goto loopUpdate;
                    }
                    else
                    {
                        return;
                    }
                }

                loopUpdate2:
                try
                {
                    if (ListMarketID.ContainsKey(item.Key))
                    {
                        driver.FindElement(By.ClassName("shopee-button--primary")).Click();
                        driver.FindElement(By.ClassName("list-name-search"));
                    }
                }
                catch (Exception)
                {
                    currentMainTry++;
                    if (currentMainTry < maxMainTry)
                    {
                        goto loopUpdate2;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            MessageBox.Show("Done");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            login();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            driver.Navigate().GoToUrl("https://seller.tokopedia.com/edit-product/" + txtIDproduct.Text);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var elharga = driver.FindElement(By.XPath("//*[@name='price']"));
            elharga.Clear();
            elharga.SendKeys(numHarga.Value.ToString());

            var elstock = driver.FindElement(By.XPath("//*[@name='stock']"));
            elstock.Clear();
            elstock.SendKeys(numStok.Value.ToString());

            driver.FindElement(By.ClassName("unf-btn__primary")).Click();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ListMarketID.Add("ITEM1", "");
            var p = new Product();
            p.price1 = 200000;
            p.stock = 153;
            UpdatingProducts.Add("ITEM1", p);

            ListMarketID.Add("ITEM2", "");
            p = new Product();
            p.price1 = 150000;
            p.stock = 22;
            UpdatingProducts.Add("ITEM2", p);


            login();
            EditAll();
        }

        private void ShopeeTest_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                driver.Navigate().GoToUrl("https://seller.shopee.co.id/");
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
                wait.Until(ExpectedConditions.AlertIsPresent());
                IAlert alert = driver.SwitchTo().Alert();
                alert.Accept();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(maxMainWait);
        }
    }
}
