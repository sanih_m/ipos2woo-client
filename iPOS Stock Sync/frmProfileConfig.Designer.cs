﻿namespace iPOS_Stock_Sync
{
    partial class frmProfileConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfileConfig));
            this.button2 = new System.Windows.Forms.Button();
            this.btnEnableStock = new System.Windows.Forms.Button();
            this.btnDisableRolePrice = new System.Windows.Forms.Button();
            this.btnEnableRolePrice = new System.Windows.Forms.Button();
            this.btnImportCustom1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtWooSecret = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtWooKey = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbMGetCustomer = new System.Windows.Forms.CheckBox();
            this.cbMSendProduct = new System.Windows.Forms.CheckBox();
            this.cbMGetSales = new System.Windows.Forms.CheckBox();
            this.cbMSendCustomer = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDevMain = new System.Windows.Forms.TextBox();
            this.cbAutoStart = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label98 = new System.Windows.Forms.Label();
            this.cmbPrimaryKey = new System.Windows.Forms.ComboBox();
            this.Tipe = new System.Windows.Forms.Label();
            this.cbDbType = new System.Windows.Forms.ComboBox();
            this.txtPort = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbDatabase = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHost = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label90 = new System.Windows.Forms.Label();
            this.cmbWooRestMethod = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.cmbWooPermalink = new System.Windows.Forms.ComboBox();
            this.txtLevel4 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.chbWooSendPublish = new System.Windows.Forms.CheckBox();
            this.chbSyncRakToWeight = new System.Windows.Forms.CheckBox();
            this.chbSyncIposToDraft = new System.Windows.Forms.CheckBox();
            this.txtLevel3 = new System.Windows.Forms.TextBox();
            this.txtLevel2 = new System.Windows.Forms.TextBox();
            this.txtLevel1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbPriceType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtWooPassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtWooUsername = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtWooURL = new System.Windows.Forms.TextBox();
            this.chbWoocommerce = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gbProxyTokped = new System.Windows.Forms.GroupBox();
            this.txtProxyPassTokped = new System.Windows.Forms.TextBox();
            this.txtProxyUserTokped = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.cmbProxyModeTokped = new System.Windows.Forms.ComboBox();
            this.txtProxyPortTokped = new System.Windows.Forms.NumericUpDown();
            this.txtProxyAddressTokped = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.chbProxyTokped = new System.Windows.Forms.CheckBox();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmbStokTokped = new System.Windows.Forms.ComboBox();
            this.label59 = new System.Windows.Forms.Label();
            this.chbSyncTokped = new System.Windows.Forms.CheckBox();
            this.btnManageTokpedCache = new System.Windows.Forms.Button();
            this.txtCheckIDScheduleTokped = new System.Windows.Forms.MaskedTextBox();
            this.cmbPriceTokped = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.gbJualFromIposTokped = new System.Windows.Forms.GroupBox();
            this.cmbJualAkunBLainTokped = new System.Windows.Forms.ComboBox();
            this.cmbJualAkunPotTokped = new System.Windows.Forms.ComboBox();
            this.cmbJualAkunKreditTokped = new System.Windows.Forms.ComboBox();
            this.cmbJualAkunTunaiTokped = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.cmbJualStokMinusTokped = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.txtJualUserIDTokped = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtJualPrefixNomorTokped = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.cmbJualTabTokped = new System.Windows.Forms.ComboBox();
            this.btnJualHapusTokped = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.txtJualGudangTokped = new System.Windows.Forms.TextBox();
            this.txtJualSalesTokped = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtJualPelangganTokped = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.cmbJualTipeBayarTokped = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cmbJualTujuanTokped = new System.Windows.Forms.ComboBox();
            this.chbAmbilPenjualanTokped = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDevTokped = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtPasswordTokped = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtEmailTokped = new System.Windows.Forms.TextBox();
            this.chbTokped = new System.Windows.Forms.CheckBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gbProxyShopee = new System.Windows.Forms.GroupBox();
            this.txtProxyPassShopee = new System.Windows.Forms.TextBox();
            this.txtProxyUserShopee = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.cmbProxyModeShopee = new System.Windows.Forms.ComboBox();
            this.txtProxyPortShopee = new System.Windows.Forms.NumericUpDown();
            this.txtProxyAddressShopee = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.chbProxyShopee = new System.Windows.Forms.CheckBox();
            this.label70 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cmbStokShopee = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.chbSyncShopee = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnManageShopeeCache = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbPriceShopee = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbJualAkunBLainShopee = new System.Windows.Forms.ComboBox();
            this.cmbJualAkunPotShopee = new System.Windows.Forms.ComboBox();
            this.cmbJualAkunKreditShopee = new System.Windows.Forms.ComboBox();
            this.cmbJualAkunTunaiShopee = new System.Windows.Forms.ComboBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.cmbJualBAdminShopee = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this.cmbJualOngkirShopee = new System.Windows.Forms.ComboBox();
            this.label77 = new System.Windows.Forms.Label();
            this.cmbJualStokMinusShopee = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.txtJualUserIDShopee = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.txtJualPrefixNomorShopee = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.cmbJualTabShopee = new System.Windows.Forms.ComboBox();
            this.btnJualHapusShopee = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.txtJualGudangShopee = new System.Windows.Forms.TextBox();
            this.txtJualSalesShopee = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.txtJualPelangganShopee = new System.Windows.Forms.TextBox();
            this.cmbJualTipeBayarShopee = new System.Windows.Forms.ComboBox();
            this.cmbJualTujuanShopee = new System.Windows.Forms.ComboBox();
            this.chbAmbilPenjualanShopee = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDevShopee = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPasswordShopee = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtEmailShopee = new System.Windows.Forms.TextBox();
            this.chbShopee = new System.Windows.Forms.CheckBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtProxyPassBukalapak = new System.Windows.Forms.TextBox();
            this.txtProxyUserBukalapak = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.cmbProxyModeBukalapak = new System.Windows.Forms.ComboBox();
            this.txtProxyPortBukalapak = new System.Windows.Forms.NumericUpDown();
            this.txtProxyAddressBukalapak = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.chbProxyBukalapak = new System.Windows.Forms.CheckBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.cmbBrowserBukalapak = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.cmbJualTipeBayarBukalapak = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.cmbJualTujuanBukalapak = new System.Windows.Forms.ComboBox();
            this.chbAmbilPenjualanBukalapak = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btnManageBukalapakCache = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.txtDevBukalapak = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cmbPriceBukalapak = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtPasswordBukalapak = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtEmailBukalapak = new System.Windows.Forms.TextBox();
            this.chbBukalapak = new System.Windows.Forms.CheckBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtProxyPassLazada = new System.Windows.Forms.TextBox();
            this.txtProxyUserLazada = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.cmbProxyModeLazada = new System.Windows.Forms.ComboBox();
            this.txtProxyPortLazada = new System.Windows.Forms.NumericUpDown();
            this.txtProxyAddressLazada = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.chbProxyLazada = new System.Windows.Forms.CheckBox();
            this.label76 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label45 = new System.Windows.Forms.Label();
            this.cmbJualTipeBayarLazada = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.cmbJualTujuanLazada = new System.Windows.Forms.ComboBox();
            this.chbAmbilPenjualanLazada = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.btnManageLazadaCache = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.txtDevLazada = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cmbPriceLazada = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtPasswordLazada = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtEmailLazada = new System.Windows.Forms.TextBox();
            this.chbLazada = new System.Windows.Forms.CheckBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtProxyPassBlibli = new System.Windows.Forms.TextBox();
            this.txtProxyUserBlibli = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.cmbProxyModeBlibli = new System.Windows.Forms.ComboBox();
            this.txtProxyPortBlibli = new System.Windows.Forms.NumericUpDown();
            this.txtProxyAddressBlibli = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.chbProxyBlibli = new System.Windows.Forms.CheckBox();
            this.label89 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label91 = new System.Windows.Forms.Label();
            this.cmbJualTipeBayarBlibli = new System.Windows.Forms.ComboBox();
            this.label92 = new System.Windows.Forms.Label();
            this.cmbJualTujuanBlibli = new System.Windows.Forms.ComboBox();
            this.chbAmbilPenjualanBlibli = new System.Windows.Forms.CheckBox();
            this.label93 = new System.Windows.Forms.Label();
            this.btnManageBlibliCache = new System.Windows.Forms.Button();
            this.label94 = new System.Windows.Forms.Label();
            this.txtDevBlibli = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.cmbPriceBlibli = new System.Windows.Forms.ComboBox();
            this.label96 = new System.Windows.Forms.Label();
            this.txtPasswordBlibli = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.txtEmailBlibli = new System.Windows.Forms.TextBox();
            this.chbBlibli = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbEngineShopee = new System.Windows.Forms.ComboBox();
            this.label99 = new System.Windows.Forms.Label();
            this.cmbProfileShopee = new System.Windows.Forms.ComboBox();
            this.label100 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPort)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.gbProxyTokped.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortTokped)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.gbJualFromIposTokped.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.gbProxyShopee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortShopee)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortBukalapak)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortLazada)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortBlibli)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(905, 497);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 24);
            this.button2.TabIndex = 41;
            this.button2.Text = "Disable Stock Manager";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnEnableStock
            // 
            this.btnEnableStock.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnableStock.Location = new System.Drawing.Point(757, 497);
            this.btnEnableStock.Name = "btnEnableStock";
            this.btnEnableStock.Size = new System.Drawing.Size(142, 24);
            this.btnEnableStock.TabIndex = 40;
            this.btnEnableStock.Text = "Enable Stock Manager";
            this.btnEnableStock.UseVisualStyleBackColor = true;
            this.btnEnableStock.Visible = false;
            this.btnEnableStock.Click += new System.EventHandler(this.btnEnableStock_Click);
            // 
            // btnDisableRolePrice
            // 
            this.btnDisableRolePrice.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisableRolePrice.Location = new System.Drawing.Point(905, 469);
            this.btnDisableRolePrice.Name = "btnDisableRolePrice";
            this.btnDisableRolePrice.Size = new System.Drawing.Size(138, 24);
            this.btnDisableRolePrice.TabIndex = 39;
            this.btnDisableRolePrice.Text = "Disable Role Price";
            this.btnDisableRolePrice.UseVisualStyleBackColor = true;
            this.btnDisableRolePrice.Visible = false;
            this.btnDisableRolePrice.Click += new System.EventHandler(this.btnDisableRolePrice_Click);
            // 
            // btnEnableRolePrice
            // 
            this.btnEnableRolePrice.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnableRolePrice.Location = new System.Drawing.Point(757, 469);
            this.btnEnableRolePrice.Name = "btnEnableRolePrice";
            this.btnEnableRolePrice.Size = new System.Drawing.Size(142, 24);
            this.btnEnableRolePrice.TabIndex = 38;
            this.btnEnableRolePrice.Text = "Enable Role Price";
            this.btnEnableRolePrice.UseVisualStyleBackColor = true;
            this.btnEnableRolePrice.Visible = false;
            this.btnEnableRolePrice.Click += new System.EventHandler(this.btnEnableRolePrice_Click);
            // 
            // btnImportCustom1
            // 
            this.btnImportCustom1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportCustom1.Location = new System.Drawing.Point(757, 440);
            this.btnImportCustom1.Name = "btnImportCustom1";
            this.btnImportCustom1.Size = new System.Drawing.Size(142, 23);
            this.btnImportCustom1.TabIndex = 14;
            this.btnImportCustom1.Text = "button1";
            this.btnImportCustom1.UseVisualStyleBackColor = true;
            this.btnImportCustom1.Visible = false;
            this.btnImportCustom1.Click += new System.EventHandler(this.btnImportCustom1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(761, 618);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Secret";
            this.label7.Visible = false;
            // 
            // txtWooSecret
            // 
            this.txtWooSecret.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWooSecret.Location = new System.Drawing.Point(820, 615);
            this.txtWooSecret.Name = "txtWooSecret";
            this.txtWooSecret.Size = new System.Drawing.Size(230, 22);
            this.txtWooSecret.TabIndex = 46;
            this.txtWooSecret.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(761, 592);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "Key";
            this.label6.Visible = false;
            // 
            // txtWooKey
            // 
            this.txtWooKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWooKey.Location = new System.Drawing.Point(820, 589);
            this.txtWooKey.Name = "txtWooKey";
            this.txtWooKey.Size = new System.Drawing.Size(230, 22);
            this.txtWooKey.TabIndex = 44;
            this.txtWooKey.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(652, 635);
            this.tabControl1.TabIndex = 48;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbMGetCustomer);
            this.tabPage1.Controls.Add(this.cbMSendProduct);
            this.tabPage1.Controls.Add(this.cbMGetSales);
            this.tabPage1.Controls.Add(this.cbMSendCustomer);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.txtDevMain);
            this.tabPage1.Controls.Add(this.cbAutoStart);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(633, 607);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Umum";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // cbMGetCustomer
            // 
            this.cbMGetCustomer.AutoSize = true;
            this.cbMGetCustomer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMGetCustomer.Location = new System.Drawing.Point(154, 62);
            this.cbMGetCustomer.Name = "cbMGetCustomer";
            this.cbMGetCustomer.Size = new System.Drawing.Size(140, 17);
            this.cbMGetCustomer.TabIndex = 59;
            this.cbMGetCustomer.Text = "Ambil Data Pelanggan";
            this.cbMGetCustomer.UseVisualStyleBackColor = true;
            // 
            // cbMSendProduct
            // 
            this.cbMSendProduct.AutoSize = true;
            this.cbMSendProduct.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMSendProduct.Location = new System.Drawing.Point(12, 108);
            this.cbMSendProduct.Name = "cbMSendProduct";
            this.cbMSendProduct.Size = new System.Drawing.Size(118, 17);
            this.cbMSendProduct.TabIndex = 58;
            this.cbMSendProduct.Text = "Kirim Data Produk";
            this.cbMSendProduct.UseVisualStyleBackColor = true;
            // 
            // cbMGetSales
            // 
            this.cbMGetSales.AutoSize = true;
            this.cbMGetSales.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMGetSales.Location = new System.Drawing.Point(12, 85);
            this.cbMGetSales.Name = "cbMGetSales";
            this.cbMGetSales.Size = new System.Drawing.Size(136, 17);
            this.cbMGetSales.TabIndex = 57;
            this.cbMGetSales.Text = "Ambil Data Penjualan";
            this.cbMGetSales.UseVisualStyleBackColor = true;
            // 
            // cbMSendCustomer
            // 
            this.cbMSendCustomer.AutoSize = true;
            this.cbMSendCustomer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMSendCustomer.Location = new System.Drawing.Point(154, 85);
            this.cbMSendCustomer.Name = "cbMSendCustomer";
            this.cbMSendCustomer.Size = new System.Drawing.Size(136, 17);
            this.cbMSendCustomer.TabIndex = 56;
            this.cbMSendCustomer.Text = "Kirim Data Pelanggan";
            this.cbMSendCustomer.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(9, 36);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 13);
            this.label22.TabIndex = 55;
            this.label22.Text = "Mode Developer";
            // 
            // txtDevMain
            // 
            this.txtDevMain.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDevMain.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevMain.Location = new System.Drawing.Point(107, 34);
            this.txtDevMain.Name = "txtDevMain";
            this.txtDevMain.Size = new System.Drawing.Size(176, 22);
            this.txtDevMain.TabIndex = 54;
            this.txtDevMain.UseSystemPasswordChar = true;
            // 
            // cbAutoStart
            // 
            this.cbAutoStart.AutoSize = true;
            this.cbAutoStart.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAutoStart.Location = new System.Drawing.Point(12, 62);
            this.cbAutoStart.Name = "cbAutoStart";
            this.cbAutoStart.Size = new System.Drawing.Size(105, 17);
            this.cbAutoStart.TabIndex = 53;
            this.cbAutoStart.Text = "Mulai Otomatis";
            this.cbAutoStart.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(9, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 52;
            this.label15.Text = "Profile Name";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(107, 6);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(176, 22);
            this.txtName.TabIndex = 51;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label98);
            this.tabPage2.Controls.Add(this.cmbPrimaryKey);
            this.tabPage2.Controls.Add(this.Tipe);
            this.tabPage2.Controls.Add(this.cbDbType);
            this.tabPage2.Controls.Add(this.txtPort);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.cmbDatabase);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.txtPassword);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.txtUsername);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.txtHost);
            this.tabPage2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(633, 607);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Database IPOS";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(9, 178);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(67, 13);
            this.label98.TabIndex = 57;
            this.label98.Text = "SKU Produk";
            // 
            // cmbPrimaryKey
            // 
            this.cmbPrimaryKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrimaryKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPrimaryKey.FormattingEnabled = true;
            this.cmbPrimaryKey.Items.AddRange(new object[] {
            "Kode Item",
            "Barcode"});
            this.cmbPrimaryKey.Location = new System.Drawing.Point(89, 175);
            this.cmbPrimaryKey.Name = "cmbPrimaryKey";
            this.cmbPrimaryKey.Size = new System.Drawing.Size(230, 21);
            this.cmbPrimaryKey.TabIndex = 56;
            // 
            // Tipe
            // 
            this.Tipe.AutoSize = true;
            this.Tipe.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tipe.Location = new System.Drawing.Point(9, 42);
            this.Tipe.Name = "Tipe";
            this.Tipe.Size = new System.Drawing.Size(29, 13);
            this.Tipe.TabIndex = 55;
            this.Tipe.Text = "Tipe";
            // 
            // cbDbType
            // 
            this.cbDbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDbType.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDbType.FormattingEnabled = true;
            this.cbDbType.Items.AddRange(new object[] {
            "IPOS 4",
            "IPOS 5",
            "Custom"});
            this.cbDbType.Location = new System.Drawing.Point(89, 39);
            this.cbDbType.Name = "cbDbType";
            this.cbDbType.Size = new System.Drawing.Size(230, 21);
            this.cbDbType.TabIndex = 54;
            this.cbDbType.SelectedIndexChanged += new System.EventHandler(this.cbDbType_SelectedIndexChanged);
            // 
            // txtPort
            // 
            this.txtPort.Enabled = false;
            this.txtPort.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(145, 66);
            this.txtPort.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(174, 22);
            this.txtPort.TabIndex = 53;
            this.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(86, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 52;
            this.label10.Text = "Port";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "Database";
            // 
            // cmbDatabase
            // 
            this.cmbDatabase.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDatabase.FormattingEnabled = true;
            this.cmbDatabase.Location = new System.Drawing.Point(89, 148);
            this.cmbDatabase.Name = "cmbDatabase";
            this.cmbDatabase.Size = new System.Drawing.Size(230, 21);
            this.cmbDatabase.TabIndex = 50;
            this.cmbDatabase.SelectedIndexChanged += new System.EventHandler(this.cmbDatabase_SelectedIndexChanged);
            this.cmbDatabase.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbDatabase_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(86, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 49;
            this.label3.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Enabled = false;
            this.txtPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(145, 120);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(174, 22);
            this.txtPassword.TabIndex = 48;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(86, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 47;
            this.label2.Text = "Username";
            // 
            // txtUsername
            // 
            this.txtUsername.Enabled = false;
            this.txtUsername.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.Location = new System.Drawing.Point(145, 92);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(174, 22);
            this.txtUsername.TabIndex = 46;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Host";
            // 
            // txtHost
            // 
            this.txtHost.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHost.Location = new System.Drawing.Point(89, 11);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(230, 22);
            this.txtHost.TabIndex = 44;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label90);
            this.tabPage3.Controls.Add(this.cmbWooRestMethod);
            this.tabPage3.Controls.Add(this.label64);
            this.tabPage3.Controls.Add(this.cmbWooPermalink);
            this.tabPage3.Controls.Add(this.txtLevel4);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.chbWooSendPublish);
            this.tabPage3.Controls.Add(this.chbSyncRakToWeight);
            this.tabPage3.Controls.Add(this.chbSyncIposToDraft);
            this.tabPage3.Controls.Add(this.txtLevel3);
            this.tabPage3.Controls.Add(this.txtLevel2);
            this.tabPage3.Controls.Add(this.txtLevel1);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.cmbPriceType);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.txtWooPassword);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.txtWooUsername);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.txtWooURL);
            this.tabPage3.Controls.Add(this.chbWoocommerce);
            this.tabPage3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(633, 607);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Woocommerce";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(11, 274);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(47, 13);
            this.label90.TabIndex = 67;
            this.label90.Text = "Metode";
            // 
            // cmbWooRestMethod
            // 
            this.cmbWooRestMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWooRestMethod.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbWooRestMethod.FormattingEnabled = true;
            this.cmbWooRestMethod.Items.AddRange(new object[] {
            "1 (Flurl)",
            "2 (RestClient)"});
            this.cmbWooRestMethod.Location = new System.Drawing.Point(89, 271);
            this.cmbWooRestMethod.Name = "cmbWooRestMethod";
            this.cmbWooRestMethod.Size = new System.Drawing.Size(231, 21);
            this.cmbWooRestMethod.TabIndex = 66;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(11, 57);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(57, 13);
            this.label64.TabIndex = 65;
            this.label64.Text = "Permalink";
            // 
            // cmbWooPermalink
            // 
            this.cmbWooPermalink.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWooPermalink.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbWooPermalink.FormattingEnabled = true;
            this.cmbWooPermalink.Items.AddRange(new object[] {
            "Custom / Pretty",
            "Plain"});
            this.cmbWooPermalink.Location = new System.Drawing.Point(89, 54);
            this.cmbWooPermalink.Name = "cmbWooPermalink";
            this.cmbWooPermalink.Size = new System.Drawing.Size(231, 21);
            this.cmbWooPermalink.TabIndex = 64;
            // 
            // txtLevel4
            // 
            this.txtLevel4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel4.Location = new System.Drawing.Point(89, 243);
            this.txtLevel4.Name = "txtLevel4";
            this.txtLevel4.Size = new System.Drawing.Size(230, 22);
            this.txtLevel4.TabIndex = 63;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(11, 246);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 62;
            this.label25.Text = "Level 4";
            // 
            // chbWooSendPublish
            // 
            this.chbWooSendPublish.AutoSize = true;
            this.chbWooSendPublish.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbWooSendPublish.Location = new System.Drawing.Point(14, 321);
            this.chbWooSendPublish.Name = "chbWooSendPublish";
            this.chbWooSendPublish.Size = new System.Drawing.Size(110, 17);
            this.chbWooSendPublish.TabIndex = 61;
            this.chbWooSendPublish.Text = "Kirim Data Terbit";
            this.chbWooSendPublish.UseVisualStyleBackColor = true;
            // 
            // chbSyncRakToWeight
            // 
            this.chbSyncRakToWeight.AutoSize = true;
            this.chbSyncRakToWeight.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbSyncRakToWeight.Location = new System.Drawing.Point(183, 298);
            this.chbSyncRakToWeight.Name = "chbSyncRakToWeight";
            this.chbSyncRakToWeight.Size = new System.Drawing.Size(118, 17);
            this.chbSyncRakToWeight.TabIndex = 60;
            this.chbSyncRakToWeight.Text = "Rak Sebagai Berat";
            this.chbSyncRakToWeight.UseVisualStyleBackColor = true;
            // 
            // chbSyncIposToDraft
            // 
            this.chbSyncIposToDraft.AutoSize = true;
            this.chbSyncIposToDraft.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbSyncIposToDraft.Location = new System.Drawing.Point(14, 298);
            this.chbSyncIposToDraft.Name = "chbSyncIposToDraft";
            this.chbSyncIposToDraft.Size = new System.Drawing.Size(162, 17);
            this.chbSyncIposToDraft.TabIndex = 59;
            this.chbSyncIposToDraft.Text = "Produk Baru Sebagai Draft";
            this.chbSyncIposToDraft.UseVisualStyleBackColor = true;
            // 
            // txtLevel3
            // 
            this.txtLevel3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel3.Location = new System.Drawing.Point(89, 217);
            this.txtLevel3.Name = "txtLevel3";
            this.txtLevel3.Size = new System.Drawing.Size(230, 22);
            this.txtLevel3.TabIndex = 58;
            // 
            // txtLevel2
            // 
            this.txtLevel2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel2.Location = new System.Drawing.Point(89, 189);
            this.txtLevel2.Name = "txtLevel2";
            this.txtLevel2.Size = new System.Drawing.Size(230, 22);
            this.txtLevel2.TabIndex = 57;
            // 
            // txtLevel1
            // 
            this.txtLevel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLevel1.Location = new System.Drawing.Point(89, 162);
            this.txtLevel1.Name = "txtLevel1";
            this.txtLevel1.Size = new System.Drawing.Size(230, 22);
            this.txtLevel1.TabIndex = 56;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 220);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 55;
            this.label14.Text = "Level 3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(11, 193);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 54;
            this.label13.Text = "Level 2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(11, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 53;
            this.label12.Text = "Level 1";
            // 
            // cmbPriceType
            // 
            this.cmbPriceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriceType.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriceType.FormattingEnabled = true;
            this.cmbPriceType.Items.AddRange(new object[] {
            "Plugin Role Based Price",
            "Gunakan hanya Level 1",
            "Gunakan hanya Level 2",
            "Gunakan hanya Level 3",
            "Gunakan hanya Level 4",
            "Plugin TokoPressID (reseller & agen)"});
            this.cmbPriceType.Location = new System.Drawing.Point(89, 135);
            this.cmbPriceType.Name = "cmbPriceType";
            this.cmbPriceType.Size = new System.Drawing.Size(230, 21);
            this.cmbPriceType.TabIndex = 52;
            this.cmbPriceType.SelectedIndexChanged += new System.EventHandler(this.cmbPriceType_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(11, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "Sistem Harga";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 50;
            this.label8.Text = "Password";
            // 
            // txtWooPassword
            // 
            this.txtWooPassword.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWooPassword.Location = new System.Drawing.Point(89, 107);
            this.txtWooPassword.Name = "txtWooPassword";
            this.txtWooPassword.PasswordChar = '*';
            this.txtWooPassword.Size = new System.Drawing.Size(230, 22);
            this.txtWooPassword.TabIndex = 49;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 48;
            this.label9.Text = "Username";
            // 
            // txtWooUsername
            // 
            this.txtWooUsername.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWooUsername.Location = new System.Drawing.Point(89, 81);
            this.txtWooUsername.Name = "txtWooUsername";
            this.txtWooUsername.Size = new System.Drawing.Size(230, 22);
            this.txtWooUsername.TabIndex = 47;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "URL";
            // 
            // txtWooURL
            // 
            this.txtWooURL.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWooURL.Location = new System.Drawing.Point(89, 26);
            this.txtWooURL.Name = "txtWooURL";
            this.txtWooURL.Size = new System.Drawing.Size(230, 22);
            this.txtWooURL.TabIndex = 45;
            // 
            // chbWoocommerce
            // 
            this.chbWoocommerce.AutoSize = true;
            this.chbWoocommerce.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbWoocommerce.Location = new System.Drawing.Point(14, 9);
            this.chbWoocommerce.Name = "chbWoocommerce";
            this.chbWoocommerce.Size = new System.Drawing.Size(182, 17);
            this.chbWoocommerce.TabIndex = 42;
            this.chbWoocommerce.Text = "Sinkronisasi Ke Woocommerce";
            this.chbWoocommerce.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gbProxyTokped);
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.gbJualFromIposTokped);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.txtDevTokped);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Controls.Add(this.txtPasswordTokped);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.txtEmailTokped);
            this.tabPage4.Controls.Add(this.chbTokped);
            this.tabPage4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(633, 607);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tokopedia";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gbProxyTokped
            // 
            this.gbProxyTokped.Controls.Add(this.txtProxyPassTokped);
            this.gbProxyTokped.Controls.Add(this.txtProxyUserTokped);
            this.gbProxyTokped.Controls.Add(this.label67);
            this.gbProxyTokped.Controls.Add(this.cmbProxyModeTokped);
            this.gbProxyTokped.Controls.Add(this.txtProxyPortTokped);
            this.gbProxyTokped.Controls.Add(this.txtProxyAddressTokped);
            this.gbProxyTokped.Controls.Add(this.label65);
            this.gbProxyTokped.Controls.Add(this.chbProxyTokped);
            this.gbProxyTokped.Controls.Add(this.label68);
            this.gbProxyTokped.Location = new System.Drawing.Point(9, 453);
            this.gbProxyTokped.Name = "gbProxyTokped";
            this.gbProxyTokped.Size = new System.Drawing.Size(613, 109);
            this.gbProxyTokped.TabIndex = 61;
            this.gbProxyTokped.TabStop = false;
            this.gbProxyTokped.Text = "       Gunakan Server Proxy";
            // 
            // txtProxyPassTokped
            // 
            this.txtProxyPassTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyPassTokped.Location = new System.Drawing.Point(298, 80);
            this.txtProxyPassTokped.Name = "txtProxyPassTokped";
            this.txtProxyPassTokped.Size = new System.Drawing.Size(149, 22);
            this.txtProxyPassTokped.TabIndex = 67;
            this.txtProxyPassTokped.UseSystemPasswordChar = true;
            // 
            // txtProxyUserTokped
            // 
            this.txtProxyUserTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyUserTokped.Location = new System.Drawing.Point(141, 79);
            this.txtProxyUserTokped.Name = "txtProxyUserTokped";
            this.txtProxyUserTokped.Size = new System.Drawing.Size(151, 22);
            this.txtProxyUserTokped.TabIndex = 65;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(13, 83);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(122, 13);
            this.label67.TabIndex = 64;
            this.label67.Text = "Username && Password";
            // 
            // cmbProxyModeTokped
            // 
            this.cmbProxyModeTokped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProxyModeTokped.FormattingEnabled = true;
            this.cmbProxyModeTokped.Items.AddRange(new object[] {
            "Otomatis (Proxy ipos2woo)",
            "Manual (Proxy pribadi)"});
            this.cmbProxyModeTokped.Location = new System.Drawing.Point(141, 24);
            this.cmbProxyModeTokped.Name = "cmbProxyModeTokped";
            this.cmbProxyModeTokped.Size = new System.Drawing.Size(218, 21);
            this.cmbProxyModeTokped.TabIndex = 63;
            // 
            // txtProxyPortTokped
            // 
            this.txtProxyPortTokped.Location = new System.Drawing.Point(298, 51);
            this.txtProxyPortTokped.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtProxyPortTokped.Name = "txtProxyPortTokped";
            this.txtProxyPortTokped.Size = new System.Drawing.Size(61, 22);
            this.txtProxyPortTokped.TabIndex = 62;
            // 
            // txtProxyAddressTokped
            // 
            this.txtProxyAddressTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyAddressTokped.Location = new System.Drawing.Point(141, 51);
            this.txtProxyAddressTokped.Name = "txtProxyAddressTokped";
            this.txtProxyAddressTokped.Size = new System.Drawing.Size(151, 22);
            this.txtProxyAddressTokped.TabIndex = 61;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(13, 27);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(37, 13);
            this.label65.TabIndex = 60;
            this.label65.Text = "Mode";
            // 
            // chbProxyTokped
            // 
            this.chbProxyTokped.AutoSize = true;
            this.chbProxyTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbProxyTokped.Location = new System.Drawing.Point(10, 0);
            this.chbProxyTokped.Name = "chbProxyTokped";
            this.chbProxyTokped.Size = new System.Drawing.Size(15, 14);
            this.chbProxyTokped.TabIndex = 43;
            this.chbProxyTokped.UseVisualStyleBackColor = true;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(13, 54);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(78, 13);
            this.label68.TabIndex = 55;
            this.label68.Text = "Alamat && Port";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cmbStokTokped);
            this.groupBox4.Controls.Add(this.label59);
            this.groupBox4.Controls.Add(this.chbSyncTokped);
            this.groupBox4.Controls.Add(this.btnManageTokpedCache);
            this.groupBox4.Controls.Add(this.txtCheckIDScheduleTokped);
            this.groupBox4.Controls.Add(this.cmbPriceTokped);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Location = new System.Drawing.Point(9, 116);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(612, 136);
            this.groupBox4.TabIndex = 60;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "       Sinkronisasi Data Produk";
            // 
            // cmbStokTokped
            // 
            this.cmbStokTokped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStokTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStokTokped.FormattingEnabled = true;
            this.cmbStokTokped.Items.AddRange(new object[] {
            "Tidak",
            "Ya"});
            this.cmbStokTokped.Location = new System.Drawing.Point(129, 78);
            this.cmbStokTokped.Name = "cmbStokTokped";
            this.cmbStokTokped.Size = new System.Drawing.Size(122, 21);
            this.cmbStokTokped.TabIndex = 59;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(11, 81);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(79, 13);
            this.label59.TabIndex = 60;
            this.label59.Text = "Samakan Stok";
            // 
            // chbSyncTokped
            // 
            this.chbSyncTokped.AutoSize = true;
            this.chbSyncTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbSyncTokped.Location = new System.Drawing.Point(10, 0);
            this.chbSyncTokped.Name = "chbSyncTokped";
            this.chbSyncTokped.Size = new System.Drawing.Size(15, 14);
            this.chbSyncTokped.TabIndex = 43;
            this.chbSyncTokped.UseVisualStyleBackColor = true;
            // 
            // btnManageTokpedCache
            // 
            this.btnManageTokpedCache.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageTokpedCache.Location = new System.Drawing.Point(129, 21);
            this.btnManageTokpedCache.Name = "btnManageTokpedCache";
            this.btnManageTokpedCache.Size = new System.Drawing.Size(122, 23);
            this.btnManageTokpedCache.TabIndex = 54;
            this.btnManageTokpedCache.Text = "Kelola Manual";
            this.btnManageTokpedCache.UseVisualStyleBackColor = true;
            this.btnManageTokpedCache.Click += new System.EventHandler(this.btnManageTokpedCache_Click);
            // 
            // txtCheckIDScheduleTokped
            // 
            this.txtCheckIDScheduleTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckIDScheduleTokped.Location = new System.Drawing.Point(131, 50);
            this.txtCheckIDScheduleTokped.Mask = "00:00";
            this.txtCheckIDScheduleTokped.Name = "txtCheckIDScheduleTokped";
            this.txtCheckIDScheduleTokped.Size = new System.Drawing.Size(42, 22);
            this.txtCheckIDScheduleTokped.TabIndex = 58;
            this.txtCheckIDScheduleTokped.ValidatingType = typeof(System.DateTime);
            // 
            // cmbPriceTokped
            // 
            this.cmbPriceTokped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriceTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriceTokped.FormattingEnabled = true;
            this.cmbPriceTokped.Items.AddRange(new object[] {
            "Tidak",
            "Level 1",
            "Level 2",
            "Level 3",
            "Level 4"});
            this.cmbPriceTokped.Location = new System.Drawing.Point(129, 105);
            this.cmbPriceTokped.Name = "cmbPriceTokped";
            this.cmbPriceTokped.Size = new System.Drawing.Size(122, 21);
            this.cmbPriceTokped.TabIndex = 50;
            this.cmbPriceTokped.SelectedIndexChanged += new System.EventHandler(this.cmbPriceTokped_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(11, 53);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(97, 13);
            this.label28.TabIndex = 56;
            this.label28.Text = "Cek ID Setiap Jam";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(11, 108);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 51;
            this.label18.Text = "Samakan Harga";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(11, 26);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 13);
            this.label26.TabIndex = 55;
            this.label26.Text = "Daftar ID Produk";
            // 
            // gbJualFromIposTokped
            // 
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualAkunBLainTokped);
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualAkunPotTokped);
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualAkunKreditTokped);
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualAkunTunaiTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label51);
            this.gbJualFromIposTokped.Controls.Add(this.label84);
            this.gbJualFromIposTokped.Controls.Add(this.label85);
            this.gbJualFromIposTokped.Controls.Add(this.label86);
            this.gbJualFromIposTokped.Controls.Add(this.label78);
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualStokMinusTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label61);
            this.gbJualFromIposTokped.Controls.Add(this.txtJualUserIDTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label58);
            this.gbJualFromIposTokped.Controls.Add(this.txtJualPrefixNomorTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label49);
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualTabTokped);
            this.gbJualFromIposTokped.Controls.Add(this.btnJualHapusTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label50);
            this.gbJualFromIposTokped.Controls.Add(this.label48);
            this.gbJualFromIposTokped.Controls.Add(this.txtJualGudangTokped);
            this.gbJualFromIposTokped.Controls.Add(this.txtJualSalesTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label47);
            this.gbJualFromIposTokped.Controls.Add(this.txtJualPelangganTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label40);
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualTipeBayarTokped);
            this.gbJualFromIposTokped.Controls.Add(this.label39);
            this.gbJualFromIposTokped.Controls.Add(this.cmbJualTujuanTokped);
            this.gbJualFromIposTokped.Controls.Add(this.chbAmbilPenjualanTokped);
            this.gbJualFromIposTokped.Location = new System.Drawing.Point(9, 258);
            this.gbJualFromIposTokped.Name = "gbJualFromIposTokped";
            this.gbJualFromIposTokped.Size = new System.Drawing.Size(612, 188);
            this.gbJualFromIposTokped.TabIndex = 59;
            this.gbJualFromIposTokped.TabStop = false;
            this.gbJualFromIposTokped.Text = "      Ambil Data Penjualan  ( Hanya Untuk IPOS )";
            // 
            // cmbJualAkunBLainTokped
            // 
            this.cmbJualAkunBLainTokped.FormattingEnabled = true;
            this.cmbJualAkunBLainTokped.Items.AddRange(new object[] {
            "4-1700"});
            this.cmbJualAkunBLainTokped.Location = new System.Drawing.Point(387, 158);
            this.cmbJualAkunBLainTokped.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunBLainTokped.Name = "cmbJualAkunBLainTokped";
            this.cmbJualAkunBLainTokped.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunBLainTokped.TabIndex = 109;
            // 
            // cmbJualAkunPotTokped
            // 
            this.cmbJualAkunPotTokped.FormattingEnabled = true;
            this.cmbJualAkunPotTokped.Items.AddRange(new object[] {
            "4-1500"});
            this.cmbJualAkunPotTokped.Location = new System.Drawing.Point(387, 132);
            this.cmbJualAkunPotTokped.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunPotTokped.Name = "cmbJualAkunPotTokped";
            this.cmbJualAkunPotTokped.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunPotTokped.TabIndex = 108;
            // 
            // cmbJualAkunKreditTokped
            // 
            this.cmbJualAkunKreditTokped.FormattingEnabled = true;
            this.cmbJualAkunKreditTokped.Items.AddRange(new object[] {
            "1-210"});
            this.cmbJualAkunKreditTokped.Location = new System.Drawing.Point(387, 105);
            this.cmbJualAkunKreditTokped.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunKreditTokped.Name = "cmbJualAkunKreditTokped";
            this.cmbJualAkunKreditTokped.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunKreditTokped.TabIndex = 107;
            // 
            // cmbJualAkunTunaiTokped
            // 
            this.cmbJualAkunTunaiTokped.FormattingEnabled = true;
            this.cmbJualAkunTunaiTokped.Items.AddRange(new object[] {
            "1-1110"});
            this.cmbJualAkunTunaiTokped.Location = new System.Drawing.Point(387, 78);
            this.cmbJualAkunTunaiTokped.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunTunaiTokped.Name = "cmbJualAkunTunaiTokped";
            this.cmbJualAkunTunaiTokped.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunTunaiTokped.TabIndex = 106;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(282, 161);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(87, 13);
            this.label51.TabIndex = 105;
            this.label51.Text = "Akun Biaya Lain";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(282, 107);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(67, 13);
            this.label84.TabIndex = 104;
            this.label84.Text = "Akun Kredit";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(282, 134);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(88, 13);
            this.label85.TabIndex = 103;
            this.label85.Text = "Akun Potongan";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(282, 81);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(90, 13);
            this.label86.TabIndex = 102;
            this.label86.Text = "Akun Tunai / DP";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(7, 108);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(127, 13);
            this.label78.TabIndex = 91;
            this.label78.Text = "Stok Barang Bisa Minus";
            // 
            // cmbJualStokMinusTokped
            // 
            this.cmbJualStokMinusTokped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualStokMinusTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualStokMinusTokped.FormattingEnabled = true;
            this.cmbJualStokMinusTokped.Items.AddRange(new object[] {
            "Tidak",
            "Ya"});
            this.cmbJualStokMinusTokped.Location = new System.Drawing.Point(152, 105);
            this.cmbJualStokMinusTokped.Name = "cmbJualStokMinusTokped";
            this.cmbJualStokMinusTokped.Size = new System.Drawing.Size(124, 21);
            this.cmbJualStokMinusTokped.TabIndex = 90;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(455, 84);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(71, 13);
            this.label61.TabIndex = 70;
            this.label61.Text = "User ID IPOS";
            // 
            // txtJualUserIDTokped
            // 
            this.txtJualUserIDTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualUserIDTokped.Location = new System.Drawing.Point(539, 79);
            this.txtJualUserIDTokped.Name = "txtJualUserIDTokped";
            this.txtJualUserIDTokped.Size = new System.Drawing.Size(62, 22);
            this.txtJualUserIDTokped.TabIndex = 69;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(282, 26);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(83, 13);
            this.label58.TabIndex = 68;
            this.label58.Text = "Awalan Nomor";
            // 
            // txtJualPrefixNomorTokped
            // 
            this.txtJualPrefixNomorTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualPrefixNomorTokped.Location = new System.Drawing.Point(387, 23);
            this.txtJualPrefixNomorTokped.Name = "txtJualPrefixNomorTokped";
            this.txtJualPrefixNomorTokped.Size = new System.Drawing.Size(62, 22);
            this.txtJualPrefixNomorTokped.TabIndex = 67;
            this.txtJualPrefixNomorTokped.Text = "TP-";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(7, 27);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(114, 13);
            this.label49.TabIndex = 66;
            this.label49.Text = "Target Tab Penjualan";
            // 
            // cmbJualTabTokped
            // 
            this.cmbJualTabTokped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTabTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTabTokped.FormattingEnabled = true;
            this.cmbJualTabTokped.Items.AddRange(new object[] {
            "Siap Dikirim",
            "Pesanan Baru",
            "Semua Pesanan"});
            this.cmbJualTabTokped.Location = new System.Drawing.Point(152, 24);
            this.cmbJualTabTokped.Name = "cmbJualTabTokped";
            this.cmbJualTabTokped.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTabTokped.TabIndex = 65;
            // 
            // btnJualHapusTokped
            // 
            this.btnJualHapusTokped.Location = new System.Drawing.Point(9, 132);
            this.btnJualHapusTokped.Name = "btnJualHapusTokped";
            this.btnJualHapusTokped.Size = new System.Drawing.Size(143, 21);
            this.btnJualHapusTokped.TabIndex = 64;
            this.btnJualHapusTokped.Text = "Hapus Penjualan";
            this.btnJualHapusTokped.UseVisualStyleBackColor = true;
            this.btnJualHapusTokped.Click += new System.EventHandler(this.btnJualHapusTokped_Click);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(455, 57);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(62, 13);
            this.label50.TabIndex = 61;
            this.label50.Text = "Kode Sales";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(455, 29);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(78, 13);
            this.label48.TabIndex = 58;
            this.label48.Text = "Kode Gudang";
            // 
            // txtJualGudangTokped
            // 
            this.txtJualGudangTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualGudangTokped.Location = new System.Drawing.Point(539, 23);
            this.txtJualGudangTokped.Name = "txtJualGudangTokped";
            this.txtJualGudangTokped.Size = new System.Drawing.Size(62, 22);
            this.txtJualGudangTokped.TabIndex = 57;
            // 
            // txtJualSalesTokped
            // 
            this.txtJualSalesTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualSalesTokped.Location = new System.Drawing.Point(539, 51);
            this.txtJualSalesTokped.Name = "txtJualSalesTokped";
            this.txtJualSalesTokped.Size = new System.Drawing.Size(62, 22);
            this.txtJualSalesTokped.TabIndex = 59;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(282, 54);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(91, 13);
            this.label47.TabIndex = 56;
            this.label47.Text = "Kode Pelanggan";
            // 
            // txtJualPelangganTokped
            // 
            this.txtJualPelangganTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualPelangganTokped.Location = new System.Drawing.Point(387, 51);
            this.txtJualPelangganTokped.Name = "txtJualPelangganTokped";
            this.txtJualPelangganTokped.Size = new System.Drawing.Size(62, 22);
            this.txtJualPelangganTokped.TabIndex = 50;
            this.txtJualPelangganTokped.Text = "UMUM";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(7, 81);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(132, 13);
            this.label40.TabIndex = 55;
            this.label40.Text = "Tipe Pembayaran di Ipos";
            // 
            // cmbJualTipeBayarTokped
            // 
            this.cmbJualTipeBayarTokped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTipeBayarTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTipeBayarTokped.FormattingEnabled = true;
            this.cmbJualTipeBayarTokped.Items.AddRange(new object[] {
            "Kredit",
            "Tunai"});
            this.cmbJualTipeBayarTokped.Location = new System.Drawing.Point(152, 78);
            this.cmbJualTipeBayarTokped.Name = "cmbJualTipeBayarTokped";
            this.cmbJualTipeBayarTokped.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTipeBayarTokped.TabIndex = 54;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(7, 54);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(135, 13);
            this.label39.TabIndex = 53;
            this.label39.Text = "Tujuan Penjualan di Ipos";
            // 
            // cmbJualTujuanTokped
            // 
            this.cmbJualTujuanTokped.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTujuanTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTujuanTokped.FormattingEnabled = true;
            this.cmbJualTujuanTokped.Items.AddRange(new object[] {
            "Pesanan Penjualan",
            "Penjualan",
            "Kasir"});
            this.cmbJualTujuanTokped.Location = new System.Drawing.Point(152, 51);
            this.cmbJualTujuanTokped.Name = "cmbJualTujuanTokped";
            this.cmbJualTujuanTokped.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTujuanTokped.TabIndex = 52;
            // 
            // chbAmbilPenjualanTokped
            // 
            this.chbAmbilPenjualanTokped.AutoSize = true;
            this.chbAmbilPenjualanTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAmbilPenjualanTokped.Location = new System.Drawing.Point(9, 0);
            this.chbAmbilPenjualanTokped.Name = "chbAmbilPenjualanTokped";
            this.chbAmbilPenjualanTokped.Size = new System.Drawing.Size(15, 14);
            this.chbAmbilPenjualanTokped.TabIndex = 42;
            this.chbAmbilPenjualanTokped.UseVisualStyleBackColor = true;
            this.chbAmbilPenjualanTokped.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(6, 91);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "Mode Developer";
            // 
            // txtDevTokped
            // 
            this.txtDevTokped.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDevTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevTokped.Location = new System.Drawing.Point(124, 88);
            this.txtDevTokped.Name = "txtDevTokped";
            this.txtDevTokped.Size = new System.Drawing.Size(176, 22);
            this.txtDevTokped.TabIndex = 52;
            this.txtDevTokped.UseSystemPasswordChar = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 63);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 49;
            this.label23.Text = "Password";
            // 
            // txtPasswordTokped
            // 
            this.txtPasswordTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordTokped.Location = new System.Drawing.Point(124, 60);
            this.txtPasswordTokped.Name = "txtPasswordTokped";
            this.txtPasswordTokped.Size = new System.Drawing.Size(176, 22);
            this.txtPasswordTokped.TabIndex = 48;
            this.txtPasswordTokped.UseSystemPasswordChar = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(6, 35);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 13);
            this.label24.TabIndex = 47;
            this.label24.Text = "Email";
            // 
            // txtEmailTokped
            // 
            this.txtEmailTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailTokped.Location = new System.Drawing.Point(124, 32);
            this.txtEmailTokped.Name = "txtEmailTokped";
            this.txtEmailTokped.Size = new System.Drawing.Size(176, 22);
            this.txtEmailTokped.TabIndex = 46;
            // 
            // chbTokped
            // 
            this.chbTokped.AutoSize = true;
            this.chbTokped.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbTokped.Location = new System.Drawing.Point(9, 9);
            this.chbTokped.Name = "chbTokped";
            this.chbTokped.Size = new System.Drawing.Size(160, 17);
            this.chbTokped.TabIndex = 41;
            this.chbTokped.Text = "Hubungkan Ke Tokopedia";
            this.chbTokped.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.cmbProfileShopee);
            this.tabPage5.Controls.Add(this.label100);
            this.tabPage5.Controls.Add(this.cmbEngineShopee);
            this.tabPage5.Controls.Add(this.label99);
            this.tabPage5.Controls.Add(this.gbProxyShopee);
            this.tabPage5.Controls.Add(this.groupBox6);
            this.tabPage5.Controls.Add(this.groupBox1);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Controls.Add(this.txtDevShopee);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.txtPasswordShopee);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.txtEmailShopee);
            this.tabPage5.Controls.Add(this.chbShopee);
            this.tabPage5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(644, 609);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Shopee";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gbProxyShopee
            // 
            this.gbProxyShopee.Controls.Add(this.txtProxyPassShopee);
            this.gbProxyShopee.Controls.Add(this.txtProxyUserShopee);
            this.gbProxyShopee.Controls.Add(this.label66);
            this.gbProxyShopee.Controls.Add(this.cmbProxyModeShopee);
            this.gbProxyShopee.Controls.Add(this.txtProxyPortShopee);
            this.gbProxyShopee.Controls.Add(this.txtProxyAddressShopee);
            this.gbProxyShopee.Controls.Add(this.label69);
            this.gbProxyShopee.Controls.Add(this.chbProxyShopee);
            this.gbProxyShopee.Controls.Add(this.label70);
            this.gbProxyShopee.Location = new System.Drawing.Point(6, 455);
            this.gbProxyShopee.Name = "gbProxyShopee";
            this.gbProxyShopee.Size = new System.Drawing.Size(613, 109);
            this.gbProxyShopee.TabIndex = 62;
            this.gbProxyShopee.TabStop = false;
            this.gbProxyShopee.Text = "       Gunakan Server Proxy";
            // 
            // txtProxyPassShopee
            // 
            this.txtProxyPassShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyPassShopee.Location = new System.Drawing.Point(298, 80);
            this.txtProxyPassShopee.Name = "txtProxyPassShopee";
            this.txtProxyPassShopee.Size = new System.Drawing.Size(149, 22);
            this.txtProxyPassShopee.TabIndex = 67;
            this.txtProxyPassShopee.UseSystemPasswordChar = true;
            // 
            // txtProxyUserShopee
            // 
            this.txtProxyUserShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyUserShopee.Location = new System.Drawing.Point(141, 79);
            this.txtProxyUserShopee.Name = "txtProxyUserShopee";
            this.txtProxyUserShopee.Size = new System.Drawing.Size(151, 22);
            this.txtProxyUserShopee.TabIndex = 65;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(13, 83);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(122, 13);
            this.label66.TabIndex = 64;
            this.label66.Text = "Username && Password";
            // 
            // cmbProxyModeShopee
            // 
            this.cmbProxyModeShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProxyModeShopee.FormattingEnabled = true;
            this.cmbProxyModeShopee.Items.AddRange(new object[] {
            "Otomatis (Proxy ipos2woo)",
            "Manual (Proxy pribadi)"});
            this.cmbProxyModeShopee.Location = new System.Drawing.Point(141, 24);
            this.cmbProxyModeShopee.Name = "cmbProxyModeShopee";
            this.cmbProxyModeShopee.Size = new System.Drawing.Size(218, 21);
            this.cmbProxyModeShopee.TabIndex = 63;
            // 
            // txtProxyPortShopee
            // 
            this.txtProxyPortShopee.Location = new System.Drawing.Point(298, 51);
            this.txtProxyPortShopee.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtProxyPortShopee.Name = "txtProxyPortShopee";
            this.txtProxyPortShopee.Size = new System.Drawing.Size(61, 22);
            this.txtProxyPortShopee.TabIndex = 62;
            // 
            // txtProxyAddressShopee
            // 
            this.txtProxyAddressShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyAddressShopee.Location = new System.Drawing.Point(141, 51);
            this.txtProxyAddressShopee.Name = "txtProxyAddressShopee";
            this.txtProxyAddressShopee.Size = new System.Drawing.Size(151, 22);
            this.txtProxyAddressShopee.TabIndex = 61;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(13, 27);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(37, 13);
            this.label69.TabIndex = 60;
            this.label69.Text = "Mode";
            // 
            // chbProxyShopee
            // 
            this.chbProxyShopee.AutoSize = true;
            this.chbProxyShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbProxyShopee.Location = new System.Drawing.Point(10, 0);
            this.chbProxyShopee.Name = "chbProxyShopee";
            this.chbProxyShopee.Size = new System.Drawing.Size(15, 14);
            this.chbProxyShopee.TabIndex = 43;
            this.chbProxyShopee.UseVisualStyleBackColor = true;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(13, 54);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(78, 13);
            this.label70.TabIndex = 55;
            this.label70.Text = "Alamat && Port";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cmbStokShopee);
            this.groupBox6.Controls.Add(this.label60);
            this.groupBox6.Controls.Add(this.chbSyncShopee);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.btnManageShopeeCache);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.cmbPriceShopee);
            this.groupBox6.Location = new System.Drawing.Point(6, 114);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(617, 105);
            this.groupBox6.TabIndex = 61;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "     Sinkronisasi Data Produk";
            // 
            // cmbStokShopee
            // 
            this.cmbStokShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStokShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStokShopee.FormattingEnabled = true;
            this.cmbStokShopee.Items.AddRange(new object[] {
            "Tidak",
            "Ya"});
            this.cmbStokShopee.Location = new System.Drawing.Point(112, 47);
            this.cmbStokShopee.Name = "cmbStokShopee";
            this.cmbStokShopee.Size = new System.Drawing.Size(122, 21);
            this.cmbStokShopee.TabIndex = 63;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(13, 50);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(79, 13);
            this.label60.TabIndex = 64;
            this.label60.Text = "Samakan Stok";
            // 
            // chbSyncShopee
            // 
            this.chbSyncShopee.AutoSize = true;
            this.chbSyncShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbSyncShopee.Location = new System.Drawing.Point(6, 0);
            this.chbSyncShopee.Name = "chbSyncShopee";
            this.chbSyncShopee.Size = new System.Drawing.Size(15, 14);
            this.chbSyncShopee.TabIndex = 62;
            this.chbSyncShopee.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(13, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 13);
            this.label27.TabIndex = 61;
            this.label27.Text = "Daftar ID Produk";
            // 
            // btnManageShopeeCache
            // 
            this.btnManageShopeeCache.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageShopeeCache.Location = new System.Drawing.Point(112, 18);
            this.btnManageShopeeCache.Name = "btnManageShopeeCache";
            this.btnManageShopeeCache.Size = new System.Drawing.Size(122, 23);
            this.btnManageShopeeCache.TabIndex = 60;
            this.btnManageShopeeCache.Text = "Kelola Manual";
            this.btnManageShopeeCache.UseVisualStyleBackColor = true;
            this.btnManageShopeeCache.Click += new System.EventHandler(this.btnManageShopeeCache_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(13, 77);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 13);
            this.label19.TabIndex = 59;
            this.label19.Text = "Samakan Harga";
            // 
            // cmbPriceShopee
            // 
            this.cmbPriceShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriceShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriceShopee.FormattingEnabled = true;
            this.cmbPriceShopee.Items.AddRange(new object[] {
            "Tanpa Harga",
            "Level 1",
            "Level 2",
            "Level 3",
            "Level 4"});
            this.cmbPriceShopee.Location = new System.Drawing.Point(112, 74);
            this.cmbPriceShopee.Name = "cmbPriceShopee";
            this.cmbPriceShopee.Size = new System.Drawing.Size(122, 21);
            this.cmbPriceShopee.TabIndex = 58;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbJualAkunBLainShopee);
            this.groupBox1.Controls.Add(this.cmbJualAkunPotShopee);
            this.groupBox1.Controls.Add(this.cmbJualAkunKreditShopee);
            this.groupBox1.Controls.Add(this.cmbJualAkunTunaiShopee);
            this.groupBox1.Controls.Add(this.label83);
            this.groupBox1.Controls.Add(this.label82);
            this.groupBox1.Controls.Add(this.label81);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this.cmbJualBAdminShopee);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this.cmbJualOngkirShopee);
            this.groupBox1.Controls.Add(this.label77);
            this.groupBox1.Controls.Add(this.cmbJualStokMinusShopee);
            this.groupBox1.Controls.Add(this.label62);
            this.groupBox1.Controls.Add(this.txtJualUserIDShopee);
            this.groupBox1.Controls.Add(this.label57);
            this.groupBox1.Controls.Add(this.txtJualPrefixNomorShopee);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.cmbJualTabShopee);
            this.groupBox1.Controls.Add(this.btnJualHapusShopee);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.label54);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Controls.Add(this.txtJualGudangShopee);
            this.groupBox1.Controls.Add(this.txtJualSalesShopee);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.txtJualPelangganShopee);
            this.groupBox1.Controls.Add(this.cmbJualTipeBayarShopee);
            this.groupBox1.Controls.Add(this.cmbJualTujuanShopee);
            this.groupBox1.Controls.Add(this.chbAmbilPenjualanShopee);
            this.groupBox1.Location = new System.Drawing.Point(6, 225);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(617, 224);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "      Ambil Data Penjualan (Hanya Untuk IPOS) ";
            // 
            // cmbJualAkunBLainShopee
            // 
            this.cmbJualAkunBLainShopee.FormattingEnabled = true;
            this.cmbJualAkunBLainShopee.Items.AddRange(new object[] {
            "4-1700"});
            this.cmbJualAkunBLainShopee.Location = new System.Drawing.Point(398, 156);
            this.cmbJualAkunBLainShopee.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunBLainShopee.Name = "cmbJualAkunBLainShopee";
            this.cmbJualAkunBLainShopee.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunBLainShopee.TabIndex = 101;
            // 
            // cmbJualAkunPotShopee
            // 
            this.cmbJualAkunPotShopee.FormattingEnabled = true;
            this.cmbJualAkunPotShopee.Items.AddRange(new object[] {
            "4-1500"});
            this.cmbJualAkunPotShopee.Location = new System.Drawing.Point(398, 129);
            this.cmbJualAkunPotShopee.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunPotShopee.Name = "cmbJualAkunPotShopee";
            this.cmbJualAkunPotShopee.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunPotShopee.TabIndex = 100;
            // 
            // cmbJualAkunKreditShopee
            // 
            this.cmbJualAkunKreditShopee.FormattingEnabled = true;
            this.cmbJualAkunKreditShopee.Items.AddRange(new object[] {
            "1-210"});
            this.cmbJualAkunKreditShopee.Location = new System.Drawing.Point(398, 102);
            this.cmbJualAkunKreditShopee.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunKreditShopee.Name = "cmbJualAkunKreditShopee";
            this.cmbJualAkunKreditShopee.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunKreditShopee.TabIndex = 99;
            // 
            // cmbJualAkunTunaiShopee
            // 
            this.cmbJualAkunTunaiShopee.FormattingEnabled = true;
            this.cmbJualAkunTunaiShopee.Items.AddRange(new object[] {
            "1-1110"});
            this.cmbJualAkunTunaiShopee.Location = new System.Drawing.Point(398, 76);
            this.cmbJualAkunTunaiShopee.Margin = new System.Windows.Forms.Padding(2);
            this.cmbJualAkunTunaiShopee.Name = "cmbJualAkunTunaiShopee";
            this.cmbJualAkunTunaiShopee.Size = new System.Drawing.Size(63, 21);
            this.cmbJualAkunTunaiShopee.TabIndex = 98;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(293, 158);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(87, 13);
            this.label83.TabIndex = 97;
            this.label83.Text = "Akun Biaya Lain";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(293, 105);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(67, 13);
            this.label82.TabIndex = 95;
            this.label82.Text = "Akun Kredit";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(293, 132);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(88, 13);
            this.label81.TabIndex = 93;
            this.label81.Text = "Akun Potongan";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(11, 160);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(101, 13);
            this.label80.TabIndex = 91;
            this.label80.Text = "Ambil Biaya Admin";
            // 
            // cmbJualBAdminShopee
            // 
            this.cmbJualBAdminShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualBAdminShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualBAdminShopee.FormattingEnabled = true;
            this.cmbJualBAdminShopee.Items.AddRange(new object[] {
            "Tidak",
            "Sebagai Potongan"});
            this.cmbJualBAdminShopee.Location = new System.Drawing.Point(156, 158);
            this.cmbJualBAdminShopee.Name = "cmbJualBAdminShopee";
            this.cmbJualBAdminShopee.Size = new System.Drawing.Size(124, 21);
            this.cmbJualBAdminShopee.TabIndex = 90;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(11, 132);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(108, 13);
            this.label79.TabIndex = 89;
            this.label79.Text = "Ambil Ongkos Kirim";
            // 
            // cmbJualOngkirShopee
            // 
            this.cmbJualOngkirShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualOngkirShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualOngkirShopee.FormattingEnabled = true;
            this.cmbJualOngkirShopee.Items.AddRange(new object[] {
            "Tidak",
            "Sebagai Biaya Lain"});
            this.cmbJualOngkirShopee.Location = new System.Drawing.Point(156, 129);
            this.cmbJualOngkirShopee.Name = "cmbJualOngkirShopee";
            this.cmbJualOngkirShopee.Size = new System.Drawing.Size(124, 21);
            this.cmbJualOngkirShopee.TabIndex = 88;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(11, 105);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(127, 13);
            this.label77.TabIndex = 87;
            this.label77.Text = "Stok Barang Bisa Minus";
            this.label77.Click += new System.EventHandler(this.label77_Click);
            // 
            // cmbJualStokMinusShopee
            // 
            this.cmbJualStokMinusShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualStokMinusShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualStokMinusShopee.FormattingEnabled = true;
            this.cmbJualStokMinusShopee.Items.AddRange(new object[] {
            "Tidak",
            "Ya"});
            this.cmbJualStokMinusShopee.Location = new System.Drawing.Point(156, 102);
            this.cmbJualStokMinusShopee.Name = "cmbJualStokMinusShopee";
            this.cmbJualStokMinusShopee.Size = new System.Drawing.Size(124, 21);
            this.cmbJualStokMinusShopee.TabIndex = 86;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(466, 79);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(71, 13);
            this.label62.TabIndex = 83;
            this.label62.Text = "User ID IPOS";
            // 
            // txtJualUserIDShopee
            // 
            this.txtJualUserIDShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualUserIDShopee.Location = new System.Drawing.Point(550, 76);
            this.txtJualUserIDShopee.Name = "txtJualUserIDShopee";
            this.txtJualUserIDShopee.Size = new System.Drawing.Size(62, 22);
            this.txtJualUserIDShopee.TabIndex = 82;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(293, 24);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(83, 13);
            this.label57.TabIndex = 81;
            this.label57.Text = "Awalan Nomor";
            // 
            // txtJualPrefixNomorShopee
            // 
            this.txtJualPrefixNomorShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualPrefixNomorShopee.Location = new System.Drawing.Point(398, 21);
            this.txtJualPrefixNomorShopee.Name = "txtJualPrefixNomorShopee";
            this.txtJualPrefixNomorShopee.Size = new System.Drawing.Size(62, 22);
            this.txtJualPrefixNomorShopee.TabIndex = 80;
            this.txtJualPrefixNomorShopee.Text = "SP-";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(11, 78);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(132, 13);
            this.label41.TabIndex = 79;
            this.label41.Text = "Tipe Pembayaran di Ipos";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(11, 51);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(135, 13);
            this.label42.TabIndex = 78;
            this.label42.Text = "Tujuan Penjualan di Ipos";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(11, 24);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(114, 13);
            this.label52.TabIndex = 77;
            this.label52.Text = "Target Tab Penjualan";
            // 
            // cmbJualTabShopee
            // 
            this.cmbJualTabShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTabShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTabShopee.FormattingEnabled = true;
            this.cmbJualTabShopee.Items.AddRange(new object[] {
            "PD (Telah Diproses)",
            "Dikirim",
            "Selesai",
            "Belum Bayar",
            "Semua",
            "PD (Belum Diproses)",
            "PD (Semua)"});
            this.cmbJualTabShopee.Location = new System.Drawing.Point(156, 21);
            this.cmbJualTabShopee.Name = "cmbJualTabShopee";
            this.cmbJualTabShopee.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTabShopee.TabIndex = 76;
            // 
            // btnJualHapusShopee
            // 
            this.btnJualHapusShopee.Location = new System.Drawing.Point(9, 189);
            this.btnJualHapusShopee.Name = "btnJualHapusShopee";
            this.btnJualHapusShopee.Size = new System.Drawing.Size(143, 21);
            this.btnJualHapusShopee.TabIndex = 75;
            this.btnJualHapusShopee.Text = "Hapus Penjualan";
            this.btnJualHapusShopee.UseVisualStyleBackColor = true;
            this.btnJualHapusShopee.Click += new System.EventHandler(this.btnJualHapusShopee_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(293, 79);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(90, 13);
            this.label53.TabIndex = 74;
            this.label53.Text = "Akun Tunai / DP";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(466, 52);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(62, 13);
            this.label54.TabIndex = 72;
            this.label54.Text = "Kode Sales";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(466, 24);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(78, 13);
            this.label55.TabIndex = 70;
            this.label55.Text = "Kode Gudang";
            // 
            // txtJualGudangShopee
            // 
            this.txtJualGudangShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualGudangShopee.Location = new System.Drawing.Point(550, 20);
            this.txtJualGudangShopee.Name = "txtJualGudangShopee";
            this.txtJualGudangShopee.Size = new System.Drawing.Size(61, 22);
            this.txtJualGudangShopee.TabIndex = 69;
            // 
            // txtJualSalesShopee
            // 
            this.txtJualSalesShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualSalesShopee.Location = new System.Drawing.Point(550, 48);
            this.txtJualSalesShopee.Name = "txtJualSalesShopee";
            this.txtJualSalesShopee.Size = new System.Drawing.Size(62, 22);
            this.txtJualSalesShopee.TabIndex = 71;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(293, 52);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(91, 13);
            this.label56.TabIndex = 68;
            this.label56.Text = "Kode Pelanggan";
            // 
            // txtJualPelangganShopee
            // 
            this.txtJualPelangganShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJualPelangganShopee.Location = new System.Drawing.Point(398, 49);
            this.txtJualPelangganShopee.Name = "txtJualPelangganShopee";
            this.txtJualPelangganShopee.Size = new System.Drawing.Size(62, 22);
            this.txtJualPelangganShopee.TabIndex = 67;
            this.txtJualPelangganShopee.Text = "UMUM";
            // 
            // cmbJualTipeBayarShopee
            // 
            this.cmbJualTipeBayarShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTipeBayarShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTipeBayarShopee.FormattingEnabled = true;
            this.cmbJualTipeBayarShopee.Items.AddRange(new object[] {
            "Kredit",
            "Tunai"});
            this.cmbJualTipeBayarShopee.Location = new System.Drawing.Point(156, 75);
            this.cmbJualTipeBayarShopee.Name = "cmbJualTipeBayarShopee";
            this.cmbJualTipeBayarShopee.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTipeBayarShopee.TabIndex = 54;
            // 
            // cmbJualTujuanShopee
            // 
            this.cmbJualTujuanShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTujuanShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTujuanShopee.FormattingEnabled = true;
            this.cmbJualTujuanShopee.Items.AddRange(new object[] {
            "Pesanan Penjualan",
            "Penjualan",
            "Kasir"});
            this.cmbJualTujuanShopee.Location = new System.Drawing.Point(156, 48);
            this.cmbJualTujuanShopee.Name = "cmbJualTujuanShopee";
            this.cmbJualTujuanShopee.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTujuanShopee.TabIndex = 52;
            // 
            // chbAmbilPenjualanShopee
            // 
            this.chbAmbilPenjualanShopee.AutoSize = true;
            this.chbAmbilPenjualanShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAmbilPenjualanShopee.Location = new System.Drawing.Point(9, 0);
            this.chbAmbilPenjualanShopee.Name = "chbAmbilPenjualanShopee";
            this.chbAmbilPenjualanShopee.Size = new System.Drawing.Size(15, 14);
            this.chbAmbilPenjualanShopee.TabIndex = 42;
            this.chbAmbilPenjualanShopee.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(5, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 13);
            this.label21.TabIndex = 54;
            this.label21.Text = "Mode Developer";
            // 
            // txtDevShopee
            // 
            this.txtDevShopee.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDevShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevShopee.Location = new System.Drawing.Point(123, 86);
            this.txtDevShopee.Name = "txtDevShopee";
            this.txtDevShopee.Size = new System.Drawing.Size(176, 22);
            this.txtDevShopee.TabIndex = 53;
            this.txtDevShopee.UseSystemPasswordChar = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(5, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 50;
            this.label16.Text = "Password";
            // 
            // txtPasswordShopee
            // 
            this.txtPasswordShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordShopee.Location = new System.Drawing.Point(123, 58);
            this.txtPasswordShopee.Name = "txtPasswordShopee";
            this.txtPasswordShopee.Size = new System.Drawing.Size(176, 22);
            this.txtPasswordShopee.TabIndex = 49;
            this.txtPasswordShopee.UseSystemPasswordChar = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(5, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 48;
            this.label17.Text = "Email";
            // 
            // txtEmailShopee
            // 
            this.txtEmailShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailShopee.Location = new System.Drawing.Point(123, 30);
            this.txtEmailShopee.Name = "txtEmailShopee";
            this.txtEmailShopee.Size = new System.Drawing.Size(176, 22);
            this.txtEmailShopee.TabIndex = 47;
            // 
            // chbShopee
            // 
            this.chbShopee.AutoSize = true;
            this.chbShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbShopee.Location = new System.Drawing.Point(8, 7);
            this.chbShopee.Name = "chbShopee";
            this.chbShopee.Size = new System.Drawing.Size(145, 17);
            this.chbShopee.TabIndex = 41;
            this.chbShopee.Text = "Hubungkan Ke Shopee";
            this.chbShopee.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chbShopee.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox7);
            this.tabPage6.Controls.Add(this.label63);
            this.tabPage6.Controls.Add(this.cmbBrowserBukalapak);
            this.tabPage6.Controls.Add(this.groupBox2);
            this.tabPage6.Controls.Add(this.label29);
            this.tabPage6.Controls.Add(this.btnManageBukalapakCache);
            this.tabPage6.Controls.Add(this.label30);
            this.tabPage6.Controls.Add(this.txtDevBukalapak);
            this.tabPage6.Controls.Add(this.label31);
            this.tabPage6.Controls.Add(this.cmbPriceBukalapak);
            this.tabPage6.Controls.Add(this.label32);
            this.tabPage6.Controls.Add(this.txtPasswordBukalapak);
            this.tabPage6.Controls.Add(this.label33);
            this.tabPage6.Controls.Add(this.txtEmailBukalapak);
            this.tabPage6.Controls.Add(this.chbBukalapak);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(633, 607);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Bukalapak";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtProxyPassBukalapak);
            this.groupBox7.Controls.Add(this.txtProxyUserBukalapak);
            this.groupBox7.Controls.Add(this.label71);
            this.groupBox7.Controls.Add(this.cmbProxyModeBukalapak);
            this.groupBox7.Controls.Add(this.txtProxyPortBukalapak);
            this.groupBox7.Controls.Add(this.txtProxyAddressBukalapak);
            this.groupBox7.Controls.Add(this.label72);
            this.groupBox7.Controls.Add(this.chbProxyBukalapak);
            this.groupBox7.Controls.Add(this.label73);
            this.groupBox7.Location = new System.Drawing.Point(6, 197);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(613, 109);
            this.groupBox7.TabIndex = 72;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "       Gunakan Server Proxy";
            // 
            // txtProxyPassBukalapak
            // 
            this.txtProxyPassBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyPassBukalapak.Location = new System.Drawing.Point(298, 80);
            this.txtProxyPassBukalapak.Name = "txtProxyPassBukalapak";
            this.txtProxyPassBukalapak.Size = new System.Drawing.Size(149, 22);
            this.txtProxyPassBukalapak.TabIndex = 67;
            this.txtProxyPassBukalapak.UseSystemPasswordChar = true;
            // 
            // txtProxyUserBukalapak
            // 
            this.txtProxyUserBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyUserBukalapak.Location = new System.Drawing.Point(141, 79);
            this.txtProxyUserBukalapak.Name = "txtProxyUserBukalapak";
            this.txtProxyUserBukalapak.Size = new System.Drawing.Size(151, 22);
            this.txtProxyUserBukalapak.TabIndex = 65;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(13, 83);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(122, 13);
            this.label71.TabIndex = 64;
            this.label71.Text = "Username && Password";
            // 
            // cmbProxyModeBukalapak
            // 
            this.cmbProxyModeBukalapak.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProxyModeBukalapak.FormattingEnabled = true;
            this.cmbProxyModeBukalapak.Items.AddRange(new object[] {
            "Otomatis (Proxy ipos2woo)",
            "Manual (Proxy pribadi)"});
            this.cmbProxyModeBukalapak.Location = new System.Drawing.Point(141, 24);
            this.cmbProxyModeBukalapak.Name = "cmbProxyModeBukalapak";
            this.cmbProxyModeBukalapak.Size = new System.Drawing.Size(218, 21);
            this.cmbProxyModeBukalapak.TabIndex = 63;
            // 
            // txtProxyPortBukalapak
            // 
            this.txtProxyPortBukalapak.Location = new System.Drawing.Point(298, 51);
            this.txtProxyPortBukalapak.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtProxyPortBukalapak.Name = "txtProxyPortBukalapak";
            this.txtProxyPortBukalapak.Size = new System.Drawing.Size(61, 22);
            this.txtProxyPortBukalapak.TabIndex = 62;
            // 
            // txtProxyAddressBukalapak
            // 
            this.txtProxyAddressBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyAddressBukalapak.Location = new System.Drawing.Point(141, 51);
            this.txtProxyAddressBukalapak.Name = "txtProxyAddressBukalapak";
            this.txtProxyAddressBukalapak.Size = new System.Drawing.Size(151, 22);
            this.txtProxyAddressBukalapak.TabIndex = 61;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(13, 27);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(37, 13);
            this.label72.TabIndex = 60;
            this.label72.Text = "Mode";
            // 
            // chbProxyBukalapak
            // 
            this.chbProxyBukalapak.AutoSize = true;
            this.chbProxyBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbProxyBukalapak.Location = new System.Drawing.Point(10, 0);
            this.chbProxyBukalapak.Name = "chbProxyBukalapak";
            this.chbProxyBukalapak.Size = new System.Drawing.Size(15, 14);
            this.chbProxyBukalapak.TabIndex = 43;
            this.chbProxyBukalapak.UseVisualStyleBackColor = true;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(13, 54);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(78, 13);
            this.label73.TabIndex = 55;
            this.label73.Text = "Alamat && Port";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Red;
            this.label63.Location = new System.Drawing.Point(6, 173);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(85, 13);
            this.label63.TabIndex = 71;
            this.label63.Text = "Modul Browser";
            // 
            // cmbBrowserBukalapak
            // 
            this.cmbBrowserBukalapak.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBrowserBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBrowserBukalapak.FormattingEnabled = true;
            this.cmbBrowserBukalapak.Items.AddRange(new object[] {
            "Google Chrome",
            "Mozilla Firefox"});
            this.cmbBrowserBukalapak.Location = new System.Drawing.Point(123, 170);
            this.cmbBrowserBukalapak.Name = "cmbBrowserBukalapak";
            this.cmbBrowserBukalapak.Size = new System.Drawing.Size(176, 21);
            this.cmbBrowserBukalapak.TabIndex = 70;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.cmbJualTipeBayarBukalapak);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.cmbJualTujuanBukalapak);
            this.groupBox2.Controls.Add(this.chbAmbilPenjualanBukalapak);
            this.groupBox2.Location = new System.Drawing.Point(18, 459);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(244, 88);
            this.groupBox2.TabIndex = 69;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "      Ambil Data Penjualan ";
            this.groupBox2.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(7, 56);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(94, 13);
            this.label43.TabIndex = 55;
            this.label43.Text = "Tipe Pembayaran";
            // 
            // cmbJualTipeBayarBukalapak
            // 
            this.cmbJualTipeBayarBukalapak.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTipeBayarBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTipeBayarBukalapak.FormattingEnabled = true;
            this.cmbJualTipeBayarBukalapak.Items.AddRange(new object[] {
            "Kredit",
            "Tunai"});
            this.cmbJualTipeBayarBukalapak.Location = new System.Drawing.Point(109, 53);
            this.cmbJualTipeBayarBukalapak.Name = "cmbJualTipeBayarBukalapak";
            this.cmbJualTipeBayarBukalapak.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTipeBayarBukalapak.TabIndex = 54;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(7, 29);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(97, 13);
            this.label44.TabIndex = 53;
            this.label44.Text = "Tujuan Penjualan";
            // 
            // cmbJualTujuanBukalapak
            // 
            this.cmbJualTujuanBukalapak.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTujuanBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTujuanBukalapak.FormattingEnabled = true;
            this.cmbJualTujuanBukalapak.Items.AddRange(new object[] {
            "Pesanan Penjualan",
            "Penjualan",
            "Kasir"});
            this.cmbJualTujuanBukalapak.Location = new System.Drawing.Point(109, 26);
            this.cmbJualTujuanBukalapak.Name = "cmbJualTujuanBukalapak";
            this.cmbJualTujuanBukalapak.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTujuanBukalapak.TabIndex = 52;
            // 
            // chbAmbilPenjualanBukalapak
            // 
            this.chbAmbilPenjualanBukalapak.AutoSize = true;
            this.chbAmbilPenjualanBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAmbilPenjualanBukalapak.Location = new System.Drawing.Point(9, 0);
            this.chbAmbilPenjualanBukalapak.Name = "chbAmbilPenjualanBukalapak";
            this.chbAmbilPenjualanBukalapak.Size = new System.Drawing.Size(15, 14);
            this.chbAmbilPenjualanBukalapak.TabIndex = 42;
            this.chbAmbilPenjualanBukalapak.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(5, 118);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 13);
            this.label29.TabIndex = 68;
            this.label29.Text = "Data Produk";
            // 
            // btnManageBukalapakCache
            // 
            this.btnManageBukalapakCache.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageBukalapakCache.Location = new System.Drawing.Point(123, 113);
            this.btnManageBukalapakCache.Name = "btnManageBukalapakCache";
            this.btnManageBukalapakCache.Size = new System.Drawing.Size(176, 23);
            this.btnManageBukalapakCache.TabIndex = 67;
            this.btnManageBukalapakCache.Text = "Kelola Manual";
            this.btnManageBukalapakCache.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(6, 145);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 13);
            this.label30.TabIndex = 66;
            this.label30.Text = "Mode Developer";
            // 
            // txtDevBukalapak
            // 
            this.txtDevBukalapak.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDevBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevBukalapak.Location = new System.Drawing.Point(123, 142);
            this.txtDevBukalapak.Name = "txtDevBukalapak";
            this.txtDevBukalapak.Size = new System.Drawing.Size(176, 22);
            this.txtDevBukalapak.TabIndex = 65;
            this.txtDevBukalapak.UseSystemPasswordChar = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(5, 89);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(112, 13);
            this.label31.TabIndex = 64;
            this.label31.Text = "Harga Yg Digunakan";
            // 
            // cmbPriceBukalapak
            // 
            this.cmbPriceBukalapak.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriceBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriceBukalapak.FormattingEnabled = true;
            this.cmbPriceBukalapak.Items.AddRange(new object[] {
            "Tanpa Harga",
            "Level 1",
            "Level 2",
            "Level 3",
            "Level 4"});
            this.cmbPriceBukalapak.Location = new System.Drawing.Point(123, 86);
            this.cmbPriceBukalapak.Name = "cmbPriceBukalapak";
            this.cmbPriceBukalapak.Size = new System.Drawing.Size(176, 21);
            this.cmbPriceBukalapak.TabIndex = 63;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(6, 61);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(56, 13);
            this.label32.TabIndex = 62;
            this.label32.Text = "Password";
            // 
            // txtPasswordBukalapak
            // 
            this.txtPasswordBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordBukalapak.Location = new System.Drawing.Point(123, 58);
            this.txtPasswordBukalapak.Name = "txtPasswordBukalapak";
            this.txtPasswordBukalapak.Size = new System.Drawing.Size(176, 22);
            this.txtPasswordBukalapak.TabIndex = 61;
            this.txtPasswordBukalapak.UseSystemPasswordChar = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(6, 33);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(34, 13);
            this.label33.TabIndex = 60;
            this.label33.Text = "Email";
            // 
            // txtEmailBukalapak
            // 
            this.txtEmailBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailBukalapak.Location = new System.Drawing.Point(123, 30);
            this.txtEmailBukalapak.Name = "txtEmailBukalapak";
            this.txtEmailBukalapak.Size = new System.Drawing.Size(176, 22);
            this.txtEmailBukalapak.TabIndex = 59;
            // 
            // chbBukalapak
            // 
            this.chbBukalapak.AutoSize = true;
            this.chbBukalapak.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbBukalapak.Location = new System.Drawing.Point(8, 7);
            this.chbBukalapak.Name = "chbBukalapak";
            this.chbBukalapak.Size = new System.Drawing.Size(159, 17);
            this.chbBukalapak.TabIndex = 58;
            this.chbBukalapak.Text = "Hubungkan Ke Bukalapak";
            this.chbBukalapak.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chbBukalapak.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox8);
            this.tabPage7.Controls.Add(this.groupBox3);
            this.tabPage7.Controls.Add(this.label34);
            this.tabPage7.Controls.Add(this.btnManageLazadaCache);
            this.tabPage7.Controls.Add(this.label35);
            this.tabPage7.Controls.Add(this.txtDevLazada);
            this.tabPage7.Controls.Add(this.label36);
            this.tabPage7.Controls.Add(this.cmbPriceLazada);
            this.tabPage7.Controls.Add(this.label37);
            this.tabPage7.Controls.Add(this.txtPasswordLazada);
            this.tabPage7.Controls.Add(this.label38);
            this.tabPage7.Controls.Add(this.txtEmailLazada);
            this.tabPage7.Controls.Add(this.chbLazada);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(633, 607);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Lazada";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtProxyPassLazada);
            this.groupBox8.Controls.Add(this.txtProxyUserLazada);
            this.groupBox8.Controls.Add(this.label74);
            this.groupBox8.Controls.Add(this.cmbProxyModeLazada);
            this.groupBox8.Controls.Add(this.txtProxyPortLazada);
            this.groupBox8.Controls.Add(this.txtProxyAddressLazada);
            this.groupBox8.Controls.Add(this.label75);
            this.groupBox8.Controls.Add(this.chbProxyLazada);
            this.groupBox8.Controls.Add(this.label76);
            this.groupBox8.Location = new System.Drawing.Point(6, 169);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(613, 109);
            this.groupBox8.TabIndex = 81;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "       Gunakan Server Proxy";
            // 
            // txtProxyPassLazada
            // 
            this.txtProxyPassLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyPassLazada.Location = new System.Drawing.Point(298, 80);
            this.txtProxyPassLazada.Name = "txtProxyPassLazada";
            this.txtProxyPassLazada.Size = new System.Drawing.Size(149, 22);
            this.txtProxyPassLazada.TabIndex = 67;
            this.txtProxyPassLazada.UseSystemPasswordChar = true;
            // 
            // txtProxyUserLazada
            // 
            this.txtProxyUserLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyUserLazada.Location = new System.Drawing.Point(141, 79);
            this.txtProxyUserLazada.Name = "txtProxyUserLazada";
            this.txtProxyUserLazada.Size = new System.Drawing.Size(151, 22);
            this.txtProxyUserLazada.TabIndex = 65;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(13, 83);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(122, 13);
            this.label74.TabIndex = 64;
            this.label74.Text = "Username && Password";
            // 
            // cmbProxyModeLazada
            // 
            this.cmbProxyModeLazada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProxyModeLazada.FormattingEnabled = true;
            this.cmbProxyModeLazada.Items.AddRange(new object[] {
            "Otomatis (Proxy ipos2woo)",
            "Manual (Proxy pribadi)"});
            this.cmbProxyModeLazada.Location = new System.Drawing.Point(141, 24);
            this.cmbProxyModeLazada.Name = "cmbProxyModeLazada";
            this.cmbProxyModeLazada.Size = new System.Drawing.Size(218, 21);
            this.cmbProxyModeLazada.TabIndex = 63;
            // 
            // txtProxyPortLazada
            // 
            this.txtProxyPortLazada.Location = new System.Drawing.Point(298, 51);
            this.txtProxyPortLazada.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtProxyPortLazada.Name = "txtProxyPortLazada";
            this.txtProxyPortLazada.Size = new System.Drawing.Size(61, 22);
            this.txtProxyPortLazada.TabIndex = 62;
            // 
            // txtProxyAddressLazada
            // 
            this.txtProxyAddressLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyAddressLazada.Location = new System.Drawing.Point(141, 51);
            this.txtProxyAddressLazada.Name = "txtProxyAddressLazada";
            this.txtProxyAddressLazada.Size = new System.Drawing.Size(151, 22);
            this.txtProxyAddressLazada.TabIndex = 61;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(13, 27);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(37, 13);
            this.label75.TabIndex = 60;
            this.label75.Text = "Mode";
            // 
            // chbProxyLazada
            // 
            this.chbProxyLazada.AutoSize = true;
            this.chbProxyLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbProxyLazada.Location = new System.Drawing.Point(10, 0);
            this.chbProxyLazada.Name = "chbProxyLazada";
            this.chbProxyLazada.Size = new System.Drawing.Size(15, 14);
            this.chbProxyLazada.TabIndex = 43;
            this.chbProxyLazada.UseVisualStyleBackColor = true;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(13, 54);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(78, 13);
            this.label76.TabIndex = 55;
            this.label76.Text = "Alamat && Port";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.cmbJualTipeBayarLazada);
            this.groupBox3.Controls.Add(this.label46);
            this.groupBox3.Controls.Add(this.cmbJualTujuanLazada);
            this.groupBox3.Controls.Add(this.chbAmbilPenjualanLazada);
            this.groupBox3.Location = new System.Drawing.Point(26, 424);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(244, 88);
            this.groupBox3.TabIndex = 80;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "      Ambil Data Penjualan ";
            this.groupBox3.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(7, 56);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(94, 13);
            this.label45.TabIndex = 55;
            this.label45.Text = "Tipe Pembayaran";
            // 
            // cmbJualTipeBayarLazada
            // 
            this.cmbJualTipeBayarLazada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTipeBayarLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTipeBayarLazada.FormattingEnabled = true;
            this.cmbJualTipeBayarLazada.Items.AddRange(new object[] {
            "Kredit",
            "Tunai"});
            this.cmbJualTipeBayarLazada.Location = new System.Drawing.Point(109, 53);
            this.cmbJualTipeBayarLazada.Name = "cmbJualTipeBayarLazada";
            this.cmbJualTipeBayarLazada.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTipeBayarLazada.TabIndex = 54;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(7, 29);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(97, 13);
            this.label46.TabIndex = 53;
            this.label46.Text = "Tujuan Penjualan";
            // 
            // cmbJualTujuanLazada
            // 
            this.cmbJualTujuanLazada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTujuanLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTujuanLazada.FormattingEnabled = true;
            this.cmbJualTujuanLazada.Items.AddRange(new object[] {
            "Pesanan Penjualan",
            "Penjualan",
            "Kasir"});
            this.cmbJualTujuanLazada.Location = new System.Drawing.Point(109, 26);
            this.cmbJualTujuanLazada.Name = "cmbJualTujuanLazada";
            this.cmbJualTujuanLazada.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTujuanLazada.TabIndex = 52;
            // 
            // chbAmbilPenjualanLazada
            // 
            this.chbAmbilPenjualanLazada.AutoSize = true;
            this.chbAmbilPenjualanLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAmbilPenjualanLazada.Location = new System.Drawing.Point(9, 0);
            this.chbAmbilPenjualanLazada.Name = "chbAmbilPenjualanLazada";
            this.chbAmbilPenjualanLazada.Size = new System.Drawing.Size(15, 14);
            this.chbAmbilPenjualanLazada.TabIndex = 42;
            this.chbAmbilPenjualanLazada.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(5, 117);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 13);
            this.label34.TabIndex = 79;
            this.label34.Text = "Data Produk";
            // 
            // btnManageLazadaCache
            // 
            this.btnManageLazadaCache.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageLazadaCache.Location = new System.Drawing.Point(123, 112);
            this.btnManageLazadaCache.Name = "btnManageLazadaCache";
            this.btnManageLazadaCache.Size = new System.Drawing.Size(176, 23);
            this.btnManageLazadaCache.TabIndex = 78;
            this.btnManageLazadaCache.Text = "Kelola Manual";
            this.btnManageLazadaCache.UseVisualStyleBackColor = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(6, 144);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(92, 13);
            this.label35.TabIndex = 77;
            this.label35.Text = "Mode Developer";
            // 
            // txtDevLazada
            // 
            this.txtDevLazada.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDevLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevLazada.Location = new System.Drawing.Point(123, 141);
            this.txtDevLazada.Name = "txtDevLazada";
            this.txtDevLazada.Size = new System.Drawing.Size(176, 22);
            this.txtDevLazada.TabIndex = 76;
            this.txtDevLazada.UseSystemPasswordChar = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(5, 88);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(112, 13);
            this.label36.TabIndex = 75;
            this.label36.Text = "Harga Yg Digunakan";
            // 
            // cmbPriceLazada
            // 
            this.cmbPriceLazada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriceLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriceLazada.FormattingEnabled = true;
            this.cmbPriceLazada.Items.AddRange(new object[] {
            "Tanpa Harga",
            "Level 1",
            "Level 2",
            "Level 3",
            "Level 4"});
            this.cmbPriceLazada.Location = new System.Drawing.Point(123, 85);
            this.cmbPriceLazada.Name = "cmbPriceLazada";
            this.cmbPriceLazada.Size = new System.Drawing.Size(176, 21);
            this.cmbPriceLazada.TabIndex = 74;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(6, 60);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(56, 13);
            this.label37.TabIndex = 73;
            this.label37.Text = "Password";
            // 
            // txtPasswordLazada
            // 
            this.txtPasswordLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordLazada.Location = new System.Drawing.Point(123, 57);
            this.txtPasswordLazada.Name = "txtPasswordLazada";
            this.txtPasswordLazada.Size = new System.Drawing.Size(176, 22);
            this.txtPasswordLazada.TabIndex = 72;
            this.txtPasswordLazada.UseSystemPasswordChar = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(6, 32);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(34, 13);
            this.label38.TabIndex = 71;
            this.label38.Text = "Email";
            // 
            // txtEmailLazada
            // 
            this.txtEmailLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailLazada.Location = new System.Drawing.Point(123, 29);
            this.txtEmailLazada.Name = "txtEmailLazada";
            this.txtEmailLazada.Size = new System.Drawing.Size(176, 22);
            this.txtEmailLazada.TabIndex = 70;
            // 
            // chbLazada
            // 
            this.chbLazada.AutoSize = true;
            this.chbLazada.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbLazada.Location = new System.Drawing.Point(8, 6);
            this.chbLazada.Name = "chbLazada";
            this.chbLazada.Size = new System.Drawing.Size(141, 17);
            this.chbLazada.TabIndex = 69;
            this.chbLazada.Text = "Hubungkan Ke Lazada";
            this.chbLazada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chbLazada.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox5);
            this.tabPage8.Controls.Add(this.groupBox9);
            this.tabPage8.Controls.Add(this.label93);
            this.tabPage8.Controls.Add(this.btnManageBlibliCache);
            this.tabPage8.Controls.Add(this.label94);
            this.tabPage8.Controls.Add(this.txtDevBlibli);
            this.tabPage8.Controls.Add(this.label95);
            this.tabPage8.Controls.Add(this.cmbPriceBlibli);
            this.tabPage8.Controls.Add(this.label96);
            this.tabPage8.Controls.Add(this.txtPasswordBlibli);
            this.tabPage8.Controls.Add(this.label97);
            this.tabPage8.Controls.Add(this.txtEmailBlibli);
            this.tabPage8.Controls.Add(this.chbBlibli);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(633, 607);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Blibli";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtProxyPassBlibli);
            this.groupBox5.Controls.Add(this.txtProxyUserBlibli);
            this.groupBox5.Controls.Add(this.label87);
            this.groupBox5.Controls.Add(this.cmbProxyModeBlibli);
            this.groupBox5.Controls.Add(this.txtProxyPortBlibli);
            this.groupBox5.Controls.Add(this.txtProxyAddressBlibli);
            this.groupBox5.Controls.Add(this.label88);
            this.groupBox5.Controls.Add(this.chbProxyBlibli);
            this.groupBox5.Controls.Add(this.label89);
            this.groupBox5.Location = new System.Drawing.Point(7, 169);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(613, 109);
            this.groupBox5.TabIndex = 87;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "       Gunakan Server Proxy";
            // 
            // txtProxyPassBlibli
            // 
            this.txtProxyPassBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyPassBlibli.Location = new System.Drawing.Point(294, 76);
            this.txtProxyPassBlibli.Name = "txtProxyPassBlibli";
            this.txtProxyPassBlibli.Size = new System.Drawing.Size(149, 22);
            this.txtProxyPassBlibli.TabIndex = 67;
            this.txtProxyPassBlibli.UseSystemPasswordChar = true;
            // 
            // txtProxyUserBlibli
            // 
            this.txtProxyUserBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyUserBlibli.Location = new System.Drawing.Point(137, 76);
            this.txtProxyUserBlibli.Name = "txtProxyUserBlibli";
            this.txtProxyUserBlibli.Size = new System.Drawing.Size(151, 22);
            this.txtProxyUserBlibli.TabIndex = 65;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(9, 80);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(122, 13);
            this.label87.TabIndex = 64;
            this.label87.Text = "Username && Password";
            // 
            // cmbProxyModeBlibli
            // 
            this.cmbProxyModeBlibli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProxyModeBlibli.FormattingEnabled = true;
            this.cmbProxyModeBlibli.Items.AddRange(new object[] {
            "Otomatis (Proxy ipos2woo)",
            "Manual (Proxy pribadi)"});
            this.cmbProxyModeBlibli.Location = new System.Drawing.Point(137, 21);
            this.cmbProxyModeBlibli.Name = "cmbProxyModeBlibli";
            this.cmbProxyModeBlibli.Size = new System.Drawing.Size(218, 21);
            this.cmbProxyModeBlibli.TabIndex = 63;
            // 
            // txtProxyPortBlibli
            // 
            this.txtProxyPortBlibli.Location = new System.Drawing.Point(294, 48);
            this.txtProxyPortBlibli.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtProxyPortBlibli.Name = "txtProxyPortBlibli";
            this.txtProxyPortBlibli.Size = new System.Drawing.Size(61, 22);
            this.txtProxyPortBlibli.TabIndex = 62;
            // 
            // txtProxyAddressBlibli
            // 
            this.txtProxyAddressBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProxyAddressBlibli.Location = new System.Drawing.Point(137, 48);
            this.txtProxyAddressBlibli.Name = "txtProxyAddressBlibli";
            this.txtProxyAddressBlibli.Size = new System.Drawing.Size(151, 22);
            this.txtProxyAddressBlibli.TabIndex = 61;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(9, 24);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(37, 13);
            this.label88.TabIndex = 60;
            this.label88.Text = "Mode";
            // 
            // chbProxyBlibli
            // 
            this.chbProxyBlibli.AutoSize = true;
            this.chbProxyBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbProxyBlibli.Location = new System.Drawing.Point(10, 0);
            this.chbProxyBlibli.Name = "chbProxyBlibli";
            this.chbProxyBlibli.Size = new System.Drawing.Size(15, 14);
            this.chbProxyBlibli.TabIndex = 43;
            this.chbProxyBlibli.UseVisualStyleBackColor = true;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(9, 51);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(78, 13);
            this.label89.TabIndex = 55;
            this.label89.Text = "Alamat && Port";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label91);
            this.groupBox9.Controls.Add(this.cmbJualTipeBayarBlibli);
            this.groupBox9.Controls.Add(this.label92);
            this.groupBox9.Controls.Add(this.cmbJualTujuanBlibli);
            this.groupBox9.Controls.Add(this.chbAmbilPenjualanBlibli);
            this.groupBox9.Location = new System.Drawing.Point(7, 284);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(244, 88);
            this.groupBox9.TabIndex = 84;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "      Ambil Data Penjualan ";
            this.groupBox9.Visible = false;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(7, 56);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(94, 13);
            this.label91.TabIndex = 55;
            this.label91.Text = "Tipe Pembayaran";
            // 
            // cmbJualTipeBayarBlibli
            // 
            this.cmbJualTipeBayarBlibli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTipeBayarBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTipeBayarBlibli.FormattingEnabled = true;
            this.cmbJualTipeBayarBlibli.Items.AddRange(new object[] {
            "Kredit",
            "Tunai"});
            this.cmbJualTipeBayarBlibli.Location = new System.Drawing.Point(109, 53);
            this.cmbJualTipeBayarBlibli.Name = "cmbJualTipeBayarBlibli";
            this.cmbJualTipeBayarBlibli.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTipeBayarBlibli.TabIndex = 54;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(7, 29);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(97, 13);
            this.label92.TabIndex = 53;
            this.label92.Text = "Tujuan Penjualan";
            // 
            // cmbJualTujuanBlibli
            // 
            this.cmbJualTujuanBlibli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbJualTujuanBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbJualTujuanBlibli.FormattingEnabled = true;
            this.cmbJualTujuanBlibli.Items.AddRange(new object[] {
            "Pesanan Penjualan",
            "Penjualan",
            "Kasir"});
            this.cmbJualTujuanBlibli.Location = new System.Drawing.Point(109, 26);
            this.cmbJualTujuanBlibli.Name = "cmbJualTujuanBlibli";
            this.cmbJualTujuanBlibli.Size = new System.Drawing.Size(124, 21);
            this.cmbJualTujuanBlibli.TabIndex = 52;
            // 
            // chbAmbilPenjualanBlibli
            // 
            this.chbAmbilPenjualanBlibli.AutoSize = true;
            this.chbAmbilPenjualanBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAmbilPenjualanBlibli.Location = new System.Drawing.Point(9, 0);
            this.chbAmbilPenjualanBlibli.Name = "chbAmbilPenjualanBlibli";
            this.chbAmbilPenjualanBlibli.Size = new System.Drawing.Size(15, 14);
            this.chbAmbilPenjualanBlibli.TabIndex = 42;
            this.chbAmbilPenjualanBlibli.UseVisualStyleBackColor = true;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(3, 117);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(71, 13);
            this.label93.TabIndex = 83;
            this.label93.Text = "Data Produk";
            // 
            // btnManageBlibliCache
            // 
            this.btnManageBlibliCache.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManageBlibliCache.Location = new System.Drawing.Point(121, 112);
            this.btnManageBlibliCache.Name = "btnManageBlibliCache";
            this.btnManageBlibliCache.Size = new System.Drawing.Size(176, 23);
            this.btnManageBlibliCache.TabIndex = 82;
            this.btnManageBlibliCache.Text = "Kelola Manual";
            this.btnManageBlibliCache.UseVisualStyleBackColor = true;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Red;
            this.label94.Location = new System.Drawing.Point(4, 144);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(92, 13);
            this.label94.TabIndex = 81;
            this.label94.Text = "Mode Developer";
            // 
            // txtDevBlibli
            // 
            this.txtDevBlibli.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDevBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevBlibli.Location = new System.Drawing.Point(121, 141);
            this.txtDevBlibli.Name = "txtDevBlibli";
            this.txtDevBlibli.Size = new System.Drawing.Size(176, 22);
            this.txtDevBlibli.TabIndex = 80;
            this.txtDevBlibli.UseSystemPasswordChar = true;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(3, 88);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(112, 13);
            this.label95.TabIndex = 79;
            this.label95.Text = "Harga Yg Digunakan";
            // 
            // cmbPriceBlibli
            // 
            this.cmbPriceBlibli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriceBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriceBlibli.FormattingEnabled = true;
            this.cmbPriceBlibli.Items.AddRange(new object[] {
            "Tanpa Harga",
            "Level 1",
            "Level 2",
            "Level 3",
            "Level 4"});
            this.cmbPriceBlibli.Location = new System.Drawing.Point(121, 85);
            this.cmbPriceBlibli.Name = "cmbPriceBlibli";
            this.cmbPriceBlibli.Size = new System.Drawing.Size(176, 21);
            this.cmbPriceBlibli.TabIndex = 78;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(4, 60);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(56, 13);
            this.label96.TabIndex = 77;
            this.label96.Text = "Password";
            // 
            // txtPasswordBlibli
            // 
            this.txtPasswordBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordBlibli.Location = new System.Drawing.Point(121, 57);
            this.txtPasswordBlibli.Name = "txtPasswordBlibli";
            this.txtPasswordBlibli.Size = new System.Drawing.Size(176, 22);
            this.txtPasswordBlibli.TabIndex = 76;
            this.txtPasswordBlibli.UseSystemPasswordChar = true;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(4, 32);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(34, 13);
            this.label97.TabIndex = 75;
            this.label97.Text = "Email";
            // 
            // txtEmailBlibli
            // 
            this.txtEmailBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailBlibli.Location = new System.Drawing.Point(121, 29);
            this.txtEmailBlibli.Name = "txtEmailBlibli";
            this.txtEmailBlibli.Size = new System.Drawing.Size(176, 22);
            this.txtEmailBlibli.TabIndex = 74;
            // 
            // chbBlibli
            // 
            this.chbBlibli.AutoSize = true;
            this.chbBlibli.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbBlibli.Location = new System.Drawing.Point(6, 6);
            this.chbBlibli.Name = "chbBlibli";
            this.chbBlibli.Size = new System.Drawing.Size(131, 17);
            this.chbBlibli.TabIndex = 73;
            this.chbBlibli.Text = "Hubungkan Ke Blibli";
            this.chbBlibli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chbBlibli.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.btnConnect);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 594);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(652, 41);
            this.panel1.TabIndex = 49;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(105, 8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "Batal";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(10, 8);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(89, 23);
            this.btnConnect.TabIndex = 9;
            this.btnConnect.Text = "Simpan";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // cmbEngineShopee
            // 
            this.cmbEngineShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEngineShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEngineShopee.FormattingEnabled = true;
            this.cmbEngineShopee.Items.AddRange(new object[] {
            "Chromium (Default)",
            "Google Chrome"});
            this.cmbEngineShopee.Location = new System.Drawing.Point(365, 30);
            this.cmbEngineShopee.Name = "cmbEngineShopee";
            this.cmbEngineShopee.Size = new System.Drawing.Size(155, 21);
            this.cmbEngineShopee.TabIndex = 65;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Red;
            this.label99.Location = new System.Drawing.Point(315, 33);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(38, 13);
            this.label99.TabIndex = 66;
            this.label99.Text = "Mesin";
            // 
            // cmbProfileShopee
            // 
            this.cmbProfileShopee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProfileShopee.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProfileShopee.FormattingEnabled = true;
            this.cmbProfileShopee.Items.AddRange(new object[] {
            "Spesifik (Default)",
            "Global (Hanya bisa 1 akun)"});
            this.cmbProfileShopee.Location = new System.Drawing.Point(365, 59);
            this.cmbProfileShopee.Name = "cmbProfileShopee";
            this.cmbProfileShopee.Size = new System.Drawing.Size(155, 21);
            this.cmbProfileShopee.TabIndex = 67;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Red;
            this.label100.Location = new System.Drawing.Point(315, 62);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(34, 13);
            this.label100.TabIndex = 68;
            this.label100.Text = "Profil";
            // 
            // frmProfileConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 635);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtWooSecret);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtWooKey);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnImportCustom1);
            this.Controls.Add(this.btnEnableStock);
            this.Controls.Add(this.btnDisableRolePrice);
            this.Controls.Add(this.btnEnableRolePrice);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmProfileConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Profile Configuration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProfileConfig_FormClosing);
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPort)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.gbProxyTokped.ResumeLayout(false);
            this.gbProxyTokped.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortTokped)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gbJualFromIposTokped.ResumeLayout(false);
            this.gbJualFromIposTokped.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.gbProxyShopee.ResumeLayout(false);
            this.gbProxyShopee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortShopee)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortBukalapak)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortLazada)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtProxyPortBlibli)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnEnableStock;
        private System.Windows.Forms.Button btnDisableRolePrice;
        private System.Windows.Forms.Button btnEnableRolePrice;
        private System.Windows.Forms.Button btnImportCustom1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtWooSecret;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtWooKey;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox cbMGetCustomer;
        private System.Windows.Forms.CheckBox cbMSendProduct;
        private System.Windows.Forms.CheckBox cbMGetSales;
        private System.Windows.Forms.CheckBox cbMSendCustomer;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDevMain;
        private System.Windows.Forms.CheckBox cbAutoStart;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label Tipe;
        private System.Windows.Forms.ComboBox cbDbType;
        private System.Windows.Forms.NumericUpDown txtPort;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbDatabase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHost;
        private System.Windows.Forms.TextBox txtLevel4;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox chbWooSendPublish;
        private System.Windows.Forms.CheckBox chbSyncRakToWeight;
        private System.Windows.Forms.CheckBox chbSyncIposToDraft;
        private System.Windows.Forms.TextBox txtLevel3;
        private System.Windows.Forms.TextBox txtLevel2;
        private System.Windows.Forms.TextBox txtLevel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbPriceType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtWooPassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtWooUsername;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWooURL;
        private System.Windows.Forms.CheckBox chbWoocommerce;
        private System.Windows.Forms.Button btnManageTokpedCache;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDevTokped;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbPriceTokped;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtPasswordTokped;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtEmailTokped;
        private System.Windows.Forms.CheckBox chbTokped;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtDevShopee;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPasswordShopee;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtEmailShopee;
        private System.Windows.Forms.CheckBox chbShopee;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.MaskedTextBox txtCheckIDScheduleTokped;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btnManageBukalapakCache;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtDevBukalapak;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cmbPriceBukalapak;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtPasswordBukalapak;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtEmailBukalapak;
        private System.Windows.Forms.CheckBox chbBukalapak;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnManageLazadaCache;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtDevLazada;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cmbPriceLazada;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtPasswordLazada;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtEmailLazada;
        private System.Windows.Forms.CheckBox chbLazada;
        private System.Windows.Forms.GroupBox gbJualFromIposTokped;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cmbJualTujuanTokped;
        private System.Windows.Forms.CheckBox chbAmbilPenjualanTokped;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox cmbJualTipeBayarTokped;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbJualTipeBayarShopee;
        private System.Windows.Forms.ComboBox cmbJualTujuanShopee;
        private System.Windows.Forms.CheckBox chbAmbilPenjualanShopee;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cmbJualTipeBayarBukalapak;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cmbJualTujuanBukalapak;
        private System.Windows.Forms.CheckBox chbAmbilPenjualanBukalapak;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox cmbJualTipeBayarLazada;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cmbJualTujuanLazada;
        private System.Windows.Forms.CheckBox chbAmbilPenjualanLazada;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtJualPelangganTokped;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtJualGudangTokped;
        private System.Windows.Forms.TextBox txtJualSalesTokped;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button btnJualHapusTokped;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.ComboBox cmbJualTabTokped;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ComboBox cmbJualTabShopee;
        private System.Windows.Forms.Button btnJualHapusShopee;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtJualGudangShopee;
        private System.Windows.Forms.TextBox txtJualSalesShopee;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtJualPelangganShopee;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtJualPrefixNomorShopee;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtJualPrefixNomorTokped;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cmbStokTokped;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.CheckBox chbSyncTokped;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnManageShopeeCache;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmbPriceShopee;
        private System.Windows.Forms.CheckBox chbSyncShopee;
        private System.Windows.Forms.ComboBox cmbStokShopee;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtJualUserIDTokped;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtJualUserIDShopee;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox cmbBrowserBukalapak;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ComboBox cmbWooPermalink;
        private System.Windows.Forms.GroupBox gbProxyTokped;
        private System.Windows.Forms.ComboBox cmbProxyModeTokped;
        private System.Windows.Forms.NumericUpDown txtProxyPortTokped;
        private System.Windows.Forms.TextBox txtProxyAddressTokped;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.CheckBox chbProxyTokped;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtProxyPassTokped;
        private System.Windows.Forms.TextBox txtProxyUserTokped;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox gbProxyShopee;
        private System.Windows.Forms.TextBox txtProxyPassShopee;
        private System.Windows.Forms.TextBox txtProxyUserShopee;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox cmbProxyModeShopee;
        private System.Windows.Forms.NumericUpDown txtProxyPortShopee;
        private System.Windows.Forms.TextBox txtProxyAddressShopee;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.CheckBox chbProxyShopee;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtProxyPassBukalapak;
        private System.Windows.Forms.TextBox txtProxyUserBukalapak;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.ComboBox cmbProxyModeBukalapak;
        private System.Windows.Forms.NumericUpDown txtProxyPortBukalapak;
        private System.Windows.Forms.TextBox txtProxyAddressBukalapak;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.CheckBox chbProxyBukalapak;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtProxyPassLazada;
        private System.Windows.Forms.TextBox txtProxyUserLazada;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.ComboBox cmbProxyModeLazada;
        private System.Windows.Forms.NumericUpDown txtProxyPortLazada;
        private System.Windows.Forms.TextBox txtProxyAddressLazada;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.CheckBox chbProxyLazada;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.ComboBox cmbJualStokMinusShopee;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ComboBox cmbJualStokMinusTokped;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.ComboBox cmbJualBAdminShopee;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox cmbJualOngkirShopee;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.ComboBox cmbJualAkunBLainShopee;
        private System.Windows.Forms.ComboBox cmbJualAkunPotShopee;
        private System.Windows.Forms.ComboBox cmbJualAkunKreditShopee;
        private System.Windows.Forms.ComboBox cmbJualAkunTunaiShopee;
        private System.Windows.Forms.ComboBox cmbJualAkunBLainTokped;
        private System.Windows.Forms.ComboBox cmbJualAkunPotTokped;
        private System.Windows.Forms.ComboBox cmbJualAkunKreditTokped;
        private System.Windows.Forms.ComboBox cmbJualAkunTunaiTokped;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtProxyPassBlibli;
        private System.Windows.Forms.TextBox txtProxyUserBlibli;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.ComboBox cmbProxyModeBlibli;
        private System.Windows.Forms.NumericUpDown txtProxyPortBlibli;
        private System.Windows.Forms.TextBox txtProxyAddressBlibli;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox chbProxyBlibli;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.ComboBox cmbJualTipeBayarBlibli;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.ComboBox cmbJualTujuanBlibli;
        private System.Windows.Forms.CheckBox chbAmbilPenjualanBlibli;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Button btnManageBlibliCache;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox txtDevBlibli;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.ComboBox cmbPriceBlibli;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox txtPasswordBlibli;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox txtEmailBlibli;
        private System.Windows.Forms.CheckBox chbBlibli;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.ComboBox cmbWooRestMethod;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.ComboBox cmbPrimaryKey;
        private System.Windows.Forms.ComboBox cmbEngineShopee;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.ComboBox cmbProfileShopee;
        private System.Windows.Forms.Label label100;
    }
}