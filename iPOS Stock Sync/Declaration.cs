﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace iPOS_Stock_Sync
{
    [Serializable]
    public enum Marketplace
    {
        Tokopedia,
        Shopee,
        Bukalapak,
        Lazada,
        Blibli,
    }

    [Serializable]
    public class WarehouseStock
    {
        public string name;
        public float stock;
    }

    [Serializable]
    public class ProductUnit
    {
        public string name;
        public decimal conversion;
        public string barcode;
        public decimal cost;
        public Dictionary<string, ProductPrice> prices;
    }

    [Serializable]
    public class ProductPrice
    {
        public int level;
        public long price;
    }

    [Serializable]
    public class SyncedStatus
    {
        public bool is_woo;
    }

    [Serializable]
    public class Product : ICloneable
    {
        public string id;
        
        public string code = "";
        public string name = "";
        public float weight = 0;
        public string description = "";


        public float stock = -1;
        public Dictionary<String, WarehouseStock> stockdetail = new Dictionary<string, WarehouseStock>();
       
        public long price_main; 
        public long price1 = -1;
        public long price2 = -1;
        public long price3 = -1;
        public long price4 = -1;
        
        public string publish_date = "";

        public string unit = "";
        public Boolean active = true;
        public Boolean interacted = false;

        public Dictionary<String, ProductUnit> units = new Dictionary<string, ProductUnit>();
   
        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }

    [Serializable]
    public class SingleProduct : ICloneable
    {
        public string sku = "";
        public string id = "";
        public string name = "";
        public float weight = 0;
        public string description = "";
        public float stock = -1;
        public Dictionary<String, WarehouseStock> stockdetail = new Dictionary<string, WarehouseStock>();
        public decimal cost = -1;
        public long price1 = -1;
        public long price2 = -1;
        public long price3 = -1;
        public long price4 = -1;
        public string publish_date = "";
        public string unit = "";
        public Boolean active = true;
        public Boolean interacted = false;

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }

    [Serializable]
    public class Customer : ICloneable
    {
        public string id;
        public string code;
        public string name;
        public string address;
        public int id_monitoring;

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }

    [Serializable]
    public class Sales : ICloneable
    {
        public string id;
        public string code;
        

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }

    public class TokopediaProduct
    {
        public string id_ipos;
        public string sku;
        public int level_index;
        public long level_price;
        public float stock;
    }

    public class BlibliProduct
    {
        public string id_ipos;
        public string sku;
        public int level_index;
        public long level_price;
        public float stock;
    }

    public class IPOSSalesOrder
    {
        public string number;
        public string buyer;
        public string address;
        public DateTime date;
        public string products;
        public string shipinfo;
        public long otherfee;
        public decimal cut_pct;
        public long cut_value;
        public long total;
        
    }

    public class IPOSOptionSalesOrder
    {
        public string warehouse;
        public string customer;
        public string seller;
        public string userid = "IPOS2WOO";
        public int iposver = 5;
    }

    public class IPOSOfficeSale
    {
        public string number;
        public string buyer;
        public string address;
        public DateTime date;
        public string products;
        public string shipinfo;
        public long otherfee;
        public decimal cut_pct;
        public long cut_value;
        public long total;
        
    }

    public class IPOSOptionOfficeSale
    {
        public string warehouse;
        public string customer;
        public string seller;
        public int paymethod;
        public string acc_tunai;
        public string acc_kredit;
        public string acc_pot;
        public string acc_blain;
        public string userid = "IPOS2WOO";
        public int iposver = 5;
    }

    public class IPOSCashierSale
    {
        public string number;
        public string buyer;
        public string address;
        public DateTime date;
        public string products;
        public string shipinfo;
        public long otherfee;
        public decimal cut_pct;
        public long cut_value;
        public long total;
        public int iposver = 5;
    }


    public class IPOSOptionCashierSale
    {
        public string warehouse;
        public string customer;
        public string seller;
        public int paymethod;
        public string acc_tunai;
        public string acc_kredit;
        public string acc_pot;
        public string acc_blain;
        public string userid = "IPOS2WOO";
        public int iposver = 5;
    }


    public class TokopediaSale
    {
        public string invoice;
        public int type;
        public string buyer;
        public string address;
        public DateTime date;
        public string products;
        public string status;
        public string shipname;
        public string shiptrack;
        public long subtotal;
        public long shipfee;
        public long total;
        public string link;
        public bool is_pass = false;
    }

    public class BlibliSale
    {
        public string invoice;
        public int type;
        public string buyer;
        public string address;
        public DateTime date;
        public string products;
        public string status;
        public string shipname;
        public string shiptrack;
        public long subtotal;
        public long shipfee;
        public long total;
        public string link;
        public bool is_pass = false;
    }

    public class ShopeeSale
    {
        public string invoice;
        public int type;
        public string buyer;
        public string address;
        public DateTime date;
        public string products;
        public string status;
        public string shipname;
        public string shiptrack;
        public long subtotal;
        public long shipfee;
        public long adminfee;
        public long total;
        public string link;
        public bool is_pass = false;
    }


    public class ShopeeProduct
    {
        public string id_ipos;
        public string sku;
        public int level_index;
        public long level_price;
        public float stock;
    }

    public class BukalapakProduct
    {
        public string id_ipos;
        public string sku;
        public int level_index;
        public long level_price;
        public float stock;
    }

    public class LazadaProduct
    {
        public string id_ipos;
        public string sku;
        public int level_index;
        public long level_price;
        public float stock;
    }

    public class MonitoredProduct
    {
        public string id;
        public Nullable<DateTime> woocommerce_at;
        public string woocommerce_publish_at;
        public string sku;
        public string name;
        public string prices;
        public float weight;
        public string description;
        public float stock;
        public string stockdetail;
        public string unit;

        public int tokopedia_sync = 0;
        public string tokopedia_id = "0";
        public int tokopedia_status;
        public long tokopedia_price;
        public float tokopedia_stock;

        public int shopee_sync = 0;
        public string shopee_id = "0";
        public int shopee_status;
        public long shopee_price;
        public float shopee_stock;

        public int bukalapak_sync = 0;
        public string bukalapak_id = "0";
        public int bukalapak_status;
        public long bukalapak_price;
        public float bukalapak_stock;

        public int lazada_sync = 0;
        public string lazada_id = "0";
        public int lazada_status;
        public long lazada_price;
        public float lazada_stock;

        public int blibli_sync = 0;
        public string blibli_id = "0";
        public int blibli_status;
        public long blibli_price;
        public float blibli_stock;
    }

    public class MonitoredCustomer
    {
        public string id;
        public string code;
        public string name;
        public string address;
        public int id_monitoring;
    }

    public class MonitoredSales
    {
        public string id;
        public string code;
    }

    [System.Serializable]
    public class ItemConfig
    {
        public string sku;
        public string warehouse;
        public bool ignored;
    }

    [Serializable]
    public struct KeyValuePair<K, ItemConfig>
    {
        private string text;
        private Color color;

        public KeyValuePair(string text, Color color) : this()
        {
            this.text = text;
            this.color = color;
        }

        public K Key { get; set; }
        public ItemConfig Value { get; set; }
    }


    [System.Serializable]
    public class AppConfig
    {
        public string email = "";
        public string password = "";
        public string monitorURL = "https://app.ipos2woo.com";
        public string monitorToken = "";
        public string branchID = "";
        public bool startup = false;
        public bool autohide = false;
        public List<ProfileConfig> profiles = new List<ProfileConfig>();
        public string version = "";
    }

    [System.Serializable]
    public class ProfileConfig
    {
        public int id = 0;
        public string name = "";
        public bool active = true;
        public string main_dev = "";
        public bool autostart = false;
        public bool main_get_customer = false;
        public bool main_send_customer = false;
        public bool main_get_sales = false;
        public bool main_send_product = true;
        public string chrome_profile = "";
        public string warehouse = "";
        public int interval = 60;

        public int db_type = -1;
        public string host = "localhost";
        public string username = "postgres";
        public string password;
        public string database = "postgres";
        public int port = 5444;
        public int primary_key = 0; //PK_CODE = 0 //PK_BARCODE = 1
       
        public bool woo_on = false;
        public string woo_url = "";
        public int woo_permalink = 0;
        public string woo_key = "";
        public string woo_secret = "";
        public string woo_username = "";
        public string woo_password = "";
        public int woo_price = 1;
        public string woo_role1 = "";
        public string woo_role2 = "";
        public string woo_role3 = "";
        public string woo_role4 = "";
        public bool woo_to_draft = false;
        public bool woo_sync_weight = false;
        public bool woo_send_publish = false;
        public int woo_rest_method = 0;

        public bool tokped_on = false;
        public string tokped_email = "";
        public string tokped_password = "";
        public bool tokped_sync_on = true;
        public int tokped_stock = 1;
        public int tokped_price = 0;
        public string tokped_dev = "";
        public string tokped_checkid_schedule = "";
        public bool tokped_jual_on = false;
        public int tokped_jual_tujuan = 0;
        public int tokped_jual_tipebayar = 0;
        public int tokped_jual_stokminus = 0;
        public string tokped_sale_prefix = "TP-";
        public string tokped_sale_tab = "confirm_shipping";
        public string tokped_sale_custcode = "UMUM";
        public string tokped_sale_warehousecode = "UTM";
        public string tokped_sale_sellercode = "";
        public string tokped_sale_acctunai = "1-1110";
        public string tokped_sale_acckredit = "1-1210";
        public string tokped_sale_accpot = "4-1500";
        public string tokped_sale_accblain = "4-1700";
        public string tokped_sale_userid = "IPOS2WOO";
        public bool tokped_proxy_enabled = false;
        public int tokped_proxy_mode = 1;
        public string tokped_proxy_address = "";
        public string tokped_proxy_port = "";
        public string tokped_proxy_username = "";
        public string tokped_proxy_password = "";

        public bool shopee_on = false;
        public int shopee_profile = 0;
        public int shopee_engine = 0;
        public string shopee_email = "";
        public string shopee_password = "";
        public bool shopee_sync_on = true;
        public int shopee_stock = 1;
        public int shopee_price = 0;
        public string shopee_dev = "";
        public bool shopee_jual_on = false;
        public int shopee_jual_tujuan = 0;
        public int shopee_jual_tipebayar = 0;
        public int shopee_jual_stokminus = 0;
        public int shopee_jual_ongkir = 0;
        public int shopee_jual_badmin = 0;
        public string shopee_sale_prefix = "SP-";
        public string shopee_sale_tab = "toship";
        public string shopee_sale_custcode = "UMUM";
        public string shopee_sale_warehousecode = "";
        public string shopee_sale_sellercode = "";
        public string shopee_sale_acctunai = "1-1110";
        public string shopee_sale_acckredit = "1-1210";
        public string shopee_sale_accpot = "4-1500";
        public string shopee_sale_accblain = "4-1700";
        public string shopee_sale_userid = "IPOS2WOO";
        public bool shopee_proxy_enabled = false;
        public int shopee_proxy_mode = 1;
        public string shopee_proxy_address = "";
        public string shopee_proxy_port = "";
        public string shopee_proxy_username = "";
        public string shopee_proxy_password = "";

        public bool bukalapak_on = false;
        public string bukalapak_email = "";
        public string bukalapak_password = "";
        public bool bukalapak_sync_on = true;
        public int bukalapak_stock = 0;
        public int bukalapak_price = 0;
        public string bukalapak_dev = "";
        public bool bukalapak_jual_on = false;
        public int bukalapak_jual_tujuan = 0;
        public int bukalapak_jual_tipebayar = 0;
        public int bukalapak_browser = 0;
        public bool bukalapak_proxy_enabled = false;
        public int bukalapak_proxy_mode = 1;
        public string bukalapak_proxy_address = "";
        public string bukalapak_proxy_port = "";
        public string bukalapak_proxy_username = "";
        public string bukalapak_proxy_password = "";


        public bool lazada_on = false;
        public string lazada_email = "";
        public string lazada_password = "";
        public bool lazada_sync_on = true;
        public int lazada_stock = 0;
        public int lazada_price = 0;
        public string lazada_dev = "";
        public bool lazada_jual_on = false;
        public int lazada_jual_tujuan = 0;
        public int lazada_jual_tipebayar = 0;
        public bool lazada_proxy_enabled = false;
        public int lazada_proxy_mode = 1;
        public string lazada_proxy_address = "";
        public string lazada_proxy_port = "";
        public string lazada_proxy_username = "";
        public string lazada_proxy_password = "";

        public bool blibli_on = false;
        public string blibli_email = "";
        public string blibli_password = "";
        public bool blibli_sync_on = true;
        public int blibli_stock = 0;
        public int blibli_price = 0;
        public string blibli_dev = "";
        public bool blibli_jual_on = false;
        public int blibli_jual_tujuan = 0;
        public int blibli_jual_tipebayar = 0;
        public int blibli_browser = 0;
        public bool blibli_proxy_enabled = false;
        public int blibli_proxy_mode = 1;
        public string blibli_proxy_address = "";
        public string blibli_proxy_port = "";
        public string blibli_proxy_username = "";
        public string blibli_proxy_password = "";

        public KeyValuePair<string, ItemConfig> itemConfig = new KeyValuePair<string, ItemConfig>();
    }

    public class ProfileConst
    {
        public const int PK_CODE = 0;
        public const int PK_BARCODE = 1;
    }
    

    [System.Serializable]
    public class WooCache
    {
        public string lastUrl;
        public DateTime lastupdate;
        public Dictionary<string, string> list_queue = new Dictionary<string, string>();
        public Dictionary<string, string> list_sku = new Dictionary<string, string>(); //addnya ada di iposcore
        public Dictionary<string, string> list_id = new Dictionary<string, string>();
    }

    [System.Serializable]
    public class LastProductCache
    {
        public DateTime lastupdate;
        public Dictionary<string, SingleProduct> data = new Dictionary<string, SingleProduct>();
    }

    [System.Serializable]
    public class LastCustomerCache
    {
        public DateTime lastupdate;
        public Dictionary<string, Customer> data = new Dictionary<string, Customer>();
    }

    [System.Serializable]
    public class LastSalesCache
    {
        public DateTime lastupdate;
        public Dictionary<string, Sales> data = new Dictionary<string, Sales>();
    }

    [System.Serializable]
    public class CacheTokopediaProduct
    {
        public DateTime lastupdate;
        public Dictionary<string, string> marketID = new Dictionary<string, string>();
        public Dictionary<string, TokopediaProduct> products = new Dictionary<string, TokopediaProduct>();
        public Dictionary<string, TokopediaSale> sales = new Dictionary<string, TokopediaSale>();
    }

    [System.Serializable]
    public class CacheShopeeProduct
    {
        public DateTime lastupdate;
        public Dictionary<string, string> marketID = new Dictionary<string, string>();
        public Dictionary<string, ShopeeProduct> products = new Dictionary<string, ShopeeProduct>();
        public Dictionary<string, ShopeeSale> sales = new Dictionary<string, ShopeeSale>();
    }

    [System.Serializable]
    public class CacheBukalapakProduct
    {
        public DateTime lastupdate;
        public Dictionary<string, string> marketID = new Dictionary<string, string>();
        public Dictionary<string, BukalapakProduct> products = new Dictionary<string, BukalapakProduct>();
    }

    [System.Serializable]
    public class CacheLazadaProduct
    {
        public DateTime lastupdate;
        public Dictionary<string, string> marketID = new Dictionary<string, string>();
        public Dictionary<string, LazadaProduct> products = new Dictionary<string, LazadaProduct>();
    }

    [System.Serializable]
    public class CacheBlibliProduct
    {
        public DateTime lastupdate;
        public Dictionary<string, string> marketID = new Dictionary<string, string>();
        public Dictionary<string, BlibliProduct> products = new Dictionary<string, BlibliProduct>();
    }

}
