﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iPOS_Stock_Sync
{
    public partial class ucMarketplaceLog : UserControl
    {
        List<Tuple<string, Color>> logQueue = new List<Tuple<string, Color>>();
        public bool isClearLog = false;
        public bool isWorking = false;
        public int timeoutSecs = 0;
        public int maxSecs = 300;

        public event EventHandler SyncCacheClick;
        public event EventHandler SyncSalesMonitoringClick;
        public event EventHandler ImportSalesClick;
        public event EventHandler WorkTimeout;

        public ucMarketplaceLog()
        {
            InitializeComponent();
        }


        // public property
        public Boolean CheckID
        {
            get
            {
               return chbCheckID.Checked;
            }
            set
            {
                chbCheckID.Checked = value;
            }
        }

        public Boolean SyncMarketplace
        {
            get
            {
                return chbSyncMarketplace.Checked;
            }
            set
            {
                chbSyncMarketplace.Checked = value;
            }
        }

        public Boolean SendMonitoring
        {
            get
            {
                return chbSendMonitoring.Checked;
            }
            set
            {
                chbSendMonitoring.Checked = value;
            }
        }

        private void txtLog_TextChanged(object sender, EventArgs e)
        {

        }

        void AppendLogToControl(string text, Color color, bool addNewLine = false)
        {
            txtLog.SuspendLayout();
            txtLog.SelectionColor = color;
            txtLog.AppendText(addNewLine
                ? $"{text}{Environment.NewLine}"
                : text);
            txtLog.ScrollToCaret();
            txtLog.ResumeLayout();
            timeoutSecs = 0;
        }

        public void Log(string text, Color? color = null)
        {
            logQueue.Add(new Tuple<string, Color>(text, color ?? Color.White));
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                
                if (isClearLog)
                {
                    txtLog.Clear();
                    isClearLog = false;
                }
                if (logQueue.Count > 0)
                {
                    if (txtLog.Lines != null && txtLog.Lines.LongLength > 1000)
                    {
                        txtLog.Clear();
                    }
                    while (logQueue.Count > 0)
                    {
                        AppendLogToControl(logQueue[0].Item1, logQueue[0].Item2, true);
                        logQueue.RemoveAt(0);
                    }
                }
                
                if (isWorking)
                {
                    timeoutSecs++;
                    if (timeoutSecs >= maxSecs)
                    {
                        timeoutSecs = 0;
                        //WorkTimeout?.Invoke(this, e);
                    }
                }
                else
                {
                    timeoutSecs = 0;
                }
                
            }
            catch (Exception ex)
            {
                AppendLogToControl(ex.Message, Color.Red, true);
            }
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var pos = txtLog.Find(txtSearch.Text);
                if (pos > -1)
                {
                    txtLog.Focus();
                    txtLog.SelectionStart = txtLog.Find(txtSearch.Text);
                    txtLog.ScrollToCaret();
                }
            }
        }

        public void ClearLog()
        {
            txtLog.Clear();
        }

        private void samakanDataProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SyncCacheClick?.Invoke(this, e);
        }

        private void btnSyncMore_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        private void txtLog_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            timeoutSecs += 1000000;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void kirimDataPenjualanTerakhirKeMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SyncSalesMonitoringClick?.Invoke(this, e);
        }

        private void imporDataPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportSalesClick?.Invoke(this, e);
        }
    }
}
