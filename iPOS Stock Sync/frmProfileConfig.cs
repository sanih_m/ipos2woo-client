﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Npgsql;
using SimpleJSON;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Win32;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace iPOS_Stock_Sync
{
    public partial class frmProfileConfig : Form
    {
        int idxProfile = -1;
        bool isNew = false;

        List<String> chromeProfiles = new List<string>();
        List<String> tokopediaOrderTabs = new List<string>(new string[] { "confirm_shipping","new_order","all_order" });
        List<String> shopeeOrderTabs = new List<string>(new string[] { "toship-processed", "shipping", "completed", "unpaid", "", "toship-to_process", "toship" });

        public frmProfileConfig(int idx)
        {
            if(idx == -1)
            {
                var id = 0;
                if (Program.config.profiles.Count > 0)
                {
                    id = Program.config.profiles[Program.config.profiles.Count - 1].id + 1;
                }
                else
                {
                    id = 1;
                }
                Program.config.profiles.Add(new ProfileConfig());
                idx = Program.config.profiles.Count - 1;
                Program.config.profiles[idx].id = id;
                
                isNew = true;
            }
            else
            {
                isNew = false;
            }
            idxProfile = idx;
            InitializeComponent();
           
        }

        //void loadChromeProfiles()
        //{
        //    chromeProfiles.Clear();
        //    var chrome = Path.Combine(Environment.GetFolderPath(
        //    Environment.SpecialFolder.LocalApplicationData), "Google\\Chrome\\User Data");
        //    cmbChromeProfile.Items.Clear();
        //    cmbChromeProfile.Items.Add("Default (Incognito)");
        //    if (Directory.Exists(chrome))
        //    {
        //        var obj = JObject.Parse(File.ReadAllText(chrome + "\\Local State"));
        //        if (obj != null)
        //        {
        //            var arr = obj["profile"]["info_cache"];
        //            var profiles = arr.ToObject<Dictionary<string, JObject>>();
        //            foreach (System.Collections.Generic.KeyValuePair<string, JObject> item in profiles)
        //            {
        //                chromeProfiles.Add(item.Key);
        //                cmbChromeProfile.Items.Add(item.Value["shortcut_name"]);
        //            }
        //        }
        //    }
        //}
        
        private void frmConfig_Load(object sender, EventArgs e)
        {

            //loadChromeProfiles();

            txtDevMain.Text = Program.config.profiles[idxProfile].main_dev;
            cbAutoStart.Checked = Program.config.profiles[idxProfile].autostart;
            cbMGetSales.Checked = Program.config.profiles[idxProfile].main_get_sales;
            cbMGetCustomer.Checked = Program.config.profiles[idxProfile].main_get_customer;
            cbMSendCustomer.Checked = Program.config.profiles[idxProfile].main_send_customer;
            cbMSendProduct.Checked = Program.config.profiles[idxProfile].main_send_product;
            //cmbChromeProfile.SelectedIndex = chromeProfiles.IndexOf(Program.config.profiles[idxProfile].chrome_profile)+1;
            //if(cmbChromeProfile.SelectedIndex <= 0)
            //{
            //    cmbChromeProfile.SelectedIndex = 0;
            //    Program.config.profiles[idxProfile].chrome_profile = "";
            //}

            cbDbType.SelectedIndex = Program.config.profiles[idxProfile].db_type;
            cbDbType_SelectedIndexChanged(sender, e);
            txtName.Text = Program.config.profiles[idxProfile].name;
            txtPort.Value = Program.config.profiles[idxProfile].port;
            txtHost.Text = Program.config.profiles[idxProfile].host;
            txtUsername.Text = Program.config.profiles[idxProfile].username;
            txtPassword.Text = Program.config.profiles[idxProfile].password;
            cmbPrimaryKey.SelectedIndex = Program.config.profiles[idxProfile].primary_key;
            if(Program.module == Program.Appmodule.Ipos)
            {
                LoadDatabase();
            }

            chbWoocommerce.Checked = Program.config.profiles[idxProfile].woo_on;
            txtWooURL.Text = Program.config.profiles[idxProfile].woo_url;
            cmbWooPermalink.SelectedIndex = Program.config.profiles[idxProfile].woo_permalink;
            txtWooKey.Text = Program.config.profiles[idxProfile].woo_key;
            txtWooSecret.Text = Program.config.profiles[idxProfile].woo_secret;
            txtWooUsername.Text = Program.config.profiles[idxProfile].woo_username;
            txtWooPassword.Text = Program.config.profiles[idxProfile].woo_password;
            cmbPriceType.SelectedIndex = Program.config.profiles[idxProfile].woo_price;
            txtLevel1.Text = Program.config.profiles[idxProfile].woo_role1;
            txtLevel2.Text = Program.config.profiles[idxProfile].woo_role2;
            txtLevel3.Text = Program.config.profiles[idxProfile].woo_role3;
            txtLevel4.Text = Program.config.profiles[idxProfile].woo_role4;
            if (cmbPriceType.SelectedIndex == 0 || cmbPriceType.SelectedIndex == 5)
            {
                txtLevel1.Enabled = true;
                txtLevel2.Enabled = true;
                txtLevel3.Enabled = true;
                txtLevel4.Enabled = true;
            }
            chbSyncIposToDraft.Checked = Program.config.profiles[idxProfile].woo_to_draft;
            chbSyncRakToWeight.Checked = Program.config.profiles[idxProfile].woo_sync_weight;
            chbWooSendPublish.Checked = Program.config.profiles[idxProfile].woo_send_publish;
            cmbWooRestMethod.SelectedIndex = Program.config.profiles[idxProfile].woo_rest_method;

            chbTokped.Checked = Program.config.profiles[idxProfile].tokped_on;
            txtEmailTokped.Text = Program.config.profiles[idxProfile].tokped_email;
            txtPasswordTokped.Text = Program.config.profiles[idxProfile].tokped_password;
            chbSyncTokped.Checked = Program.config.profiles[idxProfile].tokped_sync_on;
            cmbStokTokped.SelectedIndex = Program.config.profiles[idxProfile].tokped_stock;
            cmbPriceTokped.SelectedIndex = Program.config.profiles[idxProfile].tokped_price;
            txtDevTokped.Text = Program.config.profiles[idxProfile].tokped_dev;
            txtCheckIDScheduleTokped.Text = Program.config.profiles[idxProfile].tokped_checkid_schedule;
            chbAmbilPenjualanTokped.Checked = Program.config.profiles[idxProfile].tokped_jual_on;
            cmbJualTujuanTokped.SelectedIndex = Program.config.profiles[idxProfile].tokped_jual_tujuan;
            cmbJualTipeBayarTokped.SelectedIndex = Program.config.profiles[idxProfile].tokped_jual_tipebayar;
            cmbJualStokMinusTokped.SelectedIndex = Program.config.profiles[idxProfile].tokped_jual_stokminus;
            txtJualPrefixNomorTokped.Text = Program.config.profiles[idxProfile].tokped_sale_prefix;
            cmbJualTabTokped.SelectedIndex = tokopediaOrderTabs.IndexOf(Program.config.profiles[idxProfile].tokped_sale_tab);
            txtJualPelangganTokped.Text = Program.config.profiles[idxProfile].tokped_sale_custcode;
            txtJualGudangTokped.Text = Program.config.profiles[idxProfile].tokped_sale_warehousecode;
            txtJualSalesTokped.Text = Program.config.profiles[idxProfile].tokped_sale_sellercode;
            cmbJualAkunTunaiTokped.Text = Program.config.profiles[idxProfile].tokped_sale_acctunai;
            cmbJualAkunKreditTokped.Text = Program.config.profiles[idxProfile].tokped_sale_acckredit;
            cmbJualAkunPotTokped.Text = Program.config.profiles[idxProfile].tokped_sale_accpot;
            cmbJualAkunBLainTokped.Text = Program.config.profiles[idxProfile].tokped_sale_accblain;
            txtJualUserIDTokped.Text = Program.config.profiles[idxProfile].tokped_sale_userid;
            chbProxyTokped.Checked = Program.config.profiles[idxProfile].tokped_proxy_enabled;
            cmbProxyModeTokped.SelectedIndex = Program.config.profiles[idxProfile].tokped_proxy_mode;
            txtProxyAddressTokped.Text = Program.config.profiles[idxProfile].tokped_proxy_address;
            txtProxyPortTokped.Text = Program.config.profiles[idxProfile].tokped_proxy_port;
            txtProxyUserTokped.Text = Program.config.profiles[idxProfile].tokped_proxy_username;
            txtProxyPassTokped.Text = Program.config.profiles[idxProfile].tokped_proxy_password;


            chbShopee.Checked = Program.config.profiles[idxProfile].shopee_on;
            cmbEngineShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_engine;
            cmbProfileShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_profile;
            txtEmailShopee.Text = Program.config.profiles[idxProfile].shopee_email;
            txtPasswordShopee.Text = Program.config.profiles[idxProfile].shopee_password;
            chbSyncShopee.Checked = Program.config.profiles[idxProfile].shopee_sync_on;
            cmbStokShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_stock;
            cmbPriceShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_price;
            txtDevShopee.Text = Program.config.profiles[idxProfile].shopee_dev;
            chbAmbilPenjualanShopee.Checked = Program.config.profiles[idxProfile].shopee_jual_on;
            cmbJualTujuanShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_jual_tujuan;
            cmbJualTipeBayarShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_jual_tipebayar;
            cmbJualStokMinusShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_jual_stokminus;
            cmbJualOngkirShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_jual_ongkir;
            cmbJualBAdminShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_jual_badmin;
            txtJualPrefixNomorShopee.Text = Program.config.profiles[idxProfile].shopee_sale_prefix;
            cmbJualTabShopee.SelectedIndex = shopeeOrderTabs.IndexOf(Program.config.profiles[idxProfile].shopee_sale_tab);
            txtJualPelangganShopee.Text = Program.config.profiles[idxProfile].shopee_sale_custcode;
            txtJualGudangShopee.Text = Program.config.profiles[idxProfile].shopee_sale_warehousecode;
            txtJualSalesShopee.Text = Program.config.profiles[idxProfile].shopee_sale_sellercode;
            cmbJualAkunTunaiShopee.Text = Program.config.profiles[idxProfile].shopee_sale_acctunai;
            cmbJualAkunKreditShopee.Text = Program.config.profiles[idxProfile].shopee_sale_acckredit;
            cmbJualAkunPotShopee.Text = Program.config.profiles[idxProfile].shopee_sale_accpot;
            cmbJualAkunBLainShopee.Text = Program.config.profiles[idxProfile].shopee_sale_accblain;
            txtJualUserIDShopee.Text = Program.config.profiles[idxProfile].shopee_sale_userid;
            chbProxyShopee.Checked = Program.config.profiles[idxProfile].shopee_proxy_enabled;
            cmbProxyModeShopee.SelectedIndex = Program.config.profiles[idxProfile].shopee_proxy_mode;
            txtProxyAddressShopee.Text = Program.config.profiles[idxProfile].shopee_proxy_address;
            txtProxyPortShopee.Text = Program.config.profiles[idxProfile].shopee_proxy_port;
            txtProxyUserShopee.Text = Program.config.profiles[idxProfile].shopee_proxy_username;
            txtProxyPassShopee.Text = Program.config.profiles[idxProfile].shopee_proxy_password;

            chbBukalapak.Checked = Program.config.profiles[idxProfile].bukalapak_on;
            txtEmailBukalapak.Text = Program.config.profiles[idxProfile].bukalapak_email;
            txtPasswordBukalapak.Text = Program.config.profiles[idxProfile].bukalapak_password;
            cmbPriceBukalapak.SelectedIndex = Program.config.profiles[idxProfile].bukalapak_price;
            txtDevBukalapak.Text = Program.config.profiles[idxProfile].bukalapak_dev;
            chbAmbilPenjualanBukalapak.Checked = Program.config.profiles[idxProfile].bukalapak_jual_on;
            cmbJualTujuanBukalapak.SelectedIndex = Program.config.profiles[idxProfile].bukalapak_jual_tujuan;
            cmbJualTipeBayarBukalapak.SelectedIndex = Program.config.profiles[idxProfile].bukalapak_jual_tipebayar;
            cmbBrowserBukalapak.SelectedIndex = Program.config.profiles[idxProfile].bukalapak_browser;
            chbProxyBukalapak.Checked = Program.config.profiles[idxProfile].bukalapak_proxy_enabled;
            cmbProxyModeBukalapak.SelectedIndex = Program.config.profiles[idxProfile].bukalapak_proxy_mode;
            txtProxyAddressBukalapak.Text = Program.config.profiles[idxProfile].bukalapak_proxy_address;
            txtProxyPortBukalapak.Text = Program.config.profiles[idxProfile].bukalapak_proxy_port;
            txtProxyUserBukalapak.Text = Program.config.profiles[idxProfile].bukalapak_proxy_username;
            txtProxyPassBukalapak.Text = Program.config.profiles[idxProfile].bukalapak_proxy_password;

            chbLazada.Checked = Program.config.profiles[idxProfile].lazada_on;
            txtEmailLazada.Text = Program.config.profiles[idxProfile].lazada_email;
            txtPasswordLazada.Text = Program.config.profiles[idxProfile].lazada_password;
            cmbPriceLazada.SelectedIndex = Program.config.profiles[idxProfile].lazada_price;
            txtDevLazada.Text = Program.config.profiles[idxProfile].lazada_dev;
            chbAmbilPenjualanLazada.Checked = Program.config.profiles[idxProfile].lazada_jual_on;
            cmbJualTujuanLazada.SelectedIndex = Program.config.profiles[idxProfile].lazada_jual_tujuan;
            cmbJualTipeBayarLazada.SelectedIndex = Program.config.profiles[idxProfile].lazada_jual_tipebayar;
            chbProxyLazada.Checked = Program.config.profiles[idxProfile].lazada_proxy_enabled;
            cmbProxyModeLazada.SelectedIndex = Program.config.profiles[idxProfile].lazada_proxy_mode;
            txtProxyAddressLazada.Text = Program.config.profiles[idxProfile].lazada_proxy_address;
            txtProxyPortLazada.Text = Program.config.profiles[idxProfile].lazada_proxy_port;
            txtProxyUserLazada.Text = Program.config.profiles[idxProfile].lazada_proxy_username;
            txtProxyPassLazada.Text = Program.config.profiles[idxProfile].lazada_proxy_password;

            chbBlibli.Checked = Program.config.profiles[idxProfile].blibli_on;
            txtEmailBlibli.Text = Program.config.profiles[idxProfile].blibli_email;
            txtPasswordBlibli.Text = Program.config.profiles[idxProfile].blibli_password;
            cmbPriceBlibli.SelectedIndex = Program.config.profiles[idxProfile].blibli_price;
            txtDevBlibli.Text = Program.config.profiles[idxProfile].blibli_dev;
            chbAmbilPenjualanBlibli.Checked = Program.config.profiles[idxProfile].blibli_jual_on;
            cmbJualTujuanBlibli.SelectedIndex = Program.config.profiles[idxProfile].blibli_jual_tujuan;
            cmbJualTipeBayarBlibli.SelectedIndex = Program.config.profiles[idxProfile].blibli_jual_tipebayar;
            chbProxyBlibli.Checked = Program.config.profiles[idxProfile].blibli_proxy_enabled;
            cmbProxyModeBlibli.SelectedIndex = Program.config.profiles[idxProfile].blibli_proxy_mode;
            txtProxyAddressBlibli.Text = Program.config.profiles[idxProfile].blibli_proxy_address;
            txtProxyPortBlibli.Text = Program.config.profiles[idxProfile].blibli_proxy_port;
            txtProxyUserBlibli.Text = Program.config.profiles[idxProfile].blibli_proxy_username;
            txtProxyPassBlibli.Text = Program.config.profiles[idxProfile].blibli_proxy_password;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Equals(""))
            {
                MessageBox.Show("Nama profil harus diisi.");
                return;
            }

            if (Program.module == Program.Appmodule.Ipos)
            {
                if (cbDbType.SelectedIndex == -1)
                {
                    MessageBox.Show("Silahkan pilih tipe ipos.");
                    return;
                }

                if (cmbDatabase.Text.Equals(""))
                {
                    MessageBox.Show("Silahkan pilih database.");
                    return;
                }

                if (chbAmbilPenjualanTokped.Checked)
                {
                    if(txtJualGudangTokped.Text.Equals(""))
                    {
                        MessageBox.Show("Silahkan isi kode gudang untuk pengambilan penjualan Tokopedia.");
                        return;
                    }
                }
            }

            if(Program.config.profiles[idxProfile].primary_key != cmbPrimaryKey.SelectedIndex)
            {
                if (MessageBox.Show("Mengganti SKU produk akan menyebabkan kunci utama produk menjadi berganti. Harap reset terlebih dahulu semua cache ipos2woo secara manual. Apa anda yakin?", "Peringatan", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
            }

            Program.config.profiles[idxProfile].name = txtName.Text;
            Program.config.profiles[idxProfile].main_dev = txtDevMain.Text;
            Program.config.profiles[idxProfile].autostart = cbAutoStart.Checked;
            Program.config.profiles[idxProfile].main_get_sales = cbMGetSales.Checked;
            Program.config.profiles[idxProfile].main_get_customer = cbMGetCustomer.Checked;
            Program.config.profiles[idxProfile].main_send_customer = cbMSendCustomer.Checked;
            Program.config.profiles[idxProfile].main_send_product = cbMSendProduct.Checked;
            //Program.config.profiles[idxProfile].chrome_profile = cmbChromeProfile.SelectedIndex > 0 ? chromeProfiles[cmbChromeProfile.SelectedIndex-1] : "";

            Program.config.profiles[idxProfile].db_type = cbDbType.SelectedIndex;
            Program.config.profiles[idxProfile].host = txtHost.Text;
            Program.config.profiles[idxProfile].username = txtUsername.Text;
            Program.config.profiles[idxProfile].password = txtPassword.Text;
            Program.config.profiles[idxProfile].database = cmbDatabase.Text;
            Program.config.profiles[idxProfile].port = (int)txtPort.Value;
            Program.config.profiles[idxProfile].primary_key = cmbPrimaryKey.SelectedIndex;
            
            Program.config.profiles[idxProfile].woo_on = chbWoocommerce.Checked;
            Program.config.profiles[idxProfile].woo_url = txtWooURL.Text;
            Program.config.profiles[idxProfile].woo_permalink = cmbWooPermalink.SelectedIndex;
            Program.config.profiles[idxProfile].woo_key = txtWooKey.Text;
            Program.config.profiles[idxProfile].woo_secret = txtWooSecret.Text;
            Program.config.profiles[idxProfile].woo_username = txtWooUsername.Text;
            Program.config.profiles[idxProfile].woo_password = txtWooPassword.Text;
            Program.config.profiles[idxProfile].woo_price = cmbPriceType.SelectedIndex;
            Program.config.profiles[idxProfile].woo_role1 = txtLevel1.Text;
            Program.config.profiles[idxProfile].woo_role2 = txtLevel2.Text;
            Program.config.profiles[idxProfile].woo_role3 = txtLevel3.Text;
            Program.config.profiles[idxProfile].woo_role4 = txtLevel4.Text;
            Program.config.profiles[idxProfile].woo_to_draft = chbSyncIposToDraft.Checked;
            Program.config.profiles[idxProfile].woo_sync_weight = chbSyncRakToWeight.Checked;
            Program.config.profiles[idxProfile].woo_send_publish = chbWooSendPublish.Checked;
            Program.config.profiles[idxProfile].woo_rest_method = cmbWooRestMethod.SelectedIndex;

            Program.config.profiles[idxProfile].tokped_on = chbTokped.Checked;
            Program.config.profiles[idxProfile].tokped_email = txtEmailTokped.Text;
            Program.config.profiles[idxProfile].tokped_password = txtPasswordTokped.Text;
            Program.config.profiles[idxProfile].tokped_dev = txtDevTokped.Text;
            Program.config.profiles[idxProfile].tokped_sync_on = chbSyncTokped.Checked;
            Program.config.profiles[idxProfile].tokped_stock = cmbStokTokped.SelectedIndex;
            Program.config.profiles[idxProfile].tokped_price = cmbPriceTokped.SelectedIndex;
            if (txtCheckIDScheduleTokped.Text.Trim().Length == 1)
            {
                Program.config.profiles[idxProfile].tokped_checkid_schedule = "";
            }
            else
            {
                Program.config.profiles[idxProfile].tokped_checkid_schedule = txtCheckIDScheduleTokped.Text;
            }
            Program.config.profiles[idxProfile].tokped_jual_on = chbAmbilPenjualanTokped.Checked;
            Program.config.profiles[idxProfile].tokped_jual_tujuan = cmbJualTujuanTokped.SelectedIndex;
            Program.config.profiles[idxProfile].tokped_jual_tipebayar = cmbJualTipeBayarTokped.SelectedIndex;
            Program.config.profiles[idxProfile].tokped_jual_stokminus = cmbJualStokMinusTokped.SelectedIndex;
            Program.config.profiles[idxProfile].tokped_sale_prefix = txtJualPrefixNomorTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_tab = tokopediaOrderTabs[cmbJualTabTokped.SelectedIndex];
            Program.config.profiles[idxProfile].tokped_sale_custcode = txtJualPelangganTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_warehousecode = txtJualGudangTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_sellercode = txtJualSalesTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_acctunai = cmbJualAkunTunaiTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_acckredit = cmbJualAkunKreditTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_accpot = cmbJualAkunPotTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_accblain = cmbJualAkunBLainTokped.Text;
            Program.config.profiles[idxProfile].tokped_sale_userid = txtJualUserIDTokped.Text;
            Program.config.profiles[idxProfile].tokped_proxy_enabled = chbProxyTokped.Checked;
            Program.config.profiles[idxProfile].tokped_proxy_mode = cmbProxyModeTokped.SelectedIndex;
            Program.config.profiles[idxProfile].tokped_proxy_address = txtProxyAddressTokped.Text;
            Program.config.profiles[idxProfile].tokped_proxy_port = txtProxyPortTokped.Text;
            Program.config.profiles[idxProfile].tokped_proxy_username = txtProxyUserTokped.Text;
            Program.config.profiles[idxProfile].tokped_proxy_password = txtProxyPassTokped.Text;

            Program.config.profiles[idxProfile].shopee_on = chbShopee.Checked;
            Program.config.profiles[idxProfile].shopee_engine = cmbEngineShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_profile = cmbProfileShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_email = txtEmailShopee.Text;
            Program.config.profiles[idxProfile].shopee_password = txtPasswordShopee.Text;
            Program.config.profiles[idxProfile].shopee_sync_on = chbSyncShopee.Checked;
            Program.config.profiles[idxProfile].shopee_stock = cmbStokShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_price = cmbPriceShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_dev = txtDevShopee.Text;
            Program.config.profiles[idxProfile].shopee_jual_on = chbAmbilPenjualanShopee.Checked;
            Program.config.profiles[idxProfile].shopee_jual_tujuan = cmbJualTujuanShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_jual_tipebayar = cmbJualTipeBayarShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_jual_stokminus = cmbJualStokMinusShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_jual_ongkir = cmbJualOngkirShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_jual_badmin = cmbJualBAdminShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_sale_prefix = txtJualPrefixNomorShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_tab = shopeeOrderTabs[cmbJualTabShopee.SelectedIndex];
            Program.config.profiles[idxProfile].shopee_sale_custcode = txtJualPelangganShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_warehousecode = txtJualGudangShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_sellercode = txtJualSalesShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_acctunai = cmbJualAkunTunaiShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_acckredit = cmbJualAkunKreditShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_accpot = cmbJualAkunPotShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_accblain = cmbJualAkunBLainShopee.Text;
            Program.config.profiles[idxProfile].shopee_sale_userid = txtJualUserIDShopee.Text;
            Program.config.profiles[idxProfile].shopee_proxy_enabled = chbProxyShopee.Checked;
            Program.config.profiles[idxProfile].shopee_proxy_mode = cmbProxyModeShopee.SelectedIndex;
            Program.config.profiles[idxProfile].shopee_proxy_address = txtProxyAddressShopee.Text;
            Program.config.profiles[idxProfile].shopee_proxy_port = txtProxyPortShopee.Text;
            Program.config.profiles[idxProfile].shopee_proxy_username = txtProxyUserShopee.Text;
            Program.config.profiles[idxProfile].shopee_proxy_password = txtProxyPassShopee.Text;

            Program.config.profiles[idxProfile].bukalapak_on = chbBukalapak.Checked;
            Program.config.profiles[idxProfile].bukalapak_email = txtEmailBukalapak.Text;
            Program.config.profiles[idxProfile].bukalapak_password = txtPasswordBukalapak.Text;
            Program.config.profiles[idxProfile].bukalapak_price = cmbPriceBukalapak.SelectedIndex;
            Program.config.profiles[idxProfile].bukalapak_dev = txtDevBukalapak.Text;
            Program.config.profiles[idxProfile].bukalapak_jual_on = chbAmbilPenjualanBukalapak.Checked;
            Program.config.profiles[idxProfile].bukalapak_jual_tujuan = cmbJualTipeBayarBukalapak.SelectedIndex;
            Program.config.profiles[idxProfile].bukalapak_jual_tipebayar = cmbJualTipeBayarBukalapak.SelectedIndex;
            Program.config.profiles[idxProfile].bukalapak_browser = cmbBrowserBukalapak.SelectedIndex;
            Program.config.profiles[idxProfile].bukalapak_proxy_enabled = chbProxyBukalapak.Checked;
            Program.config.profiles[idxProfile].bukalapak_proxy_mode = cmbProxyModeBukalapak.SelectedIndex;
            Program.config.profiles[idxProfile].bukalapak_proxy_address = txtProxyAddressBukalapak.Text;
            Program.config.profiles[idxProfile].bukalapak_proxy_port = txtProxyPortBukalapak.Text;
            Program.config.profiles[idxProfile].bukalapak_proxy_username = txtProxyUserBukalapak.Text;
            Program.config.profiles[idxProfile].bukalapak_proxy_password = txtProxyPassBukalapak.Text;

            Program.config.profiles[idxProfile].lazada_on = chbLazada.Checked;
            Program.config.profiles[idxProfile].lazada_email = txtEmailLazada.Text;
            Program.config.profiles[idxProfile].lazada_password = txtPasswordLazada.Text;
            Program.config.profiles[idxProfile].lazada_price = cmbPriceLazada.SelectedIndex;
            Program.config.profiles[idxProfile].lazada_dev = txtDevLazada.Text;
            Program.config.profiles[idxProfile].lazada_jual_on = chbAmbilPenjualanLazada.Checked;
            Program.config.profiles[idxProfile].lazada_jual_tujuan = cmbJualTujuanLazada.SelectedIndex;
            Program.config.profiles[idxProfile].lazada_jual_tipebayar = cmbJualTipeBayarLazada.SelectedIndex;
            Program.config.profiles[idxProfile].lazada_proxy_enabled = chbProxyLazada.Checked;
            Program.config.profiles[idxProfile].lazada_proxy_mode = cmbProxyModeLazada.SelectedIndex;
            Program.config.profiles[idxProfile].lazada_proxy_address = txtProxyAddressLazada.Text;
            Program.config.profiles[idxProfile].lazada_proxy_port = txtProxyPortLazada.Text;
            Program.config.profiles[idxProfile].lazada_proxy_username = txtProxyUserLazada.Text;
            Program.config.profiles[idxProfile].lazada_proxy_password = txtProxyPassLazada.Text;

            Program.config.profiles[idxProfile].blibli_on = chbBlibli.Checked;
            Program.config.profiles[idxProfile].blibli_email = txtEmailBlibli.Text;
            Program.config.profiles[idxProfile].blibli_password = txtPasswordBlibli.Text;
            Program.config.profiles[idxProfile].blibli_price = cmbPriceBlibli.SelectedIndex;
            Program.config.profiles[idxProfile].blibli_dev = txtDevBlibli.Text;
            Program.config.profiles[idxProfile].blibli_jual_on = chbAmbilPenjualanBlibli.Checked;
            Program.config.profiles[idxProfile].blibli_jual_tujuan = cmbJualTujuanBlibli.SelectedIndex;
            Program.config.profiles[idxProfile].blibli_jual_tipebayar = cmbJualTipeBayarBlibli.SelectedIndex;
            Program.config.profiles[idxProfile].blibli_proxy_enabled = chbProxyBlibli.Checked;
            Program.config.profiles[idxProfile].blibli_proxy_mode = cmbProxyModeBlibli.SelectedIndex;
            Program.config.profiles[idxProfile].blibli_proxy_address = txtProxyAddressBlibli.Text;
            Program.config.profiles[idxProfile].blibli_proxy_port = txtProxyPortBlibli.Text;
            Program.config.profiles[idxProfile].blibli_proxy_username = txtProxyUserBlibli.Text;
            Program.config.profiles[idxProfile].blibli_proxy_password = txtProxyPassBlibli.Text;

            Program.SaveConfig();
            isNew = false;
            this.DialogResult = DialogResult.OK;
        }

        private void txtHost_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtWooKey_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbDatabase_Click(object sender, EventArgs e)
        {

        }

        private void cmbDatabase_DropDown(object sender, EventArgs e)
        {

        }

        private void cmbDatabase_MouseClick(object sender, MouseEventArgs e)
        {
            LoadDatabase();
        }

        void LoadDatabase()
        {
            var connString = "Host=" + txtHost.Text + ";Port=" + txtPort.Text + ";Username=" + txtUsername.Text + ";Password=" + txtPassword.Text + ";Database=postgres;Persist Security Info=True;";

            using (var conn = new NpgsqlConnection(connString))
            {
                try
                {
                    conn.Open();
                }
                catch (Exception)
                {
                    cmbDatabase.SelectedIndex = -1;
                    cmbDatabase.Items.Clear();
                    return;
                }

                // Retrieve all rows
                using (var cmd = new NpgsqlCommand("SELECT datname FROM pg_database WHERE datistemplate = false", conn))
                using (var reader = cmd.ExecuteReader())
                {
                    cmbDatabase.Items.Clear();
                    while (reader.Read())
                    {
                        cmbDatabase.Items.Add(reader.GetString(0));
                    }
                    try
                    {
                        cmbDatabase.SelectedIndex = cmbDatabase.Items.IndexOf(Program.config.profiles[idxProfile].database);
                    }
                    catch (Exception)
                    {
                        cmbDatabase.SelectedIndex = cmbDatabase.Items.IndexOf("postgres");
                    }
                }
            }
        }

        private void cmbPriceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtLevel1.Enabled = cmbPriceType.SelectedIndex == 0 || cmbPriceType.SelectedIndex == 5;
            txtLevel2.Enabled = cmbPriceType.SelectedIndex == 0 || cmbPriceType.SelectedIndex == 5;
            txtLevel3.Enabled = cmbPriceType.SelectedIndex == 0 || cmbPriceType.SelectedIndex == 5;
            txtLevel4.Enabled = cmbPriceType.SelectedIndex == 0 || cmbPriceType.SelectedIndex == 5;
        }


        private void cbRunStartup_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cbEnableStockManager_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cbEnableRolePrice_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cbEnableStockManager_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void cbEnableRolePrice_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void cbEnableRolePrice_Click(object sender, EventArgs e)
        {

        }

        private void cbEnableStockManager_Click(object sender, EventArgs e)
        {

        }

        private void cmbLevel1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbLevel2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        //void toggleRolePrice(bool enable)
        //{
        //    var client = new RestClient(Program.config.woo_url);
        //    var request = new RestRequest("/wp-json/custom-routes/v1/updatemetaproducts", Method.POST);
        //    request.AddParameter("secret", "iposXwoocommerce");
        //    request.AddParameter("key", "_enable_role_based_price");
        //    request.AddParameter("value", enable ? "1" : "");
        //    IRestResponse response = client.Execute(request);
        //    var content = response.Content;
        //    var numeric = -1;
        //    if (!int.TryParse(content, out numeric))
        //    {
        //        MessageBox.Show("Wordpress Custom API is not set : " + content);
        //    }
        //    else
        //    {
        //        MessageBox.Show("Role price enabled : " + enable);
        //    }
        //}

        //void toggleStockManager(bool enable)
        //{
        //    var client = new RestClient(Program.config.woo_url);
        //    var request = new RestRequest("/wp-json/custom-routes/v1/updatemetaproducts", Method.POST);
        //    request.AddParameter("secret", "iposXwoocommerce");
        //    request.AddParameter("key", "_manage_stock");
        //    request.AddParameter("value", enable ? "yes" : "no");
        //    IRestResponse response = client.Execute(request);
        //    var content = response.Content;
        //    var numeric = -1;
        //    if (!int.TryParse(content, out numeric))
        //    {
        //        MessageBox.Show("Wordpress Custom API is not set : " + content);
        //    }
        //    else
        //    {
        //        MessageBox.Show("Stock manager enabled : " + enable);
        //    }
        //}

        private void btnEnableRolePrice_Click(object sender, EventArgs e)
        {
            //toggleRolePrice(true);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnDisableRolePrice_Click(object sender, EventArgs e)
        {
            //toggleRolePrice(false);
        }

        private void btnEnableStock_Click(object sender, EventArgs e)
        {
            //toggleStockManager(true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //toggleStockManager(false);
        }

        private void cbDebugMode_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnImportCustom1_Click(object sender, EventArgs e)
        {
            //Program.SaveConfig();
            //if (Program.ConnectDB())
            //{

            //    var sql = "SELECT i.name, i.sku as code, c.name as category "+
            //              "from items i left " +
            //              "join categories c on i.category_id = c.id " +
            //              "order by i.name";

            //    Microsoft.Office.Interop.Excel.Application oXL;
            //    Microsoft.Office.Interop.Excel._Workbook oWB;
            //    Microsoft.Office.Interop.Excel._Worksheet oSheet;
            //    Microsoft.Office.Interop.Excel.Range oRng;
            //    object misvalue = System.Reflection.Missing.Value;
           
            //        //Start Excel and get Application object.
            //        oXL = new Microsoft.Office.Interop.Excel.Application();

            //        //Get a new workbook.
            //        oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
            //        oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

            //        //Add table headers going cell by cell.
            //        oSheet.Cells[1, 1] = "Nama";
            //        oSheet.Cells[1, 2] = "Kode";
            //        oSheet.Cells[1, 3] = "Kategori";
                   
            //        //Format A1:D1 as bold, vertical alignment = center.
            //        oSheet.get_Range("A1", "D1").Font.Bold = true;
            //        oSheet.get_Range("A1", "D1").VerticalAlignment =
            //            Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

            //        var row = 2;
            //        using (var cmd = new NpgsqlCommand(sql, Program.conn))
            //        using (var reader = cmd.ExecuteReader())
            //        {
            //            while (reader.Read())
            //            {
            //                oSheet.Cells[row, 1] = reader.GetString(0);
            //                oSheet.Cells[row, 2] = reader.GetString(1);
            //                if (!reader.IsDBNull(2))
            //                {
            //                    oSheet.Cells[row, 3] = reader.GetString(2);
            //                }
            //                row++;
            //            }
            //        }

            //        oXL.Visible = false;
            //        oXL.UserControl = false;
            //        oWB.SaveAs(Application.StartupPath+"/exported.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
            //            false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
            //            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            //        oWB.Close();
             

            //}
        }

        private void frmProfileConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isNew)
            {
                Program.config.profiles.RemoveAt(Program.config.profiles.Count - 1);
            }
        }

        private void cbDbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbDbType.SelectedIndex == 0)
            {
                txtPort.Value = 5444;
                txtUsername.Text = "admin";
                txtPassword.Text = "";
            }
            else if(cbDbType.SelectedIndex == 1)
            {
                txtPort.Value = 5444;
                txtUsername.Text = "sysi5adm";
                txtPassword.Text = "";
            }
            txtPort.Enabled = cbDbType.SelectedIndex == 2;
            txtUsername.Enabled = cbDbType.SelectedIndex == 2;
            txtPassword.Enabled = cbDbType.SelectedIndex == 2;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btnManageTokpedCache_Click(object sender, EventArgs e)
        {
            var frm = new frmCacheManager(frmCacheManager.CacheType.Tokopedia, idxProfile);
            frm.ShowDialog();
        }

        private void btnManageShopeeCache_Click(object sender, EventArgs e)
        {
            var frm = new frmCacheManager(frmCacheManager.CacheType.Shopee, idxProfile);
            frm.ShowDialog();
        }

        private void btnResetCache_Click(object sender, EventArgs e)
        {

        }

        private void btnRebuildCacheTokped_Click(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnJualHapusTokped_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Apa anda yakin?","peringatan",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (MessageBox.Show("Semua data sinkronisasi penjualan akan dihapus.", "peringatan", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    try
                    {
                        var prefix = Program.config.profiles[idxProfile].tokped_sale_prefix;
                        var l = prefix.Length;
                        var sql = "delete from tbl_pesandt where SUBSTRING(notransaksi,1," + l + ") = '"+ prefix +"';" + "" +
                            "delete from tbl_pesanhd where SUBSTRING(notransaksi,1," + l + ") = '" + prefix + "';" +
                            "delete from tbl_ikdt where SUBSTRING(notransaksi,1," + l + ") = '" + prefix + "';" +
                            "delete from tbl_ikhd where SUBSTRING(notransaksi,1," + l + ") = '" + prefix + "';";
                        using (var cmd = new NpgsqlCommand(sql, Program.Syncers[Program.config.profiles[idxProfile].name].instance_ipos.conn))
                        {
                            cmd.ExecuteScalar();
                            Program.Syncers[Program.config.profiles[idxProfile].name].instance_tokped.ResetSaleCache();
                        }
                        MessageBox.Show("Berhasil dihapus");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.StackTrace);
                    }
                }
            }
        }

        private void btnJualHapusShopee_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apa anda yakin?", "peringatan", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (MessageBox.Show("Semua data sinkronisasi penjualan akan dihapus.", "peringatan", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    try
                    {
                        var prefix = Program.config.profiles[idxProfile].shopee_sale_prefix;
                        var l = prefix.Length;
                        var sql = "delete from tbl_pesandt where SUBSTRING(notransaksi,1," + l + ") = '" + prefix + "';" + "" +
                            "delete from tbl_pesanhd where SUBSTRING(notransaksi,1," + l + ") = '" + prefix + "';" +
                            "delete from tbl_ikdt where SUBSTRING(notransaksi,1," + l + ") = '" + prefix + "';" +
                            "delete from tbl_ikhd where SUBSTRING(notransaksi,1," + l + ") = '" + prefix + "';";
                        using (var cmd = new NpgsqlCommand(sql, Program.Syncers[Program.config.profiles[idxProfile].name].instance_ipos.conn))
                        {
                            cmd.ExecuteScalar();
                            Program.Syncers[Program.config.profiles[idxProfile].name].instance_shopee.ResetSaleCache();
                        }
                        MessageBox.Show("Berhasil dihapus");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.StackTrace);
                    }
                }
            }
        }

        private void cmbPriceTokped_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label69_Click(object sender, EventArgs e)
        {

        }

        private void label77_Click(object sender, EventArgs e)
        {

        }

        private void label81_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
