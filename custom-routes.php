<?php
 
class Slug_Custom_Route extends WP_REST_Controller {
 
  /**
   * Register the routes for the objects of the controller.
   */
  public function register_routes() {
    $version = '1';
    $namespace = 'custom-routes/v' . $version;
    $base = 'route';
    register_rest_route( $namespace, '/roleprice', array(
      array(
        'methods'             => WP_REST_Server::CREATABLE,
        'callback'            => array( $this, 'update_price' ),
      ),
    ) );
  }
  
  /**
   * Update batch post meta for role based price
   *
   * @param WP_REST_Request $request Full data about the request.
   * @return WP_REST_Response
   */
  public function update_price( $request ) {
     //get the id of the post object array
    $current = md5('iposXwoocommerce');
    $secret = md5($request->get_param('secret'));
    if($current != $secret){
        return new WP_REST_Response(array('code'=>'rest_forbidden','message'=>'Sorry, you are not allowed to do that.','data'=>array('status'=>403)), 403 );
    }
    
    $post_id = $request['id'];
    $data = $request->get_param('data');
    $arr = json_decode($data,FALSE);
    $count = 0;
    foreach($arr as $product){
        $meta = get_post_meta( $product->id ,'_role_based_price',true);
        foreach($product->roles as $role){
            if($role->regular_price){
                $meta[$role->role]["regular_price"] = $role->regular_price;            
            }
            if($role->selling_price){
                $meta[$role->role]["selling_price"] = $role->selling_price;            
            }
        }
        if(!update_post_meta($product->id, '_role_based_price', $meta)){
            $count++;    
        }
    }
    return new WP_REST_Response( $count, 200 );
  }
}


$custom = new Slug_Custom_Route();
$custom->register_routes();